package models;

import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import utility.Conexion;

/**
 * Clase para el manejo de Conceptos
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class Concepto extends Banco{
	private String codigo;
	private String concepto;
	private int banco;
	private double precio;
	private int descuento;
	private int zona;
	private String nombre_zona;
	private Conexion conn;
	
	public Concepto(){} 
	
	public Concepto(String codigo, String concepto, int banco, double precio, int descuento, int zona){  
		this.codigo = codigo;
		this.concepto = concepto;
		this.banco = banco;
		this.precio = precio;
		this.descuento = descuento;
		this.zona = zona;
	}
	
	public String getCodigo(){
		return codigo;
	}
	
	public String getConcepto(){
		return concepto;
	}
	
	public int getBanco(){
		return banco;
	}
	
	public double getPrecio(){
		return  precio;
	}
	
	public int getDescuento(){
		return descuento;
	}
	
	public int getZona(){
		return zona;
	}
	
	public String getNombreZona(){
		return nombre_zona;
	}
	
	public void setCodigo(String codigo){
		this.codigo = codigo;
	}
	
	public void setConcepto(String concepto){
		this.concepto = concepto;
	}
	
	public void setBanco(int banco){
		this.banco = banco;
	}
	
	public void setPrecio(double precio){
		this.precio = precio;
	}
	
	public void setDescuento(int descuento){
		this.descuento = descuento;
	}
	
	public void setZona(int zona){
		this.zona = zona;
	}
	
	public void SetNombreZona(String nombre_zona){
		this.nombre_zona = nombre_zona;
	}
	
	/* Desconecta la variable de conexion creada por el objeto en la llmada del metodo
	 */
	public void desconectar() throws SQLException{ 
		conn.desConectar();
	}
	
	/* Registra un nuevo concepto
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{ 
		Conexion conn = new Conexion();
		boolean status = false;
		String query = "INSERT INTO ads_facturacion_conceptos (codigo_concepto, nombre_concepto, fk_id_banco, status_concepto, precio_concepto, porcentaje_descuento, id_zona) VALUES ('"+codigo+"','"+concepto+"','"+banco+"', 'A', "+precio+", "+descuento+", "+zona+")";
		if(!ValidarCodigo(codigo)){
			status = conn.ejecutar(query);
			if(status == true){
				String ruta = new File ("").getAbsolutePath ();
				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Registro realizado con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
			}
		}
		conn.desConectar();
		return status;
	}
	
	/* Edita el registro del concepto
	 * @param id_cliente id del concepto
	 * @return retorna si la modificacion se realizo con exito
	 */
	public boolean Editar(String id_concepto) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "UPDATE ads_facturacion_conceptos SET fk_id_banco = "+banco+", codigo_concepto = '"+codigo+"' , nombre_concepto = '"+concepto+"', precio_concepto = "+precio+", porcentaje_descuento = "+descuento+", id_zona = "+zona+" WHERE codigo_concepto ='"+id_concepto+"'";
		boolean status = conn.ejecutar(query);
		if (status == true){
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		conn.desConectar();
		return status;
	}
	
	/* Trae un registro segun la primary key
	 * @param id_concepto id del concepto
	 */
	public void TraerRegistro(String id_concepto) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "SELECT codigo_concepto, nombre_concepto, fk_id_banco, precio_concepto, porcentaje_descuento, porcentaje_descuento, id_zona, nombre_banco, nombre  FROM ads_facturacion_conceptos JOIN  ads_bancos ON ads_bancos.pk_id = ads_facturacion_conceptos.fk_id_banco LEFT JOIN ads_zonas ON ads_facturacion_conceptos.id_zona = ads_zonas.id WHERE codigo_concepto = '"+id_concepto+"'";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			codigo = rs.getString("codigo_concepto");
			concepto = rs.getString("nombre_concepto");
			banco = rs.getInt("fk_id_banco");
			precio = rs.getDouble("precio_concepto");
			descuento = rs.getInt("porcentaje_descuento");
			zona = rs.getInt("id_zona");
			setNombre(rs.getString("nombre_banco"));
			nombre_zona = rs.getString("nombre");
			rs.close();
		}
		conn.desConectar();
	}
	
	/* Cambia el estado del concepto facturable
	 * @param id_codigo codigo del concepto
	 * @return estado del concepto facturable
	 */
	public String CambiarEstado(String id_codigo) throws SQLException{
		Conexion conn = new Conexion();
		String estado = null;
		String busca_estado = "SELECT status_concepto FROM ads_facturacion_conceptos WHERE codigo_concepto  = '"+id_codigo+"'";
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("status_concepto");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE ads_facturacion_conceptos SET status_concepto = 'A' WHERE  codigo_concepto  = '"+id_codigo+"'";
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar el Concepto?","Activar Concepto",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Concepto Activado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "A";
	        }else{
	        	estado = "I";
	        }
		}else if(estado.equals("A")){
			String query = "UPDATE ads_facturacion_conceptos SET status_concepto = 'I' WHERE  codigo_concepto  = '"+id_codigo+"'";
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar el Concepto?","Desactivar Concepto",1,0, icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Concepto Desactivado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "I";
	        }else{
	        	estado = "A";
	        }
		}
		conn.desConectar();
		return estado;
	}
	
	/* Lista todos los conceptos registrados 
	 * @return lista de conceptos registrados
	 */
	public ResultSet ListarConceptos() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT codigo_concepto, nombre_concepto, nombre_banco, precio_concepto, porcentaje_descuento, status_concepto FROM ads_bancos, ads_facturacion_conceptos WHERE fk_id_banco = ads_bancos.pk_id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Lista todos los conceptos registrados 
	 * @return lista de conceptos registrados
	 * @note la consulta de esta metodo es mas corto en el SELECT
	 */
	public ResultSet ListarConceptosResumen() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT codigo_concepto, nombre_concepto,  precio_concepto, porcentaje_descuento FROM ads_bancos, ads_facturacion_conceptos WHERE fk_id_banco = ads_bancos.pk_id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra los conceptos segun el nombre
	 * @param argumento nombre del concepto
	 * @return lista de conceptos filtrados
	 */
	public ResultSet FiltraConcepto(String argumento) throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT codigo_concepto, nombre_concepto, nombre_banco, precio_concepto, porcentaje_descuento, status_concepto FROM ads_bancos, ads_facturacion_conceptos WHERE fk_id_banco = ads_bancos.pk_id AND nombre_concepto ILIKE '%"+argumento+"%'";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra los clientes segun el nombre
	 * @param argumento nombre del concepto
	 * @return lista de conceptos filtrados
	 * @note la consulta de esta metodo es mas corto en el SELECT
	 */
	public ResultSet FiltraConceptoResumen(String argumento) throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT codigo_concepto, nombre_concepto,  precio_concepto, porcentaje_descuento FROM ads_bancos, ads_facturacion_conceptos WHERE fk_id_banco = ads_bancos.pk_id AND nombre_concepto ILIKE '%"+argumento+"%'";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Lista las zonas en la que los conceptos pueden ser aplicados
	 * @return lista de zonas
	 */
	public ResultSet ListarZonas() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT * FROM ads_zonas ORDER BY id";
		ResultSet rs = conn.Consulta(query);
		return rs;
	}
	
	/* Valida si el codigo del concepto se encuentra en uso
	 * @param codigo codigo del concepto
	 * @return cantidad de filas de la consulta
	 */
	public boolean ValidarCodigo(String codigo) throws SQLException{ 
		boolean rows = false;
		Conexion conn = new Conexion();
		String query = "SELECT * FROM ads_facturacion_conceptos WHERE codigo_concepto = '"+codigo+"'";
		ResultSet rs = conn.Consulta(query);
		if(conn.getRowsCount(rs) != 0){
			rows = true;
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"\\images\\error.png"));
			JOptionPane.showMessageDialog(null, "El c�digo de concepto ya esta en uso","Error" , JOptionPane.ERROR_MESSAGE, icono);
		}
		conn.desConectar();
		return rows;
	}
	
	/* Retorna el id segun el codigo del concepto
	 * @param cod codigo del concepto
	 * @return id del concepto
	 */
	public int RetornaID(String cod) throws SQLException{
		int id = 0;
		Conexion conn = new Conexion();
		String query = "SELECT id_concepto FROM ads_facturacion_conceptos WHERE codigo_concepto ='"+cod+"'";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			id = rs.getInt("id_concepto");
			rs.close();
		}
		conn.desConectar();
		return id;
	}
}
