package models;

import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import utility.Conexion;
import utility.StringMD;

/**
 * Clase para el manejo de Clientes
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class Usuario {
	private String nombre;
	private String usuario;
	private String clave;
	private Conexion conn;
	
	public Usuario(){} 
	
	public Usuario(String usuario){
		this.usuario = usuario;
	}
	
	public Usuario(String nombre, String usuario, String clave){ 
		this.nombre = nombre;
		this.usuario = usuario;
		this.clave = clave;
	}
	
	public String getNombre(){ 
		return nombre;
	}
	
	public String getUsuario(){
		return usuario;
	}
	
	public String getClave(){
		return clave;
	}
	
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public void setUsario(String usuario){
		this.usuario = usuario;
	}
	
	public void setClave(String clave){
		this.clave = clave;
	}
	
	/* Desconecta la variable de conexion creada por el objeto en la llmada del metodo
	 */
	public void desconectar() throws SQLException{ 
		conn.desConectar();
	}
	
	/* Verifica si la clave coincide con la confirmacion
	 * @param clave contrase�a ingresada
	 * @param confirmacion confirmacion de la contrase�a ingresada
	 * @return devuelve si la clave coincide o no 
	 */
	public boolean ClavesIguales(String clave, String confirmacion){ 
		if(clave.equals(confirmacion)){
			return true;
		}else{
			return false;
		}
	}
	
	/* Registra un nuevo usuario
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{ 
		boolean registro = false;
		Conexion conn = new Conexion();
		String encriptado = StringMD.getStringMessageDigest(clave, StringMD.MD2);
		String sentencia = "INSERT INTO tb_administrador (nombre, usuario, clave, status) VALUES ('"+nombre+"','"+usuario+"','"+encriptado+"', 'A');";
		String validacion = "SELECT * FROM tb_administrador WHERE usuario = '"+usuario+"'";
		
		String ruta = new File ("").getAbsolutePath ();
		Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
		ResultSet rs = conn.Consulta(validacion); 
		if (conn.getRowsCount(rs) != 0){ 
			JOptionPane.showMessageDialog(null, "El nombre de usuario ya esta en uso","Error" , JOptionPane.ERROR_MESSAGE, icono);
		}else{
			registro = conn.ejecutar(sentencia);
			Permiso Opermiso = new Permiso();
			Opermiso.RegistrarPermisos(Integer.parseInt(RetornaUltimoUsuario()));
			if (registro == true){
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Registro realizado con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
			}
			conn.desConectar();
		}
		return registro;
	}
	
	/* Busca el ultimo registro ingresado
	 * @return ultimo registro de usuario ingresado
	 */
	public String RetornaUltimoUsuario() throws SQLException{ 
		String id = null;
		Conexion conn = new Conexion();
		String query = "SELECT MAX(id_admin) as max_id FROM tb_administrador";
		ResultSet busca;
		
		busca = conn.Consulta(query);
		if(busca.next()){
			id = busca.getString("max_id");
			busca.close();
		}
		conn.desConectar();
		return id;	
	}
	
	/* Edita el registro de usuario
	 * @param id_usuario id del usuario
	 * @return retorna si la modificacion se realizo con exito
	 */
	public boolean Editar(int id_usuario) throws SQLException{  
		Conexion conn = new Conexion();
		String sentencia = "UPDATE tb_administrador SET nombre = '"+nombre+"' , usuario = '"+usuario+"' WHERE id_admin = "+id_usuario;
		boolean registro = conn.ejecutar(sentencia);
		if(registro == true){
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		conn.desConectar();
		return registro;
	}
	
	/* Trae un registro segun la primary key
	 * @param id_usuario id del usuario
	 */
	public void TraerRegistro(int id_usuario) throws SQLException{ 
		Conexion conn = new Conexion();
		String query ="SELECT nombre, usuario FROM tb_administrador WHERE id_admin = "+id_usuario+"";
		
		ResultSet buscar = conn.Consulta(query);
		if(buscar.next()) {
			this.nombre = buscar.getString("nombre");
			this.usuario = buscar.getString("usuario");
			buscar.close();
		}
		conn.desConectar();
	}
	
	/* Cambia el estado del usuario
	 * @param id_user id del usuario
	 * @return estado del usuario
	 */
	public String CambiarEstado(int id_user) throws SQLException{
		String status = "";
		String estado = "";
		Conexion conn = new Conexion();
		String busca_estado = "SELECT status FROM tb_administrador WHERE id_admin = "+id_user;
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("status");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE tb_administrador SET status = 'A' WHERE id_admin = "+id_user;
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar el Usuario?","Activar Usuario",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Usuario activado", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
				status = "A";
	        }else{
	        	status = "I";
	        }
		}else if(estado.equals("A")){
			String query = "UPDATE tb_administrador SET status = 'I' WHERE id_admin = "+id_user;
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar el Usuario?","Desactivar Usuario",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Usuario desactivado", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
				status = "I";
	        }else{
	        	status = "A";
	        }
		}
		conn.desConectar();
		return status;
	}
	
	/* Reinicia la clave de acceso del usuario seleccionado
	 * @param id_user id del usuario
	 */
	public void ReiniciarClave(int id_user) throws SQLException{ 
		Conexion conn = new Conexion();
		String busca_usuario = "SELECT usuario FROM tb_administrador WHERE id_admin = "+id_user;
		
		ResultSet busca = conn.Consulta(busca_usuario);
		if(busca.next()) {
			busca_usuario = busca.getString("usuario");
			busca.close();
		}
		String ruta = new File ("").getAbsolutePath ();
		Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
		if(JOptionPane.showConfirmDialog(null, "�Est�s seguro de reiniciar la clave del usuario "+busca_usuario+"?","Reiniciar Clave",1,0,icono)==0){
			String encriptado = StringMD.getStringMessageDigest("admin"+busca_usuario, StringMD.MD2);
			String query = "UPDATE tb_administrador SET clave = '"+encriptado+"' WHERE id_admin = "+id_user;
			conn.ejecutar(query);
			icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Clave reiniciada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
			
        }
		conn.desConectar();
	}
	
	/* Permite cambiar la clave de un usuario
	 * @param user nombre de usuario
	 * @param actual clave actual
	 * @param nueva clave nueva
	 * @param confirmacion confirmacion de la clave nueva
	 * @return devuelve si se realizo el cambio de la contrase�a
	 */
	public boolean CambiarClave(String user, String actual, String nueva, String confirmacion) throws SQLException{ 
		boolean cambio = false;
		Conexion conn = new Conexion();
		ResultSet busca;
		String clave_encontrada = "";
		String busca_clave = "SELECT clave, nombre, usuario FROM tb_administrador WHERE usuario = '"+user+"'";
		busca = conn.Consulta(busca_clave);
		if(busca.next()) {
			clave_encontrada = busca.getString("clave");
			user = busca.getString("usuario");
			busca.close();
		}
		String ruta = new File ("").getAbsolutePath ();
		Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
		String encriptado = StringMD.getStringMessageDigest(actual, StringMD.MD2);
		if(clave_encontrada.equals(encriptado) ){
			if(nueva.equals(confirmacion)){
				String nueva_clave = StringMD.getStringMessageDigest(nueva, StringMD.MD2);
				String query = "UPDATE tb_administrador SET clave = '"+nueva_clave+"' WHERE usuario ='"+user+"'";
				conn.ejecutar(query);
				cambio = true;
				JOptionPane.showMessageDialog(null, "Contrase�a modificada con �xito","�xito", JOptionPane.INFORMATION_MESSAGE,icono);			
			}else{
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
				JOptionPane.showMessageDialog(null, "Las contrase�as no coinciden","Error", JOptionPane.ERROR_MESSAGE,icono);
			}
		}else{
			icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "Contrase�a actual inv�lida","Error", JOptionPane.ERROR_MESSAGE, icono);
		}
		conn.desConectar();
		return cambio;
	}
	
	/* Permite dar acceso al sistema
	 * @return devulve si el usuario puede o no ingresar al sistema
	 */
	public boolean IngresoSistema(String clave) throws SQLException{
		Conexion conn = new Conexion();
		boolean ingreso = false;
		String query = "SELECT * FROM tb_administrador WHERE usuario = '"+usuario+"'";
		String busca_estado = "SELECT status FROM tb_administrador WHERE usuario = '"+usuario+"'";
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			busca_estado = busca.getString("status");
			busca.close();     
		}
		String ruta = new File ("").getAbsolutePath ();
		Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
		ResultSet rs = conn.Consulta(query);
		if (conn.getRowsCount(rs) != 0){
			if(busca_estado.equals("A")){
				String clave_encontrada = null;
				String busca_clave = "SELECT clave, nombre, usuario FROM tb_administrador WHERE usuario = '"+usuario+"'";
				busca = conn.Consulta(busca_clave);
					if(busca.next()) {
						clave_encontrada = busca.getString("clave");
						this.nombre = busca.getString("nombre");
						busca.close();
					}
					String encriptado = StringMD.getStringMessageDigest(clave, StringMD.MD2);
					if(clave_encontrada.equals(encriptado) ){
						ingreso = true;
					}else{
						JOptionPane.showMessageDialog(null, "Contrase�a Inv�lida","Error", JOptionPane.ERROR_MESSAGE, icono);
					}
			}else{
				JOptionPane.showMessageDialog(null, "El usuario se encuentra desactivado, comuniquese con el administrador","Error", JOptionPane.ERROR_MESSAGE, icono);
			}
		}else{
			JOptionPane.showMessageDialog(null, "El nombre de Usuario no existe","Error", JOptionPane.ERROR_MESSAGE, icono);
		}
		conn.desConectar();	
		return ingreso;
	}
	
	/* Lista todos los usuarios registrados
	 * @return lista con los usuarios registrados
	 */
	public ResultSet ListarUsuarios() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT id_admin, usuario, nombre, status FROM tb_administrador ORDER BY id_admin";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra los usuarios segun nombre de usuario
	 * @param argumento nombre de usuario
	 */
	public ResultSet FiltraUsuario(String argumento) throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT id_admin, usuario, nombre, status FROM tb_administrador WHERE usuario ILIKE '%"+argumento+"%' ORDER BY id_admin";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Retorna el id del usuario segun el nombre
	 * @param name nombre de usuario
	 * @return id del usuario
	 */
	public int RetornaIdUsuario(String name) throws SQLException{
		int user = 0;
		Conexion conn = new Conexion();
		String sentencia = "SELECT id_admin FROM tb_administrador WHERE usuario ='"+name+"'";
		ResultSet busca;
		busca = conn.Consulta(sentencia);
		
		if(busca.next()){
			user = Integer.parseInt(busca.getString("id_admin"));
			busca.close();
		}
		conn.desConectar();
		return user;
	}
}
