package models;

import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import utility.Conexion;

/**
 * Clase para el manejo de Clientes
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class Cliente {
	private String rif;
	private String razon_social;
	private String direccion;
	private String telefono;
	private String correo;
	private String representante;
	private String telefono_representante;
	private String correo_representante;
	private Conexion conn;
	
	public Cliente(){}
	
	public Cliente(String rif, String razon_social, String direccion, String telefono, String correo, String representante, String telefono_representante, String correo_representante){ 
		this.rif = rif;
		this.razon_social = razon_social;
		this.direccion = direccion;
		this.telefono = telefono;
		this.correo = correo;
		this.representante = representante;
		this.telefono_representante = telefono_representante;
		this.correo_representante = correo_representante;
	}
	
	public String getRif(){
		return rif;
	}
	
	public String getRazonSocial(){
		return razon_social;
	}
	
	public String getDireccion(){
		return direccion;
	}
	
	public String getTelefono(){
		return telefono;
	}
	
	public String getCorreo(){
		return correo;
	}
	
	public String getRepresentante(){
		return representante;
	}
	
	public String getTelefonoRepresentante(){
		return telefono_representante;
	}
	
	public String getCorreoRepresentante(){
		return correo_representante;
	}
	
	public void setRif(String rif){
		this.rif = rif;
	}
	
	public void setRazonSocial(String razon_social){
		this.razon_social = razon_social;
	}
	
	public void setDireccion(String direccion){
		this.direccion = direccion;
	}
	
	public void setTelefono(String telefono){
		this.telefono = telefono;
	}
	
	public void setCorreo(String correo){
		this.correo = correo;
	}
	
	public void setRepresentante(String representante){
		this.representante = representante;
	}
	
	public void setTelefonoRepresentante(String telefono_representante){
		this.telefono_representante = telefono_representante;
	}
	
	public void setCorreoRepresentante(String correo_representante){
		this.correo_representante = correo_representante;
	}
	
	/* Desconecta la variable de conexion creada por el objeto en la llmada del metodo
	 */
	public void desconectar() throws SQLException{ 
		conn.desConectar();
	}
	
	/* Registra un nuevo cliente
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{//Registra un nuevo cliente
		boolean registro = false;
		Conexion conn = new Conexion();
		Date hoy;
   		String fecha;
   		SimpleDateFormat formatter;
   		formatter = new SimpleDateFormat("yyyy-MM-dd");
   		hoy = new Date();
   		fecha = formatter.format(hoy); //Se busca la fecha actual del sistema
   		String query = "INSERT INTO ads_clientes (fk_id_usuario, fecha, status, razon_social, telefono_cliente, correo_cliente, direccion_cliente, fecha_registro, rif, representante_legal, telefono_representante, correo_representante) values (0, null, 'A', '"+razon_social+"', '"+telefono+"', '"+correo+"', '"+direccion+"', '"+fecha+"', '"+rif+"' , '"+representante+"', '"+telefono_representante+"', '"+correo_representante+"')";
   		
   		if(!ValidarRif(rif)){ //Validar rif
   			registro = conn.ejecutar(query);
			if (registro == true){
				String ruta = new File ("").getAbsolutePath ();
				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Registro de Cliente realizado con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
			}
   		}else{
   			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "El rif del cliente ya est� registrado","Error" , JOptionPane.ERROR_MESSAGE, icono);
   		}
   		conn.desConectar();
		return registro;
	}
	
	/* Edita el registro del cliente
	 * @param id_cliente id del cliente
	 * @return retorna si la modificacion se realizo con exito
	 */
	public boolean Editar(int id_cliente) throws SQLException{
		Conexion conn = new Conexion();
		String query = "UPDATE ads_clientes SET  razon_social = '"+razon_social+"', telefono_cliente = '"+telefono+"', correo_cliente = '"+correo+"', direccion_cliente = '"+direccion+"', rif = '"+rif+"', representante_legal = '"+representante+"', telefono_representante = '"+telefono_representante+"', correo_representante = '"+correo_representante+"' WHERE pk_id = '"+id_cliente+"'";
		boolean status = conn.ejecutar(query);
		if (status == true){
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		conn.desConectar();
		return status;
	}
	
	/* Trae un registro segun la primary key
	 * @param id_cliente id del cliente
	 */
	public void TraerRegistro(int id_cliente) throws SQLException{ 
		Conexion conn = new Conexion();
		String query ="SELECT rif, razon_social, direccion_cliente, telefono_cliente, correo_cliente, representante_legal, telefono_representante, correo_representante FROM ads_clientes WHERE pk_id = "+id_cliente+"";
		ResultSet buscar = conn.Consulta(query);
		if(buscar.next()) {
			this.rif = buscar.getString("rif");
			this.razon_social = buscar.getString("razon_social");
			this.direccion = buscar.getString("direccion_cliente");
			this.telefono = buscar.getString("telefono_cliente");
			this.correo= buscar.getString("correo_cliente");
			this.representante = buscar.getString("representante_legal");
			this.telefono_representante = buscar.getString("telefono_representante");
			this.correo_representante = buscar.getString("correo_representante");
			buscar.close();
		}
		conn.desConectar();
	}
	
	/* Cambia el estado del cliente
	 * @param id_cliente id del cliente
	 * @return estado del cliente
	 */
	public String CambiarEstado(int id_cliente) throws SQLException{ 
		Conexion conn = new Conexion();
		String estado = null;
		String busca_estado = "SELECT status FROM ads_clientes WHERE fk_id_usuario = 0 AND pk_id ="+id_cliente;
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("status");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE ads_clientes SET status = 'A' WHERE pk_id = "+id_cliente;
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar al Cliente?","Activar Cliente",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Cliente activado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "A";
	        }else{
	        	estado = "I";
	        }	
		}else if(estado.equals("A")){
			String ruta = new File ("").getAbsolutePath ();
			String query = "UPDATE ads_clientes SET status = 'I' WHERE pk_id = "+id_cliente;
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar al Cliente?","Desactivar Cliente",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Cliente desactivado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "I";
			}else{
	        	estado = "A";
	        }	
		}
		conn.desConectar();
		return estado;
	}
	
	/* Valida si el RIF se necuentra registrado
	 * @param rif RIF del cliente
	 * @return cantidas de filas de la consulta
	 */
	public boolean ValidarRif(String rif) throws SQLException{
		boolean rows = false;
		Conexion conn = new Conexion();
		String query = "SELECT * FROM ads_clientes WHERE rif = '"+rif+"'";
		ResultSet rs = conn.Consulta(query);
		if(conn.getRowsCount(rs) != 0){
			rows = true;
		}
		return rows;
	}
	
	/* Lista todos los clientes registrados 
	 * @return lista de clientes registrados
	 */
	public ResultSet ListarClientes() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT pk_id,rif, razon_social, correo_cliente, representante_legal, telefono_representante, status FROM ads_clientes WHERE fk_id_usuario = 0 ORDER BY pk_id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Lista todos los clientes registrados 
	 * @return lista de clientes registrados
	 * @note la consulta de esta metodo es mas corto en el SELECT
	 */
	public ResultSet ListarClientesResumen() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT pk_id, rif, razon_social,  telefono_cliente, correo_cliente FROM ads_clientes WHERE fk_id_usuario = 0 ORDER BY pk_id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra los clientes segun la razon social
	 * @param argumento razon social del cliente
	 * @return lista de clientes filtrados
	 */
	public ResultSet FiltraClientes(String argumento) throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT pk_id,rif, razon_social, correo_cliente, representante_legal, telefono_representante, status FROM ads_clientes WHERE fk_id_usuario = 0 AND razon_social ILIKE '%"+argumento+"%' ORDER BY pk_id ";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra los clientes segun la razon social
	 * @param argumento razon social del cliente
	 * @return lista de clientes filtrados
	 * @note la consulta de esta metodo es mas corto en el SELECT
	 */
	public ResultSet FiltraClientesResumen(String argumento) throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT pk_id, rif, razon_social,  telefono_cliente, correo_cliente FROM ads_clientes WHERE fk_id_usuario = 0 AND razon_social ILIKE '%"+argumento+"%' ORDER BY pk_id ";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Retorna el id del cliente segun el RIF
	 * @param rif RIF del cliente
	 * @return id del cliente
	 */
	public int RetornaID(String rif) throws SQLException{
		int id = 0;
		Conexion conn = new Conexion();
		String query = "SELECT pk_id FROM ads_clientes WHERE rif = '"+rif+"'";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			id = rs.getInt("pk_id");
			rs.close();
		}
		return id;
	}
}
