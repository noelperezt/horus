package models;

import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import utility.Conexion;
import utility.Lpermiso;

/**
 * Clase para el manejo de la permisologia
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class Permiso {
	private Conexion conn;
	
	public Permiso(){} 
	
	/* Desconecta la variable de conexion creada por el objeto en la llmada del metodo
	 */
	public void desconectar() throws SQLException{
		conn.desConectar();
	}
	
	/* Registra un nuevo permiso
	 * @return retorna si el registro se realizo con exito
	 */
	public void RegistrarPermisos(int usuario) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "SELECT * FROM ads_facturacion_submodulos";
		ResultSet busca = conn.Consulta(query);
		while(busca.next()){
			query = "SELECT * FROM ads_facturacion_permisos WHERE fk_id_usuario = "+usuario+" AND fk_id_modulo = "+busca.getString("fk_id_modulo")+" AND fk_id_submodulo = "+busca.getString("pk_id");		
			if(conn.getRowsCount(conn.Consulta(query)) == 0){
				query = "INSERT INTO ads_facturacion_permisos (fk_id_usuario, fk_id_modulo, fk_id_submodulo, estado) VALUES ("+usuario+","+busca.getString("fk_id_modulo")+", "+busca.getString("pk_id")+", 'I')";
				conn.ejecutar(query);
			}
		}
		conn.desConectar();
	}
	
	/* Lista los permisos de us usuario seleccionado
	 * @param usuario nombre del usuario
	 * @return lista de permisos
	 */
	public ResultSet ListarPermisos(int usuario) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "SELECT ads_facturacion_permisos.pk_id, ads_facturacion_modulos.descripcion, ads_facturacion_submodulos.descripcion, ads_facturacion_permisos.estado FROM ads_facturacion_modulos, ads_facturacion_submodulos, ads_facturacion_permisos WHERE ads_facturacion_permisos.fk_id_modulo = ads_facturacion_modulos.pk_id AND ads_facturacion_permisos.fk_id_submodulo = ads_facturacion_submodulos.pk_id AND ads_facturacion_permisos.fk_id_usuario = "+usuario+" ORDER BY ads_facturacion_permisos.fk_id_modulo";
		ResultSet rs = conn.Consulta(query);
		return rs;
	}
	
	/* Cambia el estado del permiso
	 * @param permiso id del permiso
	 * @return estado del permiso
	 */
	public String CambiarEstado(int permiso) throws SQLException{ 
		Conexion conn = new Conexion();
		String estado = null;
		String busca_estado = "SELECT estado FROM ads_facturacion_permisos WHERE pk_id = "+permiso;
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("estado");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE ads_facturacion_permisos SET estado = 'A' WHERE pk_id= "+permiso;
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar el Permiso a el Usuario?","Activar Permiso",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Permiso Activado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "A";
	        }else{
	        	estado = "I";
	        }
		}else if(estado.equals("A")){
			String query = "UPDATE ads_facturacion_permisos SET estado = 'I' WHERE pk_id= "+permiso;
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar el Permiso a el Usuario?","Desactivar Permiso",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Permiso Desactivado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "I";
	        }else{
	        	estado = "A";
	        }
		}
		conn.desConectar();
		return estado;
	}
	
	/* Retorna una lista con los permisos del usuario
	 * @param id_usuario id del usuario
	 * @return lista de permisos
	 */
	public Lpermiso Permisos(int id_usuario) throws SQLException{ 
		Conexion conn = new Conexion();
		Lpermiso Permisos = new Lpermiso();
		String query = "SELECT fk_id_submodulo, estado, descripcion FROM ads_facturacion_permisos, ads_facturacion_submodulos WHERE fk_id_submodulo = ads_facturacion_submodulos.pk_id AND fk_id_usuario = "+id_usuario;
		ResultSet busca = conn.Consulta(query);
		while(busca.next()){
			int id = Integer.parseInt(busca.getString("fk_id_submodulo"));
			Permisos.InsFinal(id, busca.getString("estado"), busca.getString("descripcion"));
		}
		conn.desConectar();
		return Permisos;
	}
}
