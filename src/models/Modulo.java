package models;

import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import utility.Conexion;
import utility.Lpermiso;

/**
 * Clase para el manejo de los modulos
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class Modulo {
	private int id;
	private String descripcion;
	private String visibilidad;
	private Conexion conn;
	
	public Modulo(){} 

	public Modulo(String descripcion){ 
		this.descripcion = descripcion;
	}
	
	public int getId(){
		return id;
	}
	
	public String getDescripcion(){
		return descripcion;
	}
	
	public String getVisibilidad(){
		return visibilidad;
	}
	
	public void setId(int id){
		this.id = id;
	}
	
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}
	
	public void setVisibilidad(String visibilidad){
		this.visibilidad = visibilidad;
	}
	
	/* Desconecta la variable de conexion creada por el objeto en la llmada del metodo
	 */
	public void desconectar() throws SQLException{ 
		conn.desConectar();
	}
	
	public Conexion getConn(){
		return conn;
	}
	
	/* Registra un nuevo modulo
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{
		Conexion conn = new Conexion();
		String query = "INSERT INTO ads_facturacion_modulos (descripcion,visible) values ('"+descripcion+"','S')";
		boolean status = conn.ejecutar(query);
		if(status == true){
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Registro realizado con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		conn.desConectar();
		return status;
	}
	
	/* Edita el registro del modulo
	 * @param id_modulo id del modulo
	 * @return retorna si la modificacion se realizo con exito
	 */
	public boolean Editar(int id_modulo) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "UPDATE ads_facturacion_modulos SET descripcion = '"+descripcion+"' WHERE pk_id ="+id_modulo;
		boolean status = conn.ejecutar(query);
		if (status == true){
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		conn.desConectar();
		return status;
	}
	
	/* Trae un registro segun la primary key
	 * @param id_modulo id del modulo
	 */
	public void TraerRegistro(int id_modulo) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "SELECT descripcion FROM ads_facturacion_modulos WHERE pk_id = "+id_modulo;
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			this.descripcion = rs.getString("descripcion");
			rs.close();
		}
		conn.desConectar();
	}
	
	/* Cambia la visibilidad del modulo
	 * @param id_modulo id del modulo
	 * @return visibilidad del modulo
	 */
	public String CambiarVisibilidad(int id_modulo) throws SQLException{ 
		Conexion conn = new Conexion();
		String visibilidad = null;
		
		String busca_estado = "SELECT visible FROM ads_facturacion_modulos WHERE pk_id = "+id_modulo;
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			visibilidad = busca.getString("visible");
			busca.close();
		}
		if(visibilidad.equals("N")){
			String query = "UPDATE ads_facturacion_modulos SET visible = 'S' WHERE pk_id = "+id_modulo;
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar la visibilidad del M�dulo?","Activar visibilidad del M�dulo",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "M�dulo Visible", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				visibilidad = "S";
	        }else{
	        	visibilidad = "N";
	        }
		}else if(visibilidad.equals("S")){
			String query = "UPDATE ads_facturacion_modulos SET visible = 'N' WHERE pk_id = "+id_modulo;
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar la visibilidad del M�dulo?","Desactivar visibilidad del M�dulo",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "M�dulo Invisible", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				visibilidad = "N";
	        }else{
	        	visibilidad = "S";
	        }
		}
		conn.desConectar();
		return visibilidad;
	}
	
	/* Lista todos los modulos registrados 
	 * @return lista de modulos registrados
	 */
	public ResultSet ListarModulos() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT * FROM ads_facturacion_modulos ORDER BY pk_id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Devuelve una lista con la visibilidad del modulo
	 * @return lista con los estados de visibilidad de los modulos
	 */
	public Lpermiso Visibilidad() throws SQLException{ 
		Lpermiso visibilidad = new Lpermiso();
        Conexion conn = new Conexion();
        String query = "SELECT * FROM ads_facturacion_modulos ORDER BY pk_id";
		ResultSet busca = conn.Consulta(query);
		while(busca.next()){
			int id = Integer.parseInt(busca.getString("pk_id"));
			visibilidad.InsFinal(id, busca.getString("visible"),busca.getString("descripcion"));
		}
		conn.desConectar();
		return visibilidad;
	}
	
	/* Devulve si el modulo esta activo segun los submodulos a los que el usuario tiene permiso
	 * @param id_usuario id del usuario
	 * @return lista de modulos activos
	 */
	public Lpermiso ModulosActivos(int id_usuario) throws SQLException{ 
		Conexion conn = new Conexion();
		Lpermiso ModulosActivos = new Lpermiso();
		String query = "SELECT fk_id_modulo FROM ads_facturacion_permisos WHERE estado = 'A' AND fk_id_usuario = "+id_usuario+" GROUP BY fk_id_modulo";
		ResultSet busca = conn.Consulta(query);
		while(busca.next()){
			int id = Integer.parseInt(busca.getString("fk_id_modulo"));
			ModulosActivos.InsFinal(id, "", "");
		}
		conn.desConectar();
		return ModulosActivos;
	}
	
}
