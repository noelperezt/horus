package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import utility.Conexion;
import utility.Ldescuento;
import utility.Tabla;

/**
 * Clase para el manejo de las facturas
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class Factura extends Cliente{
	private Conexion conn;
	private String nro;
	
	public Factura(){
		Conexion conn = new Conexion();
		this.conn = conn;
	}
	
	/* Retorna el numero de la factura
	 * @return numero de la factura
	 */
	public String getNro(){ 
		return this.nro;
	}
	
	public void setConexion(Conexion conn){
		this.conn = conn;
	}
	
	/* Retorna la Conexion
	 */
	public Conexion getConexion(){
		return conn;
	}
	
	/* Desconecta la variable de conexion creada por el objeto en la llmada del metodo
	 */
	public void desconectar() throws SQLException{ 
		conn.desConectar();
	}
	
	/* Retorna el Iva de la configuracion de la base de datos
	 * @return iva 
	 */
	public int retornaIva() throws SQLException{ 
		int iva = 0;
		Conexion conn = new Conexion();
		String sentencia = "SELECT porcentaje_iva FROM ads_administracion";
		
		ResultSet busca;
		busca = conn.Consulta(sentencia);
		
		if(busca.next()){
			iva = busca.getInt("porcentaje_iva");
			busca.close();
		}
		
		conn.desConectar();
		return iva;
	}
	
	/* Retorna el numero asignado para la factura
	 * @param id_documento numero del documento
	 * @param tipo tipo del documento (C-> Contratacion, P->Presupuesto, M-> Marcas)
	 * @return numero de la factura
	 */
	public String RetornaNumero(int id_documento, String tipo) throws SQLException{ 
		String query = "SELECT obtener_numero_factura("+id_documento+",'"+tipo+"') as controles";
		String nro_factura = "";
		ResultSet nro = conn.Consulta(query);
		if(nro.next()){
			String controles = nro.getString("controles");
			String e = controles.replace("(", "");
			String b = e.replace(")", "");
			String factura[] = b.split(",");
			nro_factura = factura[0];
			nro.close();
		}
		return nro_factura;
	}
	
	/* Almacena en la BD los datos faltantes de la factura provenientes de una contratacion
	 * @param iva iva de la factura
	 * @param contratacion numero de la contratacion
	 * @param descuento total de descauento aplicado
	 * @param monto_gravado monto gravado de la factura
	 * @param id_factura id de la factura
	 * @param user nombre del usuario que realiza registro
	 */
	public void FacturaContratacion(int iva, int contratacion, int descuento, double monto_gravado, int id_factura, String user) throws SQLException{ 
		Conexion conn = new Conexion();
		double sub_total;
		double monto_iva;
		double total_pagar;
		
		sub_total = monto_gravado - ((monto_gravado * descuento)/100);
		monto_iva = (sub_total * iva)/100;
		total_pagar = sub_total + monto_iva;
		
		String query = "SELECT fk_id_cliente FROM ads_contrataciones WHERE pk_id = '"+contratacion+"'";
		
		int cliente = 0;
		ResultSet buscador = conn.Consulta(query);
		if(buscador.next()){
			cliente = buscador.getInt("fk_id_cliente");
			buscador.close();
		}
		Usuario Ouser = new Usuario();
		query = "UPDATE ads_facturacion SET fk_id_cliente = '"+cliente+"', total_pago = '"+total_pagar+"', porc_iva = '"+iva+"', monto_iva = '"+monto_iva+"',monto_gravado = '"+sub_total+"', porcentaje_descuento = '"+descuento+"', tipo = 'C', id_usuario = '"+Ouser.RetornaIdUsuario(user)+"' WHERE pk_id='"+id_factura+"'";
		conn.ejecutar(query);
		conn.desConectar();
	}
	
	/* Almacena en la BD los datos faltantes dela factura proveniente de una venta directa o presupuesto
	 * @param iva iva de la factura
	 * @param venta monto de venta
	 * @param descuento descuento de la factura
	 * @param monto_gravado monto gravado de la venta
	 * @param id_factura id de la factura
	 * @param id_cliente id del cliente
	 * @param user nombre del usuario
	 */
	public void FacturaVentaDirecta(int iva, int venta, int descuento, double monto_gravado, int id_factura,int id_cliente, String user) throws SQLException{ 
		double sub_total;
		double monto_iva;
		double total_pagar;
		
		sub_total = monto_gravado;
		monto_iva = (sub_total * iva)/100;
		total_pagar = sub_total + monto_iva;
		
		Usuario Ouser = new Usuario();
		String query = "UPDATE ads_facturacion SET fk_id_cliente = '"+id_cliente+"', total_pago = '"+total_pagar+"', porc_iva = '"+iva+"', monto_iva = '"+monto_iva+"',monto_gravado = '"+sub_total+"', porcentaje_descuento = '"+descuento+"', id_usuario = '"+Ouser.RetornaIdUsuario(user)+"' WHERE pk_id='"+id_factura+"'";
		conn.ejecutar(query);
	}
	
	/* Almacena el detalle de la factura desde el detalle de la contratacion
	 * @param contratacion id de la contratacion
	 * @param lista lista de descuentos
	 * @param id_factura id de la factura
	 */
	public void DetalleFacturaContratacion(int contratacion, Ldescuento lista, int id_factura) throws SQLException{
		Conexion conn = new Conexion();
		String query = "SELECT ads_contrataciones_detalle.pk_id, fk_concepto, cpm, precio_concepto, ads_facturacion_conceptos.porcentaje_descuento FROM ads_facturacion_conceptos, ads_contrataciones_detalle WHERE fk_concepto = ads_facturacion_conceptos.id_concepto AND fk_id_contratacion = "+contratacion;
		ResultSet rs = conn.Consulta(query);
		while(rs.next()){
			double precio_concepto = rs.getDouble("precio_concepto");
			int descuento_concepto = rs.getInt("porcentaje_descuento");
			int cantidad = rs.getInt("cpm");
			ADSProductos OADS = new ADSProductos();
			int descuento_porcentaje_extra = OADS.BuscarDescuento(lista, contratacion, rs.getInt("pk_id"));
			
			double precio_unit = precio_concepto - ((precio_concepto *(descuento_concepto + descuento_porcentaje_extra))/100);
			double total_detalle = precio_unit * cantidad;
			
			query = "INSERT INTO ads_facturacion_detalle (id_factura, id_concepto, cantidad, total, porc_desc, precio) VALUES ('"+id_factura+"', '"+rs.getInt("fk_concepto")+"', '"+cantidad+"','"+total_detalle+"', '"+(descuento_concepto + descuento_porcentaje_extra)+"', '"+precio_unit+"')";
			conn.ejecutar(query);
			conn.desConectar();
		}
	}
	
	/* Almacena el detalle de la factura desde la tabla conceptos
	 * @param conceptos tabla de conceptos
	 * @param id_factura id de la factura
	 */
	public void DetalleFacturaVentaDirecta(Tabla conceptos, int id_factura) throws SQLException{
		for(int i = 0; i < conceptos.getRowCount(); i++){
			double precio_concepto = Double.parseDouble(conceptos.getValueAt(i, 3).toString());
			int descuento_concepto = Integer.parseInt(conceptos.getValueAt(i, 5).toString());
			double cantidad = Double.parseDouble(conceptos.getValueAt(i, 1).toString());
			
			double precio_unit = precio_concepto - ((precio_concepto *(descuento_concepto))/100);
			double total_detalle = precio_unit * cantidad;
			
			Concepto Oconcepto = new Concepto();
			
			String query = "INSERT INTO ads_facturacion_detalle (id_factura, id_concepto, cantidad, total, porc_desc, precio) VALUES ('"+id_factura+"', '"+Oconcepto.RetornaID(conceptos.getValueAt(i, 0).toString())+"', '"+cantidad+"','"+total_detalle+"', '"+descuento_concepto+"', '"+precio_unit+"')";
			conn.ejecutar(query);
		}
	}
	
	/* Almacena el datalle de los tipos de pago
	 * @param pagos tabla de pagos
	 * @param id_factura id de la factura
	 */
	public void RegistroTiposPago(Tabla pagos, int id_factura) throws SQLException{
		Banco Obanco = new Banco();
		for(int i = 0; i < pagos.getRowCount(); i ++){
			String banco = "null";
			if(!pagos.getValueAt(i, 3).toString().equals("")){	
				banco = Obanco.RetornaIdBanco(pagos.getValueAt(i, 3).toString())+"";
			}
			String doc = "null";
			if(!pagos.getValueAt(i, 2).toString().equals("")){	
				doc = pagos.getValueAt(i, 2).toString();
			}
			String query = "INSERT INTO ads_factura_tipos_pago (fk_id_factura, fk_id_pago, monto_transaccion, numero_documento, fk_id_banco) VALUES ("+id_factura+","+pagos.getValueAt(i, 0)+", "+pagos.getValueAt(i, 1)+", "+doc+", "+banco+")";
			conn.ejecutar(query);
		}
		//conn.setCommitTrans();
	}
	
	/* Retorna el id de la factura segun el numero generado
	 * @param nro_factura numero de la factura
	 */
	public int IdFactura(String nro_factura) throws SQLException{
		
		String query = "SELECT pk_id FROM ads_facturacion WHERE numero_factura = '"+nro_factura+"'";
		int id_factura = 0;
		ResultSet nro = conn.Consulta(query);
		if(nro.next()) {
			id_factura = nro.getInt("pk_id");
			nro.close();
		}
		//conn.desConectar();
		return id_factura;
	}
	
	/* Retorna el id del cliente que solicito la contratacion 
	 * @param id_contratacion id de la contratacion
	 */
	public void RetornaClienteContratacion(int id_contratacion) throws SQLException{
		Conexion conn = new Conexion();
		String query = "SELECT tb_usuario.nombre, tb_usuario.rif_cedula, tb_usuario.direccion_envio, tb_usuario.nro_telefono FROM ads_clientes, tb_usuario, ads_contrataciones WHERE ads_clientes.fk_id_usuario = tb_usuario.id_usuario AND ads_clientes.pk_id = ads_contrataciones.fk_id_cliente AND ads_contrataciones.pk_id = '"+id_contratacion+"';";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()) {
			setRif(rs.getString("rif_cedula")); 
			setRazonSocial(rs.getString("nombre"));
			setDireccion(rs.getString("direccion_envio")); 
			setTelefono(rs.getString("nro_telefono")); 
			rs.close();
		}
		conn.desConectar();
	}
	
	/* Retorna si la factura esta dentro o fuera del periodo
	 * @param n_fact nuemero de la factura
	 * @return retorna si la factura esta dentro o fuera de periodo
	 */
	public boolean ValidarFechaFactura(String n_fact) throws SQLException{
		boolean status = false;
		Conexion conn = new Conexion();
		Date hoy;
   		String fecha;
   		SimpleDateFormat formatter;
   		formatter = new SimpleDateFormat("yyyy-MM-dd");
   		hoy = new Date();
   		fecha = formatter.format(hoy);
   		String query = "SELECT fecha_factura FROM ads_facturacion WHERE numero_factura = '"+n_fact+"'";
   		ResultSet rs = conn.Consulta(query);
   		String mes = "";
		String anio = "";
		if(rs.next()) {
			String fecha_factura = rs.getString("fecha_factura");
			String factura_fecha[] = fecha_factura.split("-");
			anio = factura_fecha[0];
			mes = factura_fecha[1];
			rs.close();
		}
   		String fecha_actual[] = fecha.split("-");
   		
   		if(fecha_actual[0].equals(anio) && fecha_actual[1].equals(mes)){
   			status = true;
   		}else{
   			status = false;
   		}
   		conn.desConectar();
   		return status;	
	}
	
	/* Retorna el listado de los bancos almacenados en la tabla tb_bancos
	 * @return lista de bancos
	 */
	public ResultSet RetornaBancos() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT * FROM tb_bancos";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Retorna los tipos de pago disponibles
	 * @return lista de tipos de pago
	 */
	public ResultSet RetornaTiposPago() throws SQLException{
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT * FROM ads_tipospago WHERE status = 'A' ORDER BY id_pago";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Retorna el tipo de pago junto con su id para el JcomboBox del modulo de tipos de pago
	 * @param pago id del tipo de pago
	 * @return numero junto al nombre del tipo de pago
	 */
	public String RetornaCadenaPago(int pago) throws SQLException{ 
		String cadena = "";
		Conexion conn = new Conexion();
		String query = "SELECT * FROM ads_tipospago WHERE id_pago ="+pago;
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			cadena = rs.getString("id_pago")+"-"+rs.getString("descripcion");
			rs.close();
		}
		conn.desConectar();
		return cadena;	
	}
	
	/* Retorna el banco mas su codigo para el JcomboBox del modulo de tipos de pago
	 * @param banco codigo del banco
	 * @return numero junto al nombre del banco
	 */
	public String RetornaCadenaBanco(String banco) throws SQLException{
		String cadena = "Ninguno";
		if(!(banco.equals(""))){
			Conexion conn = new Conexion();
			String query = "SELECT * FROM tb_bancos WHERE codigo_banco = '"+banco+"'";
			ResultSet rs = conn.Consulta(query);
			if (rs.next()){
				cadena = rs.getString("codigo_banco")+"-"+rs.getString("nombre_banco");
				rs.close();
			}
			conn.desConectar();
		}
		return cadena;
	}
	
	/* Consulta en la base de datos el numero de la siguiente factura
	 */
	public void NroFactura() throws SQLException{ 
		String n_pre = "";
		String nro = "";
		this.conn = new Conexion();
		String query = "SELECT numero_factura FROM ads_facturacion ORDER BY numero_factura DESC LIMIT 1";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			nro = ""+(rs.getInt("numero_factura")+1);
			rs.close();
		}
		for(int i = 0; i< 7-nro.length(); i++){
			n_pre = n_pre + "0";
		}
		nro = n_pre + nro;
		this.nro = nro;
	}
	
	public void RegistroCompleto(int iva, int venta, int descuento, double monto_gravado, int id_factura,int id_cliente, String user, Tabla conceptos, Tabla pagos) throws SQLException{ 
		double sub_total;
		double monto_iva;
		double total_pagar;
		
		sub_total = monto_gravado;
		monto_iva = (sub_total * iva)/100;
		total_pagar = sub_total + monto_iva;
		
		Usuario Ouser = new Usuario();
		String query = "UPDATE ads_facturacion SET fk_id_cliente = '"+id_cliente+"', total_pago = '"+total_pagar+"', porc_iva = '"+iva+"', monto_iva = '"+monto_iva+"',monto_gravado = '"+sub_total+"', porcentaje_descuento = '"+descuento+"', id_usuario = '"+Ouser.RetornaIdUsuario(user)+"' WHERE pk_id='"+id_factura+"'";
		conn.ejecutar(query);
		
		for(int i = 0; i < conceptos.getRowCount(); i++){
			double precio_concepto = Double.parseDouble(conceptos.getValueAt(i, 3).toString());
			int descuento_concepto = Integer.parseInt(conceptos.getValueAt(i, 5).toString());
			double cantidad = Double.parseDouble(conceptos.getValueAt(i, 1).toString());
			
			double precio_unit = precio_concepto - ((precio_concepto *(descuento_concepto))/100);
			double total_detalle = precio_unit * cantidad;
			
			Concepto Oconcepto = new Concepto();
			
			query = "INSERT INTO ads_facturacion_detalle (id_factura, id_concepto, cantidad, total, porc_desc, precio) VALUES ('"+id_factura+"', '"+Oconcepto.RetornaID(conceptos.getValueAt(i, 0).toString())+"', '"+cantidad+"','"+total_detalle+"', '"+descuento_concepto+"', '"+precio_unit+"')";
			conn.ejecutar(query);
		}
		
		Banco Obanco = new Banco();
		for(int i = 0; i < pagos.getRowCount(); i ++){
			String banco = "null";
			if(!pagos.getValueAt(i, 3).toString().equals("")){	
				banco = Obanco.RetornaIdBanco(pagos.getValueAt(i, 3).toString())+"";
			}
			String doc = "null";
			if(!pagos.getValueAt(i, 2).toString().equals("")){	
				doc = pagos.getValueAt(i, 2).toString();
			}
			query = "INSERT INTO ads_factura_tipos_pago (fk_id_factura, fk_id_pago, monto_transaccion, numero_documento, fk_id_banco) VALUES ("+id_factura+","+pagos.getValueAt(i, 0)+", "+pagos.getValueAt(i, 1)+", "+doc+", "+banco+")";
			conn.ejecutar(query);
		}
		
	}
}
