package models;

import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import utility.Conexion;

/**
 * Clase para el manejo de tipos de pago
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */


public class TiposPago {
	private String descripcion;
	private Conexion conn;
	
	public TiposPago(){} 
	
	public TiposPago(String descripcion){ 
		this.descripcion = descripcion;
	}
	
	public String getDescripcion(){
		return descripcion;
	}
	
	public void setDescripcion(String descripcion){
		this.descripcion = descripcion;
	}
	
	public void desconectar() throws SQLException{
		conn.desConectar();
	}
	
	/* Registra un nuevo tipo de pago
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "INSERT INTO ads_tipospago (descripcion,status) VALUES ('"+descripcion+"','A')";
		boolean status = conn.ejecutar(query);
		if (status == true){
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Registro realizado con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		conn.desConectar();
		return status;
	}
	
	/* Edita el registro del tipo de pago
	 * @param id_tipopago id del tipo de pago
	 * @return retorna si la modificacion se realizo con exito
	 */
	public boolean Editar(int id_tipopago) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "UPDATE ads_tipospago SET descripcion = '"+descripcion+"' WHERE id_pago = "+id_tipopago;
		boolean status = conn.ejecutar(query);
		if(status == true){
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		conn.desConectar();
		return status;
	}
	
	/* Trae un registro segun la primary key
	 * @param id_tipopago id del tipo de pago
	 */
	public void TraerRegistro(int id_tipopago) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "SELECT * FROM ads_tipospago WHERE id_pago = "+id_tipopago;
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			this.descripcion = rs.getString("descripcion");
			rs.close();
		}
		conn.desConectar();
	}
	
	/* Lista todos los tipos de pago registrados
	 * @return lista de tipos de pago resgistrados
	 */
	public ResultSet ListarTiposPago() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT * FROM ads_tipospago ORDER BY id_pago";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Cambia el estado del tipo de pago
	 * @param id_codigo id del tipo de pago
	 * @return estado del tipo de pago
	 */
	public String CambiarEstado(String id_codigo) throws SQLException{// Cambia el estado de un tipo de pago
		Conexion conn = new Conexion();
		String estado = null;
		String busca_estado = "SELECT status FROM ads_tipospago WHERE id_pago  = '"+id_codigo+"'";
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("status");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE ads_tipospago SET status = 'A' WHERE  id_pago  = '"+id_codigo+"'";
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar el Tipo de pago?","Activar Concepto",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Tipo de pago Activado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "A";
	        }else{
	        	estado = "I";
	        }
		}else if(estado.equals("A")){
			String query = "UPDATE ads_tipospago SET status = 'I' WHERE  id_pago  = '"+id_codigo+"'";
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar el Tipo de pago?","Desactivar Concepto",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Tipo de pago Desactivado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "I";
	        }else{
	        	estado = "A";
	        }
		}
		conn.desConectar();
		return estado;
	}
}
