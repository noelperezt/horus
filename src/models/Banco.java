package models;

import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import utility.Conexion;

/**
 * Clase para el manejo de Bancos
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class Banco {
	private String nombre;
	private String cuenta;
	private String keyid;
	private String publickeyid;
	private Conexion conn;
	
	public Banco(){}
	
	public Banco(String nombre, String cuenta, String keyid, String publickeyid){
		this.nombre = nombre;
		this.cuenta = cuenta;
		this.keyid = keyid;
		this.publickeyid = publickeyid;
	}
	
	public String getNombre(){
		return nombre;
	}
	
	public String getCuenta(){
		return cuenta;
	}
	
	public String getKeyId(){
		return keyid;
	}
	
	public String getPublicKeyId(){
		return publickeyid;
	}
	
	public void setNombre(String nombre){
		this.nombre = nombre;
	}
	
	public void setCuenta(String cuenta){
		this.cuenta = cuenta;
	}
	
	public void setKeyId(String keyid){
		this.keyid = keyid;
	}
	
	public void setPublicKeyId(String publickeyid){
		this.publickeyid = publickeyid;
	}
	
	/* Desconecta la variable de conexion creada por el objeto en la llamada del metodo
	 */
	public void desconectar() throws SQLException{  
		conn.desConectar();
	}
	
	public Conexion getConn(){
		return conn;
	}
	
	/* Registra un nuevo Banco
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "INSERT INTO ads_bancos (nombre_banco, cuenta_bancaria, keyid, publickeyid) VALUES ('"+nombre+"','"+cuenta+"','"+keyid+"','"+publickeyid+"')";
		boolean status = conn.ejecutar(query);
		if (status == true){
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Registro realizado con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		conn.desConectar();
		return status;
	}
	
	/* Edita el registro del Banco
	 * @param id_banco id del banco
	 * @return retorna si la modificacion se realizo con exito
	 */
	public boolean Editar(int id_banco) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "UPDATE ads_bancos SET nombre_banco = '"+nombre+"', cuenta_bancaria = '"+cuenta+"', keyid = '"+keyid+"', publickeyid = '"+publickeyid+"' WHERE pk_id = "+id_banco;
		boolean status = conn.ejecutar(query);
		if(status == true){
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE,icono);
		}
		conn.desConectar();
		return status;
	}
	
	/* Trae un registro segun la primary key 
	 * @param id_banco id del banco
	 */
	public void TraerRegistro(int id_banco) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "SELECT * FROM ads_bancos WHERE pk_id = "+id_banco;
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			this.nombre = rs.getString("nombre_banco");
			this.cuenta = rs.getString("cuenta_bancaria");
			this.keyid = rs.getString("keyid");
			this.publickeyid = rs.getString("publickeyid");
			rs.close();
		}
		conn.desConectar();
	}
	
	/* Cambia el estado de un banco registrado
	 * @param id_banco id del banco
	 * @return retorna el estado del banco
	 */
	public String CambiarEstado(int id_banco) throws SQLException{ 
		Conexion conn = new Conexion();
		String estado = null;
		String busca_estado = "SELECT status_banco FROM ads_bancos WHERE pk_id = "+id_banco;
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("status_banco");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE ads_bancos SET status_banco = 'A' WHERE pk_id = "+id_banco;
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar el Banco?","Activar Banco",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Banco Activado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "A";
	        }else{
	        	estado = "I";
	        }
		}else if(estado.equals("A")){
			String query = "UPDATE ads_bancos SET status_banco = 'I' WHERE pk_id = "+id_banco;
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar el Banco?","Desactivar Banco",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Banco Desactivado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "I";
	        }else{
	        	estado = "A";
	        }
		}
		conn.desConectar();
		return estado;
	}
	
	/* Lista todos los bancos registrados
	 * @return lista de bancos registrados
	 */
	public ResultSet ListarBancos() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT * FROM ads_bancos ORDER BY pk_id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra bancos segun el nombre
	 * @param argumento nombre del banco
	 * @return lista de bancos filtrados
	 */
	public ResultSet FiltraBancos(String argumento) throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT * FROM ads_bancos WHERE nombre_banco ILIKE '%"+argumento+"%' ORDER BY pk_id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Retorna el id del banco segun el codigo
	 * @param cod_banco codigo del banco
	 * @return id del banco
	 */
	public int RetornaIdBanco(String cod_banco) throws SQLException{
		Conexion conn = new Conexion();
		String query = "SELECT id_banco FROM tb_bancos WHERE codigo_banco = '"+cod_banco+"'";
		ResultSet rs = conn.Consulta(query);
		int id = 0;
		if(rs.next()){
			id = rs.getInt("id_banco");
			rs.close();
		}
		conn.desConectar();
		return id;		
	}
}
