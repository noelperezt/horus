package models;

import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import utility.Conexion;
import utility.Lpermiso;

/**
 * Clase para el manejo de Submodulos
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class Submodulo extends Modulo{
	private int padre;
	private String estado;
	private Conexion conn;
	
	public Submodulo(){} 
	
	public Submodulo(int padre, String descripcion){ 
		this.padre = padre;
		this.setDescripcion(descripcion);
	}
	
	public int getPadre(){
		return padre;
	}
	
	public String getEstado(){
		return estado;
	}
	
	/* Desconecta la variable de conexion creada por el objeto en la llmada del metodo
	 */
	public void desconectar() throws SQLException{ 
		conn.desConectar();
	}
	
	/* Registra un nuevo submodulo
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "INSERT INTO ads_facturacion_submodulos (fk_id_modulo, descripcion, status) VALUES ('"+padre+"','"+getDescripcion()+"', 'A')";
		boolean status = conn.ejecutar(query);
		if(status == true){
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Registro realizado con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		conn.desConectar();
		return status;
	}
	
	/* Edita el registro del submodulo
	 * @param id_submodulo id del submodulo
	 * @return retorna si la modificacion se realizo con exito
	 */
	public boolean Editar(int id_submodulo) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "UPDATE ads_facturacion_submodulos SET fk_id_modulo = "+padre+", descripcion = '"+getDescripcion()+"' WHERE pk_id ="+id_submodulo;
		boolean status = conn.ejecutar(query);
		if(status == true){
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
		}
		conn.desConectar();
		return status;
	}
	
	/* Trae un registro segun la primary key
	 * @param id_submodulo id del sub modulo
	 */
	public void TraerRegistro(int id_submodulo) throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "SELECT * FROM ads_facturacion_submodulos WHERE pk_id ="+id_submodulo;
		ResultSet rs = conn.Consulta(query);
		if (rs.next()){
			padre = rs.getInt("fk_id_modulo");
			setDescripcion(rs.getString("descripcion"));
			rs.close();
		}
		conn.desConectar();
	}
	
	/* Cambia el estado del submodulo
	 * @param id_codigo codigo del submodulo
	 * @return estado del concepto submodulo
	 */
	public String CambiarEstado(String id_codigo) throws SQLException{
		Conexion conn = new Conexion();
		String estado = null;
		String busca_estado = "SELECT status FROM ads_facturacion_submodulos WHERE pk_id  = '"+id_codigo+"'";
		ResultSet busca = conn.Consulta(busca_estado);
		if(busca.next()) {
			estado = busca.getString("status");
			busca.close();
		}
		if(estado.equals("I")){
			String query = "UPDATE ads_facturacion_submodulos SET status = 'A' WHERE  pk_id  = '"+id_codigo+"'";
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de activar el Subm�dulo?","Activar Subm�dulo",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Subm�dulo Activado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "A";
	        }else{
	        	estado = "I";
	        }
		}else if(estado.equals("A")){
			String query = "UPDATE ads_facturacion_submodulos SET status = 'I' WHERE  pk_id  = '"+id_codigo+"'";
			String ruta = new File ("").getAbsolutePath ();
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
			if(JOptionPane.showConfirmDialog(null, "�Est� seguro de desactivar el Subm�dulo?","Desactivar Subm�dulo",1,0,icono)==0){
				conn.ejecutar(query);
				icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Subm�dulo Desactivado", "Exito", JOptionPane.INFORMATION_MESSAGE, icono);
				estado = "I";
	        }else{
	        	estado = "A";
	        }
		}
		conn.desConectar();
		return estado;
	}
	
	/* Retorna la descripcion del padre
	 * @return descripcion del modulo padre
	 */
	public String RetornaDescripcionPadre() throws SQLException{ 
		String descripcion = "";
		Conexion conn = new Conexion();
		String query = "SELECT * FROM ads_facturacion_modulos WHERE pk_id="+padre;
		ResultSet rs = conn.Consulta(query);
		if (rs.next()){
			descripcion = rs.getString("descripcion");
			rs.close();
		}
		conn.desConectar();
		return descripcion;
	}
	
	/* Retorna lista de submodulos
	 * @return lista de submodulos
	 */
	public ResultSet ListarSubmodulos() throws SQLException{
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT ads_facturacion_submodulos.pk_id, ads_facturacion_modulos.descripcion, ads_facturacion_submodulos.descripcion ,ads_facturacion_submodulos.status FROM ads_facturacion_modulos, ads_facturacion_submodulos WHERE ads_facturacion_modulos.pk_id = ads_facturacion_submodulos.fk_id_modulo ORDER BY ads_facturacion_modulos.pk_id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Devuelve una lista con los estados de los submodulos
	 * @return lista con los estados de los submodulos
	 */
	public Lpermiso SubmodulosActivos() throws SQLException{ 
		Lpermiso visibilidad = new Lpermiso();
        Conexion conn = new Conexion();
        String query = "SELECT * FROM ads_facturacion_submodulos ORDER BY pk_id";
		ResultSet busca = conn.Consulta(query);
		while(busca.next()){
			int id = Integer.parseInt(busca.getString("pk_id"));
			visibilidad.InsFinal(id, busca.getString("status"),busca.getString("descripcion"));
		}
		conn.desConectar();
		return visibilidad;
	}
}
