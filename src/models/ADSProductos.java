package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;

import utility.Conexion;
import utility.Ldescuento;
import utility.Ndescuento;

/**
 * Clase para traer datos de las contrataciones de DUMGA ADS Productos
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class ADSProductos {
	private Conexion conn;
	
	public ADSProductos(){}
	
	public Conexion getConn(){
		return conn;
	}
	
	public void desconectar() throws SQLException{
		conn.desConectar();
	}
	
	/* 
	 * Lista contrataciones pendientes por facturar
	 * @return lista de contrataciones pendientes
	 * @note para que esta funcion liste la contratacion para su facturacion el pago debera ser verificado previamente
	 */
	public ResultSet ListarContrataciones() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT ads_contrataciones.pk_id, nombre, descripcion, total_pago, fecha_transaccion, fecha_pago, fecha_pago_confirmado FROM ads_contrataciones, tb_usuario, ads_clientes WHERE ads_contrataciones.status = 'I' AND fecha_pago_confirmado IS NOT NULL AND fk_id_cliente = ads_clientes.pk_id AND fk_id_usuario = tb_usuario.id_usuario  ORDER BY pk_id";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}

	/* Lista las contrataciones pendiente por confirmar el pago
	 * @return lista de contrataciones pendientes por confirmar pago
	 */
	public ResultSet ListarPendientePago() throws SQLException{ //Lista todas las contrataciones pendientes por confirmar el pago
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT pk_id, fecha_pago, recibo, total_pago FROM ads_contrataciones WHERE recibo IS NOT NULL AND fecha_pago IS NOT NULL AND fecha_pago_confirmado IS NULL";
		ResultSet rs = conn.Consulta(query);
		return rs;
	}

	/* Confirma el pago de la contratacion
	 * @param id_contratacion id de la contratacion
	 * @param id_user id del usuario
	 * @return retorna si la actualizacion fue exitosa
	 */
	public boolean VerificarPago(int id_contratacion, int id_user) throws SQLException{ 
		boolean status = false;
   		SimpleDateFormat formatter;
   		formatter = new SimpleDateFormat("yyyy-MM-dd");
   		Date hoy = new Date();
   		String fecha = formatter.format(hoy); 
		Conexion conn = new Conexion();
		String query = "UPDATE ads_contrataciones SET fecha_pago_confirmado = '"+fecha+"', id_usuario = "+id_user+", status = 'I' WHERE pk_id ="+id_contratacion;
		status = conn.ejecutar(query);
		conn.desConectar();
		return status;
	}
	
	/* Cuenta cantidad de contrataciones pendientes
	 * @return cantidad de contrataciones pendientes
	 */
	public int CuentaContratacionesPendientes() throws SQLException{ 
		int contrataciones = 0;
		Conexion conn = new Conexion();
		String total = null;
		String query = "SELECT COUNT (*) as cuenta FROM ads_contrataciones WHERE status = 'I' AND fecha_pago_confirmado IS NOT NULL";
		ResultSet busca = conn.Consulta(query);
		if(busca.next()) {
			total = busca.getString("cuenta");
			busca.close();
		}
		if ( !((total == null) || (total.equals(""))) ) {
			contrataciones= Integer.parseInt(total);
		}
		conn.desConectar();
		return contrataciones;
	}
	
	/* Activa contratacion y cambia el estado a facturada
	 */
	public void ActivarContratacion(int id_contratacion) throws SQLException{
		Conexion conn = new Conexion();
		String query = "UPDATE ads_contrataciones SET status = 'A', facturada = 'S' WHERE pk_id='"+id_contratacion+"'";
		conn.ejecutar(query);
		conn.desConectar();
	}
	
	/* Desactiva contratacion y cambia su estado a no facturada
	 */
	public void DesactivarContratacion(int id_contratacion) throws SQLException{
		Conexion conn = new Conexion();
		String query = "UPDATE ads_contrataciones SET status = 'I', facturada = 'N' WHERE pk_id='"+id_contratacion+"'";
		conn.ejecutar(query);
		conn.desConectar();
	}
	
	/* Retorna el monto gravado de la contratacion
	 * @param id_contratacion id de la contratacion
	 * @return monto gravado de la contratacion
	 */
	public double RetornaMontoGravado(int id_contratacion) throws SQLException{ 
		double monto_gravado = 0.0;
		Conexion conn = new Conexion();
		String sentencia = "SELECT monto_gravado FROM ads_contrataciones WHERE pk_id ="+id_contratacion;
		
		ResultSet busca;
		busca = conn.Consulta(sentencia);
		
		if(busca.next()){
			monto_gravado = busca.getDouble("monto_gravado");
			busca.close();
		}
		
		conn.desConectar();
		return monto_gravado;
	}
	
	/* Determina si la contratacion existe para no tener dos facturas activas con la misma contratacion
	 * @param id_contratacion id de la contratacion
	 * @return devuelve si existe o no existe la contratacion
	 */
	public boolean ContratacionExistente(int id_contratacion) throws SQLException{ 
		boolean existe = false;
		Conexion conn = new Conexion();
		String query  = "SELECT * FROM ads_facturacion WHERE anulada = 'N' AND numero_documento = "+id_contratacion;
		ResultSet rs = conn.Consulta(query);
		if (conn.getRowsCount(rs) != 0){
			query = "UPDATE ads_contrataciones SET status = 'A', facturada = 'S' WHERE pk_id='"+id_contratacion+"'";
			conn.ejecutar(query);
			existe = true;
		}
		return existe;
	}
	
	/* Lee de la lista y determina el porcentaje total del descuento de los item de la contratacion
	 * @param lista lista de descuentos
	 * @param contratacion id de contratacion
	 * @return descuento total
	 */
	public int PorcentajeDescuentoTotal(Ldescuento lista , int contratacion){ 
		int descuento = 0;
		Ndescuento actual;
		actual = lista.Tope();
		while(actual != null){
			if(actual.GetId() == contratacion ){
				descuento = descuento + actual.GetDescuento();
			}	
			actual = actual.GetSig();
		}
		return descuento;
	}
	
	/* Determina el porcentaje de descuento de un item del detalle de la contratacion
	 * @param lista lista de descuentos
	 * @param contratacion id de la contratacion
	 * @param detalle id del detalle de la contratacion
	 * @return descuento
	 */
	public int BuscarDescuento(Ldescuento lista, int contratacion, int detalle){ 
		int descuento = 0;
		Ndescuento actual;
		actual = lista.Tope();
		while(actual != null){
			if(actual.GetId() == contratacion && actual.GetIdConcepto() == detalle){
				descuento = actual.GetDescuento();
				break;
			}	
			actual = actual.GetSig();
		}
		return descuento;
	}
	
	/* Filtra lista de contrataciones pendientes segun id de contratacion
	 * @param argumento id de la contratacion
	 * @return lista de contrataciones pendientes filtradas
	 */
	public ResultSet ListaContratacionesNro(String argumento) throws SQLException{
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT ads_contrataciones.pk_id, nombre, descripcion, total_pago, fecha_transaccion, fecha_pago, fecha_pago_confirmado FROM ads_contrataciones, tb_usuario, ads_clientes WHERE ads_contrataciones.status = 'I' AND fecha_pago_confirmado IS NOT NULL AND fk_id_cliente = ads_clientes.pk_id AND fk_id_usuario = tb_usuario.id_usuario AND TO_CHAR(ads_contrataciones.pk_id, '9999999999') LIKE '%"+argumento+"%'  ORDER BY pk_id;";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra lista contrataciones pendientes segun la razon social
	 * @param argumento rezon social de la contratacion
	 * @return lista de contrataciones pendientes filtradas
	 */
	public ResultSet ListaContratacionesRazonSocial(String argumento) throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT ads_contrataciones.pk_id, nombre, descripcion, total_pago, fecha_transaccion, fecha_pago, fecha_pago_confirmado FROM ads_contrataciones, tb_usuario, ads_clientes WHERE ads_contrataciones.status = 'I' AND fecha_pago_confirmado IS NOT NULL AND fk_id_cliente = ads_clientes.pk_id AND fk_id_usuario = tb_usuario.id_usuario AND nombre ILIKE '%"+argumento+"%'  ORDER BY pk_id;";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Retorna el numero de la contratacion segun el numero de la factura
	 * @param n_fact numero de la factura
	 * @return id de la contratacion
	 */
	public int RetornaIdContratacion(String n_fact) throws SQLException{
		int id = 0;
		Conexion conn = new Conexion();
		String query = "SELECT numero_documento FROM ads_facturacion WHERE tipo = 'C' AND anulada = 'N' AND numero_factura ='"+n_fact+"'";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			id = rs.getInt("numero_documento");
			rs.close();
		}
		conn.desConectar();
		return id;
	}
	
}
