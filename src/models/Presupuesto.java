package models;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import utility.Conexion;
import utility.Tabla;

/**
 * Clase para el manejo de los presupuestos
 * @author Noel P�rez
 * @email nperez@pinttosoft.com
 */

public class Presupuesto extends Cliente{
	 
	private Conexion conn;
	private String nro;
	private int validez;
	private String user;
	
	public Presupuesto(){
		Conexion conn = new Conexion();
		this.conn = conn;
	} 
	public Presupuesto(Conexion conn){
		this.conn = conn;
	} 
	
	public Presupuesto(String rif, int validez, String user){
		setRif(rif);
		this.validez = validez;
		this.user = user;
		Conexion conn = new Conexion();
		this.conn = conn;
	}
	
	public void setConexion(Conexion conn){
		this.conn = conn;
	}
	
	public Conexion getConexion(){
		return conn;
	}
	 
	/* Consulta en la BD el numero del siguiente presupuesto
	 */
	public void NroPresupuesto() throws SQLException{ 
		String n_pre = "";
		String nro = "";
		this.conn = new Conexion();
		String query = "SELECT MAX(id_presupuesto) as nro FROM ads_presupuesto";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			nro = ""+(rs.getInt("nro")+1);
			rs.close();
		}
		for(int i = 0; i< 7-nro.length(); i++){
			n_pre = n_pre + "0";
		}
		this.nro = n_pre + nro;
	}
	
	public String getNumero(){ 
		return nro;
	}
	
	public void Desconectar() throws SQLException{ 
		conn.desConectar();
	}
	
	/* Registra un nuevo presupuesto
	 * @return retorna si el registro se realizo con exito
	 */
	public boolean Registrar() throws SQLException{
		boolean registro = false;
		Usuario Ouser = new Usuario();
		Date hoy;
   		String fecha;
   		SimpleDateFormat formatter;
   		formatter = new SimpleDateFormat("yyyy-MM-dd");
   		hoy = new Date();
   		fecha = formatter.format(hoy);
		String query = "INSERT INTO ads_presupuesto (id_cliente, id_usuario, fecha, procesado, dias_validez, anulado, motivo_anulacion, fecha_anulacion) VALUES ("+RetornaID(getRif())+", "+Ouser.RetornaIdUsuario(user)+", '"+fecha+"', 'N', "+validez+", 'N', NULL, NULL )";
		registro = conn.ejecutar(query);
		return registro;
	}
	
	/* Permite llenar el detalle del presupuesto
	 * @param listado tabla con el detalle del presupuesto
	 * @param id_presupuesto id del presupuesto
	 */
	public void RegistrarDetalle(Tabla listado, int id_presupuesto) throws SQLException{ 
		String query;
		Concepto Oconcepto = new Concepto();
		for (int i = 0; i < listado.getRowCount(); i++){
			query = "INSERT INTO ads_presupuesto_detalle (fk_presupuesto, id_concepto, cantidad, precio, total ) VALUES ("+id_presupuesto+", "+Oconcepto.RetornaID(listado.getValueAt(i, 0).toString())+", "+listado.getValueAt(i, 1)+", "+listado.getValueAt(i, 3)+", "+listado.getValueAt(i, 4)+")";
			conn.ejecutar(query);
		}
	}
	
	/* Lista todos los presupuestos Activos
	 * @return lista con los presupuestos activos
	 */
	public ResultSet ListarPresupuestos() throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT lpad(cast( cast( id_presupuesto as integer) as char(7)),7, '0') as nro_presupuesto, rif, razon_social, ads_presupuesto.fecha, dias_validez,(CAST(ads_presupuesto.fecha AS DATE) + CAST(dias_validez || 'days' AS INTERVAL))::DATE as fecha_vencimiento ,nombre FROM ads_presupuesto, ads_clientes, tb_administrador WHERE ads_presupuesto.id_cliente = ads_clientes.pk_id AND ads_presupuesto.id_usuario = tb_administrador.id_admin AND anulado = 'N' AND procesado = 'N' ORDER BY id_presupuesto DESC";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Filtra presupuestos segun el numero
	 * @return lista filtrada de presupuestos
	 */
	public ResultSet FiltraPresupuestos(String argumento) throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT lpad(cast( cast( id_presupuesto as integer) as char(7)),7, '0') as nro_presupuesto, rif, razon_social, ads_presupuesto.fecha, dias_validez,(CAST(ads_presupuesto.fecha AS DATE) + CAST(dias_validez || 'days' AS INTERVAL))::DATE as fecha_vencimiento ,nombre FROM ads_presupuesto, ads_clientes, tb_administrador WHERE ads_presupuesto.id_cliente = ads_clientes.pk_id AND ads_presupuesto.id_usuario = tb_administrador.id_admin AND anulado = 'N' AND TRIM(TO_CHAR(id_presupuesto, '999999999')) ILIKE '%"+argumento+"%' ORDER BY id_presupuesto DESC";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Anula todos los presupuestos expirados
	 */
	public void AnularVencidos() throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "UPDATE ads_presupuesto SET fecha_anulacion = now(), motivo_anulacion = 'Presupuesto Expirado', anulado = 'S' WHERE (CAST(ads_presupuesto.fecha AS DATE) + CAST(dias_validez || 'days' AS INTERVAL))::DATE < now()";
		conn.ejecutar(query);
		conn.desConectar();
	}
	
	/* Retorna el id del cliente segun el numero del presupuesto
	 * @param id_presupuesto numero del presupuesto
	 */
	public int RetornaIdCliente(String id_presupuesto) throws SQLException{ 
		int id = 0;
		Conexion conn = new Conexion();
		String query = "SELECT id_cliente FROM ads_presupuesto WHERE id_presupuesto ="+id_presupuesto;
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			id = rs.getInt("id_cliente");
			rs.close();
		}
		conn.desConectar();
		return id;
	} 
	
    /* Retorna la Validez de un presupuesto
     * @param id_presupuesto id del presupuesto
     * @return validez del presupuesto
     */
	public int RetornaValidez(int id_presupuesto) throws SQLException{ 
		int validez = 0;
		Conexion conn = new Conexion();
		String query = "SELECT dias_validez FROM ads_presupuesto WHERE id_presupuesto ="+id_presupuesto;
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			validez = rs.getInt("dias_validez");
			rs.close();
		}
		return validez;
	}
	
	/* Lista los detalles de un presupuesto
	 * @param id_presupuesto id del presupuesto
	 * @return lista de detalles del presupuesto
	 */
	public ResultSet ListarPresupuestosDetalle(int id_presupuesto) throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT codigo_concepto, cantidad, nombre_concepto, precio, total, 0 as descuento FROM ads_presupuesto_detalle, ads_facturacion_conceptos WHERE ads_presupuesto_detalle.id_concepto = ads_facturacion_conceptos.id_concepto AND fk_presupuesto = "+id_presupuesto;
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Cambia el estado de un presupuesto
	 * @param id_presupuesto id del presupuesto
	 */
	public void ProcesarPresupuesto(int id_presupuesto) throws SQLException{ 
		String query = "UPDATE ads_presupuesto SET procesado = 'S' WHERE id_presupuesto ="+id_presupuesto;
		conn.ejecutar(query);
	}
	
	/* Retorna si el cliente tiene presupuestos asociados
	 * @param id_cliente id del cliente
	 * @return devuelve si el cliente posee o no presupuestos asociados
	 */
	public boolean PresupuestosAsociados(int id_cliente) throws SQLException{  
		boolean status = false;
		AnularVencidos();
		Conexion conn = new Conexion();
		String query = "SELECT * FROM ads_presupuesto WHERE procesado = 'N' AND anulado = 'N' AND id_cliente = "+id_cliente;
		ResultSet rs = conn.Consulta(query);
		if (conn.getRowsCount(rs) != 0){
			status = true;
		}
		conn.desConectar();
		return status;
	}
	
	/* Retorna lista de presupuestos asociados 
	 * @param id_cliente id del cliente
	 * @return lista de presupuestos asociados
	 */
	public ResultSet ListarPresupuestosAsociados(int id_cliente) throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT lpad(cast( cast( id_presupuesto as integer) as char(7)),7, '0') as nro_presupuesto, ads_presupuesto.fecha, dias_validez,(CAST(ads_presupuesto.fecha AS DATE) + CAST(dias_validez || 'days' AS INTERVAL))::DATE as fecha_vencimiento ,nombre FROM ads_presupuesto, ads_clientes, tb_administrador WHERE ads_presupuesto.id_cliente = ads_clientes.pk_id AND ads_presupuesto.id_usuario = tb_administrador.id_admin AND anulado = 'N' AND procesado = 'N' AND id_cliente = "+id_cliente+" ORDER BY id_presupuesto DESC";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
	/* Retorna lista de presupuestos asociados segun el numero
	 * @param id_cliente id del cliente
	 * @param argumento numero del presupuesto
	 * @return lista de presupuestos filtrada
	 */
	public ResultSet FiltroPresupuestosAsociados(int id_cliente, String argumento) throws SQLException{ 
		Conexion conn = new Conexion();
		this.conn = conn;
		String query = "SELECT lpad(cast( cast( id_presupuesto as integer) as char(7)),7, '0') as nro_presupuesto, ads_presupuesto.fecha, dias_validez,(CAST(ads_presupuesto.fecha AS DATE) + CAST(dias_validez || 'days' AS INTERVAL))::DATE as fecha_vencimiento ,nombre FROM ads_presupuesto, ads_clientes, tb_administrador WHERE ads_presupuesto.id_cliente = ads_clientes.pk_id AND ads_presupuesto.id_usuario = tb_administrador.id_admin AND anulado = 'N' AND procesado = 'N' AND id_cliente = "+id_cliente+"  AND TRIM(TO_CHAR(id_presupuesto, '999999999')) ILIKE '%"+argumento+"%' ORDER BY id_presupuesto DESC";
		ResultSet rs = this.conn.Consulta(query);
		return rs;
	}
	
}
