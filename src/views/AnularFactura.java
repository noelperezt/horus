package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;

import models.Usuario;
import net.sf.jasperreports.engine.JRException;
import utility.AbstractJasperReports;
import utility.Conexion;
import utility.EstiloHorus;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;

public class AnularFactura {

	public JFrame frmAnularFactura;
	public String ruta = new File ("").getAbsolutePath ();
	private String n_fact;
	private int contratacion;
	private JTextArea textArea;
	private Usuario user;

	public int retornaUserId(String name) throws SQLException{
		int user = 0;
		Conexion conn = new Conexion();
		String sentencia = "SELECT id_admin FROM tb_administrador WHERE usuario ='"+name+"'";
		
		ResultSet busca;
		busca = conn.Consulta(sentencia);
		
		if(busca.next()){
			user = Integer.parseInt(busca.getString("id_admin"));
			busca.close();
		}
		
		conn.desConectar();
		return user;
	}
	
	public AnularFactura(String n_fact, int contratacion, Usuario user) {
		this.user = user;
		this.n_fact = n_fact;
		this.contratacion = contratacion;
		initialize();
	}
	
	@SuppressWarnings("resource")
	public void ejecutar() throws HeadlessException, JRException{
		Conexion conn = new Conexion();
		Date hoy;
   		String fecha;
   		SimpleDateFormat formatter;
   		formatter = new SimpleDateFormat("yyyy-MM-dd");
   		hoy = new Date();
   		fecha = formatter.format(hoy);
		if(!textArea.getText().equals("")){
			String funcion = "SELECT fecha_factura FROM ads_facturacion WHERE numero_factura = '"+n_fact+"'";
			ResultSet nro;
			String mes = "";
			String anio = "";
			try {
				nro = conn.Consulta(funcion);
				if(nro.next()) {
					String fecha_factura = nro.getString("fecha_factura");
					String factura_fecha[] = fecha_factura.split("-");
					anio = factura_fecha[0];
					mes = factura_fecha[1];
					nro.close();
				}
				
				SimpleDateFormat formatter1;
		   		formatter1 = new SimpleDateFormat("yyyy-MM-dd");
		   		Date hoy1 = new Date();
		   		String fecha1 = formatter1.format(hoy1);
		   		
		   		String fecha_actual[] = fecha1.split("-");
		   		
		   		if(fecha_actual[0].equals(anio) && fecha_actual[1].equals(mes)){
		   			String query = "UPDATE ads_facturacion SET anulada = 'S', fecha_anulacion = '"+fecha+"', motivo_anulacion = '"+textArea.getText()+"', id_usuario_anula = '"+retornaUserId(user.getUsuario())+"' WHERE numero_factura ='"+n_fact+"'";
					try {
						conn.ejecutar(query);
						if (contratacion != 0){
							query = "UPDATE ads_contrataciones SET status = 'I', facturada = 'N' WHERE pk_id='"+contratacion+"'";
							conn.ejecutar(query);	
						}
						Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
						JOptionPane.showMessageDialog(null, "Anulaci�n Exitosa", "Exito", JOptionPane.INFORMATION_MESSAGE,icono);
						AdministrarFacturas ventana = new AdministrarFacturas(user);
						ventana.frmAdministrarFacturas.setVisible(true);
						frmAnularFactura.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
		   			
		   		}else{
		   			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/informacion.png"));
					JOptionPane.showMessageDialog(null, "La anulaci�n est� fuera de per�odo fiscal, se generar� una nota de cr�dito", "Informaci�n", JOptionPane.INFORMATION_MESSAGE,icono);
		   			String query = "UPDATE ads_facturacion SET anulada = 'S', fecha_anulacion = '"+fecha+"', motivo_anulacion = '"+textArea.getText()+"', id_usuario_anula = '"+retornaUserId(user.getUsuario())+"' WHERE numero_factura ='"+n_fact+"'";
					conn.ejecutar(query);
					if(contratacion != 0){
						query = "UPDATE ads_contrataciones SET status = 'I', facturada = 'N' WHERE pk_id='"+contratacion+"'";
						conn.ejecutar(query);
					}	
		   			String id_fact = "SELECT pk_id FROM ads_facturacion WHERE numero_factura = '"+n_fact+"'";
		   			int id_factura = 0;
		   			
		   			ResultSet buscar = conn.Consulta(id_fact);
		   			if(buscar.next()){
		   				id_factura = Integer.parseInt(buscar.getString("pk_id").toString());
		   				buscar.close();
		   			}
		   			funcion = "SELECT obtener_numero_nota("+id_factura+") as controles";
					String nro_nota = "";
					try {
						nro = conn.Consulta(funcion);
						if(nro.next()) {
							String controles = nro.getString("controles");
							String e = controles.replace("(", "");
							String b = e.replace(")", "");
							String nota[] = b.split(",");
							nro_nota = nota[0];
							nro.close();
						}} catch (Exception exc) {
							throw new RuntimeException(exc);
					}
					//Llenar Registros Faltantes de la nota de cr�dito
					query = "SELECT tipo FROM ads_facturacion WHERE numero_factura = '"+n_fact+"'";
					String tipo_nota = "";
					buscar = null;
					buscar = conn.Consulta(query);
					
					if(buscar.next()){
						tipo_nota = buscar.getString("tipo");
						buscar.close();
					}
					
					query = "UPDATE ads_notascredito SET tipo = '"+tipo_nota+"', id_usuario = '"+retornaUserId(user.getUsuario())+"'";
					conn.ejecutar(query);
					
					query = "SELECT ads_clientes.fk_id_usuario FROM ads_clientes, ads_facturacion WHERE ads_facturacion.fk_id_cliente = ads_clientes.pk_id AND ads_facturacion.pk_id = '"+id_factura+"'";
					nro= null;
					String valor = null;
					try {
						nro = conn.Consulta(query);
						if(nro.next()) {
							valor = nro.getString("fk_id_usuario");
							nro.close();
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					String rif_cliente = "";
					String nombre_cliente = "";
					String direccion_cliente = "";
					String telefono_cliente = "";
					int id = Integer.parseInt(valor);
					if (id != 0){
						query = "SELECT tb_usuario.nombre, tb_usuario.rif_cedula, tb_usuario.direccion_envio, tb_usuario.nro_telefono FROM ads_clientes, tb_usuario, ads_facturacion WHERE ads_clientes.fk_id_usuario = tb_usuario.id_usuario AND ads_clientes.pk_id = ads_facturacion.fk_id_cliente AND ads_facturacion.pk_id = '"+id_factura+"';";
						nro = conn.Consulta(query);
						if(nro.next()) {
							rif_cliente = nro.getString("rif_cedula");
							nombre_cliente = nro.getString("nombre");
							direccion_cliente = nro.getString("direccion_envio");
							telefono_cliente = nro.getString("nro_telefono");
							nro.close();
						}
						AbstractJasperReports.createNotaCredito(conn.getConnection(), ruta+"/reports/nota credito/", id_factura, nro_nota, rif_cliente, nombre_cliente, direccion_cliente, telefono_cliente );
					}else{
						query = "SELECT ads_clientes.rif, ads_clientes.razon_social, ads_clientes.telefono_cliente, ads_clientes.direccion_cliente FROM ads_clientes, ads_facturacion WHERE ads_clientes.pk_id = ads_facturacion.fk_id_cliente AND ads_facturacion.pk_id = '"+id_factura+"';";
						nro = conn.Consulta(query);
						if(nro.next()) {
							rif_cliente = nro.getString("rif");
							nombre_cliente = nro.getString("razon_social");
							direccion_cliente = nro.getString("direccion_cliente");
							telefono_cliente = nro.getString("telefono_cliente");
							nro.close();
						}
						AbstractJasperReports.createNotaCredito(conn.getConnection(), ruta+"/reports/nota credito/", id_factura, nro_nota, rif_cliente, nombre_cliente, direccion_cliente, telefono_cliente );
					}
						VistaImpresion vista = new VistaImpresion();
						vista.setLocationRelativeTo(frmAnularFactura);
						vista.setVisible(true);
						query = "UPDATE ads_facturacion SET anulada = 'S', fecha_anulacion = '"+fecha+"', motivo_anulacion = '"+textArea.getText()+"', id_usuario_anula = '"+retornaUserId(user.getUsuario())+"' WHERE numero_factura ='"+n_fact+"'";
						conn.ejecutar(query);
						icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
						JOptionPane.showMessageDialog(null, "Anulaci�n Exitosa", "Exito", JOptionPane.INFORMATION_MESSAGE,icono);
						try {
 							AdministrarFacturas ventana = new AdministrarFacturas(user);
							ventana.frmAdministrarFacturas.setVisible(true);
							frmAnularFactura.dispose();
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}	
						try{
							AbstractJasperReports.Print();	
						}catch(JRException e){
							e.printStackTrace();
							icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
							JOptionPane.showMessageDialog(null, "Error de Impresi�n","Error" , JOptionPane.ERROR_MESSAGE,icono); 
						}	
		   		}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			
		}else{
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "Debe ingresar el motivo de la Anulaci�n","Error" , JOptionPane.ERROR_MESSAGE,icono); 
		}
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		EstiloHorus tema = new EstiloHorus();
		frmAnularFactura = new JFrame();
		frmAnularFactura.setResizable(false);
		frmAnularFactura.setTitle("Anular Factura");
		frmAnularFactura.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAnularFactura.setBounds(100, 100, 363, 226);
		frmAnularFactura.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAnularFactura.getSize();  
        frmAnularFactura.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		frmAnularFactura.getContentPane().setLayout(null);
		frmAnularFactura.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				frmAnularFactura.dispose();
				AdministrarFacturas ventana;
				try {
					ventana = new AdministrarFacturas(user);
					ventana.frmAdministrarFacturas.setVisible(true);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
		});
		
		JLabel lblMotivo = new JLabel("MOTIVO:");
		lblMotivo.setFont(new Font("Arial", Font.BOLD, 11));
		lblMotivo.setBounds(34, 44, 61, 14);
		frmAnularFactura.getContentPane().add(lblMotivo);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(89, 44, 235, 79);
		frmAnularFactura.getContentPane().add(scrollPane);
		
		textArea = new JTextArea(4,50);
		textArea.setLineWrap(true); 
		textArea.setWrapStyleWord(true); 
		textArea.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
			    if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) &&(caracter != '�') &&(caracter != '�') &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE){
			         evt.consume();
			    }	
			}
			@SuppressWarnings({ "static-access", "deprecation" })
			@Override
			public void keyPressed(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		            textArea.nextFocus();
		        }
			}
		});
		scrollPane.setViewportView(textArea);
		
		JButton btnNewButton = new JButton("Anular");
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBorderPainted(false);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.setBackground(tema.getAzul());
		btnNewButton.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		           try {
					ejecutar();
				} catch (HeadlessException | JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        }
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ejecutar();
				} catch (HeadlessException | JRException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/break.png"));
		btnNewButton.setBounds(44, 134, 116, 41);
		frmAnularFactura.getContentPane().add(btnNewButton);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setForeground(Color.WHITE);
		btnCancelar.setBorderPainted(false);
		btnCancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnCancelar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnCancelar.setBackground(tema.getAzul());
			}
		});
		btnCancelar.setBackground(tema.getAzul());
		btnCancelar.setIcon(new ImageIcon(ruta+"/images/close.png"));
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmAnularFactura.dispose();
				AdministrarFacturas ventana;
				try {
					ventana = new AdministrarFacturas(user);
					ventana.frmAdministrarFacturas.setVisible(true);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
		});
		btnCancelar.setBounds(203, 134, 116, 41);
		frmAnularFactura.getContentPane().add(btnCancelar);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 334, 180);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ANULAR FACTURA", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAnularFactura.getContentPane().add(lblNewLabel);
	}
}
