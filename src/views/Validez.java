package views;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import utility.EstiloHorus;
import javax.swing.JLabel;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class Validez extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private int validez;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			Validez dialog = new Validez();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public void setValidez(int validez){
		this.validez = validez;
	}
	
	public int getValidez(){
		return validez;
	}
	
	
	public Validez() {
		setModal(true);
		setTitle("D\u00EDas de Validez");
		String ruta = new File ("").getAbsolutePath ();
		EstiloHorus tema = new EstiloHorus();
		setResizable(false);
		setBounds(100, 100, 468, 184);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		JLabel lblValidz = new JLabel("VALID\u00C9Z:");
		lblValidz.setFont(new Font("Arial", Font.BOLD, 11));
		lblValidz.setBounds(39, 54, 61, 14);
		contentPanel.add(lblValidz);
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (textField.getText().equals("")){
					setValidez(0);
				}else{
					setValidez(Integer.parseInt(textField.getText()));
				}
				setVisible(false);
			}
		});
		textField.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || textField.getText().length() == 3 ){
		            evt.consume();
		        }
			}
		});
		textField.setText((String) null);
		textField.setColumns(10);
		textField.setBounds(95, 51, 330, 20);
		contentPanel.add(textField);
		
		JButton button = new JButton("Agregar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (textField.getText().equals("")){
					setValidez(0);
				}else{
					setValidez(Integer.parseInt(textField.getText()));
				}
				setVisible(false);
			}
		});
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button.setBackground(tema.getAzul());
			}
		});
		button.setIcon(new ImageIcon(ruta+"/images/add.png"));
		button.setForeground(Color.WHITE);
		button.setBorderPainted(false);
		button.setBackground(tema.getAzul());
		button.setBounds(83, 92, 120, 36);
		contentPanel.add(button);
		
		JButton button_1 = new JButton("Volver");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setValidez(0);
				setVisible(false);
			}
		});
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getAzul());
			}
		});
		button_1.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(tema.getAzul());
		button_1.setBounds(261, 92, 120, 36);
		contentPanel.add(button_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 442, 135);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "INGRESE D�AS DE VALID�Z", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		contentPanel.add(lblNewLabel);
	}
}
