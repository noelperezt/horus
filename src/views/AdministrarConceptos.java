package views;

import java.awt.Dimension;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import models.Concepto;
import models.Usuario;
import utility.EstiloHorus;
import utility.Tabla;

import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdministrarConceptos {

	public JFrame frmAdministrarConceptosFacturables;
	private Tabla listaconceptos; 
	private Usuario user;
	private JTextField textField;

	public AdministrarConceptos(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		String ruta = new File ("").getAbsolutePath ();
		frmAdministrarConceptosFacturables = new JFrame();
		frmAdministrarConceptosFacturables.setResizable(false);
		frmAdministrarConceptosFacturables.setTitle("Administrar Conceptos Facturables");
		frmAdministrarConceptosFacturables.setBounds(100, 100, 693, 307);
		frmAdministrarConceptosFacturables.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAdministrarConceptosFacturables.getContentPane().setLayout(null);
		frmAdministrarConceptosFacturables.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarConceptosFacturables.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAdministrarConceptosFacturables.getSize();  
        frmAdministrarConceptosFacturables.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
		EstiloHorus tema = new EstiloHorus();
        
		String campos[] = {"C�DIGO","CONCEPTO","BANCO","PRECIO","% DCTO","ESTADO"};
		int ancho[] = {1,100,90,40,1,1};
		int editable[] = null;
		Concepto Oconcepto = new Concepto();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(31, 80, 491, 168);
		listaconceptos = new Tabla(campos, editable, ancho, null);
		listaconceptos.Listar(Oconcepto.ListarConceptos());
		Oconcepto.desconectar();
		frmAdministrarConceptosFacturables.getContentPane().add(scrollPane);
		scrollPane.setViewportView(listaconceptos);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				listaconceptos.setClearGrid();
				try {
					listaconceptos.Refrescar(Oconcepto.FiltraConcepto(textField.getText()));
					Oconcepto.desconectar();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		textField.setToolTipText("Busqueda por Concepto");
		textField.setColumns(10);
		textField.setBounds(92, 41, 346, 20);
		frmAdministrarConceptosFacturables.getContentPane().add(textField);
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(31, 44, 56, 14);
		frmAdministrarConceptosFacturables.getContentPane().add(label);
		
		JButton button = new JButton("Agregar");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button.setBackground(tema.getAzul());
			}
		});
		button.setForeground(Color.WHITE);
		button.setBackground(tema.getAzul());
		button.setBorderPainted(false);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					RegistroConcepto ventana = new RegistroConcepto("0", user);
					ventana.frmRegistroDeConceptos.setVisible(true);
					frmAdministrarConceptosFacturables.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button.setIcon(new ImageIcon(ruta+"/images/add.png"));
		button.setBounds(532, 83, 125, 33);
		frmAdministrarConceptosFacturables.getContentPane().add(button);
		
		JButton button_1 = new JButton("Editar");
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getAzul());
			}
		});
		button_1.setForeground(Color.WHITE);
		button_1.setBackground(tema.getAzul());
		button_1.setBorderPainted(false);
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listaconceptos.getSelectedRow() != -1){
					String id = listaconceptos.getValueAt(listaconceptos.getSelectedRow(), 0).toString();
					try {
						RegistroConcepto ventana = new RegistroConcepto(id, user);
						ventana.frmRegistroDeConceptos.setVisible(true);
						frmAdministrarConceptosFacturables.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un Concepto para Editar","Advertencia" , JOptionPane.WARNING_MESSAGE,icono); 
				}
			}
		});
		button_1.setIcon(new ImageIcon(ruta+"/images/edit.png"));
		button_1.setBounds(532, 127, 125, 33);
		frmAdministrarConceptosFacturables.getContentPane().add(button_1);
		
		JButton button_2 = new JButton("Estado");
		button_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseExited(MouseEvent e) {
				button_2.setBackground(tema.getAzul());
			}
			@Override
			public void mouseEntered(MouseEvent e) {
				button_2.setBackground(tema.getGris());
			}
		});
		button_2.setForeground(Color.WHITE);
		button_2.setBackground(tema.getAzul());
		button_2.setBorderPainted(false);
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listaconceptos.getSelectedRow() != -1){
					String id = listaconceptos.getValueAt(listaconceptos.getSelectedRow(), 0).toString();
					try {
						listaconceptos.CambioEstadoConcepto(id, listaconceptos.getSelectedRow(), 5);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un Concepto para cambiar su Estado","Advertencia" , JOptionPane.WARNING_MESSAGE,icono); 
				}
			}
		});
		button_2.setIcon(new ImageIcon(ruta+"/images/del.png"));
		button_2.setToolTipText("Cambiar Estado");
		button_2.setBounds(532, 171, 125, 33);
		frmAdministrarConceptosFacturables.getContentPane().add(button_2);
		
		JButton button_3 = new JButton("Volver");
		button_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_3.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_3.setBackground(tema.getAzul());
			}
		});
		button_3.setForeground(Color.WHITE);
		button_3.setBackground(tema.getAzul());
		button_3.setBorderPainted(false);
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarConceptosFacturables.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		button_3.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button_3.setBounds(532, 215, 125, 33);
		frmAdministrarConceptosFacturables.getContentPane().add(button_3);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 668, 258);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ADMINISTRAR CONCEPTOS FACTURABLES", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAdministrarConceptosFacturables.getContentPane().add(lblNewLabel);
	}

}
