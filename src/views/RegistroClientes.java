package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.sql.SQLException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.JTextField;

import models.Cliente;
import models.Usuario;
import utility.EstiloHorus;

import javax.swing.JTextArea;
import javax.swing.JScrollPane;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;

public class RegistroClientes {

	public JFrame RegistroClientes;
	private JTextField razon;
	private JTextField rif;
	private JTextField telefono;
	private JTextField correo;
	private JTextField representante;
	private JTextField telefono_representante;
	private JTextField correo_representante;
	private JTextArea direccion;
	private int cliente;
	private Usuario user;
	String ruta = new File ("").getAbsolutePath ();
	
	public RegistroClientes(int cliente,Usuario user) throws SQLException{
		this.user = user;
		this.cliente = cliente;
		initialize();
	}

	public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
		String cadena= (jTextfieldS.getText()).toUpperCase();
		jTextfieldS.setText(cadena);
	}
	
	public void Ejecutar() throws SQLException{
		if(rif.getText().equals("") || razon.getText().equals("") || direccion.getText().equals("") || telefono.getText().equals("") || correo.getText().equals("")){
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
		}else{
			Pattern pat = Pattern.compile("[VEGJ]{1}[0-9]{7,9}"); // Valida formato del rif
		    Matcher mat = pat.matcher(rif.getText());   
			if(mat.find()){
				pat = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"); // Valida formato del correo
		        mat = pat.matcher(correo.getText());
		       if(mat.find()){
		    	   pat = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"); // Valida formato del correo del representante
			       mat = pat.matcher(correo_representante.getText());	
			       if(mat.find() || correo_representante.getText().equals("")){
			    	    boolean status = false;
			    	    Cliente Ocliente = new Cliente(rif.getText(), razon.getText(), direccion.getText(), telefono.getText(), correo.getText(), representante.getText(), telefono_representante.getText(), correo_representante.getText());
			    	    if (cliente == 0){
			    		   status = Ocliente.Registrar();
			    	    }else{
			    		   status = Ocliente.Editar(cliente);
				   	    }
			   			if (status == true){
			   				rif.setText("");
	   						razon.setText("");
	   						telefono.setText("");
	   						correo.setText("");
	   						direccion.setText("");
	   						representante.setText("");
	   						telefono_representante.setText("");
	   						correo_representante.setText("");
	   						AdministrarClientes ventana = new AdministrarClientes(user);
	   						ventana.frmAdministrarClientes.setVisible(true);
	   						RegistroClientes.dispose();
			   			}
			       }else{
			    	   Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			    	   JOptionPane.showMessageDialog(null, "El formato de correo es inv�lido, verifique que lo ingres� de forma correcta. Ejemplo: Prueba@Proveedor.com","Error" , JOptionPane.ERROR_MESSAGE,icono); 
			       }
		       }else{
		    	   Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
		    	   JOptionPane.showMessageDialog(null, "El formato de correo es inv�lido, verifique que lo ingres� de forma correcta. Ejemplo: Prueba@Proveedor.com","Error" , JOptionPane.ERROR_MESSAGE, icono);
		     }
			}else{
				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
				JOptionPane.showMessageDialog(null, "El formato de rif es inv�lido, verifique que lo ingres� de forma correcta. Ejemplo: J000000001","Error" , JOptionPane.ERROR_MESSAGE, icono);
			}
		}
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		RegistroClientes = new JFrame();
		RegistroClientes.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		RegistroClientes.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					RegistroClientes.dispose();
					AdministrarClientes ventana = new AdministrarClientes(user);
					ventana.frmAdministrarClientes.setVisible(true);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		RegistroClientes.setResizable(false);
		if(cliente == 0){
			RegistroClientes.setTitle("Registro de Clientes");
		}else{
			RegistroClientes.setTitle("Modificar Cliente");
		}
		
		Cliente Ocliente = new Cliente();
		Ocliente.TraerRegistro(cliente);
		
		RegistroClientes.setBounds(100, 100, 511, 521);
		RegistroClientes.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		RegistroClientes.getContentPane().setLayout(null);

		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = RegistroClientes.getSize();  
        RegistroClientes.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
		EstiloHorus tema = new EstiloHorus();
		
		JLabel label = new JLabel("DATOS DE LA EMPRESA");
		label.setOpaque(true);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(Color.WHITE);
		label.setFont(new Font("Tahoma", Font.BOLD, 11));
		label.setBackground(new Color(76, 76, 76));
		label.setBounds(43, 40, 418, 20);
		RegistroClientes.getContentPane().add(label);
		
		JLabel lblRaznSocial = new JLabel("RAZ\u00D3N SOCIAL:");
		lblRaznSocial.setFont(new Font("Arial", Font.BOLD, 11));
		lblRaznSocial.setBounds(43, 109, 89, 14);
		RegistroClientes.getContentPane().add(lblRaznSocial);
		
		razon = new JTextField();
		razon.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				razon.nextFocus();
			}
		});
		razon.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
				if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE ) {
			         evt.consume();
			    }
			}
		});
		razon.setColumns(10);
		razon.setBounds(139, 106, 322, 20);
		razon.setText(Ocliente.getRazonSocial());
		RegistroClientes.getContentPane().add(razon);
		
		JLabel lblRif = new JLabel("RIF/CI:");
		lblRif.setFont(new Font("Arial", Font.BOLD, 11));
		lblRif.setBounds(95, 78, 39, 14);
		RegistroClientes.getContentPane().add(lblRif);
		
		rif = new JTextField();
		rif.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				rif.nextFocus();
			}
		});
		rif.addFocusListener(new FocusAdapter() {
		 @Override
			public void focusLost(FocusEvent arg0) {
				if(cliente == 0){
					Cliente Ocliente = new Cliente();
					try {
						if (Ocliente.ValidarRif(rif.getText()) == true){
							Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
							JOptionPane.showMessageDialog(null, "El rif del cliente ya est� registrado","Error" , JOptionPane.ERROR_MESSAGE, icono);
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		rif.setToolTipText("Ejemplo: J000000001");
		rif.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') ||(caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && caracter != 'J' && caracter != 'V' && caracter != 'G' && caracter != 'E' && caracter != 'j' && caracter != 'v' && caracter != 'g' && caracter != 'e') || rif.getText().length()== 10){
		            evt.consume();
		        }
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				convertiraMayusculasEnJtextfield(rif);
			}
		});
		rif.setColumns(10);
		rif.setText(Ocliente.getRif());
		rif.setBounds(139, 75, 322, 20);
		RegistroClientes.getContentPane().add(rif);
		
		
		JLabel lblDireccion = new JLabel("DIRECCI\u00D3N:");
		lblDireccion.setFont(new Font("Arial", Font.BOLD, 11));
		lblDireccion.setBounds(68, 143, 61, 14);
		RegistroClientes.getContentPane().add(lblDireccion);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(139, 137, 322, 71);
		RegistroClientes.getContentPane().add(scrollPane);
		
		direccion = new JTextArea(4,50);
		direccion.setLineWrap(true); 
		direccion.setWrapStyleWord(true); 
		direccion.addKeyListener(new KeyAdapter() {
			@SuppressWarnings({ "static-access", "deprecation" })
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		            direccion.nextFocus();
		        }	
			}
		});
		direccion.setText(Ocliente.getDireccion());
		scrollPane.setViewportView(direccion);
		
		JLabel lblTelfono = new JLabel("TEL\u00C9FONO:");
		lblTelfono.setFont(new Font("Arial", Font.BOLD, 11));
		lblTelfono.setBounds(69, 222, 61, 14);
		RegistroClientes.getContentPane().add(lblTelfono);
		
		telefono = new JTextField();
		telefono.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				telefono.nextFocus();
			}
		});
		telefono.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || telefono.getText().length() == 11 ){
		            evt.consume();
		        }
			}
		});
		telefono.setColumns(10);
		telefono.setBounds(139, 219, 322, 20);
		telefono.setText(Ocliente.getTelefono());
		RegistroClientes.getContentPane().add(telefono);
		
		JLabel lblCorreoElectrnico = new JLabel("CORREO:");
		lblCorreoElectrnico.setFont(new Font("Arial", Font.BOLD, 11));
		lblCorreoElectrnico.setBounds(80, 253, 52, 14);
		RegistroClientes.getContentPane().add(lblCorreoElectrnico);
		
		correo = new JTextField();
		correo.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				correo.nextFocus();
			}
		});
		correo.setToolTipText("Ejemplo: prueba@proveedor.com");
		correo.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
			    if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE && caracter != '.' && caracter != '@' && caracter != '_' && caracter != '-')
			    {
			         evt.consume();
			    }
			}
		});
		correo.setColumns(10);
		correo.setBounds(139, 250, 322, 20);
		correo.setText(Ocliente.getCorreo());
		RegistroClientes.getContentPane().add(correo);
		
		String nombre_boton = "";
		if (cliente == 0){
			nombre_boton = "Registrar";
		}else{
			nombre_boton = "Guardar";
		}
		
		JButton registrar = new JButton(nombre_boton);
		registrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				registrar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				registrar.setBackground(tema.getAzul());
			}
		});
		registrar.setBackground(tema.getAzul());
		registrar.setBorderPainted(false);
		registrar.setForeground(Color.WHITE);
		registrar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		            try {
						Ejecutar();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
			}
		});
		registrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Ejecutar();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		JLabel lblNombreDelRepresentante = new JLabel("NOMBRE:");
		lblNombreDelRepresentante.setFont(new Font("Arial", Font.BOLD, 11));
		lblNombreDelRepresentante.setBounds(77, 323, 52, 14);
		RegistroClientes.getContentPane().add(lblNombreDelRepresentante);
		
		representante = new JTextField();
		representante.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
			    if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE){
			         evt.consume();
			    }
			}
		});
		representante.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				representante.nextFocus();
			}
		});
		representante.setColumns(10);
		representante.setBounds(138, 320, 322, 20);
		representante.setText(Ocliente.getRepresentante());
		RegistroClientes.getContentPane().add(representante);
		
		JLabel lblTelfono_1 = new JLabel("TEL\u00C9FONO:");
		lblTelfono_1.setFont(new Font("Arial", Font.BOLD, 11));
		lblTelfono_1.setBounds(68, 354, 66, 14);
		RegistroClientes.getContentPane().add(lblTelfono_1);
		
		telefono_representante = new JTextField();
		telefono_representante.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || telefono_representante.getText().length() == 11 ){
		            evt.consume();
		        }
			}
		});
		telefono_representante.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				telefono_representante.nextFocus();
			}
		});
		telefono_representante.setColumns(10);
		telefono_representante.setBounds(138, 351, 322, 20);
		telefono_representante.setText(Ocliente.getTelefonoRepresentante());
		RegistroClientes.getContentPane().add(telefono_representante);
		
		JLabel lblCorreo = new JLabel("CORREO:");
		lblCorreo.setFont(new Font("Arial", Font.BOLD, 11));
		lblCorreo.setBounds(78, 388, 52, 14);
		RegistroClientes.getContentPane().add(lblCorreo);
		
		correo_representante = new JTextField();
		correo_representante.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				correo_representante.nextFocus();
			}
		});
		correo_representante.setToolTipText("Ejemplo: prueba@proveedor.com");
		correo_representante.setColumns(10);
		correo_representante.setBounds(138, 385, 322, 20);
		correo_representante.setText(Ocliente.getCorreoRepresentante());
		RegistroClientes.getContentPane().add(correo_representante);
		registrar.setBounds(103, 426, 120, 36);
		if(cliente == 0){
			registrar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		}else{
			registrar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		}
		RegistroClientes.getContentPane().add(registrar);
		
		JButton cancelar = new JButton("Volver");
		cancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				cancelar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				cancelar.setBackground(tema.getAzul());
			}
		});
		cancelar.setBorderPainted(false);
		cancelar.setBackground(tema.getAzul());
		cancelar.setForeground(Color.WHITE);
		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					AdministrarClientes ventana = new AdministrarClientes(user);
					ventana.frmAdministrarClientes.setVisible(true);
					RegistroClientes.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		cancelar.setBounds(281, 426, 120, 36);
		cancelar.setIcon(new ImageIcon(ruta+"/images/back.png"));
		RegistroClientes.getContentPane().add(cancelar);
		String titulo;
		if (cliente == 0){
			titulo = "REGISTRO DE CLIENTES";
		}else{
			titulo = "MODIFICAR CLIENTE";
		}

		JLabel borde = new JLabel("");
		borde.setBounds(10, 11, 482, 471);
		borde.setBorder(javax.swing.BorderFactory.createTitledBorder(null, titulo, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		RegistroClientes.getContentPane().add(borde);
		
		JLabel lblDatosDelRepresentante = new JLabel("DATOS DEL REPRESENTANTE");
		lblDatosDelRepresentante.setOpaque(true);
		lblDatosDelRepresentante.setHorizontalAlignment(SwingConstants.CENTER);
		lblDatosDelRepresentante.setForeground(Color.WHITE);
		lblDatosDelRepresentante.setFont(new Font("Tahoma", Font.BOLD, 11));
		lblDatosDelRepresentante.setBackground(new Color(76, 76, 76));
		lblDatosDelRepresentante.setBounds(43, 286, 418, 20);
		RegistroClientes.getContentPane().add(lblDatosDelRepresentante);
	}
}
