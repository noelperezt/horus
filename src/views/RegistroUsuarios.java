package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JTextField;

import models.Usuario;
import utility.EstiloHorus;

import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.sql.SQLException;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class RegistroUsuarios {

	public JFrame frmRegistroDeUsuarios;
	private JTextField usuario;
	private JPasswordField clave;
	private JPasswordField confirmacion;
	private JTextField nombre;
	private Usuario user;
	String ruta = new File ("").getAbsolutePath ();
	
	public RegistroUsuarios(Usuario user) {
		this.user = user;
		initialize();
	}
	
	@SuppressWarnings("deprecation")
	public void Ejecutar(){
		if(nombre.getText().equals("") || usuario.getText().equals("") || clave.getText().equals("") || confirmacion.getText().equals("")){
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE,icono);
		}else{
			Usuario Ouser = new Usuario(nombre.getText(), usuario.getText(), clave.getText());
			if(Ouser.ClavesIguales(clave.getText(), confirmacion.getText())){
				try {
					if(Ouser.Registrar()){
						nombre.setText("");
						usuario.setText("");
						clave.setText("");
						confirmacion.setText("");
						AdministrarUsuarios ventana = new AdministrarUsuarios(user);
						ventana.frmAdministrarUsuarios.setVisible(true);
						frmRegistroDeUsuarios.dispose();
					}
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}else{
				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
				JOptionPane.showMessageDialog(null, "Las Contraseņas no Coinciden","Error" , JOptionPane.ERROR_MESSAGE, icono);
			}
		}
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmRegistroDeUsuarios = new JFrame();
		frmRegistroDeUsuarios.setTitle("Registro de Usuarios");
		frmRegistroDeUsuarios.setBounds(100, 100, 435, 255);
		frmRegistroDeUsuarios.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmRegistroDeUsuarios.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmRegistroDeUsuarios.getContentPane().setLayout(null);
		frmRegistroDeUsuarios.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					AdministrarUsuarios ventana = new AdministrarUsuarios(user);
					ventana.frmAdministrarUsuarios.setVisible(true);
					frmRegistroDeUsuarios.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});

		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmRegistroDeUsuarios.getSize();  
        frmRegistroDeUsuarios.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
		EstiloHorus tema = new EstiloHorus();
        
		nombre = new JTextField();
		nombre.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				nombre.nextFocus();
			}
		});
		nombre.setBounds(127, 32, 268, 20);
		frmRegistroDeUsuarios.getContentPane().add(nombre);
		nombre.setColumns(10);
		
		usuario = new JTextField();
		usuario.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				usuario.nextFocus();
			}
		});
		usuario.setBounds(127, 63, 268, 20);
		frmRegistroDeUsuarios.getContentPane().add(usuario);
		usuario.setColumns(10);
		
		clave = new JPasswordField();
		clave.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				clave.nextFocus();
			}
		});
		clave.setBounds(127, 94, 268, 20);
		frmRegistroDeUsuarios.getContentPane().add(clave);
		
		confirmacion = new JPasswordField();
		confirmacion.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				confirmacion.nextFocus();
			}
		});
		confirmacion.setBounds(127, 125, 268, 20);
		frmRegistroDeUsuarios.getContentPane().add(confirmacion);
		
		JLabel usuario_usuario = new JLabel("USUARIO:");
		usuario_usuario.setFont(new Font("Arial", Font.BOLD, 11));
		usuario_usuario.setBounds(68, 65, 55, 17);
		frmRegistroDeUsuarios.getContentPane().add(usuario_usuario);
		
		JLabel usurio_nombre = new JLabel("NOMBRE:");
		usurio_nombre.setFont(new Font("Arial", Font.BOLD, 11));
		usurio_nombre.setBounds(71, 35, 55, 14);
		frmRegistroDeUsuarios.getContentPane().add(usurio_nombre);
		
		JLabel usuario_clave = new JLabel("CONTRASE\u00D1A:");
		usuario_clave.setFont(new Font("Arial", Font.BOLD, 11));
		usuario_clave.setBounds(41, 97, 76, 14);
		frmRegistroDeUsuarios.getContentPane().add(usuario_clave);
		
		JLabel usuario_confirmacion = new JLabel("CONFIRMACI\u00D3N:");
		usuario_confirmacion.setFont(new Font("Arial", Font.BOLD, 11));
		usuario_confirmacion.setBounds(31, 128, 91, 14);
		frmRegistroDeUsuarios.getContentPane().add(usuario_confirmacion);
		
		JButton cancelar = new JButton("Volver");
		cancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				cancelar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				cancelar.setBackground(tema.getAzul());
			}
		});
		cancelar.setBackground(tema.getAzul());
		cancelar.setForeground(Color.WHITE);
		cancelar.setBorderPainted(false);
		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					AdministrarUsuarios ventana = new AdministrarUsuarios(user);
					ventana.frmAdministrarUsuarios.setVisible(true);
					frmRegistroDeUsuarios.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		cancelar.setBounds(238, 159, 120, 36);
		cancelar.setIcon(new ImageIcon(ruta+"/images/back.png"));
		frmRegistroDeUsuarios.getContentPane().add(cancelar);
		
		JButton registrar = new JButton("Registrar");
		registrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				registrar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				registrar.setBackground(tema.getAzul());
			}
		});
		registrar.setBackground(tema.getAzul());
		registrar.setForeground(Color.WHITE);
		registrar.setBorderPainted(false);
		registrar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		        	Ejecutar();
		        }
			}
		});
		registrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Ejecutar();	
			}
		});
		registrar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		registrar.setBounds(82, 159, 120, 36);
		frmRegistroDeUsuarios.getContentPane().add(registrar);
		
		JLabel borde = new JLabel("");
		borde.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "REGISTRO DE USUARIOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		borde.setBounds(10, 4, 408, 208);
		frmRegistroDeUsuarios.getContentPane().add(borde);
	}
}
