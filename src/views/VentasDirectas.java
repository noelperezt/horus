package views;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import utility.AbstractJasperReports;
import utility.EstiloHorus;
import utility.Tabla;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import models.Cliente;
import models.Factura;
import models.Presupuesto;
import models.Usuario;
import net.sf.jasperreports.engine.JRException;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import javax.swing.JFormattedTextField;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class VentasDirectas {
	
	public JFrame descuento;
	private JSlider slider;
	private JTextField desc;
	public JFrame frmVentasDirectas;
	private Tabla listaconcepto;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextArea textArea;
	private JButton btnAgregar;
	private JFormattedTextField subtotal;
	private JFormattedTextField iva;
	private JFormattedTextField total;
	private int volver;
	private Usuario user;
	private int presupuesto;
	private int retorno;
	double monto_sub;
	double monto_iva;
	double monto_total;

	public void setVolver(int volver){
		this.volver = volver;
	}
	
	public int getVolver(){
		return volver;
	}
	
	public VentasDirectas(Usuario user, int presupuesto, int retorno) throws SQLException {
		this.presupuesto = presupuesto;
		this.retorno = retorno;
		this.user = user;
		initialize();
	}

	public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
		String cadena= (jTextfieldS.getText()).toUpperCase();
		jTextfieldS.setText(cadena);
	}
	
	public void Totales() throws SQLException{
		Double total = 0.0;
		int descuento = 0;
		for (int i = 0; i < listaconcepto.getRowCount(); i++){
			total = total + Double.parseDouble(listaconcepto.getValueAt(i, 4).toString());
			descuento = Integer.parseInt(listaconcepto.getValueAt(i, 5).toString().toString());
			total = total - ((total * descuento)/100);
		}
		Factura Ofactura = new Factura();
		monto_sub = total;
		monto_iva = (monto_sub * Ofactura.retornaIva())/100;
		monto_total = monto_sub + monto_iva;
	}
	
	private void Actualizar(){
		subtotal.setValue(monto_sub);
		iva.setValue(monto_iva);
		total.setValue(monto_total);
	}
	
	public void setPreupuesto(int presupuesto){
		this.presupuesto = presupuesto;
	}
	
	public void Presupuestar() throws SQLException{
		Presupuesto Opresupuesto = new Presupuesto();
		Opresupuesto.TraerRegistro(Opresupuesto.RetornaIdCliente(""+presupuesto));
		textField.setText(Opresupuesto.getRif());
		textField_1.setText(Opresupuesto.getRazonSocial());
		textField_2.setText(Opresupuesto.getTelefono());
		textField_3.setText(Opresupuesto.getCorreo());
		textArea.setText(Opresupuesto.getDireccion());
		textField_1.setEnabled(false);
		textField_2.setEnabled(false);
		textField_3.setEnabled(false);
		textArea.setEnabled(false);
		listaconcepto.Listar(Opresupuesto.ListarPresupuestosDetalle(presupuesto));
		Opresupuesto.Desconectar();
		Totales();
		Actualizar();
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		
		String ruta = new File ("").getAbsolutePath ();
		frmVentasDirectas = new JFrame();
		frmVentasDirectas.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmVentasDirectas.setTitle("Ventas Directas");
		frmVentasDirectas.setResizable(false);
		frmVentasDirectas.setBounds(100, 100, 780, 533);
		frmVentasDirectas.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmVentasDirectas.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				if(retorno == 1){
					try {
						Principal ventana = new Principal(user);
						ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
						frmVentasDirectas.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}else{
					try {
						AdministrarPresupuestos ventana2 = new AdministrarPresupuestos(user);
						ventana2.frmAdministrarFacturas.setVisible(true);
						frmVentasDirectas.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		frmVentasDirectas.getContentPane().setLayout(null);
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmVentasDirectas.getSize();  
        frmVentasDirectas.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
        EstiloHorus tema = new EstiloHorus();
        
		JLabel lblN = new JLabel("N\u00BA:");
		lblN.setFont(new Font("Arial", Font.BOLD, 18));
		lblN.setBounds(642, 36, 31, 14);
		frmVentasDirectas.getContentPane().add(lblN);
		
		
		Factura Ofactura = new Factura();
		Ofactura.NroFactura();
		String nro = Ofactura.getNro();
		JLabel label_1 = new JLabel(nro); // <------------- Nro de la Factura
		label_1.setFont(new Font("Arial", Font.BOLD, 18));
		label_1.setBounds(672, 36, 76, 14);
		frmVentasDirectas.getContentPane().add(label_1);
		
		JLabel label = new JLabel("RIF/CI:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(93, 83, 39, 14);
		frmVentasDirectas.getContentPane().add(label);
		
		Cliente Ocliente = new Cliente();
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.transferFocus();
				Presupuesto Opresupuesto = new Presupuesto();
				try {
					if(Opresupuesto.PresupuestosAsociados(Ocliente.RetornaID(textField.getText()))){
						Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
						if(JOptionPane.showConfirmDialog(null, "El cliente tiene presupuesto vigente asociado �Desea mostrar la lista?","Presupuesto",1,0,icono)==0){
					       ListaPresupuesto ventana;
						try {
							ventana = new ListaPresupuesto(Ocliente.RetornaID(textField.getText()));
							ventana.setLocationRelativeTo(frmVentasDirectas);
					           ventana.setVisible(true);
					           if(ventana.getNro() > 0){
					        	   setPreupuesto(ventana.getNro());
					        	   Presupuestar();
					           }
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}      
					    }
					}
				} catch (HeadlessException | SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		textField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				try {
					if(Ocliente.ValidarRif(textField.getText())){
						Ocliente.TraerRegistro(Ocliente.RetornaID(textField.getText()));
						textField_1.setText(Ocliente.getRazonSocial());
						textField_2.setText(Ocliente.getTelefono());
						textField_3.setText(Ocliente.getCorreo());
						textArea.setText(Ocliente.getDireccion());
						textField_1.setEnabled(false);
						textField_2.setEnabled(false);
						textField_3.setEnabled(false);
						textArea.setEnabled(false);
					}else{
						if (textField.getText().equals("")){
							textField_1.setText("");
							textField_2.setText("");
							textField_3.setText("");
							textArea.setText("");
							textField_1.setEnabled(false);
							textField_2.setEnabled(false);
							textField_3.setEnabled(false);
							textArea.setEnabled(false);
						}else{
							textField_1.setText("");
							textField_2.setText("");
							textField_3.setText("");
							textArea.setText("");
							Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
							if(JOptionPane.showConfirmDialog(null, "El rif no se encuentra registrado �Desea ingresar los datos del Cliente","Registro de Clientes",1,0,icono)==0){
								textField_1.setEnabled(true);
								textField_2.setEnabled(true);
								textField_3.setEnabled(true);
								textArea.setEnabled(true);
								textField_1.requestFocus();
					        }	
						}
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		textField.setToolTipText("Ejemplo: J000000001");
		textField.setText((String) null);
		textField.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') ||(caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && caracter != 'J' && caracter != 'V' && caracter != 'G' && caracter != 'E' && caracter != 'j' && caracter != 'v' && caracter != 'g' && caracter != 'e') || textField.getText().length()== 10){
		            evt.consume();
		        }
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				convertiraMayusculasEnJtextfield(textField);
			}
		});
		textField.setColumns(10);
		textField.setBounds(133, 80, 199, 20);
		frmVentasDirectas.getContentPane().add(textField);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListaClientes lista = null;
				try {
					lista = new ListaClientes();
					lista.setLocationRelativeTo(frmVentasDirectas);
					lista.setVisible(true);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(!(lista.getRif().equals(""))){
					textField.setText(lista.getRif());
					try {
						Presupuesto Opresupuesto = new Presupuesto(); 
						Ocliente.TraerRegistro(Ocliente.RetornaID(textField.getText()));
						textField_1.setText(Ocliente.getRazonSocial());
						textField_2.setText(Ocliente.getTelefono());
						textField_3.setText(Ocliente.getCorreo());
						textArea.setText(Ocliente.getDireccion());
						textField_1.setEnabled(false);
						textField_2.setEnabled(false);
						textField_3.setEnabled(false);
						textArea.setEnabled(false);
						btnAgregar.requestFocus();
						if(Opresupuesto.PresupuestosAsociados(Ocliente.RetornaID(textField.getText()))){
							Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
							if(JOptionPane.showConfirmDialog(null, "El cliente tiene presupuesto vigente asociado �Desea mostrar la lista?","Presupuesto",1,0,icono)==0){
					           ListaPresupuesto ventana = new ListaPresupuesto(Ocliente.RetornaID(textField.getText()));
					           ventana.setLocationRelativeTo(frmVentasDirectas);
					           ventana.setVisible(true);
					           if(ventana.getNro() > 0){
					        	   setPreupuesto(ventana.getNro());
					        	   Presupuestar();
					           }
					        }
						}
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}  
		});
		btnNewButton.setBorderPainted(false);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.setBackground(tema.getAzul());
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/pendiente1.png"));
		btnNewButton.setBounds(340, 80, 52, 20);
		frmVentasDirectas.getContentPane().add(btnNewButton);
		
		JLabel label_2 = new JLabel("RAZ\u00D3N SOCIAL:");
		label_2.setFont(new Font("Arial", Font.BOLD, 11));
		label_2.setBounds(43, 109, 89, 14);
		frmVentasDirectas.getContentPane().add(label_2);
		
		textField_1 = new JTextField();
		textField_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_2.requestFocus();
			}
		});
		textField_1.setText(Ocliente.getRazonSocial());
		textField_1.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
				if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE ) {
			         evt.consume();
			    }
			}
		});
		textField_1.setEnabled(false);
		textField_1.setText((String) null);
		textField_1.setColumns(10);
		textField_1.setBounds(133, 105, 259, 20);
		frmVentasDirectas.getContentPane().add(textField_1);
		
		JLabel label_3 = new JLabel("TEL\u00C9FONO:");
		label_3.setFont(new Font("Arial", Font.BOLD, 11));
		label_3.setBounds(70, 134, 61, 14);
		frmVentasDirectas.getContentPane().add(label_3);
		
		textField_2 = new JTextField();
		textField_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_3.requestFocus();
			}
		});
		textField_2.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || textField_2.getText().length() == 11 ){
		            evt.consume();
		        }
			}
		});
		textField_2.setEnabled(false);
		textField_2.setText(Ocliente.getTelefono());
		textField_2.setText((String) null);
		textField_2.setColumns(10);
		textField_2.setBounds(133, 132, 259, 20);
		frmVentasDirectas.getContentPane().add(textField_2);
		
		JLabel label_4 = new JLabel("CORREO:");
		label_4.setFont(new Font("Arial", Font.BOLD, 11));
		label_4.setBounds(427, 83, 52, 14);
		frmVentasDirectas.getContentPane().add(label_4);
		
		textField_3 = new JTextField();
		textField_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.requestFocus();
			}
		});
		textField_3.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
			    if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE && caracter != '.' && caracter != '@' && caracter != '_' && caracter != '-')
			    {
			         evt.consume();
			    }
			}
		});
		textField_3.setEnabled(false);
		textField_3.setText(Ocliente.getCorreo());
		textField_3.setText((String) null);
		textField_3.setColumns(10);
		textField_3.setBounds(481, 80, 244, 20);
		frmVentasDirectas.getContentPane().add(textField_3);
		
		JLabel label_5 = new JLabel("DIRECCI\u00D3N:");
		label_5.setFont(new Font("Arial", Font.BOLD, 11));
		label_5.setBounds(414, 109, 61, 14);
		frmVentasDirectas.getContentPane().add(label_5);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(481, 105, 244, 43);
		frmVentasDirectas.getContentPane().add(scrollPane);
		
		textArea = new JTextArea();
		textArea.setEnabled(false);
		textArea.setText(Ocliente.getDireccion());
		scrollPane.setViewportView(textArea);
		
		JLabel lblNewLabel_2 = new JLabel("LISTA DE CONCEPTOS");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		Color gris = new Color(76, 76, 76);
		lblNewLabel_2.setBackground(gris);
		lblNewLabel_2.setOpaque(true);
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(26, 177, 723, 20);
		frmVentasDirectas.getContentPane().add(lblNewLabel_2);
		
		String campos[] = {"C�DIGO","CANT.","DESCRIPCION","P. UNITARIO","TOTAL","% DCTO"};
		int ancho[] = {20,10,200,30,30,30};
		int editable[] = null;
		@SuppressWarnings("rawtypes")
		Class[] tipos = new Class[] {java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Double.class, java.lang.Double.class, java.lang.Integer.class};
		listaconcepto = new Tabla(campos, editable, ancho,tipos);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(24, 207, 584, 170);  
		frmVentasDirectas.getContentPane().add(scrollPane_1);
		scrollPane_1.setViewportView(listaconcepto);	
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListaConceptos lista = null;
				try {
					lista = new ListaConceptos();
					lista.setLocationRelativeTo(frmVentasDirectas);
					lista.setVisible(true);
					if(lista.getFila() != null){
						Object[] vector = new Object[6];
						vector[0] = lista.getFila()[0];
						vector[1] = lista.getFila()[1];
						vector[2] = lista.getFila()[2];
						vector[3] = lista.getFila()[3];
						vector[4] = lista.getFila()[4];
						vector[5] = 0;
						listaconcepto.addRow(vector);
					}
					Totales();
					Actualizar();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnAgregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnAgregar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnAgregar.setBackground(tema.getAzul());
			}
		});
		btnAgregar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		btnAgregar.setForeground(Color.WHITE);
		btnAgregar.setBorderPainted(false);
		btnAgregar.setBackground(new Color(13, 86, 180));
		btnAgregar.setBounds(618, 208, 131, 35);
		frmVentasDirectas.getContentPane().add(btnAgregar);
		
		JButton button_1 = new JButton("Borrar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listaconcepto.getSelectedRow() != -1){
					listaconcepto.deleteRow(listaconcepto.getSelectedRow());
					try {
						Totales();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					Actualizar();
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un item para Borrar","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}	
			}
		});
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getAzul());
			}
		});
		
		JButton button_3 = new JButton("Descuento");
		button_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button_3.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_3.setBackground(tema.getAzul());
			}
		});
		button_3.setIcon(new ImageIcon(ruta+"/images/descuento.png"));
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listaconcepto.getSelectedRow() != -1){
					desc.setText(listaconcepto.getValueAt(listaconcepto.getSelectedRow(),5)+"");
					slider.setValue(Integer.parseInt(listaconcepto.getValueAt(listaconcepto.getSelectedRow(),5).toString()));
					descuento.setVisible(true);
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un item para Aplicar Descuento","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		button_3.setForeground(Color.WHITE);
		button_3.setBorderPainted(false);
		button_3.setBackground(new Color(13, 86, 180));
		button_3.setBounds(618, 253, 131, 35);
		frmVentasDirectas.getContentPane().add(button_3);
		button_1.setIcon(new ImageIcon(ruta+"/images/del.png"));
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(new Color(13, 86, 180));
		button_1.setBounds(618, 298, 131, 35);
		frmVentasDirectas.getContentPane().add(button_1);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listaconcepto.setClearGrid();
				try {
					Totales();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Actualizar();
			}
		});
		btnLimpiar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnLimpiar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnLimpiar.setBackground(tema.getAzul());
			}
		});
		btnLimpiar.setIcon(new ImageIcon(ruta+"/images/trash.png"));
		btnLimpiar.setForeground(Color.WHITE);
		btnLimpiar.setBorderPainted(false);
		btnLimpiar.setBackground(new Color(13, 86, 180));
		btnLimpiar.setBounds(618, 343, 130, 35);
		frmVentasDirectas.getContentPane().add(btnLimpiar);
		
		NumberFormat dispFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
		NumberFormat editFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
		editFormat.setGroupingUsed(false);
		NumberFormatter dnFormat = new NumberFormatter(dispFormat);
		NumberFormatter enFormat = new NumberFormatter(editFormat);
		DefaultFormatterFactory currFactory = new DefaultFormatterFactory(dnFormat, dnFormat, enFormat);
		enFormat.setAllowsInvalid(true);
		
		JLabel lblSub = new JLabel("SUB-TOTAL:");
		lblSub.setFont(new Font("Arial", Font.BOLD, 11));
		lblSub.setBounds(43, 398, 70, 14);
		frmVentasDirectas.getContentPane().add(lblSub);
		
		subtotal = new JFormattedTextField();
		subtotal.setEnabled(false);
		subtotal.setValue(0);
		subtotal.setBounds(123, 395, 148, 20);
		subtotal.setFormatterFactory(currFactory);
		frmVentasDirectas.getContentPane().add(subtotal);
		
		JLabel lblIva = new JLabel("IVA:");
		lblIva.setFont(new Font("Arial", Font.BOLD, 11));
		lblIva.setBounds(307, 398, 31, 14);
		frmVentasDirectas.getContentPane().add(lblIva);
		
		iva = new JFormattedTextField();
		iva.setEnabled(false);
		iva.setValue(0);
		iva.setBounds(340, 395, 148, 20);
		iva.setFormatterFactory(currFactory);
		frmVentasDirectas.getContentPane().add(iva);
		
		JLabel lblTotal = new JLabel("TOTAL:");
		lblTotal.setFont(new Font("Arial", Font.BOLD, 11));
		lblTotal.setBounds(539, 398, 46, 14);
		frmVentasDirectas.getContentPane().add(lblTotal);
		
		total = new JFormattedTextField();
		total.setEnabled(false);
		total.setValue(0);
		total.setBounds(588, 395, 148, 20);
		total.setFormatterFactory(currFactory);
		frmVentasDirectas.getContentPane().add(total);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		lblNewLabel_3.setBounds(26, 388, 722, 34);
		frmVentasDirectas.getContentPane().add(lblNewLabel_3);
		
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATOS GENERALES", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11), gris));
		lblNewLabel_1.setBounds(26, 61, 723, 105);
		frmVentasDirectas.getContentPane().add(lblNewLabel_1);
		
		JButton btnRegistrar = new JButton("Registrar");
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Factura Ofactura = new Factura();	
				try {
					if(!Ocliente.ValidarRif(textField.getText())){
						if(textField.getText().equals("") || textField_1.getText().equals("") || textField_2.getText().equals("") || textField_3.getText().equals("")){
							Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
							JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
						}else{
							Pattern pat = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"); // Valida formato del correo
						    Matcher mat = pat.matcher(textField_3.getText());
						    if(mat.find()){
						    	Cliente cliente = new Cliente(textField.getText(), textField_1.getText(),textArea.getText(), textField_2.getText(), textField_3.getText(), "", "", "");
								cliente.Registrar();
							}else{
								Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
							    JOptionPane.showMessageDialog(null, "El formato de correo es inv�lido, verifique que lo ingres� de forma correcta. Ejemplo: Prueba@Proveedor.com","Error" , JOptionPane.ERROR_MESSAGE, icono); 
							}
						}	
					}
					ListaTiposPago tipos = new ListaTiposPago(Double.parseDouble(total.getValue().toString()));
					tipos.setLocationRelativeTo(frmVentasDirectas);
					tipos.setVisible(true);
					Tabla pago = tipos.getListaTiposPago();
					
					if(tipos.getVenta() > 0){
						Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
						JOptionPane.showMessageDialog(null, "Existe una diferencia de "+tipos.getVenta()+" Bs. que no ha sido cancelada, no podr� continuar con el proceso de facturaci�n","Error" , JOptionPane.ERROR_MESSAGE, icono);
					}else{
						if(Ocliente.ValidarRif(textField.getText()) && listaconcepto.getRowCount() > 0 && pago.getRowCount() > 0){
							Cliente Ocliente = new Cliente();
							Ocliente.TraerRegistro(Ocliente.RetornaID(textField.getText()));
							String tipo;
							int doc;
							if(presupuesto > 0){
								tipo = "P";
								doc = presupuesto;
							}else{
								tipo = "M";
								doc = Integer.parseInt(nro);
							}
							Ofactura.getConexion().setBeginTrans(false);
							String n_factura = Ofactura.RetornaNumero(doc,tipo);
							int descuento = 0;
							for(int i = 0; i < listaconcepto.getRowCount(); i++){
								descuento = descuento + Integer.parseInt(listaconcepto.getValueAt(i, 5).toString());
							}
							Ofactura.RegistroCompleto(Ofactura.retornaIva(), Integer.parseInt(n_factura), descuento, Double.parseDouble(subtotal.getValue().toString()), Ofactura.IdFactura(n_factura), Ocliente.RetornaID(Ocliente.getRif().toString()),user.getUsuario(),listaconcepto , pago);
							if (presupuesto > 0){
								Presupuesto Opresupesto = new Presupuesto(Ofactura.getConexion());
								Opresupesto.ProcesarPresupuesto(presupuesto);
							}
							AbstractJasperReports.createFactura(ruta+"/reports/factura/", Ofactura.IdFactura(n_factura), n_factura, Ocliente.getRif(),Ocliente.getRazonSocial(), Ocliente.getDireccion(), Ocliente.getTelefono());
							AbstractJasperReports.Print();
							textField_1.setText("");
							textField_2.setText("");
							textField_3.setText("");
							textArea.setText("");
							textField_1.setEnabled(false);
							textField_2.setEnabled(false);
							textField_3.setEnabled(false);
							textArea.setEnabled(false);
							subtotal.setValue(0);
							iva.setValue(0);
							total.setValue(0);
							listaconcepto.setClearGrid();
							Ofactura.NroFactura();
							label_1.setText(Ofactura.getNro());
							textField.setText("");
							VistaImpresion carga = new VistaImpresion();
							carga.setLocationRelativeTo(frmVentasDirectas);
							carga.setVisible(true);
							Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
							JOptionPane.showMessageDialog(null, "Factura generada Exitosamente","�xito" , JOptionPane.INFORMATION_MESSAGE, icono);
							Ofactura.getConexion().setCommitTrans();
						}else{
							Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
							JOptionPane.showMessageDialog(null, "Datos incompletos para completar la factura","Error" , JOptionPane.ERROR_MESSAGE, icono);
						}
					}
				} catch (SQLException e1) {
					Ofactura.getConexion().setRollbackTrans();
					e1.printStackTrace();
				} catch (JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			
			}
		});
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnRegistrar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnRegistrar.setBackground(tema.getAzul());
			}
		});
		btnRegistrar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		btnRegistrar.setForeground(Color.WHITE);
		btnRegistrar.setBorderPainted(false);
		btnRegistrar.setBackground(new Color(13, 86, 180));
		btnRegistrar.setBounds(169, 436, 164, 44);
		frmVentasDirectas.getContentPane().add(btnRegistrar);
		
		JButton button_2 = new JButton("Volver");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(retorno == 1){
					try {
						Principal ventana = new Principal(user);
						ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
						frmVentasDirectas.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}else{
					try {
						AdministrarPresupuestos ventana2 = new AdministrarPresupuestos(user);
						ventana2.frmAdministrarFacturas.setVisible(true);
						frmVentasDirectas.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		button_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_2.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_2.setBackground(tema.getAzul());
			}
		});
		button_2.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button_2.setForeground(Color.WHITE);
		button_2.setBorderPainted(false);
		button_2.setBackground(new Color(13, 86, 180));
		button_2.setBounds(427, 436, 164, 44);
		frmVentasDirectas.getContentPane().add(button_2);
		
		if(presupuesto > 0){
			Presupuestar();
		}
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "VENTAS DIRECTAS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		lblNewLabel.setBounds(10, 11, 753, 484);
		frmVentasDirectas.getContentPane().add(lblNewLabel);
		
		
		 //************************************************ V E N T A N A  D E  D E S C U E N T O *********************************************************

		descuento = new JFrame();
		descuento.setTitle("Asignar Descuento");
		descuento.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		descuento.setResizable(false);
		descuento.setBounds(100, 100, 470, 180);
		descuento.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		descuento.getContentPane().setLayout(null);
		Dimension pantalla_descuento = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana_descuento = descuento.getSize();  
        descuento.setLocation((pantalla_descuento .width - ventana_descuento.width) / 2, (pantalla_descuento .height - ventana_descuento.height) / 2); 
		
		JLabel label1 = new JLabel("% DESCUENTO:");
		label1.setFont(new Font("Arial", Font.BOLD, 11));
		label1.setBounds(41, 48, 81, 14);
		descuento.getContentPane().add(label1);
		
		slider = new JSlider();
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				desc.setText(slider.getValue()+"");
			}
		});
		slider.setValue(0);
		slider.setToolTipText("Deslice para ajustar el % de descuento");
		slider.setBounds(127, 42, 225, 23);
		descuento.getContentPane().add(slider);
		
		desc = new JTextField();
		desc.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				desc.nextFocus();
			}
		});
		desc.setText("0");
		desc.setColumns(10);
		desc.setBounds(357, 45, 62, 20);
		desc.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || desc.getText().length() == 3 ){
		            evt.consume();
		        }
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				if(desc.getText().equals("")){
					desc.setText("0");
				}
				int valor = Integer.parseInt(desc.getText().toString());
				slider.setValue(valor);
			}
			@Override
			public void keyPressed(KeyEvent arg0) {
				if(desc.getText().equals("") || desc.getText().equals("00")){
					desc.setText("0");
				}
				int valor = Integer.parseInt(desc.getText().toString());
				slider.setValue(valor);
			}
		});
		desc.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				desc.nextFocus();
			}
		});
		descuento.getContentPane().add(desc);
		
		JButton btnAplicar = new JButton("Aplicar");
		btnAplicar.setForeground(Color.WHITE);
		btnAplicar.setBorderPainted(false);
		btnAplicar.setBackground(tema.getAzul());
		btnAplicar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnAplicar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnAplicar.setBackground(tema.getAzul());
			}
		});
		btnAplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listaconcepto.setValueAt(desc.getText(), listaconcepto.getSelectedRow(), 5);
				desc.setText("0");
				try {
					Totales();
					Actualizar();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				descuento.dispose();
			}
		});
		btnAplicar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		        	listaconcepto.setValueAt(desc.getText(), listaconcepto.getSelectedRow(), 5);
					desc.setText("0");
					try {
						Totales();
						Actualizar();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					descuento.dispose();
		        }	
			}
		});
		btnAplicar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		btnAplicar.setBounds(85, 80, 120, 36);
		descuento.getContentPane().add(btnAplicar);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setForeground(Color.WHITE);
		btnVolver.setBorderPainted(false);
		btnVolver.setBackground(tema.getAzul());
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				descuento.dispose();
			}
		});
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.setBounds(265, 80, 120, 36);
		descuento.getContentPane().add(btnVolver);
		
		JLabel lblNewLabel1 = new JLabel("");
		lblNewLabel1.setBounds(10, 11, 444, 127);
		lblNewLabel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ASIGNAR DESCUENTO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		descuento.getContentPane().add(lblNewLabel1);
		
		//******************************************************************************************************************************************************
	}
}
