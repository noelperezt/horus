package views;

import java.awt.Dimension;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JSpinnerDateEditor;

import utility.AbstractJasperReports;
import utility.Conexion;
import utility.EstiloHorus;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BusquedaContrataciones {

	public JFrame frmBuscarContrataciones;

	public BusquedaContrataciones() {
		initialize();
	}
	
	public double transformaDouble(String cifra){
		double total = 0;
		if ( !((cifra == null) || (cifra.equals(""))) ) {
			total = Double.parseDouble(cifra);
		}
		return total;
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		String ruta = new File ("").getAbsolutePath ();
		frmBuscarContrataciones = new JFrame();
		frmBuscarContrataciones.setTitle("Buscar Contrataciones");
		frmBuscarContrataciones.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmBuscarContrataciones.setBounds(100, 100, 377, 229);
		frmBuscarContrataciones.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmBuscarContrataciones.getContentPane().setLayout(null);
		frmBuscarContrataciones.setResizable(false);
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmBuscarContrataciones.getSize();  
        frmBuscarContrataciones.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);

        EstiloHorus tema = new EstiloHorus();
        
		JRadioButton todos = new JRadioButton("TODAS");
		todos.setSelected(true);
		todos.setFont(new Font("Arial", Font.BOLD, 11));
		todos.setBounds(30, 41, 74, 23);
		frmBuscarContrataciones.getContentPane().add(todos);
		
		JRadioButton facturadas = new JRadioButton("FACTURADAS");
		facturadas.setFont(new Font("Arial", Font.BOLD, 11));
		facturadas.setBounds(106, 41, 107, 23);
		frmBuscarContrataciones.getContentPane().add(facturadas);
		
		JRadioButton no_facturadas = new JRadioButton("NO FACTURADAS");
		no_facturadas.setFont(new Font("Arial", Font.BOLD, 11));
		no_facturadas.setBounds(216, 41, 123, 23);
		frmBuscarContrataciones.getContentPane().add(no_facturadas);
		
		 ButtonGroup grupo1 = new ButtonGroup();
		 grupo1.add(todos);
		 grupo1.add(facturadas);
		 grupo1.add(no_facturadas);
		
		JLabel frame = new JLabel("");
		frame.setBounds(21, 37, 327, 32);
		frame.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12), new java.awt.Color(0, 102, 255)));
		frmBuscarContrataciones.getContentPane().add(frame);
		
		JLabel lblFechaInicio = new JLabel("FECHA INICIO:");
		lblFechaInicio.setFont(new Font("Arial", Font.BOLD, 11));
		lblFechaInicio.setBounds(31, 80, 86, 14);
		frmBuscarContrataciones.getContentPane().add(lblFechaInicio);
		
		JDateChooser inicio  = new JDateChooser(null, null, null, new JSpinnerDateEditor()); 
		inicio.setBounds(115, 80, 233, 20);
		frmBuscarContrataciones.getContentPane().add(inicio);
		
		JLabel lblFechaFin = new JLabel("FECHA FIN:");
		lblFechaFin.setFont(new Font("Arial", Font.BOLD, 11));
		lblFechaFin.setBounds(46, 119, 71, 14);
		frmBuscarContrataciones.getContentPane().add(lblFechaFin);
		
		JDateChooser fin  = new JDateChooser(null, null, null, new JSpinnerDateEditor()); 
		fin.setBounds(115, 113, 233, 20);
		frmBuscarContrataciones.getContentPane().add(fin);
		
		JButton buscar = new JButton("Buscar");
		buscar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				buscar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				buscar.setBackground(tema.getAzul());
			}
		});
		buscar.setForeground(Color.WHITE);
		buscar.setBackground(tema.getAzul());
		buscar.setBorderPainted(false);
		buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Conexion conn = new Conexion();
		   		if (inicio.getDate() != null && fin.getDate() != null){
		   			if(inicio.getDate().before(fin.getDate())){
		   				SimpleDateFormat formatter;
				   		formatter = new SimpleDateFormat("yyyy-MM-dd");
				   		String fecha_inicial = formatter.format(inicio.getDate());
		   				String fecha_final = formatter.format(fin.getDate());
		   				
		   				java.sql.Date inicial_consulta = new java.sql.Date(inicio.getDate().getTime());
		   				java.sql.Date final_consulta = new java.sql.Date(fin.getDate().getTime());
		   				
		   				if (todos.isSelected()){
		   					String total_pendiente = null;
							String busca_clave = "SELECT SUM(total_pago) as pendientes FROM ads_contrataciones WHERE fecha_transaccion >= '"+fecha_inicial+"' AND fecha_transaccion <= '"+fecha_final+"' AND status = 'I' AND fecha_pago_confirmado IS NOT NULL";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total_pendiente = busca.getString("pendientes");
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							String total_facturado = null;
							busca_clave = "SELECT SUM(total_pago) as facturado FROM ads_contrataciones WHERE fecha_transaccion >= '"+fecha_inicial+"' AND fecha_transaccion <= '"+fecha_final+"' AND status = 'A' AND fecha_pago_confirmado IS NOT NULL";
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total_facturado = busca.getString("facturado");
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							int total = 0;
							busca_clave = "SELECT COUNT(*) as total FROM ads_contrataciones"; 
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
			   			
							AbstractJasperReports.TodasContrataciones(conn.getConnection(), ruta+"/reports/listado contrataciones/",ruta, transformaDouble(total_pendiente), transformaDouble(total_facturado), total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   					
		   				}else if(facturadas.isSelected()){
		   					String total_facturado = null;
							String busca_clave = "SELECT SUM(total_pago) as facturado FROM ads_contrataciones WHERE fecha_transaccion >= '"+fecha_inicial+"' AND fecha_transaccion <= '"+fecha_final+"' AND status = 'A' AND fecha_pago_confirmado IS NOT NULL";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total_facturado = busca.getString("facturado");
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							int total = 0;
							busca_clave = "SELECT COUNT(*) as total FROM ads_contrataciones WHERE fecha_transaccion >= '"+fecha_inicial+"' AND fecha_transaccion <= '"+fecha_final+"' AND status = 'A' AND fecha_pago_confirmado IS NOT NULL";
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
			   										
							AbstractJasperReports.Facturadas(conn.getConnection(), ruta+"/reports/listado contrataciones/", ruta, transformaDouble(total_facturado), total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   					
		   				}else if (no_facturadas.isSelected()){
							String busca_clave = null;
							ResultSet busca;	
							
							String total_pendiente = null;
							busca_clave = "SELECT SUM(total_pago) as pendiente FROM ads_contrataciones WHERE fecha_transaccion >= '"+fecha_inicial+"' AND fecha_transaccion <= '"+fecha_final+"'AND status = 'I'AND fecha_pago_confirmado IS NOT NULL";
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total_pendiente = busca.getString("pendiente");
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							int total = 0;
							busca_clave = "SELECT COUNT(*) as total FROM ads_contrataciones WHERE fecha_transaccion >= '"+fecha_inicial+"' AND fecha_transaccion <= '"+fecha_final+"' AND status = 'I' AND fecha_pago_confirmado IS NOT NULL";
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
			   				
							AbstractJasperReports.Pendientes(conn.getConnection(), ruta+"/reports/listado contrataciones/", ruta, transformaDouble(total_pendiente), total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   				}
		   				
			   		}else{
			   			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			   			JOptionPane.showMessageDialog(null, "La Fecha Inicial es mayor a la Fecha Final","Error" , JOptionPane.ERROR_MESSAGE, icono);
			   		}
		   		}else{
		   			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
		   			JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
		   		}
			}
		});
		buscar.setIcon(new ImageIcon(ruta+"/images/search.png"));
		buscar.setBounds(46, 144, 120, 36);
		frmBuscarContrataciones.getContentPane().add(buscar);
		
		JButton volver = new JButton("Volver");
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				volver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				volver.setBackground(tema.getAzul());
			}
		});
		volver.setBackground(tema.getAzul());
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmBuscarContrataciones.dispose();
			}
		});
		volver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		volver.setBounds(202, 144, 120, 36);
		frmBuscarContrataciones.getContentPane().add(volver);
		
		JLabel borde = new JLabel("");
		borde.setBounds(10, 11, 351, 182);
		borde.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "BUSCAR CONTRATACIONES", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmBuscarContrataciones.getContentPane().add(borde);
		
	}
}
