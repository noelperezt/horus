package views;

import java.awt.Dimension;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JSpinnerDateEditor;

import utility.AbstractJasperReports;
import utility.Conexion;
import utility.EstiloHorus;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;

public class BusquedaFactura {

	public JFrame frmBuscarFacturas;

	public BusquedaFactura() {
		initialize();
	}
	
	public double transformaDouble(String cifra){
		double total = 0;
		if ( !((cifra == null) || (cifra.equals(""))) ) {
			total = Double.parseDouble(cifra);
		}
		return total;
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		String ruta = new File ("").getAbsolutePath ();
		frmBuscarFacturas = new JFrame();
		frmBuscarFacturas.setTitle("Buscar Facturas");
		frmBuscarFacturas.setBounds(100, 100, 351, 229);
		frmBuscarFacturas.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmBuscarFacturas.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmBuscarFacturas.getContentPane().setLayout(null);
		frmBuscarFacturas.setResizable(false);
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmBuscarFacturas.getSize();  
        frmBuscarFacturas.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);
		
        EstiloHorus tema = new EstiloHorus();
        
		JRadioButton todos = new JRadioButton("TODAS");
		todos.setSelected(true);
		todos.setFont(new Font("Arial", Font.BOLD, 11));
		todos.setBounds(30, 41, 74, 23);
		frmBuscarFacturas.getContentPane().add(todos);
		
		JRadioButton anuladas = new JRadioButton("ANULADAS");
		anuladas.setFont(new Font("Arial", Font.BOLD, 11));
		anuladas.setBounds(106, 41, 95, 23);
		frmBuscarFacturas.getContentPane().add(anuladas);
		
		JRadioButton no_anuladas = new JRadioButton("NO ANULADAS");
		no_anuladas.setFont(new Font("Arial", Font.BOLD, 11));
		no_anuladas.setBounds(198, 41, 109, 23);
		frmBuscarFacturas.getContentPane().add(no_anuladas);
		
		 ButtonGroup grupo1 = new ButtonGroup();
		 grupo1.add(todos);
		 grupo1.add(anuladas);
		 grupo1.add(no_anuladas);
		
		JLabel frame = new JLabel("");
		frame.setBounds(21, 37, 300, 32);
		frame.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12), new java.awt.Color(0, 102, 255)));
		frmBuscarFacturas.getContentPane().add(frame);
		
		JLabel lblFechaInicio = new JLabel("FECHA INICIO:");
		lblFechaInicio.setFont(new Font("Arial", Font.BOLD, 11));
		lblFechaInicio.setBounds(31, 80, 86, 14);
		frmBuscarFacturas.getContentPane().add(lblFechaInicio);
		
		JDateChooser inicio  = new JDateChooser(null, null, null, new JSpinnerDateEditor()); 
		inicio.setBounds(115, 80, 206, 20);
		frmBuscarFacturas.getContentPane().add(inicio);
		
		JLabel lblFechaFin = new JLabel("FECHA FIN:");
		lblFechaFin.setFont(new Font("Arial", Font.BOLD, 11));
		lblFechaFin.setBounds(46, 119, 71, 14);
		frmBuscarFacturas.getContentPane().add(lblFechaFin);
		
		JDateChooser fin  = new JDateChooser(null, null, null, new JSpinnerDateEditor()); 
		fin.setBounds(115, 113, 206, 20);
		frmBuscarFacturas.getContentPane().add(fin);
		
		JButton buscar = new JButton("Buscar");
		buscar.setForeground(Color.WHITE);
		buscar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				buscar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				buscar.setBackground(tema.getAzul());
			}
		});
		buscar.setBackground(tema.getAzul());
		buscar.setBorderPainted(false);
		buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Conexion conn = new Conexion();
		   		if (inicio.getDate() != null && fin.getDate() != null){
		   			if(inicio.getDate().before(fin.getDate())){
		   				SimpleDateFormat formatter;
				   		formatter = new SimpleDateFormat("yyyy-MM-dd");
				   		String fecha_inicial = formatter.format(inicio.getDate());
		   				String fecha_final = formatter.format(fin.getDate());
		   				
		   				java.sql.Date inicial_consulta = new java.sql.Date(inicio.getDate().getTime());
		   				java.sql.Date final_consulta = new java.sql.Date(fin.getDate().getTime());
		   				
		   				if (todos.isSelected()){
		   					String total_anulado = null;
							String busca_clave = "SELECT sum(total_pago) as anulado FROM ads_facturacion WHERE  ads_facturacion.anulada = 'S' AND fecha_factura >= '"+fecha_inicial+"' AND fecha_factura <= '"+fecha_final+"'";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total_anulado = busca.getString("anulado");
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							String total_no_anulado = null;
							busca_clave = "SELECT sum(total_pago) as no_anulado FROM ads_facturacion WHERE  ads_facturacion.anulada = 'N' AND fecha_factura >= '"+fecha_inicial+"' AND fecha_factura <= '"+fecha_final+"'";
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total_no_anulado = busca.getString("no_anulado");
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							int total = 0;
							busca_clave = "SELECT count(*) as total FROM ads_facturacion WHERE  fecha_factura >= '"+fecha_inicial+"' AND fecha_factura <= '"+fecha_final+"'";
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
			   					
							AbstractJasperReports.TodasFacturas(conn.getConnection(), ruta+"/reports/listado facturas/",ruta, transformaDouble(total_anulado), transformaDouble(total_no_anulado), total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   					
		   				}else if(anuladas.isSelected()){
		   					String total_anulado = null;
							String busca_clave = "SELECT sum(total_pago) as anulado FROM ads_facturacion WHERE  ads_facturacion.anulada = 'S' AND fecha_factura >= '"+fecha_inicial+"' AND fecha_factura <= '"+fecha_final+"'";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total_anulado = busca.getString("anulado");
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
										
							int total = 0;
							busca_clave = "SELECT count(*) as total FROM ads_facturacion WHERE  ads_facturacion.anulada = 'S' AND fecha_factura >= '"+fecha_inicial+"' AND fecha_factura <= '"+fecha_final+"'";
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
			   										
							AbstractJasperReports.FacturasAnuladas(conn.getConnection(), ruta+"/reports/listado facturas/", ruta, transformaDouble(total_anulado), total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   					
		   				}else if (no_anuladas.isSelected()){
							String busca_clave = null;
							ResultSet busca;	
							
							String total_no_anulado = null;
							busca_clave = "SELECT sum(total_pago) as no_anulado FROM ads_facturacion WHERE ads_facturacion.anulada = 'N' AND fecha_factura >= '"+fecha_inicial+"' AND fecha_factura <= '"+fecha_final+"'";
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total_no_anulado = busca.getString("no_anulado");
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							String subtotal_no_anulado = null;
							busca_clave = "SELECT sum(monto_gravado) as subno_anulado FROM ads_facturacion WHERE ads_facturacion.anulada = 'N' AND fecha_factura >= '"+fecha_inicial+"' AND fecha_factura <= '"+fecha_final+"'";
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									subtotal_no_anulado = busca.getString("subno_anulado");
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
							String total_iva = null;
							busca_clave = "SELECT sum(monto_iva) as iva FROM ads_facturacion WHERE ads_facturacion.anulada = 'N' AND fecha_factura >= '"+fecha_inicial+"' AND fecha_factura <= '"+fecha_final+"'";
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total_iva = busca.getString("iva");
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
														
							int total = 0;
							busca_clave = "SELECT count(*) as total FROM ads_facturacion WHERE  anulada = 'N' AND fecha_factura >= '"+fecha_inicial+"' AND fecha_factura <= '"+fecha_final+"'";
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
			   										
							AbstractJasperReports.FacturasActivas(conn.getConnection(), ruta+"/reports/listado facturas/", ruta, transformaDouble(total_no_anulado),transformaDouble(subtotal_no_anulado),transformaDouble(total_iva), total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   				}
		   				
			   		}else{
			   			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			   			JOptionPane.showMessageDialog(null, "La fecha Inicial es mayor a la fecha Final","Error" , JOptionPane.ERROR_MESSAGE, icono);
			   		}
		   		}else{
		   			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
		   			JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
		   		}
			}
		});
		buscar.setIcon(new ImageIcon(ruta+"/images/search.png"));
		buscar.setBounds(37, 144, 120, 36);
		frmBuscarFacturas.getContentPane().add(buscar);
		
		JButton volver = new JButton("Volver");
		volver.setForeground(Color.WHITE);
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				volver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				volver.setBackground(tema.getAzul());
			}
		});
		volver.setBorderPainted(false);
		volver.setBackground(tema.getAzul());
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmBuscarFacturas.dispose();
			}
		});
		volver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		volver.setBounds(187, 144, 120, 36);
		frmBuscarFacturas.getContentPane().add(volver);
		
		JLabel borde = new JLabel("");
		borde.setBounds(10, 11, 327, 184);
		borde.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "BUSCAR FACTURAS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmBuscarFacturas.getContentPane().add(borde);
		
	}
}
