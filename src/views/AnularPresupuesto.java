package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import models.Usuario;
import net.sf.jasperreports.engine.JRException;
import utility.Conexion;
import utility.EstiloHorus;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;

public class AnularPresupuesto {

	public JFrame frmAnularPresupuesto;
	public String ruta = new File ("").getAbsolutePath ();
	private int n_pre;
	private JTextArea textArea;
	private Usuario user;

	public int retornaUserId(String name) throws SQLException{
		int user = 0;
		Conexion conn = new Conexion();
		String sentencia = "SELECT id_admin FROM tb_administrador WHERE usuario ='"+name+"'";
		
		ResultSet busca;
		busca = conn.Consulta(sentencia);
		
		if(busca.next()){
			user = Integer.parseInt(busca.getString("id_admin"));
			busca.close();
		}
		
		conn.desConectar();
		return user;
	}
	
	public AnularPresupuesto(int n_pre, Usuario user) {
		this.user = user;
		this.n_pre = n_pre;
		initialize();
	}
	
	public void ejecutar() throws HeadlessException, JRException, SQLException{
		Conexion conn = new Conexion();
		Date hoy;
   		String fecha;
   		SimpleDateFormat formatter;
   		formatter = new SimpleDateFormat("yyyy-MM-dd");
   		hoy = new Date();
   		fecha = formatter.format(hoy);
		if(!textArea.getText().equals("")){
			String funcion = "UPDATE ads_presupuesto SET anulado = 'S', motivo_anulacion = '"+textArea.getText()+"', fecha_anulacion ='"+fecha+"' WHERE id_presupuesto = "+n_pre;
			boolean rs = conn.ejecutar(funcion);
			if (rs){
				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
				JOptionPane.showMessageDialog(null, "Anulaci�n Exitosa", "�xito", JOptionPane.INFORMATION_MESSAGE, icono);
				AdministrarPresupuestos ventana = new AdministrarPresupuestos(user);
				ventana.frmAdministrarFacturas.setVisible(true);
				frmAnularPresupuesto.setVisible(false);
			}else{
				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
				JOptionPane.showMessageDialog(null, "Error de anulaci�n", "Error", JOptionPane.ERROR_MESSAGE,icono);
			}	
		}else{
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "Debe ingresar el motivo de la Anulaci�n","Error" , JOptionPane.ERROR_MESSAGE, icono); 
		}
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		EstiloHorus tema = new EstiloHorus();
		frmAnularPresupuesto = new JFrame();
		frmAnularPresupuesto.setResizable(false);
		frmAnularPresupuesto.setTitle("Anular Presupuesto");
		frmAnularPresupuesto.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAnularPresupuesto.setBounds(100, 100, 363, 234);
		frmAnularPresupuesto.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAnularPresupuesto.getSize();  
        frmAnularPresupuesto.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		frmAnularPresupuesto.getContentPane().setLayout(null);
		frmAnularPresupuesto.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				AdministrarPresupuestos ventana;
				try {
					ventana = new AdministrarPresupuestos(user);
					ventana.frmAdministrarFacturas.setVisible(true);
					frmAnularPresupuesto.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
		});
		
		JLabel lblMotivo = new JLabel("MOTIVO:");
		lblMotivo.setFont(new Font("Arial", Font.BOLD, 11));
		lblMotivo.setBounds(34, 44, 61, 14);
		frmAnularPresupuesto.getContentPane().add(lblMotivo);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(89, 44, 235, 79);
		frmAnularPresupuesto.getContentPane().add(scrollPane);
		
		textArea = new JTextArea(4,50);
		textArea.setLineWrap(true); 
		textArea.setWrapStyleWord(true); 
		textArea.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
			    if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) &&(caracter != '�') &&(caracter != '�') &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE){
			         evt.consume();
			    }	
			}
			@SuppressWarnings({ "static-access", "deprecation" })
			@Override
			public void keyPressed(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		            textArea.nextFocus();
		        }
			}
		});
		scrollPane.setViewportView(textArea);
		
		JButton btnNewButton = new JButton("Anular");
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBorderPainted(false);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.setBackground(tema.getAzul());
		btnNewButton.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		           try {
					ejecutar();
				} catch (HeadlessException | JRException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
		        }
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					ejecutar();
				} catch (HeadlessException | JRException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/break.png"));
		btnNewButton.setBounds(43, 140, 116, 41);
		frmAnularPresupuesto.getContentPane().add(btnNewButton);
		
		JButton btnCancelar = new JButton("Cancelar");
		btnCancelar.setForeground(Color.WHITE);
		btnCancelar.setBorderPainted(false);
		btnCancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnCancelar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnCancelar.setBackground(tema.getAzul());
			}
		});
		btnCancelar.setBackground(tema.getAzul());
		btnCancelar.setIcon(new ImageIcon(ruta+"/images/close.png"));
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministrarPresupuestos ventana;
				try {
					ventana = new AdministrarPresupuestos(user);
					ventana.frmAdministrarFacturas.setVisible(true);
					frmAnularPresupuesto.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
		});
		btnCancelar.setBounds(196, 140, 116, 41);
		frmAnularPresupuesto.getContentPane().add(btnCancelar);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 334, 186);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ANULAR PRESUPUESTO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAnularPresupuesto.getContentPane().add(lblNewLabel);
	}
}
