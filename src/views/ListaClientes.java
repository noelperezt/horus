package views;

import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.SQLException;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import models.Cliente;
import utility.EstiloHorus;
import utility.Tabla;
import javax.swing.JLabel;
import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.Color;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

@SuppressWarnings("serial")
public class ListaClientes extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private Tabla listaclientes;
	private String rif;
	
	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */
	
	public String getRif(){
		return rif;
	}
	
	public void setRif(String rif){
		this.rif = rif;
	}
	
	public ListaClientes() throws SQLException {
		setModal(true);
		String ruta = new File ("").getAbsolutePath ();
		setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		setTitle("Lista de Clientes");
		setResizable(false);
		setBounds(100, 100, 590, 433);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				setRif("");
				setVisible(false);
			}
		});
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(33, 45, 56, 14);
		contentPanel.add(label);
		
		Cliente Ocliente = new Cliente();
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listaclientes.requestFocus();
				listaclientes.setRowSelectionInterval(0, 0);
			}
		});
		textField.setBounds(86, 42, 299, 20);
		textField.setToolTipText("Busqueda por Raz\u00F3n Social");
		textField.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
				if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE ) {
			         evt.consume();
			    }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				listaclientes.setClearGrid();
				try {
					listaclientes.Refrescar(Ocliente.FiltraClientesResumen(textField.getText()));
					Ocliente.desconectar();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		contentPanel.add(textField);
		textField.setColumns(10);
		
		String campos[] = {"ID","RIF","RAZ�N SOCIAL","TEL�FONO","CORREO"};
		int ancho[] = {1,80,130,120,120};
		int editable[] = null;
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(33, 70, 514, 260);
		contentPanel.add(scrollPane);
		listaclientes = new Tabla(campos, editable, ancho, null);
		listaclientes.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyPressed(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		        	if(listaclientes.getSelectedRow() != -1){
						setRif(listaclientes.getValueAt(listaclientes.getSelectedRow(), 0).toString());
					}else{
						setRif("");
					}
					setVisible(false);
		        }	
			}
		});
		listaclientes.Listar(Ocliente.ListarClientesResumen());
		Ocliente.desconectar();
		scrollPane.setViewportView(listaclientes);
		
		EstiloHorus tema = new EstiloHorus();
		
		JButton btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listaclientes.getSelectedRow() != -1){
					setRif(listaclientes.getValueAt(listaclientes.getSelectedRow(), 0).toString());
				}else{
					setRif("");
				}
				
				setVisible(false);
			}
		});
		btnAgregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnAgregar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnAgregar.setBackground(tema.getAzul());
			}
		});
		btnAgregar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		btnAgregar.setForeground(Color.WHITE);
		btnAgregar.setBorderPainted(false);
		btnAgregar.setBackground(new Color(13, 86, 180));
		btnAgregar.setBounds(132, 344, 120, 36);
		contentPanel.add(btnAgregar);
		
		JButton button_1 = new JButton("Volver");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setRif("");
				setVisible(false);
			}
		});
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getAzul());
			}
		});
		button_1.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(new Color(13, 86, 180));
		button_1.setBounds(339, 344, 120, 36);
		contentPanel.add(button_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 564, 384);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "LISTA DE CLIENTES", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		contentPanel.add(lblNewLabel);
		
	}
}
