package views;

import java.awt.BorderLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import models.Concepto;
import utility.EstiloHorus;
import utility.Tabla;
import java.awt.Toolkit;
import java.io.File;
import java.sql.SQLException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.JScrollPane;
import java.awt.Color;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

@SuppressWarnings("serial")
public class ListaConceptos extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField textField;
	private JTextField textField_1;
	private Tabla listaconceptos;
	private Object[] fila;

	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */
	
	public Object[] getFila(){
		return fila;
	}
	
	public void setFila(Object[] fila){
		this.fila = fila; 
	}
	
	public ListaConceptos() throws SQLException {
		setModal(true);
		String ruta = new File ("").getAbsolutePath ();
		setResizable(false);
		setTitle("Lista de Conceptos");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		setBounds(100, 100, 556, 422);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		Concepto Oconcepto = new Concepto();
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(30, 41, 56, 14);
		contentPanel.add(label);
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				listaconceptos.requestFocus();
				listaconceptos.setRowSelectionInterval(0, 0);
			}
		});
		textField.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
				if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE ) {
			         evt.consume();
			    }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				listaconceptos.setClearGrid();
				try {
					listaconceptos.Refrescar(Oconcepto.FiltraConceptoResumen(textField.getText()));
					Oconcepto.desconectar();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		textField.setToolTipText("Busqueda por Concepto");
		textField.setColumns(10);
		textField.setBounds(83, 38, 299, 20);
		contentPanel.add(textField);
		
		String campos[] = {"C�DIGO","CONCEPTO","PRECIO","% DCTO"};
		int ancho[] = {1,150,40,40};
		int editable[] = null;
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(30, 66, 493, 205);
		contentPanel.add(scrollPane);
		listaconceptos = new Tabla(campos, editable, ancho,null);
		listaconceptos.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyPressed(KeyEvent evt) {
				int caracter = evt.getKeyCode();
		        if(caracter == evt.VK_ENTER){
		        	textField_1.requestFocus();
		        }
			}
		});
		listaconceptos.Listar(Oconcepto.ListarConceptosResumen());
		Oconcepto.desconectar();
		scrollPane.setViewportView(listaconceptos);
		
		JLabel lblCantidad = new JLabel("CANTIDAD:");
		lblCantidad.setFont(new Font("Arial", Font.BOLD, 11));
		lblCantidad.setBounds(35, 292, 66, 14);
		contentPanel.add(lblCantidad);
		
		textField_1 = new JTextField();
		textField_1.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || textField_1.getText().length() == 5 ){
		            evt.consume();
		        }
			}
			@SuppressWarnings("static-access")
			@Override
			public void keyPressed(KeyEvent evt) {
				int caracter = evt.getKeyCode();
		        if(caracter == evt.VK_ENTER){
		        	if(listaconceptos.getSelectedRow() != -1){
						if(!textField_1.getText().equals("")){
							fila = new Object[5];
							fila[0] = listaconceptos.getValueAt(listaconceptos.getSelectedRow(), 0).toString();
							fila[1] = Integer.parseInt(textField_1.getText());
							fila[2] = listaconceptos.getValueAt(listaconceptos.getSelectedRow(), 1);
							double desc =Double.parseDouble(listaconceptos.getValueAt(listaconceptos.getSelectedRow(), 3).toString());
							double precio_unitario = Double.parseDouble(listaconceptos.getValueAt(listaconceptos.getSelectedRow(), 2).toString());
							fila[3] = precio_unitario -((precio_unitario * desc)/100);
							int cant = Integer.parseInt(textField_1.getText());
							fila[4] = cant * (precio_unitario - ((precio_unitario * desc)/100));
							setVisible(false);
						}else{
							Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
							JOptionPane.showMessageDialog(null, "Debe ingresar la cantidad del concepto","Error" , JOptionPane.ERROR_MESSAGE, icono); 
						}	
					}
		        }
			}
		});
		textField_1.setToolTipText("Busqueda por C\u00F3digo");
		textField_1.setColumns(10);
		textField_1.setBounds(99, 289, 148, 20);
		contentPanel.add(textField_1);
		
		EstiloHorus tema = new EstiloHorus();
		
		JButton button = new JButton("Agregar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listaconceptos.getSelectedRow() != -1){
					if(!textField_1.getText().equals("")){
						fila = new Object[5];
						fila[0] = listaconceptos.getValueAt(listaconceptos.getSelectedRow(), 0).toString();
						fila[1] = Integer.parseInt(textField_1.getText());
						fila[2] = listaconceptos.getValueAt(listaconceptos.getSelectedRow(), 1);
						double desc =Double.parseDouble(listaconceptos.getValueAt(listaconceptos.getSelectedRow(), 3).toString());
						double precio_unitario = Double.parseDouble(listaconceptos.getValueAt(listaconceptos.getSelectedRow(), 2).toString());
						fila[3] = precio_unitario -((precio_unitario * desc)/100);
						int cant = Integer.parseInt(textField_1.getText());
						fila[4] = cant * (precio_unitario - ((precio_unitario * desc)/100));
						setVisible(false);
					}else{
						Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
						JOptionPane.showMessageDialog(null, "Debe ingresar la cantidad del concepto a presupuestar","Error" , JOptionPane.ERROR_MESSAGE, icono); 
					}
				}
			}
		});
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button.setBackground(tema.getAzul());
			}
		});
		button.setIcon(new ImageIcon(ruta+"/images/add.png"));
		button.setForeground(Color.WHITE);
		button.setBorderPainted(false);
		button.setBackground(new Color(13, 86, 180));
		button.setBounds(128, 334, 120, 36);
		contentPanel.add(button);
		
		JButton button_1 = new JButton("Volver");
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button_1.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getAzul());
			}
		});
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setFila(null);
				setVisible(false);
			}
		});
		button_1.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(new Color(13, 86, 180));
		button_1.setBounds(315, 334, 120, 36);
		contentPanel.add(button_1);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(30, 282, 227, 36);
		lblNewLabel_1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		contentPanel.add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 530, 375);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "LISTA DE CONCEPTOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		contentPanel.add(lblNewLabel);
	}
}
