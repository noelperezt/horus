package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Toolkit;
import java.io.File;
import java.sql.SQLException;
import javax.swing.JTextField;
import models.TiposPago;
import models.Usuario;
import utility.EstiloHorus;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RegistroTiposPago {

	public JFrame frmtiposdepago;
	private JTextField textField;
	private int tipopago;
	private Usuario user;
	public String ruta = new File ("").getAbsolutePath ();

	/**
	 * Create the application.
	 * @throws SQLException 
	 */
	public RegistroTiposPago(int tipopago, Usuario user) throws SQLException {
		this.tipopago = tipopago;
		this.user = user;
		initialize();
	}
	
	public void Ejecutar() throws SQLException{
		if(!(textField.getText().equals(""))){
			TiposPago Otipospago = new TiposPago(textField.getText());
			boolean status = false;
			if ( tipopago == 0){
				status = Otipospago.Registrar();
			}else{
				status = Otipospago.Editar(tipopago);
			}
			if(status == true){
				textField.setText("");
				AdministrarTiposPago ventana = new AdministrarTiposPago(user);
				ventana.frmAdministrartipospago.setVisible(true);
				frmtiposdepago.dispose();
			}
		}else{
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);				
		}
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmtiposdepago = new JFrame();
		frmtiposdepago.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmtiposdepago.setResizable(false);
		frmtiposdepago.setBounds(100, 100, 444, 159);
		frmtiposdepago.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmtiposdepago.getContentPane().setLayout(null);
		frmtiposdepago.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					AdministrarTiposPago ventana = new AdministrarTiposPago(user);
					ventana.frmAdministrartipospago.setVisible(true);
					frmtiposdepago.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		if (tipopago == 0){
			frmtiposdepago.setTitle("Registro de Tipos de Pago");
		}else{
			frmtiposdepago.setTitle("Modificar Tipo de Pago");
		}
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmtiposdepago.getSize();  
        frmtiposdepago.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
        EstiloHorus tema = new EstiloHorus();
        
        TiposPago Otipospago = new TiposPago();
        Otipospago.TraerRegistro(tipopago);
        
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
				if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z'))  &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE ) {
			         evt.consume();
			    }
			}
		});
		textField.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				textField.nextFocus();
			}
		});
		textField.setText(Otipospago.getDescripcion());
		textField.setColumns(10);
		textField.setBounds(120, 39, 287, 20);
		frmtiposdepago.getContentPane().add(textField);
		
		JLabel label = new JLabel("DESCRIPCI\u00D3N:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(36, 42, 74, 14);
		frmtiposdepago.getContentPane().add(label);
		
		JButton button = new JButton("Volver");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button.setBackground(tema.getAzul());
			}
		});
		button.setForeground(Color.WHITE);
		button.setBackground(tema.getAzul());
		button.setBorderPainted(false);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				AdministrarTiposPago ventana;
				try {
					ventana = new AdministrarTiposPago(user);
					ventana.frmAdministrartipospago.setVisible(true);
					frmtiposdepago.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		button.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button.setBounds(250, 70, 120, 36);
		frmtiposdepago.getContentPane().add(button);
		
		String nombre_boton = "";
		if (tipopago == 0){
			nombre_boton = "Registrar";
		}else{
			nombre_boton = "Guardar";
		}
		
		JButton btnRegistrar = new JButton(nombre_boton);
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnRegistrar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnRegistrar.setBackground(tema.getAzul());
			}
		});
		btnRegistrar.setForeground(Color.WHITE);
		btnRegistrar.setBorderPainted(false);
		btnRegistrar.setBackground(tema.getAzul());
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Ejecutar();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnRegistrar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		            try {
						Ejecutar();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
			}
		});
		if(tipopago == 0){
			btnRegistrar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		}else{
			btnRegistrar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		}
		btnRegistrar.setBounds(77, 70, 120, 36);
		frmtiposdepago.getContentPane().add(btnRegistrar);
		
		String titulo;
		if (tipopago == 0){
			titulo = "REGISTRO DE TIPOS DE PAGO";
		}else{
			titulo = "MODIFICAR TIPO DE PAGO";
		}
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 421, 110);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, titulo, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmtiposdepago.getContentPane().add(lblNewLabel);
	}
}
