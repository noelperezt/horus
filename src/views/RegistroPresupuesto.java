package views;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import utility.AbstractJasperReports;
import utility.EstiloHorus;
import utility.Tabla;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.ScrollPaneConstants;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.SwingConstants;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import models.Cliente;
import models.Factura;
import models.Presupuesto;
import models.Usuario;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import javax.swing.JFormattedTextField;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;

public class RegistroPresupuesto {

	public JFrame frmPresupuestos;
	private Tabla listaconcepto;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextArea textArea;
	private JButton btnAgregar;
	private JFormattedTextField subtotal;
	private JFormattedTextField iva;
	private JFormattedTextField total;
	private Usuario user;
	double monto_sub;
	double monto_iva;
	double monto_total;
	private String nro;

	
	public RegistroPresupuesto(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}

	public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
		String cadena= (jTextfieldS.getText()).toUpperCase();
		jTextfieldS.setText(cadena);
	}
	
	public void Totales() throws SQLException{
		Double total = 0.0;
		int descuento = 0;
		for (int i = 0; i < listaconcepto.getRowCount(); i++){
			total = total + Double.parseDouble(listaconcepto.getValueAt(i, 4).toString());
		}
		Factura Ofactura = new Factura();
		monto_sub = total - ((total * descuento)/100);
		monto_iva = (monto_sub * Ofactura.retornaIva())/100;
		monto_total = monto_sub + monto_iva;
	}
	
	private void Actualizar(){
		subtotal.setValue(monto_sub);
		iva.setValue(monto_iva);
		total.setValue(monto_total);
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		String ruta = new File ("").getAbsolutePath ();
		frmPresupuestos = new JFrame();
		frmPresupuestos.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmPresupuestos.setTitle("Presupuestos");
		frmPresupuestos.setResizable(false);
		frmPresupuestos.setBounds(100, 100, 790, 489);
		frmPresupuestos.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmPresupuestos.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmPresupuestos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		frmPresupuestos.getContentPane().setLayout(null);
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmPresupuestos.getSize();  
        frmPresupuestos.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
        EstiloHorus tema = new EstiloHorus();
        
		JLabel lblN = new JLabel("N\u00BA:");
		lblN.setFont(new Font("Arial", Font.BOLD, 18));
		lblN.setBounds(642, 36, 31, 14);
		frmPresupuestos.getContentPane().add(lblN);
		
		
		Presupuesto Opresupuesto = new Presupuesto();
		Opresupuesto.NroPresupuesto();
		nro = Opresupuesto.getNumero();
		JLabel label_1 = new JLabel(nro); // <------------- Nro del Presupuesto
		label_1.setFont(new Font("Arial", Font.BOLD, 18));
		label_1.setBounds(672, 36, 76, 14);
		frmPresupuestos.getContentPane().add(label_1);
		
		JLabel label = new JLabel("RIF/CI:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(93, 83, 39, 14);
		frmPresupuestos.getContentPane().add(label);
		
		Cliente Ocliente = new Cliente();
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField.transferFocus();
			}
		});
		textField.addFocusListener(new FocusAdapter() {
			@Override
			public void focusLost(FocusEvent arg0) {
				try {
					if(Ocliente.ValidarRif(textField.getText())){
						Ocliente.TraerRegistro(Ocliente.RetornaID(textField.getText()));
						textField_1.setText(Ocliente.getRazonSocial());
						textField_2.setText(Ocliente.getTelefono());
						textField_3.setText(Ocliente.getCorreo());
						textArea.setText(Ocliente.getDireccion());
						textField_1.setEnabled(false);
						textField_2.setEnabled(false);
						textField_3.setEnabled(false);
						textArea.setEnabled(false);
						btnAgregar.requestFocus();
					}else{
						if (textField.getText().equals("")){
							textField_1.setText("");
							textField_2.setText("");
							textField_3.setText("");
							textArea.setText("");
							textField_1.setEnabled(false);
							textField_2.setEnabled(false);
							textField_3.setEnabled(false);
							textArea.setEnabled(false);
						}else{
							textField_1.setText("");
							textField_2.setText("");
							textField_3.setText("");
							textArea.setText("");
							Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
							if(JOptionPane.showConfirmDialog(null, "El rif no se encuentra registrado �Desea ingresar los datos del Cliente","Registro de Clientes",1,0,icono)==0){
								textField_1.setEnabled(true);
								textField_2.setEnabled(true);
								textField_3.setEnabled(true);
								textArea.setEnabled(true);
								textField_1.requestFocus();
					        }	
						}
					}
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		textField.setToolTipText("Ejemplo: J000000001");
		textField.setText((String) null);
		textField.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') ||(caracter > '9')) && (caracter != evt.VK_BACK_SPACE) && caracter != 'J' && caracter != 'V' && caracter != 'G' && caracter != 'E' && caracter != 'j' && caracter != 'v' && caracter != 'g' && caracter != 'e') || textField.getText().length()== 10){
		            evt.consume();
		        }
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				convertiraMayusculasEnJtextfield(textField);
			}
		});
		textField.setColumns(10);
		textField.setBounds(133, 80, 199, 20);
		frmPresupuestos.getContentPane().add(textField);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListaClientes lista = null;
				try {
					lista = new ListaClientes();
					lista.setLocationRelativeTo(frmPresupuestos);
					lista.setVisible(true);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				if(!(lista.getRif().equals(""))){
					textField.setText(lista.getRif());
					try {
						Ocliente.TraerRegistro(Ocliente.RetornaID(textField.getText()));
						textField_1.setText(Ocliente.getRazonSocial());
						textField_2.setText(Ocliente.getTelefono());
						textField_3.setText(Ocliente.getCorreo());
						textArea.setText(Ocliente.getDireccion());
						textField_1.setEnabled(false);
						textField_2.setEnabled(false);
						textField_3.setEnabled(false);
						textArea.setEnabled(false);
						btnAgregar.requestFocus();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}  
		});
		btnNewButton.setBorderPainted(false);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.setBackground(tema.getAzul());
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/pendiente1.png"));
		btnNewButton.setBounds(340, 80, 52, 20);
		frmPresupuestos.getContentPane().add(btnNewButton);
		
		JLabel label_2 = new JLabel("RAZ\u00D3N SOCIAL:");
		label_2.setFont(new Font("Arial", Font.BOLD, 11));
		label_2.setBounds(43, 109, 89, 14);
		frmPresupuestos.getContentPane().add(label_2);
		
		textField_1 = new JTextField();
		textField_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_2.requestFocus();
			}
		});
		textField_1.setText(Ocliente.getRazonSocial());
		textField_1.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
				if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE ) {
			         evt.consume();
			    }
			}
		});
		textField_1.setEnabled(false);
		textField_1.setText((String) null);
		textField_1.setColumns(10);
		textField_1.setBounds(133, 105, 259, 20);
		frmPresupuestos.getContentPane().add(textField_1);
		
		JLabel label_3 = new JLabel("TEL\u00C9FONO:");
		label_3.setFont(new Font("Arial", Font.BOLD, 11));
		label_3.setBounds(70, 134, 61, 14);
		frmPresupuestos.getContentPane().add(label_3);
		
		textField_2 = new JTextField();
		textField_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_3.requestFocus();
			}
		});
		textField_2.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || textField_2.getText().length() == 11 ){
		            evt.consume();
		        }
			}
		});
		textField_2.setEnabled(false);
		textField_2.setText(Ocliente.getTelefono());
		textField_2.setText((String) null);
		textField_2.setColumns(10);
		textField_2.setBounds(133, 132, 259, 20);
		frmPresupuestos.getContentPane().add(textField_2);
		
		JLabel label_4 = new JLabel("CORREO:");
		label_4.setFont(new Font("Arial", Font.BOLD, 11));
		label_4.setBounds(424, 83, 52, 14);
		frmPresupuestos.getContentPane().add(label_4);
		
		textField_3 = new JTextField();
		textField_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textArea.requestFocus();
			}
		});
		textField_3.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
			    if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE && caracter != '.' && caracter != '@' && caracter != '_' && caracter != '-')
			    {
			         evt.consume();
			    }
			}
		});
		textField_3.setEnabled(false);
		textField_3.setText(Ocliente.getCorreo());
		textField_3.setText((String) null);
		textField_3.setColumns(10);
		textField_3.setBounds(481, 80, 244, 20);
		frmPresupuestos.getContentPane().add(textField_3);
		
		JLabel label_5 = new JLabel("DIRECCI\u00D3N:");
		label_5.setFont(new Font("Arial", Font.BOLD, 11));
		label_5.setBounds(410, 109, 61, 14);
		frmPresupuestos.getContentPane().add(label_5);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(481, 105, 244, 43);
		frmPresupuestos.getContentPane().add(scrollPane);
		
		textArea = new JTextArea();
		textArea.setEnabled(false);
		textArea.setText(Ocliente.getDireccion());
		scrollPane.setViewportView(textArea);
		
		JLabel lblNewLabel_2 = new JLabel("LISTA DE CONCEPTOS");
		lblNewLabel_2.setFont(new Font("Tahoma", Font.BOLD, 13));
		Color gris = new Color(76, 76, 76);
		lblNewLabel_2.setBackground(gris);
		lblNewLabel_2.setOpaque(true);
		lblNewLabel_2.setForeground(Color.WHITE);
		lblNewLabel_2.setHorizontalAlignment(SwingConstants.CENTER);
		lblNewLabel_2.setBounds(26, 177, 723, 20);
		frmPresupuestos.getContentPane().add(lblNewLabel_2);
		
		String campos[] = {"C�DIGO","CANT.","DESCRIPCION","P. UNITARIO","TOTAL"};
		int ancho[] = {20,10,200,30,30};
		int editable[] = null;
		@SuppressWarnings("rawtypes")
		Class[] tipos = new Class[] {java.lang.String.class, java.lang.Integer.class, java.lang.String.class, java.lang.Double.class, java.lang.Double.class};
		listaconcepto = new Tabla(campos, editable, ancho,tipos);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(24, 207, 599, 125);  
		frmPresupuestos.getContentPane().add(scrollPane_1);
		scrollPane_1.setViewportView(listaconcepto);	
		
		btnAgregar = new JButton("Agregar");
		btnAgregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				ListaConceptos lista = null;
				try {
					lista = new ListaConceptos();
					lista.setLocationRelativeTo(frmPresupuestos);
					lista.setVisible(true);
					if(lista.getFila() != null){
						listaconcepto.addRow(lista.getFila());
					}
					Totales();
					Actualizar();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnAgregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnAgregar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnAgregar.setBackground(tema.getAzul());
			}
		});
		btnAgregar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		btnAgregar.setForeground(Color.WHITE);
		btnAgregar.setBorderPainted(false);
		btnAgregar.setBackground(new Color(13, 86, 180));
		btnAgregar.setBounds(633, 208, 116, 34);
		frmPresupuestos.getContentPane().add(btnAgregar);
		
		JButton button_1 = new JButton("Borrar");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listaconcepto.getSelectedRow() != -1){
					listaconcepto.deleteRow(listaconcepto.getSelectedRow());
					try {
						Totales();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					Actualizar();
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un item para Borrar","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}	
			}
		});
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getAzul());
			}
		});
		button_1.setIcon(new ImageIcon(ruta+"/images/del.png"));
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(new Color(13, 86, 180));
		button_1.setBounds(633, 253, 116, 34);
		frmPresupuestos.getContentPane().add(button_1);
		
		JButton btnLimpiar = new JButton("Limpiar");
		btnLimpiar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				listaconcepto.setClearGrid();
				try {
					Totales();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				Actualizar();
			}
		});
		btnLimpiar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnLimpiar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnLimpiar.setBackground(tema.getAzul());
			}
		});
		btnLimpiar.setIcon(new ImageIcon(ruta+"/images/trash.png"));
		btnLimpiar.setForeground(Color.WHITE);
		btnLimpiar.setBorderPainted(false);
		btnLimpiar.setBackground(new Color(13, 86, 180));
		btnLimpiar.setBounds(633, 298, 116, 34);
		frmPresupuestos.getContentPane().add(btnLimpiar);
		
		NumberFormat dispFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
		NumberFormat editFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
		editFormat.setGroupingUsed(false);
		NumberFormatter dnFormat = new NumberFormatter(dispFormat);
		NumberFormatter enFormat = new NumberFormatter(editFormat);
		DefaultFormatterFactory currFactory = new DefaultFormatterFactory(dnFormat, dnFormat, enFormat);
		enFormat.setAllowsInvalid(true);
		
		JLabel lblSub = new JLabel("SUB-TOTAL:");
		lblSub.setFont(new Font("Arial", Font.BOLD, 11));
		lblSub.setBounds(34, 354, 70, 14);
		frmPresupuestos.getContentPane().add(lblSub);
		
		subtotal = new JFormattedTextField();
		subtotal.setEnabled(false);
		subtotal.setValue(0);
		subtotal.setBounds(103, 351, 148, 20);
		subtotal.setFormatterFactory(currFactory);
		frmPresupuestos.getContentPane().add(subtotal);
		
		JLabel lblIva = new JLabel("IVA:");
		lblIva.setFont(new Font("Arial", Font.BOLD, 11));
		lblIva.setBounds(304, 354, 31, 14);
		frmPresupuestos.getContentPane().add(lblIva);
		
		iva = new JFormattedTextField();
		iva.setEnabled(false);
		iva.setValue(0);
		iva.setBounds(329, 351, 148, 20);
		iva.setFormatterFactory(currFactory);
		frmPresupuestos.getContentPane().add(iva);
		
		JLabel lblTotal = new JLabel("TOTAL:");
		lblTotal.setFont(new Font("Arial", Font.BOLD, 11));
		lblTotal.setBounds(542, 354, 46, 14);
		frmPresupuestos.getContentPane().add(lblTotal);
		
		total = new JFormattedTextField();
		total.setEnabled(false);
		total.setValue(0);
		total.setBounds(588, 351, 148, 20);
		total.setFormatterFactory(currFactory);
		frmPresupuestos.getContentPane().add(total);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		lblNewLabel_3.setBounds(26, 343, 723, 34);
		frmPresupuestos.getContentPane().add(lblNewLabel_3);
		
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "DATOS GENERALES", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 11), gris));
		lblNewLabel_1.setBounds(26, 61, 723, 105);
		frmPresupuestos.getContentPane().add(lblNewLabel_1);
		
		JButton button = new JButton("Guardar");
		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent arg0) {
				try {
					if(!Ocliente.ValidarRif(textField.getText())){
						if(textField.getText().equals("") || textField_1.getText().equals("") || textField_2.getText().equals("") || textField_3.getText().equals("")){
							JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE);
						}else{
							Pattern pat = Pattern.compile("[VEGJ]{1}[0-9]{7,9}"); // Valida formato del rif
						    Matcher mat = pat.matcher(textField.getText());   
							if(mat.find()){
								pat = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@[A-Za-z0-9]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$"); // Valida formato del correo
								mat = pat.matcher(textField_3.getText());
								if(mat.find()){
									Cliente cliente = new Cliente(textField.getText(), textField_1.getText(),textArea.getText(), textField_2.getText(), textField_3.getText(), "", "", "");
									cliente.Registrar();
								}else{
									Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
									JOptionPane.showMessageDialog(null, "El formato de correo es inv�lido, verifique que lo ingres� de forma correcta. Ejemplo: Prueba@Proveedor.com","Error" , JOptionPane.ERROR_MESSAGE, icono); 
								}
							}else{
								Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
								JOptionPane.showMessageDialog(null, "El formato de rif es inv�lido, verifique que lo ingres� de forma correcta. Ejemplo: J000000001","Error" , JOptionPane.ERROR_MESSAGE, icono);
							}
						}
					}
					Validez dias = new Validez();
					dias.setLocationRelativeTo(frmPresupuestos);
					dias.setVisible(true);
					
					if(Ocliente.ValidarRif(textField.getText()) && listaconcepto.getRowCount() > 0 && dias.getValidez() > 0){
						Presupuesto Opresupuesto = new Presupuesto(textField.getText(), dias.getValidez(), user.getUsuario());
						Opresupuesto.getConexion().setBeginTrans(false);
						if (Opresupuesto.Registrar()){
							Opresupuesto.RegistrarDetalle(listaconcepto, Integer.parseInt(nro));
							Cliente Ocliente = new Cliente();
							Ocliente.TraerRegistro(Ocliente.RetornaID(textField.getText()));
							textField_1.setText("");
							textField_2.setText("");
							textField_3.setText("");
							textArea.setText("");
							textField_1.setEnabled(false);
							textField_2.setEnabled(false);
							textField_3.setEnabled(false);
							textArea.setEnabled(false);
							subtotal.setValue(0);
							iva.setValue(0);
							total.setValue(0);
							listaconcepto.setClearGrid();
							Opresupuesto.NroPresupuesto();
							label_1.setText(Opresupuesto.getNumero());
							textField.setText("");
							textField.requestFocus();
							AbstractJasperReports.createPresupuesto(ruta+"/reports/presupuesto/", Integer.parseInt(nro), nro, Ocliente.getRif(),Ocliente.getRazonSocial(), Ocliente.getDireccion(), Ocliente.getTelefono(), dias.getValidez(), ruta);
							AbstractJasperReports.showViewer();
							nro = Opresupuesto.getNumero();
							Opresupuesto.getConexion().setCommitTrans();
						}
					}else{
						Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
						JOptionPane.showMessageDialog(null, "Datos incompletos para completar el presupuesto","Error" , JOptionPane.ERROR_MESSAGE, icono);
					}			
				} catch (SQLException e1) {
					Opresupuesto.getConexion().setRollbackTrans();
					e1.printStackTrace();
				}
			
			}
		});
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button.setBackground(tema.getAzul());
			}
		});
		button.setIcon(new ImageIcon(ruta+"/images/save.png"));
		button.setForeground(Color.WHITE);
		button.setBorderPainted(false);
		button.setBackground(new Color(13, 86, 180));
		button.setBounds(168, 390, 164, 44);
		frmPresupuestos.getContentPane().add(button);
		
		JButton button_2 = new JButton("Volver");
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmPresupuestos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_2.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_2.setBackground(tema.getAzul());
			}
		});
		button_2.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button_2.setForeground(Color.WHITE);
		button_2.setBorderPainted(false);
		button_2.setBackground(new Color(13, 86, 180));
		button_2.setBounds(442, 390, 164, 44);
		frmPresupuestos.getContentPane().add(button_2);
		
		
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PRESUPUESTOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		lblNewLabel.setBounds(10, 11, 753, 440);
		frmPresupuestos.getContentPane().add(lblNewLabel);
	}
}
