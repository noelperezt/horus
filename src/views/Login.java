package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JTextField;
import models.Usuario;
import utility.EstiloHorus;
import javax.swing.JPasswordField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.sql.SQLException;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Component;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class Login {

	public JFrame frmIniciarSesin;
	private JTextField usuario;
	private JPasswordField clave;
	String ruta = new File ("").getAbsolutePath ();
	
	public Login() {
		initialize();
	}
	
	@SuppressWarnings({ "deprecation" })
	public void Ejecutar() throws SQLException{
		Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
		if(usuario.getText().equals("")){
			JOptionPane.showMessageDialog(null, "Debe introducir el nombre de Usuario","Advertencia" , JOptionPane.WARNING_MESSAGE, icono);
		}else if(clave.getText().equals("")){
			JOptionPane.showMessageDialog(null, "Debe introducir la clave de Acceso","Advertencia" , JOptionPane.WARNING_MESSAGE, icono);
		}else{
			Usuario Ouser = new Usuario(usuario.getText());
			if(Ouser.IngresoSistema(clave.getText())){
				Principal inicio = new Principal(Ouser);
				inicio.frmSistemaAdministrativoPinttosoft.setVisible(true);
				frmIniciarSesin.dispose();
			}		
		}
	}
	

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmIniciarSesin = new JFrame();
		frmIniciarSesin.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
				if(JOptionPane.showConfirmDialog(null, "�Est� seguro de cerrar el Sistema?","Cerrar Sistema",1,0,icono)==0){
		            System.exit(0);
		        }
			}
		});
		frmIniciarSesin.setTitle("Iniciar Sesi\u00F3n");
		frmIniciarSesin.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmIniciarSesin.setBounds(100, 100, 489, 189);
		frmIniciarSesin.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmIniciarSesin.getContentPane().setLayout(null);
		frmIniciarSesin.setResizable(false);
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmIniciarSesin.getSize();  
        frmIniciarSesin.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 

		EstiloHorus tema = new EstiloHorus();
        
		clave = new JPasswordField();
		clave.setBounds(207, 71, 243, 20);
		clave.addActionListener(new ActionListener() {
		     @SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
		          clave.nextFocus();
		     }
		});
		frmIniciarSesin.getContentPane().add(clave);
		
		usuario = new JTextField();
		usuario.addActionListener(new ActionListener() {
		     @SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
		          usuario.nextFocus();
		     }
		});
		usuario.setBounds(207, 40, 243, 20);
		frmIniciarSesin.getContentPane().add(usuario);
		usuario.setColumns(10);
		
		JLabel nombre_usuario = new JLabel("USUARIO:");
		nombre_usuario.setAlignmentX(Component.RIGHT_ALIGNMENT);
		nombre_usuario.setFont(new Font("Arial", Font.BOLD, 11));
		nombre_usuario.setBounds(145, 43, 56, 14);
		frmIniciarSesin.getContentPane().add(nombre_usuario);
		
		JLabel clave_usuario = new JLabel("CONTRASE\u00D1A:");
		clave_usuario.setFont(new Font("Arial", Font.BOLD, 11));
		clave_usuario.setBounds(119, 74, 78, 14);
		frmIniciarSesin.getContentPane().add(clave_usuario);
		
		JButton ingresar = new JButton("Ingresar");
		ingresar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				ingresar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				ingresar.setBackground(tema.getAzul());
			}
		});
		ingresar.setForeground(Color.WHITE);

		ingresar.setBackground(tema.getAzul());
		ingresar.setBorderPainted(false);
		ingresar.addActionListener(new ActionListener() {
			@SuppressWarnings({ })
			public void actionPerformed(ActionEvent e) {
				try {
					Ejecutar();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		ingresar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		        	try {
						Ejecutar();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }	
			}
		});
		ingresar.setBounds(141, 102, 120, 36);
		ingresar.setIcon(new ImageIcon(ruta+"/images/key.png"));
		frmIniciarSesin.getContentPane().add(ingresar);
		
		JButton salir = new JButton("Salir");
		salir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				salir.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				salir.setBackground(tema.getAzul());
			}
		});
		salir.setForeground(Color.WHITE);
		salir.setBorderPainted(false);
		salir.setBackground(tema.getAzul());
		salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
				if(JOptionPane.showConfirmDialog(null, "�Est� seguro de cerrar el Sistema?","Cerrar Sistema",1,0,icono)==0){
		            System.exit(0);
		        }
			}
		});
		salir.setBounds(295, 102, 120, 36);
		salir.setIcon(new ImageIcon(ruta+"/images/close.png"));
		frmIniciarSesin.getContentPane().add(salir);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setIcon(new ImageIcon(ruta+"/images/locked.png"));
		lblNewLabel.setBounds(20, 32, 96, 106);
		frmIniciarSesin.getContentPane().add(lblNewLabel);
		
		JLabel frame = new JLabel("");
		frame.setBounds(10, 11, 463, 142);
		frame.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "INICIAR SESI�N", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12), new java.awt.Color(13, 86, 180)));
		frmIniciarSesin.getContentPane().add(frame);
	}
}
