package views;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import models.TiposPago;
import models.Usuario;
import utility.EstiloHorus;
import utility.Tabla;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdministrarTiposPago {

	public JFrame frmAdministrartipospago;
	private Tabla listapagos;
	private Usuario user;
	private JButton btnagregar;
	private JButton btneditar;
	private JButton btneliminar;
	private JButton btnVolver;
	public String ruta = new File ("").getAbsolutePath ();
	
	
	public AdministrarTiposPago(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmAdministrartipospago = new JFrame();
		frmAdministrartipospago.setResizable(false);
		frmAdministrartipospago.setTitle("Administrar Tipos de Pago");
		frmAdministrartipospago.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAdministrartipospago.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrartipospago.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		frmAdministrartipospago.setBounds(100, 100, 547, 280);
		frmAdministrartipospago.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAdministrartipospago.getSize();  
        frmAdministrartipospago.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		frmAdministrartipospago.getContentPane().setLayout(null);
		
		EstiloHorus tema = new EstiloHorus();
		
		String campos[] = {"ID","DESCRIPCIÓN", "ESTADO"};
		int ancho[] = {1,150,1};
		int editable[] = null;
		TiposPago Otipospago = new TiposPago();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(34, 45, 346, 177);
		listapagos = new Tabla(campos, editable, ancho, null);
		listapagos.Listar(Otipospago.ListarTiposPago());
		Otipospago.desconectar();
		
		frmAdministrartipospago.getContentPane().add(scrollPane);
		scrollPane.setViewportView(listapagos);
		
		btnagregar = new JButton("Agregar");
		btnagregar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnagregar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnagregar.setBackground(tema.getAzul());
			}
		});
		btnagregar.setForeground(Color.WHITE);
		btnagregar.setBackground(tema.getAzul());
		btnagregar.setBorderPainted(false);
		btnagregar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		btnagregar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					RegistroTiposPago ventana = new RegistroTiposPago(0, user);
					ventana.frmtiposdepago.setVisible(true);;
					frmAdministrartipospago.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnagregar.setBounds(390, 46, 123, 35);
		frmAdministrartipospago.getContentPane().add(btnagregar);
		
		btneditar = new JButton("Editar");
		btneditar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btneditar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btneditar.setBackground(tema.getAzul());
			}
		});
		btneditar.setForeground(Color.WHITE);
		btneditar.setBackground(tema.getAzul());
		btneditar.setBorderPainted(false);
		btneditar.setIcon(new ImageIcon(ruta+"/images/edit.png"));
		btneditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listapagos.getSelectedRow() != -1){
					int id = Integer.parseInt(listapagos.getValueAt(listapagos.getSelectedRow(), 0).toString());
					try {
						RegistroTiposPago ventana = new RegistroTiposPago(id, user);
						ventana.frmtiposdepago.setVisible(true);;
						frmAdministrartipospago.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un tipo de pago para editar","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btneditar.setBounds(390, 92, 123, 35);
		frmAdministrartipospago.getContentPane().add(btneditar);
		
		btneliminar = new JButton("Estado");
		btneliminar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listapagos.getSelectedRow() != -1){
					String id = listapagos.getValueAt(listapagos.getSelectedRow(), 0).toString();
					try {
						listapagos.CambioEstadoTipoPago(id, listapagos.getSelectedRow(), 2);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un Tipo de pago para cambiar su Estado","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btneliminar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btneliminar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btneliminar.setBackground(tema.getAzul());
			}
		});
		btneliminar.setForeground(Color.WHITE);
		btneliminar.setBackground(tema.getAzul());
		btneliminar.setBorderPainted(false);
		btneliminar.setToolTipText("Eliminar Tipo de Pago");
		btneliminar.setIcon(new ImageIcon(ruta+"/images/del.png"));
		btneliminar.setBounds(390, 138, 123, 35);
		frmAdministrartipospago.getContentPane().add(btneliminar);
		
		btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.setBackground(tema.getAzul());
		btnVolver.setForeground(Color.WHITE);
		btnVolver.setBorderPainted(false);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrartipospago.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.setBounds(390, 186, 123, 35);
		frmAdministrartipospago.getContentPane().add(btnVolver);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 521, 233);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ADMINISTRAR TIPOS DE PAGO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAdministrartipospago.getContentPane().add(lblNewLabel);
	}

}
