package views;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import models.Presupuesto;
import models.Usuario;
import utility.AbstractJasperReports;
import utility.EstiloHorus;
import utility.Tabla;
import java.awt.Font;
import java.awt.Toolkit;
import java.sql.SQLException;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdministrarPresupuestos {

	public JFrame frmAdministrarFacturas;
	private JTextField textField;
	private Tabla listapresupuesto;
	public String ruta = new File ("").getAbsolutePath ();
	private Usuario user;
	
	public AdministrarPresupuestos(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmAdministrarFacturas = new JFrame();
		frmAdministrarFacturas.setResizable(false);
		frmAdministrarFacturas.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarFacturas.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		frmAdministrarFacturas.setTitle("Administrar Presupuestos");
		frmAdministrarFacturas.setBounds(100, 100, 901, 474);
		frmAdministrarFacturas.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmAdministrarFacturas.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAdministrarFacturas.getContentPane().setLayout(null);
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAdministrarFacturas.getSize();  
        frmAdministrarFacturas.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
        EstiloHorus tema = new EstiloHorus();
        
		JLabel lblBuscar = new JLabel("BUSCAR:");
		lblBuscar.setFont(new Font("Arial", Font.BOLD, 11));
		lblBuscar.setBounds(42, 41, 56, 14);
		frmAdministrarFacturas.getContentPane().add(lblBuscar);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				Presupuesto Opresupuesto = new Presupuesto();
				listapresupuesto.setClearGrid();
				try {
					listapresupuesto.Refrescar(Opresupuesto.FiltraPresupuestos(textField.getText().toString()));
					Opresupuesto.Desconectar();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || textField.getText().length() == 7 ){
		            evt.consume();
		        }
			}
		});
		textField.setToolTipText("Busqueda por N\u00B0 de Presupuesto");
		textField.setFont(new Font("Arial", Font.PLAIN, 11));
		textField.setColumns(10);
		textField.setBounds(98, 37, 346, 20);
		frmAdministrarFacturas.getContentPane().add(textField);
		
		String campos[] = {"N� PRESUPUESTO","RIF","RAZ�N SOCIAL","FECHA","VALIDEZ","F. VENCIMIENTO","PROCESADO POR:"};
		int ancho[] = {60,50,150,50,50,50,100};
		int editable[] = null;
		Presupuesto Opresupuesto = new Presupuesto();
		Opresupuesto.AnularVencidos();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(37, 68, 822, 292);
		listapresupuesto = new Tabla(campos, editable, ancho, null);
		listapresupuesto.Listar(Opresupuesto.ListarPresupuestos());
		Opresupuesto.Desconectar();
		
		frmAdministrarFacturas.getContentPane().add(scrollPane);
		scrollPane.setViewportView(listapresupuesto);
		
		JButton btnAnular = new JButton("Anular");
		btnAnular.setForeground(Color.WHITE);
		btnAnular.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnAnular.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnAnular.setBackground(tema.getAzul());
			}
		});
		btnAnular.setBackground(tema.getAzul());
		btnAnular.setBorderPainted(false);
		btnAnular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listapresupuesto.getSelectedRow() != -1){
					AnularPresupuesto ventana = new AnularPresupuesto(Integer.parseInt(listapresupuesto.getValueAt(listapresupuesto.getSelectedRow(), 0).toString()), user);
					ventana.frmAnularPresupuesto.setVisible(true);
					frmAdministrarFacturas.dispose();	 		
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un item para Anular","Advertencia" , JOptionPane.WARNING_MESSAGE,icono); 
				}
			}
		});
		btnAnular.setIcon(new ImageIcon(ruta+"/images/break.png"));
		btnAnular.setBounds(75, 376, 164, 44);
		frmAdministrarFacturas.getContentPane().add(btnAnular);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setForeground(Color.WHITE);
		btnVolver.setBackground(tema.getAzul());
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.setBorderPainted(false);
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Principal ventana;
				try {
					ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarFacturas.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
		});
		btnVolver.setBounds(649, 376, 164, 44);
		frmAdministrarFacturas.getContentPane().add(btnVolver);
		
		JButton btnReimprimir = new JButton("Reimprimir");
		btnReimprimir.setForeground(Color.WHITE);
		btnReimprimir.setBackground(tema.getAzul());
		btnReimprimir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnReimprimir.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnReimprimir.setBackground(tema.getAzul());
			}
		});
		btnReimprimir.setBorderPainted(false);
		btnReimprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listapresupuesto.getSelectedRow() != -1){
					int id = Integer.parseInt(listapresupuesto.getValueAt(listapresupuesto.getSelectedRow(), 0).toString());
					String nro = listapresupuesto.getValueAt(listapresupuesto.getSelectedRow(), 0).toString();
					Presupuesto Opresupuesto = new Presupuesto();
					try {
						Opresupuesto.TraerRegistro(Opresupuesto.RetornaIdCliente(id+""));
						AbstractJasperReports.createPresupuesto(ruta+"/reports/presupuesto/", id, nro, Opresupuesto.getRif(),Opresupuesto.getRazonSocial(), Opresupuesto.getDireccion(), Opresupuesto.getTelefono(), Opresupuesto.RetornaValidez(id), ruta);
						AbstractJasperReports.showViewer();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} 
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un item para Reimprimir","Advertencia" , JOptionPane.WARNING_MESSAGE,icono); 
				}
			}
		});
		btnReimprimir.setIcon(new ImageIcon(ruta+"/images/printer.png"));
		btnReimprimir.setBounds(459, 376, 164, 44);
		frmAdministrarFacturas.getContentPane().add(btnReimprimir);
		
		JButton button = new JButton("Facturar");
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if (listapresupuesto.getSelectedRow() != -1){
					VentasDirectas ventana;
					try {
						ventana = new VentasDirectas(user, Integer.parseInt(listapresupuesto.getValueAt(listapresupuesto.getSelectedRow(), 0).toString()),2);
						ventana.frmVentasDirectas.setVisible(true);
					} catch (NumberFormatException | SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					frmAdministrarFacturas.dispose();	 		
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un item para Facturar","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		button.setIcon(new ImageIcon(ruta+"/images/add.png"));
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				button.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button.setBackground(tema.getAzul());
			}
		});
		button.setForeground(Color.WHITE);
		button.setBorderPainted(false);
		button.setBackground(tema.getAzul());
		button.setBounds(267, 376, 164, 44);
		frmAdministrarFacturas.getContentPane().add(button);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 875, 428);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ADMINISTRAR PRESUPUESTOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAdministrarFacturas.getContentPane().add(lblNewLabel);
	}
}
