package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JTextField;

import models.Modulo;
import models.Usuario;
import utility.EstiloHorus;

import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.SQLException;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RegistroModudo {

	public JFrame frmRegistrarMdulo;
	private JTextField textField;
	private JLabel lblDescripcin;
	public String ruta = new File ("").getAbsolutePath ();
	private Usuario user;
	private int modulo;

	public RegistroModudo(int modulo, Usuario user) throws SQLException {
		this.user = user;
		this.modulo = modulo;
		initialize();
	}
	
	public void Ejecutar() throws SQLException{
		if(!textField.getText().equals("")){
			Modulo Omodulo = new Modulo(textField.getText());
			boolean status = false;
			if(modulo == 0){
				status = Omodulo.Registrar();
			}else{
				status = Omodulo.Editar(modulo);
			}
			if(status == true){
				textField.setText("");
				AdministrarModulos ventana = new AdministrarModulos(user);
				ventana.frmAdministrarMdulos.setVisible(true);
				frmRegistrarMdulo.dispose();
			}
		}else{
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "Hay campos vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
		}
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmRegistrarMdulo = new JFrame();
		frmRegistrarMdulo.setResizable(false);
		if(modulo == 0){
			frmRegistrarMdulo.setTitle("Registro de M\u00F3dulos");
		}else{
			frmRegistrarMdulo.setTitle("Modificar M\u00F3dulo");
		}
		frmRegistrarMdulo.setTitle("Registro de M\u00F3dulos");
		frmRegistrarMdulo.setBounds(100, 100, 444, 159);
		frmRegistrarMdulo.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmRegistrarMdulo.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmRegistrarMdulo.getContentPane().setLayout(null);
		frmRegistrarMdulo.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					AdministrarModulos ventana = new AdministrarModulos(user);
					ventana.frmAdministrarMdulos.setVisible(true);
					frmRegistrarMdulo.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmRegistrarMdulo.getSize();  
        frmRegistrarMdulo.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
        
		EstiloHorus tema = new EstiloHorus();
        
        Modulo Omudulo = new Modulo();
        Omudulo.TraerRegistro(modulo);
        	
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				textField.nextFocus();
			}
		});
		textField.setColumns(10);
		textField.setBounds(120, 39, 287, 20);
		textField.setText(Omudulo.getDescripcion());
		frmRegistrarMdulo.getContentPane().add(textField);
		
		lblDescripcin = new JLabel("DESCRIPCI\u00D3N:");
		lblDescripcin.setFont(new Font("Arial", Font.BOLD, 11));
		lblDescripcin.setBounds(36, 42, 74, 14);
		frmRegistrarMdulo.getContentPane().add(lblDescripcin);
		
		String nombre_boton = "";
		if (modulo == 0){
			nombre_boton = "Registrar";
		}else{
			nombre_boton = "Guardar";
		}
		
		JButton btnRegistrar = new JButton(nombre_boton);
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnRegistrar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnRegistrar.setBackground(tema.getAzul());
			}
		});
		btnRegistrar.setForeground(Color.WHITE);
		btnRegistrar.setBackground(tema.getAzul());
		btnRegistrar.setBorderPainted(false);
		btnRegistrar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		            try {
						Ejecutar();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }	
			}
		});
		if(modulo == 0){
			btnRegistrar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		}else{
			btnRegistrar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		}
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Ejecutar();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnRegistrar.setBounds(77, 70, 120, 36);
		frmRegistrarMdulo.getContentPane().add(btnRegistrar);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.setBackground(tema.getAzul());
		btnVolver.setForeground(Color.WHITE);
		btnVolver.setBorderPainted(false);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					AdministrarModulos ventana = new AdministrarModulos(user);
					ventana.frmAdministrarMdulos.setVisible(true);
					frmRegistrarMdulo.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.setBounds(244, 70, 120, 36);
		frmRegistrarMdulo.getContentPane().add(btnVolver);
		
		String titulo;
		if (modulo == 0){
			titulo = "REGISTRO DE M�DULOS";
		}else{
			titulo = "MODIFICAR M�DULO";
		}
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 421, 111);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, titulo, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmRegistrarMdulo.getContentPane().add(lblNewLabel);
	}
}
