package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JSpinnerDateEditor;

import utility.AbstractJasperReports;
import utility.Conexion;
import utility.EstiloHorus;

import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class BusquedaNotasCredito {

	public JFrame frmBuscarNotasCredito;

	public BusquedaNotasCredito() {
		initialize();
	}
	
	public double transformaDouble(String cifra){
		double total = 0;
		if ( !((cifra == null) || (cifra.equals(""))) ) {
			total = Double.parseDouble(cifra);
		}
		return total;
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		String ruta = new File ("").getAbsolutePath ();
		frmBuscarNotasCredito = new JFrame();
		frmBuscarNotasCredito.setTitle("Buscar Notas de Cr\u00E9dito");
		frmBuscarNotasCredito.setBounds(100, 100, 351, 190);
		frmBuscarNotasCredito.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmBuscarNotasCredito.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmBuscarNotasCredito.getContentPane().setLayout(null);
		frmBuscarNotasCredito.setResizable(false);
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmBuscarNotasCredito.getSize();  
        frmBuscarNotasCredito.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);
		
        EstiloHorus tema = new EstiloHorus();
        
		 JLabel lblFechaInicio = new JLabel("FECHA INICIO:");
		lblFechaInicio.setFont(new Font("Arial", Font.BOLD, 11));
		lblFechaInicio.setBounds(35, 49, 86, 14);
		frmBuscarNotasCredito.getContentPane().add(lblFechaInicio);
		
		JDateChooser inicio  = new JDateChooser(null, null, null, new JSpinnerDateEditor()); 
		inicio.setBounds(115, 43, 206, 20);
		frmBuscarNotasCredito.getContentPane().add(inicio);
		
		JLabel lblFechaFin = new JLabel("FECHA FIN:");
		lblFechaFin.setFont(new Font("Arial", Font.BOLD, 11));
		lblFechaFin.setBounds(51, 80, 65, 14);
		frmBuscarNotasCredito.getContentPane().add(lblFechaFin);
		
		JDateChooser fin  = new JDateChooser(null, null, null, new JSpinnerDateEditor()); 
		fin.setBounds(115, 74, 206, 20);
		frmBuscarNotasCredito.getContentPane().add(fin);
		
		JButton buscar = new JButton("Buscar");
		buscar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				buscar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				buscar.setBackground(tema.getAzul());
			}
		});
		buscar.setForeground(Color.WHITE);
		buscar.setBackground(tema.getAzul());
		buscar.setBorderPainted(false);
		buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Conexion conn = new Conexion();
		   		if (inicio.getDate() != null && fin.getDate() != null){
		   			if(inicio.getDate().before(fin.getDate())){
		   				SimpleDateFormat formatter;
				   		formatter = new SimpleDateFormat("yyyy-MM-dd");
				   		String fecha_inicial = formatter.format(inicio.getDate());
		   				String fecha_final = formatter.format(fin.getDate());
		   				
		   				java.sql.Date inicial_consulta = new java.sql.Date(inicio.getDate().getTime());
		   				java.sql.Date final_consulta = new java.sql.Date(fin.getDate().getTime());
		   				
		   				String total_no_anulado = null;
						String busca_clave = "SELECT sum(ads_facturacion.total_pago) as no_anulado FROM ads_notascredito, ads_facturacion, ads_contrataciones  WHERE ads_notascredito.numero_documento = ads_facturacion.pk_id AND ads_facturacion.numero_documento = ads_contrataciones.pk_id  AND fecha_documento >= '"+fecha_inicial+"' AND fecha_documento <= '"+fecha_final+"'";
						try {
							ResultSet busca = conn.Consulta(busca_clave);
							if(busca.next()) {
								total_no_anulado = busca.getString("no_anulado");
								busca.close();
							}
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
		   				
						int total = 0;
						 busca_clave = "SELECT COUNT(*) as total FROM ads_notascredito, ads_facturacion, ads_contrataciones  WHERE ads_notascredito.numero_documento = ads_facturacion.pk_id AND ads_facturacion.numero_documento = ads_contrataciones.pk_id  AND fecha_documento >= '"+fecha_inicial+"' AND fecha_documento <= '"+fecha_final+"'";						try {
							ResultSet busca = conn.Consulta(busca_clave);
							if(busca.next()) {
								total  = Integer.parseInt(busca.getString("total"));
								busca.close();
							}
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
						 
						AbstractJasperReports.TodasNotas(conn.getConnection(), ruta+"/reports/listado notas/", ruta,  transformaDouble(total_no_anulado), total, inicial_consulta, final_consulta);
		   				AbstractJasperReports.showViewer();
			   		
		   			}else{
		   				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			   			JOptionPane.showMessageDialog(null, "La fecha Inicial es mayor a la fecha Final","Error" , JOptionPane.ERROR_MESSAGE, icono);
			   		}
		   		}else{
		   			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
		   			JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
		   		}
			}
		});
		buscar.setIcon(new ImageIcon(ruta+"/images/search.png"));
		buscar.setBounds(35, 105, 120, 36);
		frmBuscarNotasCredito.getContentPane().add(buscar);
		
		JButton volver = new JButton("Volver");
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				volver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				volver.setBackground(tema.getAzul());
			}
		});
		volver.setBackground(tema.getAzul());
		volver.setForeground(Color.WHITE);
		volver.setBorderPainted(false);
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmBuscarNotasCredito.dispose();
			}
		});
		volver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		volver.setBounds(198, 105, 120, 36);
		frmBuscarNotasCredito.getContentPane().add(volver);
		
		JLabel borde = new JLabel("");
		borde.setBounds(10, 11, 327, 144);
		borde.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "BUSCAR NOTAS DE CR�DITO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmBuscarNotasCredito.getContentPane().add(borde);
		
	}
}
