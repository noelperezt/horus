package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import models.ADSProductos;
import models.Usuario;
import utility.Conexion;
import utility.EstiloHorus;
import utility.Tabla;

import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ConfirmacionPago {

	public JFrame frmConfirmacinDePagos;
	private Tabla listapagos;
	private Usuario user;
	
	public ConfirmacionPago(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}
	
	public int retornaUserId(String name) throws SQLException{
		int user = 0;
		Conexion conn = new Conexion();
		String sentencia = "SELECT id_admin FROM tb_administrador WHERE usuario ='"+name+"'";
		
		ResultSet busca;
		busca = conn.Consulta(sentencia);
		
		if(busca.next()){
			user = Integer.parseInt(busca.getString("id_admin"));
			busca.close();
		}
		
		conn.desConectar();
		return user;
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		String ruta = new File ("").getAbsolutePath ();
		frmConfirmacinDePagos = new JFrame();
		frmConfirmacinDePagos.setResizable(false);
		frmConfirmacinDePagos.setTitle("Confirmaci\u00F3n de Pagos");
		frmConfirmacinDePagos.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmConfirmacinDePagos.setBounds(100, 100, 744, 518);
		frmConfirmacinDePagos.getContentPane().setLayout(null);
		frmConfirmacinDePagos.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmConfirmacinDePagos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmConfirmacinDePagos.getSize();  
        frmConfirmacinDePagos.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
        EstiloHorus tema = new EstiloHorus();
        
		String campos[] = {"N� CONTRATACI�N","FECHA DE PAGO","RECIBO","TOTAL","SELECCIONAR"};
		int ancho[] = {1,1,1,1,1};
		int editable[] = {4};
		ADSProductos Oadsproductos = new ADSProductos();
		Usuario Ousuario = new Usuario();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(38, 49, 662, 356);
		listapagos = new Tabla(campos, editable, ancho, null);
		listapagos.UltimaColumnaBoolean(Oadsproductos.ListarPendientePago(), false);
		Oadsproductos.desconectar();
		scrollPane.setViewportView(listapagos);
		frmConfirmacinDePagos.getContentPane().add(scrollPane);
		
		JButton btnConfirmar = new JButton("Confirmar");
		btnConfirmar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnConfirmar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnConfirmar.setBackground(tema.getAzul());
			}
		});
		btnConfirmar.setBackground(tema.getAzul());
		btnConfirmar.setBorderPainted(false);
		btnConfirmar.setForeground(Color.WHITE);
		btnConfirmar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int contador = 0;
				for (int i = 0; i < listapagos.getRowCount(); i++){
					if (listapagos.getValueAt(i,4).toString() == "true"){ 	
						contador = contador + 1;
					}
				}
				if (contador == 0){
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar al menos un item para confirmar el pago","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}else{
					boolean status = false;
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
					if(JOptionPane.showConfirmDialog(null, "�Est� seguro de confirmar los pagos seleccionados?","Confirmar Pagos",1,0,icono)==0){
						for (int i = 0; i < listapagos.getRowCount(); i++){
							if (listapagos.getValueAt(i,4).toString() == "true"){ 
								int contratacion = Integer.parseInt(listapagos.getValueAt(i,0).toString());
								try {
									status = Oadsproductos.VerificarPago(contratacion, Ousuario.RetornaIdUsuario(user.getUsuario()));
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								listapagos.deleteRow(i);
								i = i -1;
							}
						}
						if (status == true){
							icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/listo.png"));
							JOptionPane.showMessageDialog(null, "Pagos confirmados con �xito","�xito" , JOptionPane.INFORMATION_MESSAGE, icono);
						}
					}  
				}	
			}
		});
		btnConfirmar.setIcon(new ImageIcon(ruta+"/images/check.png"));
		btnConfirmar.setBounds(152, 420, 164, 44);
		frmConfirmacinDePagos.getContentPane().add(btnConfirmar);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.setBorderPainted(false);
		btnVolver.setBackground(tema.getAzul());
		btnVolver.setForeground(Color.WHITE);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Principal ventana;
				try {
					ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				frmConfirmacinDePagos.dispose();
			}
		});
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.setBounds(413, 420, 164, 44);
		frmConfirmacinDePagos.getContentPane().add(btnVolver);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 718, 471);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CONFIMACI�N DE PAGOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmConfirmacinDePagos.getContentPane().add(lblNewLabel);
	}
}
