package views;

import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import com.itextpdf.text.Image;
import models.ADSProductos;
import models.Modulo;
import models.Submodulo;
import models.Usuario;
import utility.AbstractJasperReports;
import utility.Conexion;
import utility.Ldescuento;
import utility.Lpermiso;
import utility.Npermiso;
import java.awt.event.ActionListener;
import java.io.File;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.JLabel;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Font;
import java.awt.Color;
import javax.swing.SwingConstants;

@SuppressWarnings("serial")
public class Principal extends JFrame implements ActionListener {

	public JFrame frmSistemaAdministrativoPinttosoft;
	private JLabel label_1;
	private Usuario user;
	String ruta = new File ("").getAbsolutePath ();
	boolean descuento;
	
	public Principal(Usuario user) throws SQLException {
		javax.swing.Timer timer = new javax.swing.Timer(100, this);
        timer.start();
        this.user = user;
		initialize();
	}

	
	/* Extrae el nombre del modulo o submodulo segun su id
	 * @param lista Recibe lista con los permisos del usuario
	 * @param modulo id del modulo
	 * @return devuelve el nombre del modulo
	 */
	public String Nombre(Lpermiso lista, int modulo){
		String nombre = null;
		Npermiso actual;
		actual = lista.Tope();
		while(actual != null){
			if(actual.GetId() == modulo){
				nombre = actual.GetDescripcion();
				break;
			}	
			actual = actual.GetSig();
		}
		return nombre;
	}
	
	/* Retorna si el submodulo esta inactivo a partir de su configuracion principal
	 * @param lista Recibe lista con los permisos del usuario
	 * @param modulo id del submodulo
	 * @return devuelve si el modulo es visible o no 
	 */
	public boolean SubmoduloActivo(Lpermiso lista, int modulo) throws SQLException{
		boolean estado;
		String status = null;
		Npermiso actual;
		actual = lista.Tope();
		while(actual != null){
			if(actual.GetId() == modulo){
				status = actual.GetEstado();
				break;
			}	
			actual = actual.GetSig();
		}
		if(status.equals("A")){
			estado = true;
		}else{
			estado = false;
		}
		return estado;
	}
	
	/* Retorna si el modulo es visible a partir de su configuracion principal
	 * @param lista Recibe lista con los permisos del usuario
	 * @param modulo id del modulo
	 * @return devuelve si el modulo es visible o no 
	 */
	public boolean Visible(Lpermiso lista, int modulo) throws SQLException{
		boolean estado;
		String status = null;
		Npermiso actual;
		actual = lista.Tope();
		while(actual != null){
			if(actual.GetId() == modulo){
				status = actual.GetEstado();
				break;
			}	
			actual = actual.GetSig();
		}
		if(status.equals("S")){
			estado = true;
		}else{
			estado = false;
		}
		return estado;
	}
	
	/* Consulta si el usuario tiene submodulos activos para no mostrar solamente el nombre del padre en el menu
	 * @param lista Recibe lista con los permisos del usuario
	 * @param modulo id del modulo
	 * @return devuelve si hay modulos activos o no
	 */
	public boolean SubModulosActivos(int modulo, Lpermiso lista) throws SQLException{
		boolean estado = false;
		Npermiso actual = lista.Tope();
		while(actual != null){
			if(modulo == actual.GetId()){
				estado = true;
				break;
			}
			actual = actual.GetSig();
		}
		return estado;
	}
	
	/* Devuelve si el usuario posee permiso de acceso para el modulo
	 * @param lista Recibe lista con los permisos del usuario
	 * @param modulo id del modulo
	 * @return devuelve si el usuario tiene permiso o no
	 */
	public boolean Permiso(Lpermiso lista, int submodulo) throws SQLException{
		boolean estado = false;
		String status = null;
		Npermiso actual = lista.Tope();
		while(actual != null){
			if(actual.GetId() == submodulo){
				status = actual.GetEstado();
				break;
			}
			actual = actual.GetSig();
		}
		if (status != null){
			if(status.equals("A")){
				estado = true;
			}else{
				estado = false;
			}
		}
		return estado;
	}
	
	private void initialize() throws SQLException {
		int modulo = 0;
		int submodulo = 0;
		Usuario Ousuario = new Usuario();
		int id_usuario = Ousuario.RetornaIdUsuario(user.getUsuario());
       
		Submodulo Osubmodulo = new Submodulo();
        Lpermiso submodulosactivos = Osubmodulo.SubmodulosActivos();
        Modulo Omodulo = new Modulo();
		Lpermiso visibilidad = Omodulo.Visibilidad();
		Lpermiso ModulosActivos = Omodulo.ModulosActivos(id_usuario);
		models.Permiso Opermiso = new models.Permiso();
		Lpermiso Permisos = Opermiso.Permisos(id_usuario);
		
        ADSProductos OADS = new ADSProductos();
        int pendiente = OADS.CuentaContratacionesPendientes();
        String texto = null;
        
		frmSistemaAdministrativoPinttosoft = new JFrame();
		frmSistemaAdministrativoPinttosoft.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmSistemaAdministrativoPinttosoft.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
				if(JOptionPane.showConfirmDialog(null, "�Est� seguro de cerrar el Sistema?","Cerrar Sistema",1,0,icono)==0){
		            System.exit(0);
		        }
			}
		});
		frmSistemaAdministrativoPinttosoft.setTitle("Sistema Administrativo Horus v1.0");
		frmSistemaAdministrativoPinttosoft.setBounds(100, 100, 847, 537);
		frmSistemaAdministrativoPinttosoft.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmSistemaAdministrativoPinttosoft.getContentPane().setLayout(null);
		frmSistemaAdministrativoPinttosoft.setResizable(false);
		
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmSistemaAdministrativoPinttosoft.getSize();  
        frmSistemaAdministrativoPinttosoft.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);
		
        if (pendiente == 0){
        	texto = "No Hay Contrataciones Pendientes por Facturar                                                                                                                              ";
        }else{
        	if (pendiente == 1){
        		texto = "Tiene ("+pendiente+") Contrataci�n pendiente por Facturar                                                                                                                               ";
        	}else{
        		texto = "Tiene ("+pendiente+") Contrataciones pendientes por Facturar                                                                                                                            ";
        	}	
        }
        
		label_1 = new JLabel(texto);
		if (pendiente == 0){
			label_1.setForeground(Color.WHITE);
        }else{
        	label_1.setForeground(new Color(255, 216, 41));
        }
		label_1.setFont(new Font("Tahoma", Font.BOLD, 12));
		label_1.setBounds(20, 474, 800, 26);
		frmSistemaAdministrativoPinttosoft.getContentPane().add(label_1);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBounds(0, 0, 841, 21);
		frmSistemaAdministrativoPinttosoft.getContentPane().add(menuBar);	
		
		modulo = 1;
		if(Visible(visibilidad,modulo)){
			if(SubModulosActivos(modulo, ModulosActivos)){
				JMenu mnNewMenu = new JMenu(Nombre(visibilidad, modulo));
				mnNewMenu.setIcon(new ImageIcon(ruta+"/images/coins.png"));
				menuBar.add(mnNewMenu);
				submodulo = 1;
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenuItem mntmNewMenuItema = new JMenuItem(Nombre(Permisos, submodulo));
						mntmNewMenuItema.setIcon(new ImageIcon(ruta+"/images/pendiente.png"));
						submodulo = 2;
						if(SubmoduloActivo(submodulosactivos, submodulo)){
							if(Permiso(Permisos, submodulo)){
								descuento = true;
							}else{
								descuento = false;
							}
						}else{
							descuento = false;
						}
						mntmNewMenuItema.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								ListadoFacturas factura;
								try {
									Ldescuento lista = new Ldescuento();
									factura = new ListadoFacturas(lista,user,descuento);
									factura.frmFacturasPendientes.setVisible(true);
									frmSistemaAdministrativoPinttosoft.dispose();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								
							}
						});
						mnNewMenu.add(mntmNewMenuItema);
					}
				}
				
				submodulo = 3;
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenuItem mntmNewMenuItemb = new JMenuItem(Nombre(Permisos, submodulo));
						mntmNewMenuItemb.setIcon(new ImageIcon(ruta+"/images/cart.png"));
						mntmNewMenuItemb.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								VentasDirectas ventas;
								try {
									ventas = new VentasDirectas(user,0,1);
									ventas.frmVentasDirectas.setVisible(true);
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								frmSistemaAdministrativoPinttosoft.dispose();
							}
						});
						mnNewMenu.add(mntmNewMenuItemb);
					}
				}
				
				
				submodulo = 4;
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenuItem mntmNewMenuItemc = new JMenuItem(Nombre(Permisos, submodulo));
						mntmNewMenuItemc.setIcon(new ImageIcon(ruta+"/images/presupuesto.png"));
						mntmNewMenuItemc.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								RegistroPresupuesto presupuesto;
								try {
									presupuesto = new RegistroPresupuesto(user);
									presupuesto.frmPresupuestos.setVisible(true);
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								frmSistemaAdministrativoPinttosoft.dispose();
							}
						});
						mnNewMenu.add(mntmNewMenuItemc);
					}
				}
			}
		}
		
		modulo = 2;
		if(Visible(visibilidad,modulo)){
			if(SubModulosActivos(modulo, ModulosActivos)){
				JMenu mnFacturas = new JMenu(Nombre(visibilidad, modulo));
				mnFacturas.setIcon(new ImageIcon(ruta+"/images/ico_carpeta.png"));
				menuBar.add(mnFacturas);
		
				submodulo = 5;	
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenuItem mntmNewMenuItemd = new JMenuItem(Nombre(Permisos, submodulo));
						mntmNewMenuItemd.setIcon(new ImageIcon(ruta+"/images/ico_factura.png"));
						mntmNewMenuItemd.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								try {
									AdministrarFacturas ventana = new AdministrarFacturas(user);
									ventana.frmAdministrarFacturas.setVisible(true);
									frmSistemaAdministrativoPinttosoft.dispose();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
						});
						mnFacturas.add(mntmNewMenuItemd);
					}
				}
				
				submodulo = 6;	
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenuItem mntmNewMenuIteme = new JMenuItem(Nombre(Permisos, submodulo));
						mntmNewMenuIteme.setIcon(new ImageIcon(ruta+"/images/credito.png"));
						mntmNewMenuIteme.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								try {
									AdministrarNotas ventana = new AdministrarNotas(user);
									ventana.frmAdministrarNotas.setVisible(true);
									frmSistemaAdministrativoPinttosoft.dispose();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
						});
						mnFacturas.add(mntmNewMenuIteme);
					}
				}
				
				
				submodulo = 7;	
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenuItem mntmNewMenuItemf = new JMenuItem(Nombre(Permisos, submodulo));
						mntmNewMenuItemf.setIcon(new ImageIcon(ruta+"/images/presupuesto.png"));
						mntmNewMenuItemf.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								try {
									AdministrarPresupuestos ventana = new AdministrarPresupuestos(user);
									ventana.frmAdministrarFacturas.setVisible(true);
									frmSistemaAdministrativoPinttosoft.dispose();
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
							}
						});
						mnFacturas.add(mntmNewMenuItemf);
					}	
				}
			}
		}
		
		modulo = 3;	
		if(Visible(visibilidad,modulo)){
			if(SubModulosActivos(modulo, ModulosActivos)){
				JMenu mnNewMenu_1 = new JMenu(Nombre(visibilidad, modulo));
				mnNewMenu_1.setIcon(new ImageIcon(ruta+"/images/ico_grupo.png"));
				menuBar.add(mnNewMenu_1);
				
				submodulo = 8;
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenuItem mntmNewMenuItemg = new JMenuItem(Nombre(Permisos, submodulo));
						mntmNewMenuItemg.setIcon(new ImageIcon(ruta+"/images/tool.png"));
						mntmNewMenuItemg.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								try {
									AdministrarClientes ventana = new AdministrarClientes(user);
									ventana.frmAdministrarClientes.setVisible(true);	
									frmSistemaAdministrativoPinttosoft.dispose();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
								
							}
						});
						mnNewMenu_1.add(mntmNewMenuItemg);
					}
				}
			}	
		}
		
		modulo = 4;
		if(Visible(visibilidad,modulo)){
			if(SubModulosActivos(modulo, ModulosActivos)){
				JMenu mnReportes = new JMenu(Nombre(visibilidad, modulo));
				mnReportes.setIcon(new ImageIcon(ruta+"/images/report.png"));
				menuBar.add(mnReportes);
				
				submodulo = 9;
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenu mnNewMenuh = new JMenu(Nombre(Permisos, submodulo));
						mnNewMenuh.setIcon(new ImageIcon(ruta+"/images/ico_grupo.png"));
						mnReportes.add(mnNewMenuh);
						
						JMenuItem mntmNewMenuItem_2 = new JMenuItem("Registrados en Dumga");
						mntmNewMenuItem_2.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent e) {
								Conexion conn = new Conexion();
								AbstractJasperReports.RegistradosDumga(conn.getConnection(), ruta+"/reports/registrados dumga/", ruta);
								AbstractJasperReports.showViewer();
								try {
									conn.desConectar();
								} catch (SQLException e1) {
									// TODO Auto-generated catch block
									e1.printStackTrace();
								}
							}
						});
						mnNewMenuh.add(mntmNewMenuItem_2);
						
						JMenuItem mntmNewMenuItem_3 = new JMenuItem("Registrados Manuales");
						mntmNewMenuItem_3.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								Conexion conn = new Conexion();
								AbstractJasperReports.RegistradosClientes(conn.getConnection(), ruta+"/reports/clientes registrados/", ruta);
								AbstractJasperReports.showViewer();
								try {
									conn.desConectar();
								} catch (SQLException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
							}
						});
						mnNewMenuh.add(mntmNewMenuItem_3);
					}
				}
				
				submodulo = 10;
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenuItem mntmFacturasi = new JMenuItem(Nombre(Permisos, submodulo));
						mntmFacturasi.setIcon(new ImageIcon(ruta+"/images/ico_factura.png"));
						mntmFacturasi.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								BusquedaFactura buscar = new BusquedaFactura();
								buscar.frmBuscarFacturas.setVisible(true);
							}
						});
						mnReportes.add(mntmFacturasi);
					}
				}
				
				submodulo = 11;
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenuItem mntmCredito = new JMenuItem(Nombre(Permisos, submodulo));
						mntmCredito.setIcon(new ImageIcon(ruta+"/images/credito.png"));
						mntmCredito.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								BusquedaNotasCredito buscar = new BusquedaNotasCredito();
								buscar.frmBuscarNotasCredito.setVisible(true);
							}
						});
						mnReportes.add(mntmCredito);	
					}
				}
				
				
				submodulo = 12;
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenuItem mntmpresupuesto = new JMenuItem(Nombre(Permisos, submodulo));
						mntmpresupuesto.setIcon(new ImageIcon(ruta+"/images/presupuesto.png"));
						mntmpresupuesto.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								BusquedaPresupuesto buscar = new BusquedaPresupuesto();
								buscar.frmBuscarPresupuestos.setVisible(true);
							}
						});
						mnReportes.add(mntmpresupuesto);
					}	
				}
				
				
				submodulo = 13;
				if(SubmoduloActivo(submodulosactivos, submodulo)){
					if(Permiso(Permisos, submodulo)){
						JMenuItem mntmContrataciones = new JMenuItem(Nombre(Permisos, submodulo));
						mntmContrataciones.setIcon(new ImageIcon(ruta+"/images/money.png"));
						mntmContrataciones.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								BusquedaContrataciones buscar = new BusquedaContrataciones();
								buscar.frmBuscarContrataciones.setVisible(true);
							}
						});
						mnReportes.add(mntmContrataciones);
					}	
				}	
			}
		}
		
		modulo = 5;
		if(Visible(visibilidad,modulo)){
			JMenu mnUsuarios = new JMenu(Nombre(visibilidad, modulo));
			mnUsuarios.setIcon(new ImageIcon(ruta+"/images/ico_configuracion.png"));
			menuBar.add(mnUsuarios);
			
			submodulo = 14;
			if(SubmoduloActivo(submodulosactivos, submodulo)){
				if(Permiso(Permisos, submodulo)){
					JMenuItem mntmAdministrar = new JMenuItem(Nombre(Permisos, submodulo));
					mntmAdministrar.setIcon(new ImageIcon(ruta+"/images/user.png"));
					mntmAdministrar.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								AdministrarUsuarios ventana = new AdministrarUsuarios(user);
								ventana.frmAdministrarUsuarios.setVisible(true);
								frmSistemaAdministrativoPinttosoft.dispose();
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
					mnUsuarios.add(mntmAdministrar);
				}
			}
			
			submodulo = 15;
			if(SubmoduloActivo(submodulosactivos, submodulo)){
				if(Permiso(Permisos, submodulo)){
					JMenuItem mntmNewMenuItem_5 = new JMenuItem(Nombre(Permisos, submodulo));
					mntmNewMenuItem_5.setIcon(new ImageIcon(ruta+"/images/modulo.png"));
					mntmNewMenuItem_5.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							AdministrarModulos ventana;
							try {
								ventana = new AdministrarModulos(user);
								ventana.frmAdministrarMdulos.setVisible(true);
								frmSistemaAdministrativoPinttosoft.dispose();
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
					mnUsuarios.add(mntmNewMenuItem_5);
				}
			}
			
			submodulo = 16;
			if(SubmoduloActivo(submodulosactivos, submodulo)){
				if(Permiso(Permisos, submodulo)){
					JMenuItem mntmNewMenuItem_7 = new JMenuItem(Nombre(Permisos, submodulo));
					mntmNewMenuItem_7.setIcon(new ImageIcon(ruta+"/images/submodulo.png"));
					mntmNewMenuItem_7.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								AdministrarSubmodulos ventana = new AdministrarSubmodulos(user);
								ventana.frmAdministrarSubmdulos.setVisible(true);
								frmSistemaAdministrativoPinttosoft.dispose();
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
					mnUsuarios.add(mntmNewMenuItem_7);
				}
			}
			
			
			submodulo = 17;
			if(SubmoduloActivo(submodulosactivos, submodulo)){
				if(Permiso(Permisos, submodulo)){
					JMenuItem mntmNewMenuItem_8 = new JMenuItem(Nombre(Permisos, submodulo));
					mntmNewMenuItem_8.setIcon(new ImageIcon(ruta+"/images/bank.png"));
					mntmNewMenuItem_8.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								AdministrarBancos ventana = new AdministrarBancos(user);
								ventana.frmAdministrarBancos.setVisible(true);
								frmSistemaAdministrativoPinttosoft.dispose();
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							
						}
					});
					mnUsuarios.add(mntmNewMenuItem_8);
				}
			}
			
			
			submodulo = 18;
			if(SubmoduloActivo(submodulosactivos, submodulo)){
				if(Permiso(Permisos, submodulo)){
					JMenuItem mntmNewMenuItem_9 = new JMenuItem(Nombre(Permisos, submodulo));
					mntmNewMenuItem_9.setIcon(new ImageIcon(ruta+"/images/concepto.png"));
					mntmNewMenuItem_9.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								AdministrarConceptos ventana = new AdministrarConceptos(user);
								ventana.frmAdministrarConceptosFacturables.setVisible(true);
								frmSistemaAdministrativoPinttosoft.dispose();
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
					mnUsuarios.add(mntmNewMenuItem_9);
				}
			}
			
			
			submodulo = 19;
			if(SubmoduloActivo(submodulosactivos, submodulo)){
				if(Permiso(Permisos, submodulo)){
					JMenuItem mntmNewMenuItem_85 = new JMenuItem(Nombre(Permisos, submodulo));
					mntmNewMenuItem_85.setIcon(new ImageIcon(ruta+"/images/coin_gold.png"));
					mntmNewMenuItem_85.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								AdministrarTiposPago ventana = new AdministrarTiposPago(user);
								ventana.frmAdministrartipospago.setVisible(true);
								frmSistemaAdministrativoPinttosoft.dispose();
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
					mnUsuarios.add(mntmNewMenuItem_85);
				}
			}
			
			
			submodulo = 20;
			if(SubmoduloActivo(submodulosactivos, submodulo)){
				if(Permiso(Permisos, submodulo)){
					JMenuItem mntmNewMenuItem_42 = new JMenuItem(Nombre(Permisos, submodulo));
					mntmNewMenuItem_42.setIcon(new ImageIcon(ruta+"/images/ico_bien.png"));
					mntmNewMenuItem_42.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								ConfirmacionPago ventana = new ConfirmacionPago(user);
								ventana.frmConfirmacinDePagos.setVisible(true);
								frmSistemaAdministrativoPinttosoft.dispose();
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
					mnUsuarios.add(mntmNewMenuItem_42);
				}
			}
			
			
			submodulo = 21;
			if(SubmoduloActivo(submodulosactivos, submodulo)){
				if(Permiso(Permisos, submodulo)){
					JMenuItem mntmNewMenuItem_47 = new JMenuItem(Nombre(Permisos, submodulo));
					mntmNewMenuItem_47.setIcon(new ImageIcon(ruta+"/images/config.png"));
					mntmNewMenuItem_47.addActionListener(new ActionListener() {
						public void actionPerformed(ActionEvent e) {
							try {
								VistaPreferencias ventana = new VistaPreferencias(user);
								ventana.frmPreferencias.setVisible(true);
								frmSistemaAdministrativoPinttosoft.dispose();
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					});
					mnUsuarios.add(mntmNewMenuItem_47);
				}
			}
			
			
			JMenuItem mntmCambiarClave = new JMenuItem("Cambiar Contrase\u00F1a");
			mntmCambiarClave.setIcon(new ImageIcon(ruta+"/images/pass.png"));
			mntmCambiarClave.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {
					CambioClave ventana = new CambioClave(user);
					ventana.frmCambioDeClave.setVisible(true);
					frmSistemaAdministrativoPinttosoft.dispose();
				}
			});
			mnUsuarios.add(mntmCambiarClave);
		}
		
		JMenu mnNewMenu_2 = new JMenu("Salir");
		mnNewMenu_2.setIcon(new ImageIcon(ruta+"/images/ico_puerta_abierta.png"));
		menuBar.add(mnNewMenu_2);
		
		JMenuItem mntmCerrarSesin = new JMenuItem("Cerrar Sesi\u00F3n");
		mntmCerrarSesin.setIcon(new ImageIcon(ruta+"/images/ico_bloqueado.png"));
		mntmCerrarSesin.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
				if(JOptionPane.showConfirmDialog(null, "�Est� seguro de cerrar la sesi�n actual?","Cerrar sesi�n",1,0,icono)==0){
			        Login ini = new Login();
			        ini.frmIniciarSesin.setVisible(true);
			        frmSistemaAdministrativoPinttosoft.dispose();
			    }
			}
		});
		mnNewMenu_2.add(mntmCerrarSesin);
		
		JMenuItem mntmNewMenuItem_4 = new JMenuItem("Salir");
		mntmNewMenuItem_4.setIcon(new ImageIcon(ruta+"/images/ico_mal.png"));
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
				if(JOptionPane.showConfirmDialog(null, "�Est� seguro de cerrar el Sistema?","Cerrar Sistema",1,0,icono)==0){
		            System.exit(0);
		        }
			}
		});
		mnNewMenu_2.add(mntmNewMenuItem_4);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(60, 145, 719, 221);
		ImageIcon image = new ImageIcon(ruta+"/images/logo1.png");  
        Icon icono = new ImageIcon(image.getImage().getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(), Image.DEFAULT));
        
        JLabel label = new JLabel(user.getNombre());
        label.setBackground(Color.BLACK);
        label.setHorizontalAlignment(SwingConstants.RIGHT);
        label.setToolTipText("Sesi�n Iniciada por "+user.getUsuario());
        label.setIcon(new ImageIcon(ruta+"/images/edit_user.png"));
        label.setForeground(new Color(255, 255, 255));
        label.setFont(new Font("Tahoma", Font.BOLD, 12));
        label.setBounds(401, 32, 419, 26);
        frmSistemaAdministrativoPinttosoft.getContentPane().add(label);
        
        JLabel lblBienvenidoAPinttoadmin = new JLabel("Bienvenid@ a Horus!");
        lblBienvenidoAPinttoadmin.setIcon(new ImageIcon(ruta+"/images/admin.png"));
        lblBienvenidoAPinttoadmin.setForeground(Color.WHITE);
        lblBienvenidoAPinttoadmin.setFont(new Font("Tahoma", Font.BOLD, 12));
        lblBienvenidoAPinttoadmin.setBounds(20, 35, 211, 20);
        frmSistemaAdministrativoPinttosoft.getContentPane().add(lblBienvenidoAPinttoadmin);
        lblNewLabel.setIcon(icono);
		frmSistemaAdministrativoPinttosoft.getContentPane().add(lblNewLabel);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setOpaque(true);
		Color gris = new Color(76, 76, 76);
		lblNewLabel_1.setBackground(gris);
		lblNewLabel_1.setBounds(10, 474, 821, 26);
		frmSistemaAdministrativoPinttosoft.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("");
		lblNewLabel_2.setOpaque(true);
		Color color = new Color(13, 86, 180);
		lblNewLabel_2.setBackground(color);
		lblNewLabel_2.setBounds(10, 32, 821, 26);
		frmSistemaAdministrativoPinttosoft.getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("");
		lblNewLabel_3.setBounds(693, 399, 72, 64);
		ImageIcon image1 = new ImageIcon(ruta+"/images/pinttoadmin.png");  
        Icon icono1 = new ImageIcon(image1.getImage().getScaledInstance(lblNewLabel_3.getWidth(), lblNewLabel_3.getHeight(), Image.DEFAULT));
        lblNewLabel_3.setIcon(icono1);
        frmSistemaAdministrativoPinttosoft.getContentPane().add(lblNewLabel_3);
		
		JLabel lblHorusV = new JLabel("Horus v1.0");
		Color negro= new Color(76,76,76);
		lblHorusV.setForeground(negro);
		lblHorusV.setFont(new Font("Tahoma", Font.BOLD | Font.ITALIC, 12));
		lblHorusV.setBounds(748, 432, 72, 20);
		frmSistemaAdministrativoPinttosoft.getContentPane().add(lblHorusV);
		
		JLabel frame = new JLabel("");
		frame.setFont(new Font("Tempus Sans ITC", Font.BOLD | Font.ITALIC, 11));
		frame.setBounds(10, 62, 821, 407);
		frame.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12), new java.awt.Color(0, 102, 255)));
		frmSistemaAdministrativoPinttosoft.getContentPane().add(frame);
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		String oldText = label_1.getText();

        // Scroll right to left
        //String newText = oldText.substring(1) + oldText.substring(0, 1);

        // Scroll left to right
		int length = oldText.length();
		String newText = oldText.substring(length-1, length) + oldText.substring(0, length-1);
        label_1.setText( newText );
		
	}
}
