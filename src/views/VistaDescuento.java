package views;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import java.awt.Toolkit;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Locale;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Font;
import javax.swing.JTextField;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import models.Factura;
import models.Usuario;
import utility.Conexion;
import utility.EstiloHorus;
import utility.Ldescuento;
import utility.Ndescuento;

import javax.swing.JScrollPane;
import javax.swing.JSlider;
import javax.swing.JTable;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import javax.swing.JFormattedTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.SwingConstants;

public class VistaDescuento {

	public JFrame descuento;
	public JFrame frmAplicarDescuento;
	private JSlider slider;
	private boolean permiso_descuento;
	private JFormattedTextField sub;
	private JFormattedTextField iva;
	private JFormattedTextField total;
	private JTextField rif;
	private JTextField desc;
	private JTextField razon;
	private JTextField direc;
	private JTextField tel;
	private JTable table;
	private DefaultTableModel modelo;
	private int contratacion;
	private Ldescuento lista;
	private Usuario user;
	private double monto_sub;
	private double monto_iva;
	private double monto_total;
	
	public VistaDescuento(int contratacion, Usuario user, Ldescuento lista, boolean permiso_descuento) throws SQLException {
		setPermisoDescuento(permiso_descuento);
		this.user = user;
		setListaDescuento(lista);
		setContratacion(contratacion);
		initialize();
	}
	
	public int getContratacion(){
		return contratacion;
	}
	
	public void setContratacion(int contratacion){
		this.contratacion = contratacion;
	}

	public void vaciarTabla(){
	    while (modelo.getRowCount() > 0) modelo.removeRow(0);
	}  
	 
	public Ldescuento getListaDescuento(){
		return lista;
	}
		
	public void setListaDescuento(Ldescuento lista){
		this.lista = lista;
	}
	
	public void setPermisoDescuento(boolean permiso_descuento){
		this.permiso_descuento = permiso_descuento;
	}
	
	public boolean getPermisoDescuento(){
		return permiso_descuento;
	}
	
	public int BuscarDescuento(Ldescuento lista, int contratacion, int detalle){
		int descuento = 0;
		Ndescuento actual;
		actual = lista.Tope();
		while(actual != null){
			if(actual.GetId() == contratacion && actual.GetIdConcepto() == detalle){
				descuento = actual.GetDescuento();
				break;
			}	
			actual = actual.GetSig();
		}
		return descuento;
	}
	
	
	public void Totales() throws SQLException{
		Double total = 0.0;
		int descuento = 0;
		for (int i = 0; i < modelo.getRowCount(); i++){
			int cant = Integer.parseInt(modelo.getValueAt(i,1).toString());
			double precio_uni = Double.parseDouble(modelo.getValueAt(i,2).toString());
			int desc_item = Integer.parseInt(modelo.getValueAt(i,4).toString());
			total = total + (cant * precio_uni);
			descuento = descuento + desc_item;
		}
		Factura Ofactura = new Factura();
		monto_sub = total - ((total * descuento)/100);
		monto_iva = (monto_sub * Ofactura.retornaIva())/100;
		monto_total = monto_sub + monto_iva;
	}
	
	public void Actualizar(){
		sub.setValue(monto_sub);
		iva.setValue(monto_iva);
		total.setValue(monto_total);
	}
	
	public void rellenarTabla(){  
		 try {
			 Conexion con = new Conexion();
			 String sentencia = "SELECT ads_contrataciones_detalle.pk_id, cpm, precio_concepto, nombre_concepto FROM ads_contrataciones_detalle, ads_facturacion_conceptos WHERE ads_contrataciones_detalle.fk_concepto = ads_facturacion_conceptos.id_concepto AND fk_id_contratacion = "+contratacion;
	         ResultSet rs = con.Consulta(sentencia);              
	                                           
	         while(rs.next()){
	             Object[] fila = new Object[5];
	             fila[0] = Integer.parseInt(rs.getString("pk_id").toString()); 
	             fila[1] = rs.getInt("cpm");
	             fila[2] = Double.parseDouble(rs.getString("precio_concepto"));
	             fila[3] = rs.getString("nombre_concepto");
	             fila[4] = BuscarDescuento(lista, contratacion, Integer.parseInt(rs.getString("pk_id").toString()));
	             modelo.addRow(fila); 
	         }

	         table.updateUI();

	     } catch (SQLException e) {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	     }
	 }

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	@SuppressWarnings("serial")
	private void initialize() throws SQLException {
		descuento = new JFrame();
	    descuento.setResizable(false);
	    descuento.setTitle("Aplicar Descuento");
	    descuento.setBounds(100, 100, 734, 399);
	    descuento.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
	    descuento.getContentPane().setLayout(null);
	    

		String ruta = new File ("").getAbsolutePath ();
		frmAplicarDescuento = new JFrame();
		frmAplicarDescuento.setResizable(false);
		frmAplicarDescuento.setTitle("Aplicar Descuento");
		frmAplicarDescuento.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAplicarDescuento.setBounds(100, 100, 734, 413);
		frmAplicarDescuento.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
		frmAplicarDescuento.getContentPane().setLayout(null);
		frmAplicarDescuento.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				boolean cambios = false;
				for(int i = 0; i < table.getRowCount(); i++){
					if(lista.NodoExistente(Integer.parseInt(table.getValueAt(i, 0).toString()))){
						if(lista.cambios(Integer.parseInt(table.getValueAt(i, 0).toString()), Integer.parseInt(table.getValueAt(i, 4).toString()))){
							cambios = true;
						}
					}else if(Integer.parseInt(table.getValueAt(i, 4).toString()) != 0){
						cambios = true;
					}	
				}
				
				if(cambios == true){
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
					if(JOptionPane.showConfirmDialog(null, "Existen descuentos que no han sido guardados, �Est� seguro de abandonar el m�dulo y perder los cambios realizados?","Cerrar M�dulo",1,0,icono)==0){
						try {
							ListadoFacturas ventana = new ListadoFacturas(lista,user, permiso_descuento);
							ventana.frmFacturasPendientes.setVisible(true);
							frmAplicarDescuento.dispose();
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
			        }
				}else{
					try {
						ListadoFacturas ventana = new ListadoFacturas(lista, user, permiso_descuento);
						ventana.frmFacturasPendientes.setVisible(true);
						frmAplicarDescuento.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAplicarDescuento.getSize();  
        frmAplicarDescuento.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);
        
        EstiloHorus tema = new EstiloHorus();
        
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(28, 176, 548, 113);
		frmAplicarDescuento.getContentPane().add(scrollPane);
		
		@SuppressWarnings("rawtypes")
		Class[] types = new Class [] {
				java.lang.Integer.class, java.lang.Integer.class, java.lang.Double.class , java.lang.String.class , java.lang.Integer.class};
		
		modelo = new DefaultTableModel() {
		@Override
			public boolean isCellEditable(int fila, int columna) {
				return false; 
			}
			   
			@SuppressWarnings({ "rawtypes", "unchecked" })
			public Class getColumnClass(int columnIndex) {
				return types [columnIndex];
			}
		};
			 
		table = new JTable(modelo);
			 
		modelo.addColumn("ID"); 
		modelo.addColumn("CANT.");
		modelo.addColumn("PREC. UNIT"); 
		modelo.addColumn("DESCRIPCI�N"); 
		modelo.addColumn("% DCTO");
			 
		table.getTableHeader().setForeground(new Color(0, 102, 255));
		table.getTableHeader().setFont(new Font("Arial", Font.BOLD, 11));
			
		int[] anchos = {1,1,1,130,1};
		for(int i = 0; i < anchos.length; i++) {
			table.getColumnModel().getColumn(i).setPreferredWidth(anchos[i]);
		}	
			
		rellenarTabla();
		scrollPane.setViewportView(table);
		Totales();
		
		JLabel label1;
		label1 = new JLabel("RIF/CI:");
		label1.setFont(new Font("Arial", Font.BOLD, 11));
		label1.setBounds(80, 41, 39, 14);
		frmAplicarDescuento.getContentPane().add(label1);
		
		rif = new JTextField();
		rif.setEnabled(false);
		rif.setBounds(123, 38, 321, 20);
		frmAplicarDescuento.getContentPane().add(rif);
		rif.setColumns(10);
		
		JLabel label_1 = new JLabel("RAZ\u00D3N SOCIAL:");
		label_1.setFont(new Font("Arial", Font.BOLD, 11));
		label_1.setBounds(28, 64, 89, 14);
		frmAplicarDescuento.getContentPane().add(label_1);
		
		razon = new JTextField();
		razon.setEnabled(false);
		razon.setColumns(10);
		razon.setBounds(123, 61, 321, 20);
		frmAplicarDescuento.getContentPane().add(razon);
		
		JLabel label_2 = new JLabel("DIRECCI\u00D3N:");
		label_2.setFont(new Font("Arial", Font.BOLD, 11));
		label_2.setBounds(53, 88, 61, 14);
		frmAplicarDescuento.getContentPane().add(label_2);
		
		direc = new JTextField();
		direc.setEnabled(false);
		direc.setColumns(10);
		direc.setBounds(123, 85, 321, 20);
		frmAplicarDescuento.getContentPane().add(direc);
		
		JLabel lblTelfono = new JLabel("TEL\u00C9FONO:");
		lblTelfono.setFont(new Font("Arial", Font.BOLD, 11));
		lblTelfono.setBounds(55, 113, 61, 14);
		frmAplicarDescuento.getContentPane().add(lblTelfono);
		
		tel = new JTextField();
		tel.setEnabled(false);
		tel.setColumns(10);
		tel.setBounds(123, 110, 321, 20);
		frmAplicarDescuento.getContentPane().add(tel);
		
		JLabel lblSubtotal = new JLabel("SUB-TOTAL:");
		lblSubtotal.setFont(new Font("Arial", Font.BOLD, 11));
		lblSubtotal.setBounds(471, 52, 71, 14);
		frmAplicarDescuento.getContentPane().add(lblSubtotal);
		
		NumberFormat dispFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
		NumberFormat editFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
		editFormat.setGroupingUsed(false);
		NumberFormatter dnFormat = new NumberFormatter(dispFormat);
		NumberFormatter enFormat = new NumberFormatter(editFormat);
		DefaultFormatterFactory currFactory = new DefaultFormatterFactory(dnFormat, dnFormat, enFormat);
		enFormat.setAllowsInvalid(true);
		
		sub = new JFormattedTextField();
		sub.setEnabled(false);
		sub.setValue(monto_sub);
		sub.setBounds(545, 47, 139, 20);
		sub.setFormatterFactory(currFactory);
		frmAplicarDescuento.getContentPane().add(sub);
		
		JLabel lblIva = new JLabel("IVA:");
		lblIva.setFont(new Font("Arial", Font.BOLD, 11));
		lblIva.setBounds(471, 76, 33, 14);
		frmAplicarDescuento.getContentPane().add(lblIva);
		
		iva = new JFormattedTextField();
		iva.setEnabled(false);
		iva.setValue(monto_iva);
		iva.setBounds(498, 74, 185, 20);
		iva.setFormatterFactory(currFactory);
		frmAplicarDescuento.getContentPane().add(iva);
		
		JLabel lblTotal = new JLabel("TOTAL:");
		lblTotal.setFont(new Font("Arial", Font.BOLD, 11));
		lblTotal.setBounds(471, 102, 46, 14);
		frmAplicarDescuento.getContentPane().add(lblTotal);
		
		total = new JFormattedTextField();
		total.setEnabled(false);
		total.setValue(monto_total);
		total.setBounds(519, 100, 164, 20);
		total.setFormatterFactory(currFactory);
		frmAplicarDescuento.getContentPane().add(total);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(454, 38, 244, 93);
		lblNewLabel_1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 102, 255)));
		frmAplicarDescuento.getContentPane().add(lblNewLabel_1);
		
		
		
		JButton btnNewButton = new JButton("Asignar");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.setBackground(tema.getAzul());
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBorderPainted(false);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(table.getSelectedRow() != -1){
					desc.setText(table.getValueAt(table.getSelectedRow(),4)+"");
					slider.setValue(Integer.parseInt(table.getValueAt(table.getSelectedRow(),4).toString()));
					descuento.setVisible(true);
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un item para Aplicar Descuento","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/add.png"));
		btnNewButton.setBounds(586, 173, 112, 34);
		frmAplicarDescuento.getContentPane().add(btnNewButton);
		
		JButton btnBorrar = new JButton("Borrar");
		btnBorrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnBorrar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnBorrar.setBackground(tema.getAzul());
			}
		});
		btnBorrar.setBackground(tema.getAzul());
		btnBorrar.setForeground(Color.WHITE);
		btnBorrar.setBorderPainted(false);
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(table.getSelectedRow() != -1){
					table.setValueAt("0", table.getSelectedRow(), 4);
					try {
						Totales();
						Actualizar();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un item para Borrar Descuento","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnBorrar.setIcon(new ImageIcon(ruta+"/images/del.png"));
		btnBorrar.setBounds(586, 214, 112, 34);
		frmAplicarDescuento.getContentPane().add(btnBorrar);
		
		JButton btnBorrarTodas = new JButton("Limpiar");
		btnBorrarTodas.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnBorrarTodas.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnBorrarTodas.setBackground(tema.getAzul());
			}
		});
		btnBorrarTodas.setBackground(tema.getAzul());
		btnBorrarTodas.setForeground(Color.WHITE);
		btnBorrarTodas.setBorderPainted(false);
		btnBorrarTodas.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				for(int i = 0; i < table.getRowCount(); i++){
					table.setValueAt(0, i, 4);
				}
				try {
					Totales();
					Actualizar();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnBorrarTodas.setIcon(new ImageIcon(ruta+"/images/trash.png"));
		btnBorrarTodas.setBounds(586, 255, 112, 34);
		frmAplicarDescuento.getContentPane().add(btnBorrarTodas);
		
		JButton button = new JButton("Volver");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button.setBackground(tema.getAzul());
			}
		});
		button.setBackground(tema.getAzul());
		button.setForeground(Color.WHITE);
		button.setBorderPainted(false);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				boolean cambios = false;
				for(int i = 0; i < table.getRowCount(); i++){
					if(lista.NodoExistente(Integer.parseInt(table.getValueAt(i, 0).toString()))){
						if(lista.cambios(Integer.parseInt(table.getValueAt(i, 0).toString()), Integer.parseInt(table.getValueAt(i, 4).toString()))){
							cambios = true;
						}
					}else if(Integer.parseInt(table.getValueAt(i, 4).toString()) != 0){
						cambios = true;
					}	
				}
				if(cambios == true){
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
					if(JOptionPane.showConfirmDialog(null, "Existen descuentos que no han sido guardados, �Est� seguro de abandonar el m�dulo y perder los cambios realizados?","Cerrar M�dulo",1,0,icono)==0){
						try {
							ListadoFacturas ventana = new ListadoFacturas(lista, user, permiso_descuento);
							ventana.frmFacturasPendientes.setVisible(true);
							frmAplicarDescuento.dispose();
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}
			        }
				}else{
					try {
						ListadoFacturas ventana = new ListadoFacturas(lista, user, permiso_descuento);
						ventana.frmFacturasPendientes.setVisible(true);
						frmAplicarDescuento.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
		});
		button.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button.setBounds(409, 309, 164, 44);
		frmAplicarDescuento.getContentPane().add(button);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnGuardar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnGuardar.setBackground(tema.getAzul());
			}
		});
		btnGuardar.setBackground(tema.getAzul());
		btnGuardar.setForeground(Color.WHITE);
		btnGuardar.setBorderPainted(false);
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				for(int i = 0; i < table.getRowCount() ; i++){
					if(Integer.parseInt(table.getValueAt(i, 4).toString()) == 0){
						lista.RemoverNodo(Integer.parseInt(table.getValueAt(i, 0).toString()));
					}else{
						if(!lista.NodoExistente(Integer.parseInt(table.getValueAt(i, 0).toString()))){
							lista.InsFinal(contratacion, Integer.parseInt(table.getValueAt(i, 0).toString()), Integer.parseInt(table.getValueAt(i, 4).toString()));
						}else{
							lista.EditarDescuento(Integer.parseInt(table.getValueAt(i, 0).toString()), Integer.parseInt(table.getValueAt(i, 4).toString()));
						}
					}
				}
				ListadoFacturas ventana;
				try {
					ventana = new ListadoFacturas(lista, user, permiso_descuento);
					ventana.frmFacturasPendientes.setVisible(true);
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				frmAplicarDescuento.dispose();
			}
		});
		btnGuardar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		btnGuardar.setBounds(145, 309, 164, 44);
		frmAplicarDescuento.getContentPane().add(btnGuardar);
		
		JLabel lblNewLabel1 = new JLabel("");
		lblNewLabel1.setBounds(10, 11, 708, 364);
		lblNewLabel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "APLICAR DESCUENTO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAplicarDescuento.getContentPane().add(lblNewLabel1);
		
		Conexion con = new Conexion();
		ResultSet nro;
		String rif_cliente = "";
		String nombre_cliente = "";
		String direccion_cliente = "";
		String telefono_cliente = "";
		String query = "SELECT tb_usuario.nombre, tb_usuario.rif_cedula, tb_usuario.direccion_envio, tb_usuario.nro_telefono FROM ads_clientes, tb_usuario, ads_contrataciones WHERE ads_clientes.fk_id_usuario = tb_usuario.id_usuario AND ads_clientes.pk_id = ads_contrataciones.fk_id_cliente AND ads_contrataciones.pk_id = '"+contratacion+"';";
		nro = con.Consulta(query);
		if(nro.next()) {
			rif_cliente = nro.getString("rif_cedula");
			nombre_cliente = nro.getString("nombre");
			direccion_cliente = nro.getString("direccion_envio");
			telefono_cliente = nro.getString("nro_telefono");
			nro.close();
		}	
		
		rif.setText(rif_cliente);
		razon.setText(nombre_cliente);
		direc.setText(direccion_cliente);
		tel.setText(telefono_cliente);
		
		JLabel label_3 = new JLabel("LISTA DE CONCEPTOS");
		label_3.setOpaque(true);
		label_3.setHorizontalAlignment(SwingConstants.CENTER);
		label_3.setForeground(Color.WHITE);
		label_3.setFont(new Font("Tahoma", Font.BOLD, 13));
		label_3.setBackground(new Color(76, 76, 76));
		label_3.setBounds(28, 143, 670, 20);
		frmAplicarDescuento.getContentPane().add(label_3);
		
        //************************************************ V E N T A N A  D E  D E S C U E N T O *********************************************************

		descuento = new JFrame();
		descuento.setTitle("Asignar Descuento");
		descuento.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		descuento.setResizable(false);
		descuento.setBounds(100, 100, 470, 180);
		descuento.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		descuento.getContentPane().setLayout(null);
		Dimension pantalla_descuento = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana_descuento = descuento.getSize();  
        descuento.setLocation((pantalla_descuento .width - ventana_descuento.width) / 2, (pantalla_descuento .height - ventana_descuento.height) / 2); 
		
		JLabel label = new JLabel("% DESCUENTO:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(41, 48, 81, 14);
		descuento.getContentPane().add(label);
		
		slider = new JSlider();
		slider.setValue(0);
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent e) {
				desc.setText(slider.getValue()+"");
			}
		});
		slider.setValue(0);
		slider.setToolTipText("Deslice para ajustar el % de descuento");
		slider.setBounds(127, 42, 225, 23);
		descuento.getContentPane().add(slider);
		
		desc = new JTextField();
		desc.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				desc.nextFocus();
			}
		});
		desc.setText("0");
		desc.setColumns(10);
		desc.setBounds(357, 45, 62, 20);
		desc.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || desc.getText().length() == 3 ){
		            evt.consume();
		        }
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				if(desc.getText().equals("")){
					desc.setText("0");
				}
				int valor = Integer.parseInt(desc.getText().toString());
				slider.setValue(valor);
			}
			@Override
			public void keyPressed(KeyEvent arg0) {
				if(desc.getText().equals("") || desc.getText().equals("00")){
					desc.setText("0");
				}
				int valor = Integer.parseInt(desc.getText().toString());
				slider.setValue(valor);
			}
		});
		desc.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				desc.nextFocus();
			}
		});
		descuento.getContentPane().add(desc);
		
		JButton btnAplicar = new JButton("Aplicar");
		btnAplicar.setForeground(Color.WHITE);
		btnAplicar.setBorderPainted(false);
		btnAplicar.setBackground(tema.getAzul());
		btnAplicar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnAplicar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnAplicar.setBackground(tema.getAzul());
			}
		});
		btnAplicar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				table.setValueAt(desc.getText(), table.getSelectedRow(), 4);
				desc.setText("0");
				try {
					Totales();
					Actualizar();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				descuento.dispose();
			}
		});
		btnAplicar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		        	table.setValueAt(desc.getText(), table.getSelectedRow(), 4);
					desc.setText("0");
					try {
						Totales();
						Actualizar();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					descuento.dispose();
		        }	
			}
		});
		btnAplicar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		btnAplicar.setBounds(85, 80, 120, 36);
		descuento.getContentPane().add(btnAplicar);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setForeground(Color.WHITE);
		btnVolver.setBorderPainted(false);
		btnVolver.setBackground(tema.getAzul());
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				descuento.dispose();
			}
		});
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.setBounds(265, 80, 120, 36);
		descuento.getContentPane().add(btnVolver);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 444, 127);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ASIGNAR DESCUENTO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		descuento.getContentPane().add(lblNewLabel);
		
		//******************************************************************************************************************************************************
		
	}
}
