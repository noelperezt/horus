package views;

import java.awt.Dimension;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import models.Usuario;
import utility.EstiloHorus;
import utility.Tabla;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdministrarUsuarios {

	public JFrame frmAdministrarUsuarios;
	private Tabla listausuarios;
	private JTextField textField;
	public String ruta = new File ("").getAbsolutePath ();
	private Usuario user;

	public AdministrarUsuarios(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmAdministrarUsuarios = new JFrame();
		frmAdministrarUsuarios.setResizable(false);
		frmAdministrarUsuarios.setTitle("Administrar Usuarios");
		frmAdministrarUsuarios.setBounds(100, 100, 704, 375);
		frmAdministrarUsuarios.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmAdministrarUsuarios.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAdministrarUsuarios.getContentPane().setLayout(null);
		frmAdministrarUsuarios.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarUsuarios.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAdministrarUsuarios.getSize();  
        frmAdministrarUsuarios.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
		EstiloHorus tema = new EstiloHorus();
        
		String campos[] = {"ID","USUARIO","NOMBRE","ESTADO"};
		int ancho[] = {1,1,170,1};
		int editable[] = null;
		Usuario Ousuario = new Usuario();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(37, 66, 497, 253);
		listausuarios = new Tabla(campos, editable, ancho, null);
		listausuarios.Listar(Ousuario.ListarUsuarios());
		Ousuario.desconectar();
		
		frmAdministrarUsuarios.getContentPane().add(scrollPane);
		scrollPane.setViewportView(listausuarios);
		
		JButton btnNewButton = new JButton("Agregar");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.setBackground(tema.getAzul());
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBorderPainted(false);
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/add.png"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				RegistroUsuarios ventana = new RegistroUsuarios(user);
				ventana.frmRegistroDeUsuarios.setVisible(true);
				frmAdministrarUsuarios.dispose();
			}
		});
		btnNewButton.setBounds(544, 66, 125, 33);
		frmAdministrarUsuarios.getContentPane().add(btnNewButton);
		
		JButton btnEditar = new JButton("Editar");
		btnEditar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnEditar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnEditar.setBackground(tema.getAzul());
			}
		});
		btnEditar.setBackground(tema.getAzul());
		btnEditar.setForeground(Color.WHITE);
		btnEditar.setBorderPainted(false);
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listausuarios.getSelectedRow() != -1){
					int id = Integer.parseInt(listausuarios.getValueAt(listausuarios.getSelectedRow(), 0).toString());
					try {
						EditarUsuarios ventana = new EditarUsuarios(user, id);
						ventana.frmRegistroDeUsuarios.setVisible(true);
						frmAdministrarUsuarios.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un usuario para editar","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnEditar.setIcon(new ImageIcon(ruta+"/images/edit.png"));
		btnEditar.setBounds(544, 110, 125, 33);
		frmAdministrarUsuarios.getContentPane().add(btnEditar);
		
		JButton btnCambiarEstado = new JButton("Estado");
		btnCambiarEstado.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnCambiarEstado.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnCambiarEstado.setBackground(tema.getAzul());
			}
		});
		btnCambiarEstado.setBackground(tema.getAzul());
		btnCambiarEstado.setForeground(Color.WHITE);
		btnCambiarEstado.setBorderPainted(false);
		btnCambiarEstado.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listausuarios.getSelectedRow() != -1){
					int id = Integer.parseInt(listausuarios.getValueAt(listausuarios.getSelectedRow(), 0).toString());
					try {
						listausuarios.CambioEstadoUsuario(id, listausuarios.getSelectedRow(), 3);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un usuario para cambiar su estado","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnCambiarEstado.setIcon(new ImageIcon(ruta+"/images/del.png"));
		btnCambiarEstado.setToolTipText("Cambiar Estado");
		btnCambiarEstado.setBounds(544, 154, 125, 33);
		frmAdministrarUsuarios.getContentPane().add(btnCambiarEstado);
		
		JButton btnReiniciar = new JButton("Reiniciar");
		btnReiniciar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnReiniciar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnReiniciar.setBackground(tema.getAzul());
			}
		});
		btnReiniciar.setBackground(tema.getAzul());
		btnReiniciar.setForeground(Color.WHITE);
		btnReiniciar.setBorderPainted(false);
		btnReiniciar.setIcon(new ImageIcon(ruta+"/images/reset.png"));
		btnReiniciar.setToolTipText("Reiniciar Clave");
		btnReiniciar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listausuarios.getSelectedRow() != -1){
					int id = Integer.parseInt(listausuarios.getValueAt(listausuarios.getSelectedRow(), 0).toString());
					Usuario Ouser = new Usuario();
					try {
						Ouser.ReiniciarClave(id);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un usuario para reiniciar su clave de acceso","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnReiniciar.setBounds(544, 198, 125, 33);
		frmAdministrarUsuarios.getContentPane().add(btnReiniciar);
		
		JButton btnPermisos = new JButton("Permisos");
		btnPermisos.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnPermisos.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnPermisos.setBackground(tema.getAzul());
			}
		});
		btnPermisos.setBackground(tema.getAzul());
		btnPermisos.setForeground(Color.WHITE);
		btnPermisos.setBorderPainted(false);
		btnPermisos.setIcon(new ImageIcon(ruta+"/images/lock.png"));
		btnPermisos.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listausuarios.getSelectedRow() != -1){
					int id = Integer.parseInt(listausuarios.getValueAt(listausuarios.getSelectedRow(), 0).toString());
					try {
						EditarPermisos ventana = new EditarPermisos(id,user);
						ventana.frmModificarPermisos.setVisible(true);
						frmAdministrarUsuarios.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un usuario editar los permisos","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnPermisos.setBounds(544, 242, 125, 33);
		frmAdministrarUsuarios.getContentPane().add(btnPermisos);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.setBackground(tema.getAzul());
		btnVolver.setForeground(Color.WHITE);
		btnVolver.setBorderPainted(false);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarUsuarios.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.setBounds(542, 286, 125, 33);
		frmAdministrarUsuarios.getContentPane().add(btnVolver);
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(37, 41, 56, 14);
		frmAdministrarUsuarios.getContentPane().add(label);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				listausuarios.setClearGrid();
				try {
					listausuarios.Refrescar(Ousuario.FiltraUsuario(textField.getText()));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		textField.setToolTipText("Busqueda por Usuario");
		textField.setColumns(10);
		textField.setBounds(90, 38, 346, 20);
		frmAdministrarUsuarios.getContentPane().add(textField);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ADMINISTRAR USUARIOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		lblNewLabel.setBounds(10, 11, 678, 327);
		frmAdministrarUsuarios.getContentPane().add(lblNewLabel);
	}
}
