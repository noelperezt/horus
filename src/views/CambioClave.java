package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;

import models.Usuario;
import utility.EstiloHorus;

import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class CambioClave {

	public JFrame frmCambioDeClave;
	private JPasswordField actual;
	private JPasswordField nueva;
	private JPasswordField confirmacion;
	private JButton btnGuardar;
	private JButton btnVolver;
	private JLabel lblNewLabel_1;
	private Usuario user;
	public String ruta = new File ("").getAbsolutePath ();

	public CambioClave(Usuario user) {
		this.user = user;
		initialize();
	}

	@SuppressWarnings("deprecation")
	public void Ejecutar(){
		if(!(nueva.getText().equals("") || actual.getText().equals("") || confirmacion.getText().equals(""))){
			Usuario Ouser = new Usuario();
			try {
				if(Ouser.CambiarClave(user.getUsuario(), actual.getText(), nueva.getText(), confirmacion.getText())){
					nueva.setText("");
					actual.setText("");
					confirmacion.setText("");
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmCambioDeClave.dispose();
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}else{
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
		}
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmCambioDeClave = new JFrame();
		frmCambioDeClave.setResizable(false);
		frmCambioDeClave.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmCambioDeClave.setTitle("Cambio de Contrase\u00F1a");
		frmCambioDeClave.setBounds(100, 100, 516, 227);
		frmCambioDeClave.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmCambioDeClave.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		frmCambioDeClave.getContentPane().setLayout(null);
		
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmCambioDeClave.getSize();  
        frmCambioDeClave.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
        frmCambioDeClave.getContentPane().setLayout(null);
		
        EstiloHorus tema = new EstiloHorus();
        
		actual = new JPasswordField();
		actual.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				actual.nextFocus();
			}
		});
		actual.setFont(new Font("Arial", Font.PLAIN, 11));
		actual.setBounds(262, 40, 223, 20);
		frmCambioDeClave.getContentPane().add(actual);
		
		nueva = new JPasswordField();
		nueva.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				nueva.nextFocus();
			}
		});
		nueva.setFont(new Font("Arial", Font.PLAIN, 11));
		nueva.setBounds(262, 71, 223, 20);
		frmCambioDeClave.getContentPane().add(nueva);
		
		confirmacion = new JPasswordField();
		confirmacion.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				confirmacion.nextFocus();
			}
		});
		confirmacion.setFont(new Font("Arial", Font.PLAIN, 11));
		confirmacion.setBounds(262, 102, 223, 20);
		frmCambioDeClave.getContentPane().add(confirmacion);
		
		JLabel lblClaveActual = new JLabel("CONTRASE\u00D1A  ACTUAL:");
		lblClaveActual.setFont(new Font("Arial", Font.BOLD, 11));
		lblClaveActual.setBounds(125, 43, 127, 14);
		frmCambioDeClave.getContentPane().add(lblClaveActual);
		
		JLabel lblNuevaClave = new JLabel("CONTRASE\u00D1A NUEVA:");
		lblNuevaClave.setFont(new Font("Arial", Font.BOLD, 11));
		lblNuevaClave.setBounds(137, 74, 115, 14);
		frmCambioDeClave.getContentPane().add(lblNuevaClave);
		
		JLabel lblConfirmeContrasea = new JLabel("CONFIRME CONTRASE\u00D1A:");
		lblConfirmeContrasea.setFont(new Font("Arial", Font.BOLD, 11));
		lblConfirmeContrasea.setBounds(118, 105, 134, 14);
		frmCambioDeClave.getContentPane().add(lblConfirmeContrasea);
		
		btnGuardar = new JButton("Guardar");
		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnGuardar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnGuardar.setBackground(tema.getAzul());
			}
		});
		btnGuardar.setForeground(Color.WHITE);
		btnGuardar.setBackground(tema.getAzul());
		btnGuardar.setBorderPainted(false);
		btnGuardar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
					Ejecutar();
		        }
			}
		});
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Ejecutar();
			}
		});
		btnGuardar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		btnGuardar.setBounds(144, 138, 127, 36);
		frmCambioDeClave.getContentPane().add(btnGuardar);
		
		btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.setForeground(Color.WHITE);
		btnVolver.setBackground(tema.getAzul());
		btnVolver.setBorderPainted(false);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmCambioDeClave.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}		
			}
		});
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.setBounds(311, 138, 127, 36);
		frmCambioDeClave.getContentPane().add(btnVolver);
		
		lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(ruta+"/images/key cambio.png"));
		lblNewLabel_1.setBounds(10, 40, 141, 134);
		frmCambioDeClave.getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 492, 179);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "CAMBIO DE CONTRASEŅA", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmCambioDeClave.getContentPane().add(lblNewLabel);
	}
}
