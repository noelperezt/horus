package views;

import java.awt.Color;
import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import models.Usuario;
import utility.Conexion;
import utility.EstiloHorus;
import utility.Tabla;

import java.awt.Font;
import java.awt.Toolkit;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdministrarFacturas {

	public JFrame frmAdministrarFacturas;
	private JTextField textField;
	private Tabla listafactura;
	public String ruta = new File ("").getAbsolutePath ();
	private Usuario user;
	
	public AdministrarFacturas(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmAdministrarFacturas = new JFrame();
		frmAdministrarFacturas.setResizable(false);
		frmAdministrarFacturas.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarFacturas.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		frmAdministrarFacturas.setTitle("Administrar Facturas");
		frmAdministrarFacturas.setBounds(100, 100, 754, 473);
		frmAdministrarFacturas.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmAdministrarFacturas.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAdministrarFacturas.getContentPane().setLayout(null);
		Conexion conn = new Conexion();
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAdministrarFacturas.getSize();  
        frmAdministrarFacturas.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
        EstiloHorus tema = new EstiloHorus();
        
		JLabel lblBuscar = new JLabel("BUSCAR:");
		lblBuscar.setFont(new Font("Arial", Font.BOLD, 11));
		lblBuscar.setBounds(42, 41, 56, 14);
		frmAdministrarFacturas.getContentPane().add(lblBuscar);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				String n_fact = "";
				for(int i = 0; i< 7-textField.getText().length(); i++){
					n_fact = n_fact + "0";
				}
				n_fact = n_fact + textField.getText();
				if(n_fact.equals("0000000")){
					n_fact = "";
				}
				String sentencia = "SELECT pk_id, numero_factura, fecha_factura, total_pago, nombre FROM ads_facturacion, tb_administrador WHERE id_usuario = id_admin AND anulada = 'N' AND ads_facturacion.numero_factura ILIKE '%"+n_fact+"%' ORDER BY ads_facturacion.pk_id";
				listafactura.setClearGrid();
				try {
					listafactura.Refrescar(conn.Consulta(sentencia));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || textField.getText().length() == 7 ){
		            evt.consume();
		        }
			}
		});
		textField.setToolTipText("Busqueda por N\u00B0 de Factura");
		textField.setFont(new Font("Arial", Font.PLAIN, 11));
		textField.setColumns(10);
		textField.setBounds(98, 37, 346, 20);
		frmAdministrarFacturas.getContentPane().add(textField);
		
		String campos[] = {"ID","N� FACTURA","FECHA","TOTAL","PROCESADO POR:"};
		int ancho[] = {1,50,50,50,150};
		int editable[] = null;
		String sentencia = "SELECT pk_id, numero_factura, fecha_factura, total_pago, nombre FROM ads_facturacion, tb_administrador WHERE id_usuario = id_admin AND anulada = 'N' ORDER BY ads_facturacion.pk_id DESC";
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(37, 68, 680, 292);
		listafactura = new Tabla(campos, editable, ancho, null);
		listafactura.Listar(conn.Consulta(sentencia));
		
		frmAdministrarFacturas.getContentPane().add(scrollPane);
		scrollPane.setViewportView(listafactura);
		
		JButton btnAnular = new JButton("Anular");
		btnAnular.setForeground(Color.WHITE);
		btnAnular.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnAnular.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnAnular.setBackground(tema.getAzul());
			}
		});
		btnAnular.setBackground(tema.getAzul());
		btnAnular.setBorderPainted(false);
		btnAnular.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listafactura.getSelectedRow() != -1){
					int cont = 0;
					String query = "SELECT numero_documento FROM ads_facturacion WHERE tipo = 'C' AND numero_documento IS NOT NULL AND numero_factura = '"+listafactura.getValueAt(listafactura.getSelectedRow(), 1).toString()+"'";
					try {
						ResultSet busca = conn.Consulta(query);
						if(busca.next()){
							cont = busca.getInt("numero_documento");
							busca.close();
						}
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					AnularFactura ventana = new AnularFactura(listafactura.getValueAt(listafactura.getSelectedRow(), 1).toString(), cont, user);
					ventana.frmAnularFactura.setVisible(true);
					frmAdministrarFacturas.dispose();
						 		
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un item para Anular","Advertencia" , JOptionPane.WARNING_MESSAGE,icono); 
				}
			}
		});
		btnAnular.setIcon(new ImageIcon(ruta+"/images/break.png"));
		btnAnular.setBounds(105, 376, 164, 44);
		frmAdministrarFacturas.getContentPane().add(btnAnular);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.setForeground(Color.WHITE);
		btnVolver.setBackground(tema.getAzul());
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.setBorderPainted(false);
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Principal ventana;
				try {
					ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarFacturas.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
		});
		btnVolver.setBounds(495, 376, 164, 44);
		frmAdministrarFacturas.getContentPane().add(btnVolver);
		
		JButton btnReimprimir = new JButton("Reimprimir");
		btnReimprimir.setForeground(Color.WHITE);
		btnReimprimir.setBackground(tema.getAzul());
		btnReimprimir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnReimprimir.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnReimprimir.setBackground(tema.getAzul());
			}
		});
		btnReimprimir.setBorderPainted(false);
		btnReimprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listafactura.getSelectedRow() != -1){
					int factura = Integer.parseInt(listafactura.getValueAt(listafactura.getSelectedRow(), 0).toString());
					ReimprimirFactura a = new ReimprimirFactura(factura);
					a.frmReimprimirFactura.setVisible(true);
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un item para Reimprimir","Advertencia" , JOptionPane.WARNING_MESSAGE,icono); 
				}
			}
		});
		btnReimprimir.setIcon(new ImageIcon(ruta+"/images/printer.png"));
		btnReimprimir.setBounds(299, 376, 164, 44);
		frmAdministrarFacturas.getContentPane().add(btnReimprimir);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 730, 428);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ADMINISTRAR FACTURAS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAdministrarFacturas.getContentPane().add(lblNewLabel);
	}
}
