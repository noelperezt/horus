package views;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import models.Banco;
import models.Usuario;
import utility.EstiloHorus;

import java.awt.Font;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RegistroBancos {

	public JFrame frmRegistroDeBancos;
	private JTextField nombre;
	private JTextField cuenta;
	private JTextField keyid;
	private JTextField publickeyid;
	private Usuario user;
	private int banco;
    String ruta = new File ("").getAbsolutePath ();

	public RegistroBancos(int banco, Usuario user) throws SQLException {
		this.banco = banco;
		this.user = user;
		initialize();
	}
	
	public void Ejecutar() throws SQLException{
		if(!(nombre.getText().equals("") || cuenta.getText().equals(""))){
			Banco Obanco = new Banco(nombre.getText(), cuenta.getText(), keyid.getText(), publickeyid.getText());
			boolean status = false;
			if (banco == 0){
				status = Obanco.Registrar();
			}else{
				status = Obanco.Editar(banco);
			}
			if(status == true){
				nombre.setText("");
				cuenta.setText("");
				keyid.setText("");
				publickeyid.setText("");
				AdministrarBancos ventana = new AdministrarBancos(user);
				ventana.frmAdministrarBancos.setVisible(true);
				frmRegistroDeBancos.dispose();
			}
		}else{
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);				
		}
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmRegistroDeBancos = new JFrame();
		frmRegistroDeBancos.setResizable(false);
		frmRegistroDeBancos.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		if (banco == 0){
			frmRegistroDeBancos.setTitle("Registro de Bancos");
		}else{
			frmRegistroDeBancos.setTitle("Modificar Bancos");
		}
		frmRegistroDeBancos.setBounds(100, 100, 470, 260);
		frmRegistroDeBancos.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					AdministrarBancos ventana = new AdministrarBancos(user);
					ventana.frmAdministrarBancos.setVisible(true);
					frmRegistroDeBancos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		frmRegistroDeBancos.getContentPane().setLayout(null);
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmRegistroDeBancos.getSize();  
        frmRegistroDeBancos.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
        
		EstiloHorus tema = new EstiloHorus();
        
        Banco Obanco = new Banco();
        Obanco.TraerRegistro(banco);
		
		nombre = new JTextField();
		nombre.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				nombre.nextFocus();
			}
		});
		nombre.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
				if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z'))  &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE ) {
			         evt.consume();
			    }
			}
		});
		nombre.setText(Obanco.getNombre());
		nombre.setColumns(10);
		nombre.setBounds(124, 38, 304, 20);
		frmRegistroDeBancos.getContentPane().add(nombre);
		
		JLabel lblNombre = new JLabel("NOMBRE:");
		lblNombre.setFont(new Font("Arial", Font.BOLD, 11));
		lblNombre.setBounds(66, 41, 48, 14);
		frmRegistroDeBancos.getContentPane().add(lblNombre);
		
		cuenta = new JTextField();
		cuenta.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				cuenta.nextFocus();
			}
		});
		cuenta.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || cuenta.getText().length() == 20 ){
		            evt.consume();
		        }
			}
		});
		cuenta.setText(Obanco.getCuenta());
		cuenta.setColumns(10);
		cuenta.setBounds(124, 69, 304, 20);
		frmRegistroDeBancos.getContentPane().add(cuenta);
		
		JLabel lblNDeCuenta = new JLabel("N\u00B0 DE CUENTA:");
		lblNDeCuenta.setFont(new Font("Arial", Font.BOLD, 11));
		lblNDeCuenta.setBounds(38, 72, 76, 14);
		frmRegistroDeBancos.getContentPane().add(lblNDeCuenta);
		
		keyid = new JTextField();
		keyid.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				keyid.nextFocus();
			}
		});
		keyid.setText(Obanco.getKeyId());
		keyid.setColumns(10);
		keyid.setBounds(124, 100, 304, 20);
		frmRegistroDeBancos.getContentPane().add(keyid);
		
		JLabel lblKeyid = new JLabel("KEY ID:");
		lblKeyid.setFont(new Font("Arial", Font.BOLD, 11));
		lblKeyid.setBounds(76, 103, 38, 14);
		frmRegistroDeBancos.getContentPane().add(lblKeyid);
		
		publickeyid = new JTextField();
		publickeyid.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				publickeyid.nextFocus();
			}
		});
		publickeyid.setText(Obanco.getPublicKeyId());
		publickeyid.setColumns(10);
		publickeyid.setBounds(124, 131, 304, 20);
		frmRegistroDeBancos.getContentPane().add(publickeyid);
		
		JLabel lblPublicKeyId = new JLabel("PUBLIC KEY ID:");
		lblPublicKeyId.setFont(new Font("Arial", Font.BOLD, 11));
		lblPublicKeyId.setBounds(38, 134, 81, 14);
		frmRegistroDeBancos.getContentPane().add(lblPublicKeyId);
		
		JButton button = new JButton("Volver");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button.setBackground(tema.getAzul());
			}
		});
		button.setBackground(tema.getAzul());
		button.setForeground(Color.WHITE);
		button.setBorderPainted(false);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					AdministrarBancos ventana = new AdministrarBancos(user);
					ventana.frmAdministrarBancos.setVisible(true);
					frmRegistroDeBancos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button.setBounds(259, 168, 120, 36);
		frmRegistroDeBancos.getContentPane().add(button);
		
		String nombre_boton = "";
		if (banco == 0){
			nombre_boton = "Registrar";
		}else{
			nombre_boton = "Guardar";
		}
		
		JButton btnRegistrar = new JButton(nombre_boton);
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnRegistrar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnRegistrar.setBackground(tema.getAzul());
			}
		});
		btnRegistrar.setForeground(Color.WHITE);
		btnRegistrar.setBackground(tema.getAzul());
		btnRegistrar.setBorderPainted(false);
		btnRegistrar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		            try {
						Ejecutar();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }

			}
		});
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Ejecutar();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		if(banco == 0){
			btnRegistrar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		}else{
			btnRegistrar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		}
		btnRegistrar.setBounds(87, 168, 120, 36);
		frmRegistroDeBancos.getContentPane().add(btnRegistrar);
		
		String titulo;
		if (banco == 0){
			titulo = "REGISTRO DE BANCOS";
		}else{
			titulo = "MODIFICAR BANCOS";
		}
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 444, 210);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, titulo, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmRegistroDeBancos.getContentPane().add(lblNewLabel);
		
	}
}
