package views;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import models.Modulo;
import models.Usuario;
import utility.EstiloHorus;
import utility.Tabla;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdministrarModulos {

	public JFrame frmAdministrarMdulos;
	private Tabla listamodulos;
	private Usuario user;
	private JButton btnNewButton;
	private JButton btnE;
	private JButton btnVisible;
	private JButton btnVolver;
	public String ruta = new File ("").getAbsolutePath ();
	
	
	public AdministrarModulos(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmAdministrarMdulos = new JFrame();
		frmAdministrarMdulos.setResizable(false);
		frmAdministrarMdulos.setTitle("Administrar M\u00F3dulos");
		frmAdministrarMdulos.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAdministrarMdulos.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarMdulos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		frmAdministrarMdulos.setBounds(100, 100, 551, 285);
		frmAdministrarMdulos.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAdministrarMdulos.getSize();  
        frmAdministrarMdulos.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		frmAdministrarMdulos.getContentPane().setLayout(null);
		
		EstiloHorus tema = new EstiloHorus();
		
		String campos[] = {"ID","DESCRIPCI�N","VISIBLE"};
		int ancho[] = {1,150,1};
		int editable[] = null;
		Modulo Omodulo = new Modulo();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(34, 45, 346, 177);
		listamodulos = new Tabla(campos, editable, ancho, null);
		listamodulos.Listar(Omodulo.ListarModulos());
		Omodulo.desconectar();
		
		frmAdministrarMdulos.getContentPane().add(scrollPane);
		scrollPane.setViewportView(listamodulos);
		
		btnNewButton = new JButton("Agregar");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.setBackground(tema.getAzul());
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBorderPainted(false);
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/add.png"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					RegistroModudo ventana = new RegistroModudo(0, user);
					ventana.frmRegistrarMdulo.setVisible(true);
					frmAdministrarMdulos.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		btnNewButton.setBounds(390, 46, 123, 35);
		frmAdministrarMdulos.getContentPane().add(btnNewButton);
		
		btnE = new JButton("Editar");
		btnE.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnE.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnE.setBackground(tema.getAzul());
			}
		});
		btnE.setForeground(Color.WHITE);
		btnE.setBorderPainted(false);
		btnE.setBackground(tema.getAzul());
		btnE.setIcon(new ImageIcon(ruta+"/images/edit.png"));
		btnE.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listamodulos.getSelectedRow() != -1){
					int id = Integer.parseInt(listamodulos.getValueAt(listamodulos.getSelectedRow(), 0).toString());
					try {
						RegistroModudo ventana = new RegistroModudo(id, user);
						ventana.frmRegistrarMdulo.setVisible(true);
						frmAdministrarMdulos.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un m�dulo para editar","Advertencia" , JOptionPane.WARNING_MESSAGE,icono); 
				}
			}
		});
		btnE.setBounds(390, 92, 123, 35);
		frmAdministrarMdulos.getContentPane().add(btnE);
		
		btnVisible = new JButton("Visible");
		btnVisible.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVisible.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVisible.setBackground(tema.getAzul());
			}
		});
		btnVisible.setForeground(Color.WHITE);
		btnVisible.setBackground(tema.getAzul());
		btnVisible.setBorderPainted(false);
		btnVisible.setToolTipText("Cambiar Visibilidad");
		btnVisible.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listamodulos.getSelectedRow() != -1){
					int id = Integer.parseInt(listamodulos.getValueAt(listamodulos.getSelectedRow(), 0).toString());
					try {
						listamodulos.CambioVisivilidadModulo(id, listamodulos.getSelectedRow(), 2);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un m�dulo para cambiar su visibilidad","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnVisible.setIcon(new ImageIcon(ruta+"/images/visible.png"));
		btnVisible.setBounds(390, 138, 123, 35);
		frmAdministrarMdulos.getContentPane().add(btnVisible);
		
		btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.setForeground(Color.WHITE);
		btnVolver.setBackground(tema.getAzul());
		btnVolver.setBorderPainted(false);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarMdulos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.setBounds(390, 186, 123, 35);
		frmAdministrarMdulos.getContentPane().add(btnVolver);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 521, 233);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ADMINISTRAR M�DULOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAdministrarMdulos.getContentPane().add(lblNewLabel);
	}

}
