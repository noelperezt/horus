package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

import models.Submodulo;
import models.Usuario;
import utility.EstiloHorus;

import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JComboBox;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.awt.event.ActionEvent;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class RegistrarSubmodulo {

	public JFrame frmRegistroDeSubmdulos;
	private Usuario user;
	private JTextField textField;
	private int submodulo;
	@SuppressWarnings("rawtypes")
	private JComboBox combo;
	String ruta = new File ("").getAbsolutePath ();

	public RegistrarSubmodulo(int submodulo, Usuario user) throws SQLException {
		this.user = user;
		this.submodulo = submodulo;
		initialize();
	}
	
	public void Ejecutar() throws SQLException{
		String[] seleccionado = combo.getSelectedItem().toString().split("-");
		Submodulo Osubmodulo = new Submodulo(Integer.parseInt(seleccionado[0]), textField.getText());
		boolean status = false;
		if(submodulo == 0){
			status = Osubmodulo.Registrar();
		}else{
			status = Osubmodulo.Editar(submodulo);
		}
		if(status == true){
			AdministrarSubmodulos ventana = new AdministrarSubmodulos(user);
			ventana.frmAdministrarSubmdulos.setVisible(true);
			frmRegistroDeSubmdulos.dispose();
		}
	}
	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	@SuppressWarnings("unchecked")
	private void initialize() throws SQLException {
		frmRegistroDeSubmdulos = new JFrame();
		frmRegistroDeSubmdulos.setResizable(false);
		frmRegistroDeSubmdulos.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		if (submodulo == 0){
			frmRegistroDeSubmdulos.setTitle("Registro de SubM\u00F3dulos");
		}else{
			frmRegistroDeSubmdulos.setTitle("Modificar SubM\u00F3dulo");
		}
		frmRegistroDeSubmdulos.setBounds(100, 100, 443, 207);
		frmRegistroDeSubmdulos.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					AdministrarSubmodulos ventana = new AdministrarSubmodulos(user);
					ventana.frmAdministrarSubmdulos.setVisible(true);
					frmRegistroDeSubmdulos.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
	    Dimension ventana = frmRegistroDeSubmdulos.getSize();  
	    frmRegistroDeSubmdulos.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
	    frmRegistroDeSubmdulos.getContentPane().setLayout(null);
		frmRegistroDeSubmdulos.getContentPane().setLayout(null);
		
		EstiloHorus tema = new EstiloHorus();
		
		Submodulo Osubmodulo = new Submodulo();
		Osubmodulo.TraerRegistro(submodulo);
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				textField.nextFocus();
			}
		});
		textField.setText(Osubmodulo.getDescripcion());

		textField.setColumns(10);
		textField.setBounds(118, 75, 287, 20);
		frmRegistroDeSubmdulos.getContentPane().add(textField);
		
		JLabel lblPadre = new JLabel("PADRE:");
		lblPadre.setFont(new Font("Arial", Font.BOLD, 11));
		lblPadre.setBounds(71, 45, 44, 14);
		frmRegistroDeSubmdulos.getContentPane().add(lblPadre);
		
		@SuppressWarnings("rawtypes")
		JComboBox comboBox = new JComboBox();
		combo = comboBox;
		combo.setFont(new Font("Arial", Font.PLAIN, 12));
		combo.setBounds(118, 40, 287, 24);
		ResultSet buscar = Osubmodulo.ListarModulos();
			while (buscar.next()){
				combo.addItem(buscar.getObject("pk_id")+"-"+buscar.getObject("descripcion"));
			}
			buscar.close();
			Osubmodulo.getConn().desConectar();
		if( submodulo == 0){
			combo.setSelectedItem("");
		}else{
			combo.setSelectedItem(Osubmodulo.getPadre()+"-"+Osubmodulo.RetornaDescripcionPadre());
		}
		frmRegistroDeSubmdulos.getContentPane().add(combo);
		
		JLabel lblDescripcin = new JLabel("DESCRIPCI\u00D3N:");
		lblDescripcin.setFont(new Font("Arial", Font.BOLD, 11));
		lblDescripcin.setBounds(34, 78, 74, 14);
		frmRegistroDeSubmdulos.getContentPane().add(lblDescripcin);
		
		String boton = "";
		if (submodulo == 0){
			boton = "Registrar";
		}else{
			boton = "Guardar";
		}
		
		JButton btnRegistrar = new JButton(boton);
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnRegistrar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnRegistrar.setBackground(tema.getAzul());
			}
		});
		btnRegistrar.setBackground(tema.getAzul());
		btnRegistrar.setForeground(Color.WHITE);
		btnRegistrar.setBorderPainted(false);
		btnRegistrar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		            try {
						Ejecutar();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
			}
		});
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Ejecutar();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		if(submodulo == 0){
			btnRegistrar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		}else{
			btnRegistrar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		}
		btnRegistrar.setBounds(76, 117, 120, 36);
		frmRegistroDeSubmdulos.getContentPane().add(btnRegistrar);
		
		JButton button_1 = new JButton("Volver");
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getAzul());
			}
		});
		button_1.setForeground(Color.WHITE);
		button_1.setBackground(tema.getAzul());
		button_1.setBorderPainted(false);
		button_1.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					AdministrarSubmodulos ventana = new AdministrarSubmodulos(user);
					ventana.frmAdministrarSubmdulos.setVisible(true);
					frmRegistroDeSubmdulos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		button_1.setBounds(238, 117, 120, 36);
		frmRegistroDeSubmdulos.getContentPane().add(button_1);
		
		String titulo = "";
		if (submodulo == 0){
			titulo = "REGISTRO DE SUBM�DULOS";
		}else{
			titulo = "MODIFICAR DE SUBM�DULO";
		}
		
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 420, 161);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null,titulo, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmRegistroDeSubmdulos.getContentPane().add(lblNewLabel);
	}
}
