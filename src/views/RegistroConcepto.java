package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.Font;
import java.awt.Toolkit;
import javax.swing.JComboBox;
import javax.swing.JButton;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Locale;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;

import models.Concepto;
import models.Usuario;
import utility.EstiloHorus;

import javax.swing.event.ChangeEvent;
import javax.swing.JFormattedTextField;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;

public class RegistroConcepto {

	public JFrame frmRegistroDeConceptos;
	private JTextField codigo;
	private JTextField concepto;
	private String cod;
	@SuppressWarnings("rawtypes")
	private JComboBox combo;
	@SuppressWarnings("rawtypes")
	private JComboBox combo_zona;
	private Usuario user;
	private JFormattedTextField precio;
	private JTextField descuento;
	String ruta = new File ("").getAbsolutePath ();
	
	public RegistroConcepto(String cod, Usuario user) throws SQLException {
		this.user = user;
		this.cod = cod;
		initialize();
	}
	
	public void convertiraMayusculasEnJtextfield(javax.swing.JTextField jTextfieldS){
		String cadena= (jTextfieldS.getText()).toUpperCase();
		jTextfieldS.setText(cadena);
	}
	
	public void Ejecutar() throws SQLException{
		if(codigo.getText().equals("") || concepto.getText().equals("")){
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "Hay campos vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
		}else{
			String[] seleccionado = combo.getSelectedItem().toString().split("-");
			String zona;
			String[] zona_seleccionada = combo_zona.getSelectedItem().toString().split("-");
			if (combo_zona.getSelectedItem().toString().equals("Ninguno")){
				zona = "0";
			}else{
				zona = zona_seleccionada[0];
			}
			Concepto Oconcepto = new Concepto(codigo.getText(), concepto.getText(), Integer.parseInt(seleccionado[0]), Double.parseDouble(precio.getValue().toString()), Integer.parseInt(descuento.getText()), Integer.parseInt(zona));
			boolean status = false;
			if(cod.equals("0")){
				status = Oconcepto.Registrar();
			}else{
				status = Oconcepto.Editar(cod);
			}
			if(status == true){
				AdministrarConceptos ventana = new AdministrarConceptos(user);
				ventana.frmAdministrarConceptosFacturables.setVisible(true);
				frmRegistroDeConceptos.dispose();
			}			
		}
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private void initialize() throws SQLException {
		frmRegistroDeConceptos = new JFrame();
		frmRegistroDeConceptos.setResizable(false);
		if (cod.equals("0")){
			frmRegistroDeConceptos.setTitle("Registro de Conceptos Facturables");
		}else{
			frmRegistroDeConceptos.setTitle("Modificar Conceptos Facturables");
		}
		frmRegistroDeConceptos.setBounds(100, 100, 469, 321);
		frmRegistroDeConceptos.getContentPane().setLayout(null);
		frmRegistroDeConceptos.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmRegistroDeConceptos.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					AdministrarConceptos ventana = new AdministrarConceptos(user);
					ventana.frmAdministrarConceptosFacturables.setVisible(true);
					frmRegistroDeConceptos.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmRegistroDeConceptos.getSize();  
        frmRegistroDeConceptos.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
        
		EstiloHorus tema = new EstiloHorus();
        
        Concepto Oconcepto = new Concepto();
        Oconcepto.TraerRegistro(cod);
		
		JLabel lblCdigo = new JLabel("C\u00D3DIGO:");
		lblCdigo.setFont(new Font("Arial", Font.BOLD, 11));
		lblCdigo.setBounds(77, 41, 45, 14);
		frmRegistroDeConceptos.getContentPane().add(lblCdigo);
		
		concepto = new JTextField();
		concepto.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				concepto.nextFocus();
			}
		});
		
		codigo = new JTextField();
		codigo.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				codigo.nextFocus();
			}
		});
		codigo.addKeyListener(new KeyAdapter() {
			@Override
			public void keyTyped(KeyEvent evt) {
				if(codigo.getText().length()== 4){
		            evt.consume();
		        }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				convertiraMayusculasEnJtextfield(codigo);
			}
		});
		codigo.setText("");
		codigo.setColumns(10);
		codigo.setBounds(132, 38, 287, 20);
		codigo.setText(Oconcepto.getCodigo());
		frmRegistroDeConceptos.getContentPane().add(codigo);
		
		JLabel lblConcepto = new JLabel("CONCEPTO:");
		lblConcepto.setFont(new Font("Arial", Font.BOLD, 11));
		lblConcepto.setBounds(58, 72, 62, 14);
		frmRegistroDeConceptos.getContentPane().add(lblConcepto);
		concepto.setText("");
		concepto.setColumns(10);
		concepto.setBounds(132, 69, 287, 20);
		concepto.setText(Oconcepto.getConcepto());
		frmRegistroDeConceptos.getContentPane().add(concepto);
		
		JComboBox comboBox = new JComboBox();
		comboBox.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				comboBox.nextFocus();
			}
		});
		combo = comboBox;
		combo.setBounds(132, 100, 287, 20);
		ResultSet buscar = Oconcepto.ListarBancos();
		while (buscar.next()){
			combo.addItem(buscar.getObject("pk_id")+"-"+buscar.getObject("nombre_banco"));
		}
		Oconcepto.getConn().desConectar();
		if(cod.equals("0")){
			combo.setSelectedItem("");
		}else{
			combo.setSelectedItem(Oconcepto.getBanco()+"-"+Oconcepto.getNombre());
		}
		
		JLabel lblBanco = new JLabel("BANCO:");
		lblBanco.setFont(new Font("Arial", Font.BOLD, 11));
		lblBanco.setBounds(77, 103, 45, 14);
		frmRegistroDeConceptos.getContentPane().add(lblBanco);
		frmRegistroDeConceptos.getContentPane().add(combo);
		
		String boton = "";
		if (cod.equals("0")){
			boton = "Registrar";
		}else{
			boton = "Guardar";
		}
		
		String titulo = "";
		if (cod.equals("0")){
			titulo = "REGISTRO DE CONCEPTOS FACTURABLES";
		}else{
			titulo = "MODIFICAR CONCEPTOS FACTURABLES";
		}
		
		JSlider slider = new JSlider();
		slider.setToolTipText("Deslice para ajustar el % de descuento");
		slider.setValue(Oconcepto.getDescuento());
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
				descuento.setText(slider.getValue()+"");
			}
		});
		
		JLabel lblPrecio = new JLabel("PRECIO:");
		lblPrecio.setFont(new Font("Arial", Font.BOLD, 11));
		lblPrecio.setBounds(75, 134, 45, 14);
		frmRegistroDeConceptos.getContentPane().add(lblPrecio);
		
		//DETECTAR LA CONFIGURACI�N REGIONAL DE LA MAQUINA 
		
		// Formato de visualizaci�n
		NumberFormat dispFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
		
		// Formato de edici�n: ingl�s (separador decimal: el punto)
		NumberFormat editFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
		
		// Para la edici�n, no queremos separadores de millares
		editFormat.setGroupingUsed(false);
		
		// Creamos los formateadores de n�meros
		NumberFormatter dnFormat = new NumberFormatter(dispFormat);
		NumberFormatter enFormat = new NumberFormatter(editFormat);
		
		// Creamos la factor�a de formateadores especificando los
		// formateadores por defecto, de visualizaci�n y de edici�n
		DefaultFormatterFactory currFactory = new DefaultFormatterFactory(dnFormat, dnFormat, enFormat);
		
		// El formateador de edici�n admite caracteres incorrectos
		enFormat.setAllowsInvalid(true);
		
		precio = new JFormattedTextField();
		precio.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				precio.nextFocus();
			}
		});
		precio.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		precio.setValue(Oconcepto.getPrecio());
		precio.setColumns(10);
		precio.setBounds(132, 131, 287, 20);
		// Asignamos la factor�a al campo
		
		precio.setFormatterFactory(currFactory);
		frmRegistroDeConceptos.getContentPane().add(precio);
		
		JLabel lblDescuento = new JLabel("% DESCUENTO:");
		lblDescuento.setFont(new Font("Arial", Font.BOLD, 11));
		lblDescuento.setBounds(39, 162, 81, 14);
		frmRegistroDeConceptos.getContentPane().add(lblDescuento);
		slider.setBounds(127, 162, 225, 23);
		frmRegistroDeConceptos.getContentPane().add(slider);
		
		
		descuento = new JTextField();
		descuento.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				descuento.nextFocus();
			}
		});
		descuento.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || descuento.getText().length() == 3 ){
		            evt.consume();
		        }
			}
			@Override
			public void keyReleased(KeyEvent arg0) {
				if(descuento.getText().equals("")){
					descuento.setText("0");
				}
				int valor = Integer.parseInt(descuento.getText().toString());
				slider.setValue(valor);
			}
			@Override
			public void keyPressed(KeyEvent arg0) {
				if(descuento.getText().equals("") || descuento.getText().equals("00")){
					descuento.setText("0");
				}
				int valor = Integer.parseInt(descuento.getText().toString());
				slider.setValue(valor);
			}
		});
		descuento.setText(""+Oconcepto.getDescuento());
		descuento.setColumns(10);
		descuento.setBounds(357, 162, 62, 20);
		frmRegistroDeConceptos.getContentPane().add(descuento);
		
		JComboBox comboBox_1 = new JComboBox();
		comboBox_1.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent arg0) {
				comboBox_1.nextFocus();
			}
		});
		combo_zona = comboBox_1;
		combo_zona.addItem("Ninguno");
		combo_zona.setBounds(132, 193, 287, 20);
		buscar = Oconcepto.ListarZonas();
		while (buscar.next()){
			combo_zona.addItem(buscar.getObject("id")+"-"+buscar.getObject("nombre"));
		}
		Oconcepto.desconectar();
		if(cod.equals("0")){
			combo_zona.setSelectedItem("");
		}else{
			combo_zona.setSelectedItem(Oconcepto.getZona()+"-"+Oconcepto.getNombreZona());
		}
		
		JLabel lblZona = new JLabel("ZONA:");
		lblZona.setFont(new Font("Arial", Font.BOLD, 11));
		lblZona.setBounds(85, 196, 33, 14);
		frmRegistroDeConceptos.getContentPane().add(lblZona);
		frmRegistroDeConceptos.getContentPane().add(combo_zona);
		
		JButton button = new JButton("Volver");
		button.setForeground(Color.WHITE);
		button.setBorderPainted(false);
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button.setBackground(tema.getAzul());
			}
		});
		button.setBackground(tema.getAzul());
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					AdministrarConceptos ventana = new AdministrarConceptos(user);
					ventana.frmAdministrarConceptosFacturables.setVisible(true);
					frmRegistroDeConceptos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		JButton btnRegistrar = new JButton(boton);
		btnRegistrar.setForeground(Color.WHITE);
		btnRegistrar.setBorderPainted(false);
		btnRegistrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnRegistrar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnRegistrar.setBackground(tema.getAzul());
			}
		});
		btnRegistrar.setBackground(tema.getAzul());
		btnRegistrar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		            try {
						Ejecutar();
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		        }
			}
		});
		btnRegistrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					Ejecutar();
					
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		if(cod.equals("0")){
			btnRegistrar.setIcon(new ImageIcon(ruta+"/images/add.png"));
		}else{
			btnRegistrar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		}
		btnRegistrar.setBounds(89, 228, 120, 36);
		frmRegistroDeConceptos.getContentPane().add(btnRegistrar);
		button.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button.setBounds(262, 228, 120, 36);
		frmRegistroDeConceptos.getContentPane().add(button);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 443, 272);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null,titulo, javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmRegistroDeConceptos.getContentPane().add(lblNewLabel);
		
	}
}
