package views;

import java.awt.Dimension;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JSpinnerDateEditor;

import utility.AbstractJasperReports;
import utility.Conexion;
import utility.EstiloHorus;

import javax.swing.JButton;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Color;
import javax.swing.border.TitledBorder;
import javax.swing.UIManager;

public class BusquedaPresupuesto {

	public JFrame frmBuscarPresupuestos;

	public BusquedaPresupuesto() {
		initialize();
	}
	
	public double transformaDouble(String cifra){
		double total = 0;
		if ( !((cifra == null) || (cifra.equals(""))) ) {
			total = Double.parseDouble(cifra);
		}
		return total;
	}
	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		String ruta = new File ("").getAbsolutePath ();
		frmBuscarPresupuestos = new JFrame();
		frmBuscarPresupuestos.setTitle("Buscar Presupuestos");
		frmBuscarPresupuestos.setBounds(100, 100, 369, 285);
		frmBuscarPresupuestos.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmBuscarPresupuestos.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmBuscarPresupuestos.getContentPane().setLayout(null);
		frmBuscarPresupuestos.setResizable(false);
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmBuscarPresupuestos.getSize();  
        frmBuscarPresupuestos.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2);
		
        EstiloHorus tema = new EstiloHorus();
        
		JRadioButton todos = new JRadioButton("TODOS");
		todos.setSelected(true);
		todos.setFont(new Font("Arial", Font.BOLD, 11));
		todos.setBounds(30, 41, 74, 23);
		frmBuscarPresupuestos.getContentPane().add(todos);
		
		JRadioButton anuladas = new JRadioButton("ANULADOS");
		anuladas.setFont(new Font("Arial", Font.BOLD, 11));
		anuladas.setBounds(106, 41, 95, 23);
		frmBuscarPresupuestos.getContentPane().add(anuladas);
		
		JRadioButton no_anuladas = new JRadioButton("NO ANULADOS");
		no_anuladas.setFont(new Font("Arial", Font.BOLD, 11));
		no_anuladas.setBounds(215, 41, 109, 23);
		frmBuscarPresupuestos.getContentPane().add(no_anuladas);
		
		 ButtonGroup grupo1 = new ButtonGroup();
		 grupo1.add(todos);
		 grupo1.add(anuladas);
		 grupo1.add(no_anuladas);
		
		JLabel frame = new JLabel("");
		frame.setBounds(21, 26, 319, 47);
		frame.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "ESTADO", TitledBorder.RIGHT, TitledBorder.TOP, new java.awt.Font("Arial", 1, 12), new Color(76, 76, 76)));
		frmBuscarPresupuestos.getContentPane().add(frame);
		
		JRadioButton todos2 = new JRadioButton("TODOS");
		todos2.setSelected(true);
		todos2.setFont(new Font("Arial", Font.BOLD, 11));
		todos2.setBounds(30, 95, 74, 23);
		frmBuscarPresupuestos.getContentPane().add(todos2);
		
		JRadioButton facturado = new JRadioButton("FACTURADOS");
		facturado.setFont(new Font("Arial", Font.BOLD, 11));
		facturado.setBounds(106, 95, 107, 23);
		frmBuscarPresupuestos.getContentPane().add(facturado);
		
		JRadioButton no_facturado = new JRadioButton("NO FACTURADOS");
		no_facturado.setFont(new Font("Arial", Font.BOLD, 11));
		no_facturado.setBounds(215, 95, 120, 23);
		frmBuscarPresupuestos.getContentPane().add(no_facturado);
		
		ButtonGroup grupo2 = new ButtonGroup();
		 grupo2.add(todos2);
		 grupo2.add(facturado);
		 grupo2.add(no_facturado);
		
		JLabel frame2 = new JLabel("");
		frame2.setBounds(21, 80, 319, 47);
		frame2.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "PROCESO", TitledBorder.RIGHT, TitledBorder.TOP, new java.awt.Font("Arial", 1, 12), new Color(76, 76, 76)));
		frmBuscarPresupuestos.getContentPane().add(frame2);
		
		JLabel lblFechaInicio = new JLabel("FECHA INICIO:");
		lblFechaInicio.setFont(new Font("Arial", Font.BOLD, 11));
		lblFechaInicio.setBounds(35, 142, 86, 14);
		frmBuscarPresupuestos.getContentPane().add(lblFechaInicio);
		
		JDateChooser inicio  = new JDateChooser(null, null, null, new JSpinnerDateEditor()); 
		inicio.setBounds(118, 138, 222, 20);
		frmBuscarPresupuestos.getContentPane().add(inicio);
		
		JLabel lblFechaFin = new JLabel("FECHA FIN:");
		lblFechaFin.setFont(new Font("Arial", Font.BOLD, 11));
		lblFechaFin.setBounds(51, 171, 71, 14);
		frmBuscarPresupuestos.getContentPane().add(lblFechaFin);
		
		JDateChooser fin  = new JDateChooser(null, null, null, new JSpinnerDateEditor()); 
		fin.setBounds(118, 169, 222, 20);
		frmBuscarPresupuestos.getContentPane().add(fin);
		
		JButton buscar = new JButton("Buscar");
		buscar.setForeground(Color.WHITE);
		buscar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				buscar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				buscar.setBackground(tema.getAzul());
			}
		});
		buscar.setBackground(tema.getAzul());
		buscar.setBorderPainted(false);
		buscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Conexion conn = new Conexion();
		   		if (inicio.getDate() != null && fin.getDate() != null){
		   			if(inicio.getDate().before(fin.getDate())){
		   				SimpleDateFormat formatter;
				   		formatter = new SimpleDateFormat("yyyy-MM-dd");
				   		String fecha_inicial = formatter.format(inicio.getDate());
		   				String fecha_final = formatter.format(fin.getDate());
		   				
		   				java.sql.Date inicial_consulta = new java.sql.Date(inicio.getDate().getTime());
		   				java.sql.Date final_consulta = new java.sql.Date(fin.getDate().getTime());
		   				
		   				if(todos.isSelected() && todos2.isSelected()){
		   					int total = 0;
							String busca_clave = "SELECT count(*) as total FROM ads_presupuesto WHERE  fecha >= '"+fecha_inicial+"' AND fecha <= '"+fecha_final+"'";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							AbstractJasperReports.TodosPresupuestos(conn.getConnection(), ruta+"/reports/listado presupuestos/",ruta, total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   				}else if(todos.isSelected() && facturado.isSelected()){
		   					int total = 0;
							String busca_clave = "SELECT count(*) as total FROM ads_presupuesto WHERE  fecha >= '"+fecha_inicial+"' AND fecha <= '"+fecha_final+"' AND procesado = 'S'";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							AbstractJasperReports.TodosFacturados(conn.getConnection(), ruta+"/reports/listado presupuestos/",ruta, total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   				}else if(todos.isSelected() && no_facturado.isSelected()){
		   					int total = 0;
							String busca_clave = "SELECT count(*) as total FROM ads_presupuesto WHERE  fecha >= '"+fecha_inicial+"' AND fecha <= '"+fecha_final+"' AND procesado = 'N'";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							AbstractJasperReports.TodosNFacturados(conn.getConnection(), ruta+"/reports/listado presupuestos/",ruta, total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   				}else if(anuladas.isSelected() && todos2.isSelected()){
		   					int total = 0;
							String busca_clave = "SELECT count(*) as total FROM ads_presupuesto WHERE  fecha >= '"+fecha_inicial+"' AND fecha <= '"+fecha_final+"' AND anulado = 'S'";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							AbstractJasperReports.AnuladosTodos(conn.getConnection(), ruta+"/reports/listado presupuestos/",ruta, total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   				}else if(anuladas.isSelected() && facturado.isSelected()){
		   					int total = 0;
							String busca_clave = "SELECT count(*) as total FROM ads_presupuesto WHERE  fecha >= '"+fecha_inicial+"' AND fecha <= '"+fecha_final+"' AND procesado = 'S' AND anulado = 'S'";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							AbstractJasperReports.AnuladosFacturados(conn.getConnection(), ruta+"/reports/listado presupuestos/",ruta, total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   				}else if(anuladas.isSelected() && no_facturado.isSelected()){
		   					int total = 0;
							String busca_clave = "SELECT count(*) as total FROM ads_presupuesto WHERE  fecha >= '"+fecha_inicial+"' AND fecha <= '"+fecha_final+"' AND procesado = 'N' AND anulado = 'S'";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							AbstractJasperReports.AnuladosNFacturados(conn.getConnection(), ruta+"/reports/listado presupuestos/",ruta, total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   				}else if(no_anuladas.isSelected() && todos2.isSelected()){
		   					int total = 0;
							String busca_clave = "SELECT count(*) as total FROM ads_presupuesto WHERE  fecha >= '"+fecha_inicial+"' AND fecha <= '"+fecha_final+"' AND anulado = 'N'";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							AbstractJasperReports.NAnuladosTodos(conn.getConnection(), ruta+"/reports/listado presupuestos/",ruta, total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   				}else if(no_anuladas.isSelected() && facturado.isSelected()){
		   					int total = 0;
							String busca_clave = "SELECT count(*) as total FROM ads_presupuesto WHERE  fecha >= '"+fecha_inicial+"' AND fecha <= '"+fecha_final+"' AND procesado = 'S' AND anulado = 'N'";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							AbstractJasperReports.NAnuladosFacturados(conn.getConnection(), ruta+"/reports/listado presupuestos/",ruta, total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   				}else if(no_anuladas.isSelected() && no_facturado.isSelected()){
		   					int total = 0;
							String busca_clave = "SELECT count(*) as total FROM ads_presupuesto WHERE  fecha >= '"+fecha_inicial+"' AND fecha <= '"+fecha_final+"' AND procesado = 'N' AND anulado = 'N'";
							ResultSet busca;
							try {
								busca = conn.Consulta(busca_clave);
								if(busca.next()) {
									total  = Integer.parseInt(busca.getString("total"));
									busca.close();
								}
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
							AbstractJasperReports.NAnuladosNFacturados(conn.getConnection(), ruta+"/reports/listado presupuestos/",ruta, total, inicial_consulta, final_consulta);
							AbstractJasperReports.showViewer();
		   				} 				
			   		}else{
			   			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			   			JOptionPane.showMessageDialog(null, "La fecha Inicial es mayor a la fecha Final","Error" , JOptionPane.ERROR_MESSAGE, icono);
			   		}
		   		}else{
		   			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
		   			JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);
		   		}
			}
		});
		buscar.setIcon(new ImageIcon(ruta+"/images/search.png"));
		buscar.setBounds(44, 200, 120, 36);
		frmBuscarPresupuestos.getContentPane().add(buscar);
		
		JButton volver = new JButton("Volver");
		volver.setForeground(Color.WHITE);
		volver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				volver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				volver.setBackground(tema.getAzul());
			}
		});
		volver.setBorderPainted(false);
		volver.setBackground(tema.getAzul());
		volver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				frmBuscarPresupuestos.dispose();
			}
		});
		volver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		volver.setBounds(204, 200, 120, 36);
		frmBuscarPresupuestos.getContentPane().add(volver);
		
		JLabel borde = new JLabel("");
		borde.setBounds(10, 11, 343, 239);
		borde.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "BUSCAR PRESUPUESTOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmBuscarPresupuestos.getContentPane().add(borde);
		
	}
}
