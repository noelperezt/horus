package views;

import javax.swing.JFrame;
import java.awt.Toolkit;
import java.io.File;
import java.sql.SQLException;

import javax.swing.JLabel;
import javax.swing.JOptionPane;

import java.awt.Font;
import javax.swing.JTextField;
import models.Usuario;
import utility.EstiloHorus;
import utility.Preferencias;

import javax.swing.JButton;
import java.awt.Color;
import java.awt.Dimension;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;

public class VistaPreferencias {

	public JFrame frmPreferencias;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField textField_2;
	private JTextField textField_3;
	private JTextField textField_4;
	private JTextField textField_5;
	private JTextField textField_6;
	private JTextField textField_7;
	private Usuario user;
	public String ruta = new File ("").getAbsolutePath ();

	/**
	 * Create the application.
	 * @throws SQLException 
	 */
	public VistaPreferencias(Usuario user) throws SQLException{
		this.user = user;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		EstiloHorus tema = new EstiloHorus();
		frmPreferencias = new JFrame();
		frmPreferencias.setTitle("Preferencias");
		frmPreferencias.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmPreferencias.setResizable(false);
		frmPreferencias.setBounds(100, 100, 485, 380);
		frmPreferencias.getContentPane().setLayout(null);
		frmPreferencias.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmPreferencias.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmPreferencias.getSize();  
        frmPreferencias.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
		
		Preferencias Opreferencias = new Preferencias();
		Opreferencias.TraerRegistro();
		
		JLabel lblPorcentajeIva = new JLabel("PORCENTAJE IVA:");
		lblPorcentajeIva.setFont(new Font("Arial", Font.BOLD, 11));
		lblPorcentajeIva.setBounds(48, 45, 103, 14);
		frmPreferencias.getContentPane().add(lblPorcentajeIva);
		
		textField = new JTextField();
		textField.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_1.requestFocus();
			}
		});
		textField.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) && (caracter != '.')){
		            evt.consume();
		        }
			}
		});
		textField.setText((String) null);
		textField.setColumns(10);
		textField.setBounds(151, 42, 287, 20);
		textField.setText(Opreferencias.getIva()+"");
		frmPreferencias.getContentPane().add(textField);
		
		JLabel lblPrefijoCtrl = new JLabel("PREFIJO CTRL:");
		lblPrefijoCtrl.setFont(new Font("Arial", Font.BOLD, 11));
		lblPrefijoCtrl.setBounds(66, 74, 85, 14);
		frmPreferencias.getContentPane().add(lblPrefijoCtrl);
		
		textField_1 = new JTextField();
		textField_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_2.requestFocus();
			}
		});
		textField_1.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) && (caracter != '-')){
		            evt.consume();
		        }
			}
		});
		textField_1.setText((String) null);
		textField_1.setColumns(10);
		textField_1.setBounds(151, 70, 287, 20);
		textField_1.setText(Opreferencias.getCtrlPrefijo());
		frmPreferencias.getContentPane().add(textField_1);
		
		JLabel lblDigitosFactura = new JLabel("DIGITOS FACTURA:");
		lblDigitosFactura.setFont(new Font("Arial", Font.BOLD, 11));
		lblDigitosFactura.setBounds(43, 102, 103, 14);
		frmPreferencias.getContentPane().add(lblDigitosFactura);
		
		textField_2 = new JTextField();
		textField_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_3.requestFocus();
			}
		});
		textField_2.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		textField_2.setText((String) null);
		textField_2.setColumns(10);
		textField_2.setBounds(151, 99, 287, 20);
		textField_2.setText(Opreferencias.getDigitosFactura()+"");
		frmPreferencias.getContentPane().add(textField_2);
		
		JLabel lblDigitosCtrl = new JLabel("DIGITOS CTRL:");
		lblDigitosCtrl.setFont(new Font("Arial", Font.BOLD, 11));
		lblDigitosCtrl.setBounds(66, 133, 85, 14);
		frmPreferencias.getContentPane().add(lblDigitosCtrl);
		
		textField_3 = new JTextField();
		textField_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_4.requestFocus();
			}
		});
		textField_3.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		textField_3.setText((String) null);
		textField_3.setColumns(10);
		textField_3.setBounds(151, 130, 287, 20);
		textField_3.setText(Opreferencias.getDigitosCtrl()+"");
		frmPreferencias.getContentPane().add(textField_3);
		
		JLabel lblNmeroCtrl = new JLabel("N\u00DAMERO CTRL:");
		lblNmeroCtrl.setFont(new Font("Arial", Font.BOLD, 11));
		lblNmeroCtrl.setBounds(65, 164, 85, 14);
		frmPreferencias.getContentPane().add(lblNmeroCtrl);
		
		textField_4 = new JTextField();
		textField_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_5.requestFocus();
			}
		});
		textField_4.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		textField_4.setText((String) null);
		textField_4.setColumns(10);
		textField_4.setBounds(151, 161, 287, 20);
		textField_4.setText(Opreferencias.getNroControl()+"");
		frmPreferencias.getContentPane().add(textField_4);
		
		JLabel lblNmeroCtrlNota = new JLabel("N\u00DAMERO CTRL NOTA:");
		lblNmeroCtrlNota.setFont(new Font("Arial", Font.BOLD, 11));
		lblNmeroCtrlNota.setBounds(33, 195, 113, 14);
		frmPreferencias.getContentPane().add(lblNmeroCtrlNota);
		
		textField_5 = new JTextField();
		textField_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_6.requestFocus();
			}
		});
		textField_5.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		textField_5.setText((String) null);
		textField_5.setColumns(10);
		textField_5.setBounds(151, 192, 287, 20);
		textField_5.setText(Opreferencias.getNroControlNota()+"");
		frmPreferencias.getContentPane().add(textField_5);
		
		JLabel lblDigitosNota = new JLabel("DIGITOS NOTA:");
		lblDigitosNota.setFont(new Font("Arial", Font.BOLD, 11));
		lblDigitosNota.setBounds(66, 226, 85, 14);
		frmPreferencias.getContentPane().add(lblDigitosNota);
		
		textField_6 = new JTextField();
		textField_6.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				textField_7.requestFocus();
			}
		});
		textField_6.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		textField_6.setText((String) null);
		textField_6.setColumns(10);
		textField_6.setBounds(151, 223, 287, 20);
		textField_6.setText(Opreferencias.getDigitosNota()+"");
		frmPreferencias.getContentPane().add(textField_6);
		
		JLabel lblDigitosCtrlNota = new JLabel("DIGITOS CTRL NOTA:");
		lblDigitosCtrlNota.setFont(new Font("Arial", Font.BOLD, 11));
		lblDigitosCtrlNota.setBounds(33, 254, 118, 14);
		frmPreferencias.getContentPane().add(lblDigitosCtrlNota);
		
		textField_7 = new JTextField();
		textField_7.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				textField_7.nextFocus();
			}
		});
		textField_7.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
		            evt.consume();
		        }
			}
		});
		textField_7.setText((String) null);
		textField_7.setColumns(10);
		textField_7.setBounds(151, 251, 287, 20);
		textField_7.setText(Opreferencias.getDigitosNotaControl()+"");
		frmPreferencias.getContentPane().add(textField_7);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(textField.equals("") || textField_1.equals("") || textField_2.equals("") || textField_3.equals("")|| textField_4.equals("") || textField_5.equals("") || textField_6.equals("") || textField_7.equals("")){
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
					JOptionPane.showMessageDialog(null, "Hay Campos Vacios","Error" , JOptionPane.ERROR_MESSAGE, icono);	
				}else{
					Preferencias Opreferencias = new Preferencias(Double.parseDouble(textField.getText()), textField_1.getText(), Integer.parseInt(textField_2.getText().toString()), Integer.parseInt(textField_3.getText().toString()), Integer.parseInt(textField_4.getText().toString()), Integer.parseInt(textField_5.getText().toString()), Integer.parseInt(textField_6.getText().toString()), Integer.parseInt(textField_7.getText().toString()));
					try {
						Opreferencias.Editar();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}
			}
		});
		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnGuardar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnGuardar.setBackground(tema.getAzul());
			}
		});
		btnGuardar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		btnGuardar.setForeground(Color.WHITE);
		btnGuardar.setBorderPainted(false);
		btnGuardar.setBackground(tema.getAzul());
		btnGuardar.setBounds(96, 285, 120, 36);
		frmPreferencias.getContentPane().add(btnGuardar);
		
		JButton button_1 = new JButton("Volver");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmPreferencias.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getAzul());
			}
		});
		button_1.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(tema.getAzul());
		button_1.setBounds(268, 285, 120, 36);
		frmPreferencias.getContentPane().add(button_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 457, 329);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PREFERENCIAS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmPreferencias.getContentPane().add(lblNewLabel);
	}
}
