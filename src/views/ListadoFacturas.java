package views;

import java.awt.HeadlessException;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.io.File;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import java.awt.Dimension;
import java.awt.Toolkit;
import javax.swing.JTextField;
import models.ADSProductos;
import models.Factura;
import models.Usuario;
import net.sf.jasperreports.engine.JRException;
import utility.AbstractJasperReports;
import utility.EstiloHorus;
import utility.Ldescuento;
import utility.Tabla;

import java.awt.Font;
import javax.swing.JRadioButton;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ListadoFacturas{

	public JFrame frmFacturasPendientes;
	private Tabla listado;
	public String ruta = new File ("").getAbsolutePath ();
	private Usuario user;
	private boolean permiso_descuento;
	private JTextField textField;
	private Ldescuento lista;
	
	
	public Ldescuento getListaDescuento(){
		return lista;
	}
	
	public void setListaDescuento(Ldescuento lista){
		this.lista = lista;
	}
	
	public void setPermisoDescuento(boolean permiso_descuento){
		this.permiso_descuento = permiso_descuento;
	}
	
	public boolean getPermisoDescuento(){
		return permiso_descuento;
	}
	
	
	public ListadoFacturas(Ldescuento lista, Usuario user, boolean permiso_descuento) throws SQLException {
		setPermisoDescuento(permiso_descuento);
		setListaDescuento(lista);
		this.user = user;
		initialize();
	}
	
	
	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	
	private void initialize() throws SQLException {
		frmFacturasPendientes = new JFrame();
		frmFacturasPendientes.setResizable(false);
		frmFacturasPendientes.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmFacturasPendientes.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				Principal ventana;
				if(lista.Vacia()){
					try {
						ventana = new Principal(user);
						ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
						frmFacturasPendientes.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
					if(JOptionPane.showConfirmDialog(null, "Existen descuentos que no han sido aplicados, �Est� seguro de abandonar el m�dulo y perder los cambios realizados?","Cerrar M�dulo",1,0,icono)==0){
						try {
							ventana = new Principal(user);
							ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
							frmFacturasPendientes.dispose();
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}	
			        }
				}
			}
		});
		frmFacturasPendientes.setTitle("Facturas Pendientes");
		frmFacturasPendientes.setBounds(100, 100, 1043, 564);
		frmFacturasPendientes.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmFacturasPendientes.getContentPane().setLayout(null);
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmFacturasPendientes.getSize();  
        frmFacturasPendientes.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
        
        EstiloHorus tema = new EstiloHorus();
        
		String campos[] = {"SELECCIONAR","NRO","RAZ�N SOCIAL","DESCRIPCI�N","TOTAL DE PAGO","F. TRANSACCI�N","FECHA DE PAGO","F. CONFIRMACI�N"};
		int ancho[] = {50,1,150,170,60,60,60,60};
		int editable[] = {0};
        ADSProductos OADS = new ADSProductos();
        
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(33, 87, 971, 350);
		frmFacturasPendientes.getContentPane().add(scrollPane);
		listado = new Tabla(campos, editable, ancho, null);
		listado.PrimeraColumnaBoolean(OADS.ListarContrataciones(),false);
		OADS.desconectar();
		scrollPane.setViewportView(listado);
		
		JButton marcador = new JButton("Marcar Todas");
		marcador.setBackground(tema.getAzul());
		marcador.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				marcador.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				marcador.setBackground(tema.getAzul());
			}
		});
		marcador.setForeground(Color.WHITE);
		marcador.setBorderPainted(false);
		marcador.setIcon(new ImageIcon(ruta+"/images/check.png"));
		marcador.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(marcador.getText() == "Marcar Todas"){
					marcador.setText("Desmarcar Todas");
					marcador.setIcon(new ImageIcon(ruta+"/images/uncheck.png"));
					try {
						listado.setClearGrid();
						listado.RefrescarPrimeraColumnaBoolean(OADS.ListarContrataciones(),true);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else if(marcador.getText() == "Desmarcar Todas"){
					marcador.setText("Marcar Todas");
					marcador.setIcon(new ImageIcon(ruta+"/images/check.png"));
					try {
						listado.setClearGrid();
						listado.RefrescarPrimeraColumnaBoolean(OADS.ListarContrataciones(),false);
					} catch (SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
		});
		marcador.setBounds(143, 460, 164, 44);
		frmFacturasPendientes.getContentPane().add(marcador);

	
		JButton imprimir = new JButton("Imprimir");
		imprimir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				imprimir.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				imprimir.setBackground(tema.getAzul());
			}
		});
		imprimir.setForeground(Color.WHITE);
		imprimir.setBackground(tema.getAzul());
		imprimir.setBorderPainted(false);
		imprimir.setIcon(new ImageIcon(ruta+"/images/printer.png"));
		imprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				int contador = 0;
				for (int i = 0; i < listado.getRowCount(); i++){
					if (listado.getValueAt(i,0).toString() == "true"){ 	
						contador = contador + 1;
					}
				}
				if (contador == 0){
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar al menos un item para facturar","Advertencia" , JOptionPane.WARNING_MESSAGE,icono); 
				}else{
					VistaImpresion carga = new VistaImpresion();
					carga.setLocationRelativeTo(frmFacturasPendientes);
					carga.setVisible(true);
					for (int i = 0; i < listado.getRowCount(); i++){
						if (listado.getValueAt(i,0).toString() == "true"){ 	
							int contratacion = Integer.parseInt(listado.getValueAt(i,1).toString());
							ADSProductos OADS = new ADSProductos();
							try {
								if (OADS.ContratacionExistente(contratacion)){
									listado.deleteRow(i);
									i = i -1;
								}else{
									String tipo = "C";
									Factura Ofactura = new Factura();
									String n_factura = Ofactura.RetornaNumero(contratacion, tipo);
									Ofactura.FacturaContratacion(Ofactura.retornaIva(), contratacion, OADS.PorcentajeDescuentoTotal(lista, contratacion), OADS.RetornaMontoGravado(contratacion), Ofactura.IdFactura(n_factura), user.getUsuario());
									Ofactura.DetalleFacturaContratacion(contratacion, lista, Ofactura.IdFactura(n_factura));
									lista.RemoverDescuentoContratacion(contratacion);
									OADS.ActivarContratacion(contratacion);
									listado.deleteRow(i);
									i = i -1;
									Ofactura.RetornaClienteContratacion(contratacion);
									AbstractJasperReports.createFactura(ruta+"/reports/factura/", Ofactura.IdFactura(n_factura), n_factura, Ofactura.getRif(),Ofactura.getRazonSocial(), Ofactura.getDireccion(), Ofactura.getTelefono());
									AbstractJasperReports.Print();
								}
							} catch (HeadlessException | SQLException e) {
								e.printStackTrace();
							} catch (JRException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							
						}
					}
				}						
			}
		});
		imprimir.setBounds(337, 460, 164, 44);
		frmFacturasPendientes.getContentPane().add(imprimir);
		
		JButton salir = new JButton("Volver"); 
		salir.setBackground(tema.getAzul());
		salir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				salir.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				salir.setBackground(tema.getAzul());
			}
		});
		salir.setForeground(Color.WHITE);
		salir.setBorderPainted(false);
		salir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Principal ventana;
				if(lista.Vacia()){
					try {
						ventana = new Principal(user);
						ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
						frmFacturasPendientes.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
					if(JOptionPane.showConfirmDialog(null, "Existen descuentos que no han sido aplicados, �Est� seguro de abandonar el m�dulo y perder los cambios realizados?","Cerrar M�dulo",1,0,icono)==0){
						try {
							ventana = new Principal(user);
							ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
							frmFacturasPendientes.dispose();
						} catch (SQLException e1) {
							// TODO Auto-generated catch block
							e1.printStackTrace();
						}	
			        }
				}
			}
		});
		salir.setBounds(729, 460, 164, 44);
		salir.setIcon(new ImageIcon(ruta+"/images/back.png"));
		frmFacturasPendientes.getContentPane().add(salir);
		
		JButton btnEditar = new JButton("Descuento");
		btnEditar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnEditar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnEditar.setBackground(tema.getAzul());
			}
		});
		btnEditar.setForeground(Color.WHITE);
		btnEditar.setBackground(tema.getAzul());
		btnEditar.setBorderPainted(false);
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listado.getSelectedRow() != -1){
					int id = Integer.parseInt(listado.getValueAt(listado.getSelectedRow(),1).toString());
					try {
						VistaDescuento ventana = new VistaDescuento(id,user, lista, permiso_descuento);
						ventana.frmAplicarDescuento.setVisible(true);
						frmFacturasPendientes.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar una contrataci�n para aplicar el descuento","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnEditar.setIcon(new ImageIcon(ruta+"/images/descuento.png"));
		btnEditar.setBounds(535, 460, 164, 44);
		if(permiso_descuento == false){
			btnEditar.setEnabled(false);
		}
		frmFacturasPendientes.getContentPane().add(btnEditar);
		
		JRadioButton Contratacion = new JRadioButton("Nro Contrataci\u00F3n");
		Contratacion.setSelected(true);
		Contratacion.setBounds(35, 44, 129, 23);
		frmFacturasPendientes.getContentPane().add(Contratacion);
		
		JRadioButton Razon_Social = new JRadioButton("Raz\u00F3n Social");
		Razon_Social.setBounds(178, 44, 109, 23);
		frmFacturasPendientes.getContentPane().add(Razon_Social);
		
		 ButtonGroup grupo1 = new ButtonGroup();
		 grupo1.add(Contratacion);
		 grupo1.add(Razon_Social);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter(){
			@Override
			public void keyReleased(KeyEvent e) {
				ADSProductos OADS = new ADSProductos();
				if(Contratacion.isSelected()){
					listado.setClearGrid();
					try {
						listado.RefrescarPrimeraColumnaBoolean(OADS.ListaContratacionesNro(textField.getText()), false);
						OADS.desconectar();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					listado.setClearGrid();
					try {
						listado.RefrescarPrimeraColumnaBoolean(OADS.ListaContratacionesRazonSocial(textField.getText()), false);
						OADS.desconectar();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}
			}
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				if(Contratacion.isSelected()){
					char caracter = evt.getKeyChar();
			        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE))){
			            evt.consume();
			        }
				}
				
			}
		});
		textField.setColumns(10);
		textField.setBounds(373, 45, 382, 20);
		frmFacturasPendientes.getContentPane().add(textField);
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(320, 48, 56, 14);
		frmFacturasPendientes.getContentPane().add(label);
		
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(33, 33, 274, 43);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(0, 102, 255)));
		frmFacturasPendientes.getContentPane().add(lblNewLabel);
		
		JLabel borde = new JLabel("");
		borde.setBounds(10, 11, 1017, 514);
		borde.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "LISTADO DE FACTURAS PENDIENTES", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmFacturasPendientes.getContentPane().add(borde);
	}
}
