package views;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.text.DefaultFormatterFactory;
import javax.swing.text.NumberFormatter;
import models.Factura;
import utility.EstiloHorus;
import utility.Tabla;
import java.awt.Toolkit;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import java.awt.Color;
import java.awt.event.KeyAdapter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.NumberFormat;
import java.util.Locale;
import java.awt.Font;
import javax.swing.JFormattedTextField;
import javax.swing.JTextField;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;

import javax.swing.Icon;
import javax.swing.ImageIcon;

@SuppressWarnings("serial")
public class ListaTiposPago extends JDialog {
	private Tabla listatipospago;
	private double total;
	private JFormattedTextField venta;
	private JTextField doc;
	@SuppressWarnings("rawtypes")
	private JComboBox banco;
	private double total_venta;

	/**
	 * Create the dialog.
	 * @throws SQLException 
	 */
	public int ObtenerIDPago(String tipo){
		int valor = 0;
		if(!(tipo.equals("Ninguno"))){
			String[] valores = tipo.split("-");
			valor = Integer.parseInt(valores[0]);
		}
		return valor;
	}
	
	public String ObtenerCODBanco(String banco){
		String cod = "";
		if(!banco.equals("")){
			String[] valores = banco.split("-");
			cod = valores[0];
		}
		return cod;
	}
	
	public Tabla getListaTiposPago(){
		return listatipospago;
	}
	
	public void setListaTiposPago(Tabla listatipospago){
		this.listatipospago = listatipospago;
	}
	
	public double getVenta(){
		return total_venta;
	}
	
	public void setVenta(double venta){
		this.venta.setValue(venta);
	}
	
	public boolean ValidaTipoPago(Tabla pagos, int id_pago){
		boolean respuesta = false;
		for(int i = 0; i < pagos.getRowCount(); i ++){
			if(Integer.parseInt(pagos.getValueAt(i, 0).toString()) == id_pago){
				respuesta = true;
			}
		}
		return respuesta;
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public ListaTiposPago(double total) throws SQLException {
		String ruta = new File ("").getAbsolutePath ();
		setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
		this.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				if(Double.parseDouble(venta.getValue().toString()) > 0){
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
					if(JOptionPane.showConfirmDialog(null, "�Est� seguro de cerrar la ventana? existe una diferencia de "+venta.getText()+" y no podr� continuar con el proceso de facturaci�n","Cerrar",1,0,icono)==0){
			            dispose();
			        }
				}else{
					dispose();
				}	
			}
		});
		setModal(true);
		EstiloHorus tema = new EstiloHorus();
		this.total = total;
		setTitle("Tipos de Pago");
		setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		setResizable(false);
		setBounds(100, 100, 736, 302);
		getContentPane().setLayout(null);
		
		String campos[] = {"T. PAGO","MONTO","N� DOC.","BANCO"};
		int ancho[] = {150,150,150,150};
		int editable[] = null;
		
		JScrollPane scrollPane = new JScrollPane();
		getContentPane().add(scrollPane);
		Class[] tipos = new Class[] {java.lang.String.class, java.lang.Double.class, java.lang.String.class, java.lang.String.class};
		scrollPane.setBounds(386, 68, 319, 128);
		listatipospago = new Tabla(campos, editable, ancho, tipos);
		Factura Ofactura = new Factura();
		
		listatipospago.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyReleased(java.awt.event.KeyEvent evt) {
				int caracter = evt.getKeyCode();
		        if(caracter == evt.VK_DELETE){
		        	if(listatipospago.getRowCount() > 0){
		        		listatipospago.deleteRow(listatipospago.getSelectedRow());
					}else{
						Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
						JOptionPane.showMessageDialog(null, "Lista Vacia","Error" , JOptionPane.ERROR_MESSAGE, icono); 
					}
		        }	
		        double totales = 0.0;
		        for(int i= 0; i < listatipospago.getRowCount(); i++){
		        	totales = totales + Double.parseDouble(listatipospago.getValueAt(i, 1).toString());
		        }
		        
		        if (totales > total){
		        	Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
					JOptionPane.showMessageDialog(null, "Monto Excedido","Error" , JOptionPane.ERROR_MESSAGE, icono); 
					listatipospago.deleteRow(listatipospago.getRowCount()-1);
		        }else{
		        	venta.setValue(total-totales);
		        }
			}
		});
		scrollPane.setViewportView(listatipospago);
		
		JButton btnGuardar = new JButton("Guardar");
		btnGuardar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Double.parseDouble(venta.getValue().toString()) > 0){
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
					JOptionPane.showMessageDialog(null, "Existe una diferencia de "+venta.getText()+" que no ha sido cancelada","Error" , JOptionPane.ERROR_MESSAGE, icono); 
				}else{
					total_venta = Double.parseDouble(venta.getValue().toString());
					dispose();
				}
			}
		});
		btnGuardar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnGuardar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnGuardar.setBackground(tema.getAzul());
			}
		});
		btnGuardar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		btnGuardar.setForeground(Color.WHITE);
		btnGuardar.setBorderPainted(false);
		btnGuardar.setBackground(tema.getAzul());
		btnGuardar.setBounds(197, 213, 120, 36);
		getContentPane().add(btnGuardar);
		
		JButton button_1 = new JButton("Volver");
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(Double.parseDouble(venta.getValue().toString()) > 0){
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/interrogacion.png"));
					if(JOptionPane.showConfirmDialog(null, "�Est� seguro de cerrar la ventana? existe una diferencia de "+venta.getText()+" y no podr� continuar con el proceso de facturaci�n","Cerrar",1,0,icono)==0){
			            listatipospago.setClearGrid();
						dispose();
			        }
				}else{
					listatipospago.setClearGrid();
					dispose();
				}	
			}
		});
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getAzul());
			}
		});
		button_1.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.setBackground(tema.getAzul());
		button_1.setBounds(423, 213, 120, 36);
		getContentPane().add(button_1);
		
		JLabel label = new JLabel("TOTAL:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(511, 36, 46, 14);
		getContentPane().add(label);
		
		NumberFormat dispFormat = NumberFormat.getCurrencyInstance(Locale.getDefault());
		NumberFormat editFormat = NumberFormat.getNumberInstance(Locale.ENGLISH);
		editFormat.setGroupingUsed(false);
		NumberFormatter dnFormat = new NumberFormatter(dispFormat);
		NumberFormatter enFormat = new NumberFormatter(editFormat);
		DefaultFormatterFactory currFactory = new DefaultFormatterFactory(dnFormat, dnFormat, enFormat);
		enFormat.setAllowsInvalid(true);
		
		venta = new JFormattedTextField();
		venta.setEnabled(false);
		venta.setBounds(557, 33, 148, 20);
		venta.setFormatterFactory(currFactory);
		venta.setValue(this.total);
		getContentPane().add(venta);
		
		JLabel lblTipoDePago = new JLabel("TIPO DE PAGO:");
		lblTipoDePago.setFont(new Font("Arial", Font.BOLD, 11));
		lblTipoDePago.setBounds(42, 72, 78, 14);
		getContentPane().add(lblTipoDePago);
		
		JComboBox tipo = new JComboBox();
		ResultSet buscar = Ofactura.RetornaTiposPago();
		tipo.addItem("Ninguno");
		while (buscar.next()){
			tipo.addItem(buscar.getObject("id_pago")+"-"+buscar.getObject("descripcion"));
		}
		Ofactura.desconectar();
		tipo.setBounds(130, 69, 186, 20);
		tipo.addItemListener(new ItemListener() {
			public void itemStateChanged(ItemEvent arg0) {
				if(ObtenerIDPago(tipo.getSelectedItem().toString()) == 2){
					doc.setEnabled(false);
					banco.setEnabled(false);
					doc.setText("");
					banco.setSelectedIndex(0);
				}else{
					doc.setEnabled(true);
					banco.setEnabled(true);
				}
			}
		});
		getContentPane().add(tipo);
		
		JLabel lblMonto = new JLabel("MONTO:");
		lblMonto.setFont(new Font("Arial", Font.BOLD, 11));
		lblMonto.setBounds(79, 100, 53, 14);
		getContentPane().add(lblMonto);
		
		JFormattedTextField monto = new JFormattedTextField();
		monto.setValue(this.total);
		monto.setFormatterFactory(currFactory);
		monto.setBounds(130, 97, 186, 20);
		getContentPane().add(monto);
		
		JLabel lblNDocumento = new JLabel("N\u00BA DOCUMENTO:");
		lblNDocumento.setFont(new Font("Arial", Font.BOLD, 11));
		lblNDocumento.setBounds(35, 131, 85, 14);
		getContentPane().add(lblNDocumento);
		
		doc = new JTextField();
		doc.setEnabled(false);
		doc.setText((String) null);
		doc.setColumns(10);
		doc.setBounds(130, 128, 186, 20);
		getContentPane().add(doc);
		
		JLabel lblBanco = new JLabel("BANCO:");
		lblBanco.setFont(new Font("Arial", Font.BOLD, 11));
		lblBanco.setBounds(79, 162, 41, 14);
		getContentPane().add(lblBanco);
		
		banco = new JComboBox();
		banco.setEnabled(false);
		banco.setBounds(130, 159, 186, 20);
		buscar = Ofactura.RetornaBancos();
		banco.addItem("Ninguno");
		while (buscar.next()){
			banco.addItem(buscar.getObject("codigo_banco")+"-"+buscar.getObject("nombre_banco"));
		}
		Ofactura.desconectar();
		getContentPane().add(banco);
		
		JButton btnNewButton = new JButton("");
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/right.png"));
		btnNewButton.setBorderPainted(false);
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent arg0) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(ObtenerIDPago(tipo.getSelectedItem().toString()) != 0){
					if(!ValidaTipoPago(listatipospago, ObtenerIDPago(tipo.getSelectedItem().toString()))){
						if (Double.parseDouble(monto.getValue().toString()) != 0){
							if(ObtenerIDPago(tipo.getSelectedItem().toString()) != 2){
								if(!(doc.getText().equals("") || (banco.getSelectedItem().equals("Ninguno")))){
									double diferencia = Double.parseDouble(venta.getValue().toString()) - Double.parseDouble(monto.getValue().toString());
									if(diferencia >= 0){
										Object[] vector = new Object[4];
										vector[0] = ObtenerIDPago(tipo.getSelectedItem().toString());
										vector[1] = monto.getValue();
										vector[2] = doc.getText();
										vector[3] = ObtenerCODBanco(banco.getSelectedItem().toString());
										listatipospago.addRow(vector);
										tipo.setSelectedIndex(0);
										monto.setValue(0);
										doc.setText("");
										banco.setSelectedIndex(0);
										venta.setValue(diferencia);
									}else{
										Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
										JOptionPane.showMessageDialog(null, "Monto Excedido","Error" , JOptionPane.ERROR_MESSAGE, icono);
									}	
								}else{
									Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
									JOptionPane.showMessageDialog(null, "Debe ingresar el N� de documento y seleccionar un banco","Error" , JOptionPane.ERROR_MESSAGE, icono); 
								}
							}else{
								double diferencia = Double.parseDouble(venta.getValue().toString())  - Double.parseDouble(monto.getValue().toString());
								if(diferencia >= 0){
									Object[] vector = new Object[4];
									vector[0] = ObtenerIDPago(tipo.getSelectedItem().toString());
									vector[1] = monto.getValue();
									vector[2] = "";
									vector[3] = "";
									listatipospago.addRow(vector);
									tipo.setSelectedIndex(0);
									monto.setValue(0);
									doc.setText("");
									banco.setSelectedIndex(0);
									venta.setValue(diferencia);
								}else{
									Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
									JOptionPane.showMessageDialog(null, "Monto Excedido","Error" , JOptionPane.ERROR_MESSAGE, icono);
								}		
							}
						}else{
							Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
							JOptionPane.showMessageDialog(null, "Debe ingresar un monto superior a 0","Error" , JOptionPane.ERROR_MESSAGE, icono); 
						}
					}else{
						Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
						JOptionPane.showMessageDialog(null, "No puede ingresar montos distintos con el mismo tipo de pago","Error" , JOptionPane.ERROR_MESSAGE, icono);
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un tipo de pago","Error" , JOptionPane.ERROR_MESSAGE, icono); 
				}
			}
		});
		btnNewButton.setBounds(332, 99, 46, 23);
		btnNewButton.setBackground(tema.getAzul());
		getContentPane().add(btnNewButton);
		
		JButton button_2 = new JButton("");
		button_2.setIcon(new ImageIcon(ruta+"/images/left.png"));
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listatipospago.getSelectedRow() != -1){
					venta.setValue(Double.parseDouble(venta.getValue().toString())+Double.parseDouble(listatipospago.getValueAt(listatipospago.getSelectedRow(), 1).toString()));
					Factura Ofactura = new Factura();
					try {
						tipo.setSelectedItem(Ofactura.RetornaCadenaPago(Integer.parseInt(listatipospago.getValueAt(listatipospago.getSelectedRow(), 0).toString())));
						monto.setValue(Double.parseDouble(listatipospago.getValueAt(listatipospago.getSelectedRow(), 1).toString()));
						doc.setText(listatipospago.getValueAt(listatipospago.getSelectedRow(), 2).toString());
						banco.setSelectedItem(Ofactura.RetornaCadenaBanco(listatipospago.getValueAt(listatipospago.getSelectedRow(), 3).toString()));
					} catch (NumberFormatException | SQLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					listatipospago.deleteRow(listatipospago.getSelectedRow());
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un item","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		button_2.setBorderPainted(false);
		button_2.setForeground(Color.WHITE);
		button_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_2.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_2.setBackground(tema.getAzul());
			}
		});
		button_2.setBounds(332, 127, 46, 23);
		button_2.setBackground(tema.getAzul());
		getContentPane().add(button_2);
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setBounds(27, 54, 296, 142);
		lblNewLabel_1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		getContentPane().add(lblNewLabel_1);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "TIPOS DE PAGO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		lblNewLabel.setBounds(10, 11, 711, 256);
		getContentPane().add(lblNewLabel);
	}
}
