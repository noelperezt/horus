package views;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import models.Banco;
import models.Usuario;
import utility.EstiloHorus;
import utility.Tabla;

import javax.swing.JTextField;
import java.awt.Font;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdministrarBancos {

	public JFrame frmAdministrarBancos;
	private Tabla listabancos;
	private JTextField textField;
	private Usuario user;
	private String ruta = new File ("").getAbsolutePath ();
	
	public AdministrarBancos(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmAdministrarBancos = new JFrame();
		frmAdministrarBancos.setTitle("Administrar Bancos");
		frmAdministrarBancos.setBounds(100, 100, 723, 305);
		frmAdministrarBancos.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAdministrarBancos.getContentPane().setLayout(null);
		frmAdministrarBancos.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarBancos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAdministrarBancos.getSize();  
        frmAdministrarBancos.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
        
		EstiloHorus tema = new EstiloHorus();
        
        String campos[] = {"ID","BANCO","N� DE CUENTA","KEY ID","PUBLIC KEY ID","ESTADO"};
		int ancho[] = {1,100,100,80,80,50};
		int editable[] = null;
		Banco Obanco = new Banco();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(37, 74, 497, 163);
		listabancos = new Tabla(campos, editable, ancho, null);
		listabancos.Listar(Obanco.ListarBancos());
		Obanco.desconectar();
		
		frmAdministrarBancos.getContentPane().add(scrollPane);
		scrollPane.setViewportView(listabancos);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent arg0) {
				listabancos.setClearGrid();
				try {
					listabancos.Refrescar(Obanco.FiltraBancos(textField.getText()));
					Obanco.desconectar();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		textField.setToolTipText("Busqueda por Nombre del Banco");
		textField.setColumns(10);
		textField.setBounds(91, 43, 346, 20);
		frmAdministrarBancos.getContentPane().add(textField);
		
		JLabel label = new JLabel("BUSCAR:");
		label.setFont(new Font("Arial", Font.BOLD, 11));
		label.setBounds(37, 46, 56, 14);
		frmAdministrarBancos.getContentPane().add(label);
		
		JButton button = new JButton("Agregar");
		button.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button.setBackground(tema.getAzul());
			}
		});
		button.setBackground(tema.getAzul());
		button.setForeground(Color.WHITE);
		button.setBorderPainted(false);
		button.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					RegistroBancos ventana = new RegistroBancos(0,user);
					ventana.frmRegistroDeBancos.setVisible(true);
					frmAdministrarBancos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		button.setIcon(new ImageIcon(ruta+"/images/add.png"));
		button.setBounds(554, 72, 125, 33);
		frmAdministrarBancos.getContentPane().add(button);
		
		JButton button_1 = new JButton("Editar");
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getAzul());
			}
		});
		button_1.setBackground(tema.getAzul());
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listabancos.getSelectedRow() != -1){
					int id = Integer.parseInt(listabancos.getValueAt(listabancos.getSelectedRow(), 0).toString());
					try {
						RegistroBancos ventana = new RegistroBancos(id,user);
						ventana.frmRegistroDeBancos.setVisible(true);
						frmAdministrarBancos.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un banco para editar","Advertencia" , JOptionPane.WARNING_MESSAGE,icono); 
				}
			}
		});
		button_1.setIcon(new ImageIcon(ruta+"/images/edit.png"));
		button_1.setBounds(554, 116, 125, 33);
		frmAdministrarBancos.getContentPane().add(button_1);
		
		JButton button_2 = new JButton("Estado");
		button_2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_2.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_2.setBackground(tema.getAzul());
			}
		});
		button_2.setBackground(tema.getAzul());
		button_2.setForeground(Color.WHITE);
		button_2.setBorderPainted(false);
		button_2.setIcon(new ImageIcon(ruta+"/images/del.png"));
		button_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listabancos.getSelectedRow() != -1){
					int id = Integer.parseInt(listabancos.getValueAt(listabancos.getSelectedRow(), 0).toString());
					try {
						listabancos.CambioEstadoBanco(id, listabancos.getSelectedRow(), 5);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un usuario para cambiar su estado","Advertencia" , JOptionPane.WARNING_MESSAGE,icono); 
				}
			}
		});
		button_2.setToolTipText("Cambiar Estado");
		button_2.setBounds(554, 160, 125, 33);
		frmAdministrarBancos.getContentPane().add(button_2);
		
		JButton button_3 = new JButton("Volver");
		button_3.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_3.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_3.setBackground(tema.getAzul());
			}
		});
		button_3.setForeground(Color.WHITE);
		button_3.setBackground(tema.getAzul());
		button_3.setBorderPainted(false);
		button_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarBancos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		button_3.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button_3.setBounds(554, 204, 125, 33);
		frmAdministrarBancos.getContentPane().add(button_3);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 686, 254);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ADMINISTRAR BANCOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAdministrarBancos.getContentPane().add(lblNewLabel);
	}

}
