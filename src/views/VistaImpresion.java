package views;

import java.awt.FlowLayout;
import java.awt.Toolkit;
import java.io.File;
import javax.swing.ImageIcon;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JProgressBar;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import utility.EstiloHorus;
import utility.HiloProgreso;

@SuppressWarnings("serial")
public class VistaImpresion extends JDialog {

	private final JPanel contentPanel = new JPanel();

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			VistaImpresion dialog = new VistaImpresion();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public VistaImpresion() {
		setTitle("Imprimiendo....");
		setResizable(false);
		setModal(true);
		String ruta = new File ("").getAbsolutePath ();
		contentPanel.setLayout(new FlowLayout());
		setBounds(100, 100, 675, 208);
		setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		
		JProgressBar progressBar = new JProgressBar();
		progressBar.setStringPainted(true);
		EstiloHorus tema = new EstiloHorus();
        progressBar.setForeground(tema.getAzul());
        progressBar.setBorderPainted(false);
        HiloProgreso hilo = new HiloProgreso(progressBar);
        hilo.start();
        progressBar.addChangeListener(new ChangeListener() {
        	public void stateChanged(ChangeEvent arg0) {
                if(progressBar.getValue()==100){
                	dispose();
                }
        	}
        });
        getContentPane().setLayout(null);
        getContentPane().setLayout(null);
        progressBar.setBounds(10, 144, 430, 24);
        getContentPane().add(progressBar);
        
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setIcon(new ImageIcon(ruta+"/images/print.png"));
        lblNewLabel.setBounds(155, 10, 139, 123);
        getContentPane().add(lblNewLabel);
		
		setBounds(100, 100, 450, 208);
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		
	}

}
