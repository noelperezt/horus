package views;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;
import models.Permiso;
import models.Usuario;
import utility.EstiloHorus;
import utility.Tabla;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class EditarPermisos {

	public JFrame frmModificarPermisos;
	private int usuario;
	private Tabla listapermisos;
	private Usuario user;
	
	public EditarPermisos(int id, Usuario user) throws SQLException {
		this.user = user;
		this.usuario = id;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		String ruta = new File ("").getAbsolutePath ();
		frmModificarPermisos = new JFrame();
		frmModificarPermisos.setTitle("Modificar Permisos");
		frmModificarPermisos.setBounds(100, 100, 557, 365);
		frmModificarPermisos.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmModificarPermisos.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmModificarPermisos.getContentPane().setLayout(null);
		frmModificarPermisos.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					AdministrarUsuarios ventana = new AdministrarUsuarios(user);
					ventana.frmAdministrarUsuarios.setVisible(true);
					frmModificarPermisos.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
	    Dimension ventana = frmModificarPermisos.getSize();  
	    frmModificarPermisos.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
	    frmModificarPermisos.getContentPane().setLayout(null);
		
		EstiloHorus tema = new EstiloHorus();
	    
		Permiso Opermiso = new Permiso();
		Opermiso.RegistrarPermisos(usuario);

		String campos[] = {"ID","M�DULO","SUB-M�DULO","ESTADO"};
		int ancho[] = {1,100,100,1};
		int editable[] = null;
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(30, 45, 489, 202);
		listapermisos = new Tabla(campos, editable, ancho, null);
		listapermisos.Listar(Opermiso.ListarPermisos(usuario));
		
		frmModificarPermisos.getContentPane().add(scrollPane);
		scrollPane.setViewportView(listapermisos);
		
		JButton btnNewButton = new JButton("Volver");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBackground(tema.getAzul());
		btnNewButton.setBorderPainted(false);
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AdministrarUsuarios ventana;
				try {
					ventana = new AdministrarUsuarios(user);
					ventana.frmAdministrarUsuarios.setVisible(true);
					frmModificarPermisos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(299, 261, 164, 44);
		frmModificarPermisos.getContentPane().add(btnNewButton);
		
		JButton btnPermiso = new JButton("Permiso");
		btnPermiso.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnPermiso.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnPermiso.setBackground(tema.getAzul());
			}
		});
		
		btnPermiso.setForeground(Color.WHITE);
		btnPermiso.setBackground(tema.getAzul());
		btnPermiso.setBorderPainted(false);
		btnPermiso.setIcon(new ImageIcon(ruta+"/images/permiso.png"));
		btnPermiso.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listapermisos.getSelectedRow() != -1){
					int id = Integer.parseInt(listapermisos.getValueAt(listapermisos.getSelectedRow(), 0).toString());
					try {
						listapermisos.CambioEstadoPermiso(id, listapermisos.getSelectedRow(), 3);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un Sub-M�dulo para cambiar su Estado","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnPermiso.setBounds(83, 261, 164, 44);
		frmModificarPermisos.getContentPane().add(btnPermiso);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 527, 314);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null,"MODIFICAR PERMISOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmModificarPermisos.getContentPane().add(lblNewLabel);
	}
}
