package views;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import javax.swing.JFrame;
import javax.swing.JProgressBar;
import javax.swing.event.ChangeListener;
import com.itextpdf.text.Image;
import com.sun.awt.AWTUtilities;
import utility.EstiloHorus;
import utility.HiloProgreso;
import javax.swing.event.ChangeEvent;
import javax.swing.JLabel;
import javax.swing.Icon;
import javax.swing.ImageIcon;

public class Carga {
	public int i = 50;
	public float j = 1;
	public JFrame frame;

	public Carga() throws InterruptedException {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws InterruptedException 
	 */
	private void initialize() throws InterruptedException {
		
		String ruta = new File ("").getAbsolutePath ();
		frame = new JFrame();
		frame.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frame.setBounds(100, 100, 487, 170);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setUndecorated(true);
        
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frame.getSize();  
        frame.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
        frame.getContentPane().setLayout(null);
        
        
        JProgressBar progressBar = new JProgressBar();
        progressBar.setStringPainted(true);
        EstiloHorus tema = new EstiloHorus();
        progressBar.setForeground(tema.getAzul());
        progressBar.setBorderPainted(false);
        HiloProgreso hilo = new HiloProgreso(progressBar);
        hilo.start();
        progressBar.addChangeListener(new ChangeListener() {
        	public void stateChanged(ChangeEvent arg0) {
        		if(progressBar.getValue()==i){
        	        if(j!=1.1){
        	            AWTUtilities.setWindowOpacity(frame, j);
        	            i++;
        	            j = (float) (j - 0.02);
        	        }
        	    }

                if(progressBar.getValue()==100){
                	Login abrir = new Login();
                	abrir.frmIniciarSesin.setVisible(true);
                	frame.dispose();
                }
        	}
        });
        progressBar.setBounds(236, 72, -5, 20);
        frame.getContentPane().add(progressBar);   
        JLabel lblNewLabel = new JLabel("");
        lblNewLabel.setBounds(38, 43, 408, 85);
        ImageIcon image = new ImageIcon(ruta+"/images/imagecarga.png");  
        Icon icono = new ImageIcon(image.getImage().getScaledInstance(lblNewLabel.getWidth(), lblNewLabel.getHeight(), Image.DEFAULT));
        lblNewLabel.setIcon(icono);
        
        
        
        frame.getContentPane().add(lblNewLabel);
	}
}
