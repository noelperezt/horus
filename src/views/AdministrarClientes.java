package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.io.File;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JScrollPane;
import javax.swing.JButton;
import javax.swing.DebugGraphics;
import javax.swing.ImageIcon;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.ScrollPaneConstants;
import models.Cliente;
import models.Usuario;
import utility.EstiloHorus;
import utility.Tabla;
import java.awt.Font;
import javax.swing.JTextField;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Cursor;

public class AdministrarClientes {

	public JFrame frmAdministrarClientes;
	public Tabla listaclientes;
	public String ruta = new File ("").getAbsolutePath ();
	private JTextField textField;
	private Usuario user;
	
	public AdministrarClientes(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {	
		
		frmAdministrarClientes = new JFrame();
		frmAdministrarClientes.setResizable(false);
		frmAdministrarClientes.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarClientes.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		frmAdministrarClientes.setTitle("Administrar Clientes");
		frmAdministrarClientes.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAdministrarClientes.setBounds(100, 100, 960, 521);
		frmAdministrarClientes.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmAdministrarClientes.getContentPane().setLayout(null);

		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAdministrarClientes.getSize();  
        frmAdministrarClientes.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
        EstiloHorus tema = new EstiloHorus();
		
		JButton btnNewButton = new JButton("Agregar");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.setBackground(tema.getAzul());
		btnNewButton.setForeground(Color.WHITE);
		
		btnNewButton.setBorderPainted(false);
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				try {
					RegistroClientes ventana = new RegistroClientes(0,user);
					ventana.RegistroClientes.setVisible(true);
					frmAdministrarClientes.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/ico_agregar.png"));
		btnNewButton.setBounds(799, 41, 121, 18);
		frmAdministrarClientes.getContentPane().add(btnNewButton);
		
		String campos[] = {"ID","RIF","RAZ�N SOCIAL","CORREO","REPRESENTANTE LEGAL","TEL�FONO","ESTADO","",""};
		int ancho[] = {1,50,130,120,120,50,20,1,1};
		int editable[] = null;
		Cliente Ocliente = new Cliente();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(37, 68, 883, 342);
		listaclientes = new Tabla(campos, editable, ancho, null);
		listaclientes.TableBotones(Ocliente.ListarClientes(), frmAdministrarClientes, user);
		Ocliente.desconectar();
		
		frmAdministrarClientes.getContentPane().add(scrollPane);
		scrollPane.setViewportView(listaclientes);
		
		JButton btnActualizar = new JButton("Actualizar");
		btnActualizar.setForeground(Color.WHITE);
		btnActualizar.setAutoscrolls(true);
		btnActualizar.setDefaultCapable(false);
		btnActualizar.setDebugGraphicsOptions(DebugGraphics.NONE_OPTION);
		btnActualizar.setDoubleBuffered(true);
		btnActualizar.setRolloverEnabled(false);
		btnActualizar.setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		btnActualizar.setRequestFocusEnabled(false);
		btnActualizar.setBorder(null);
		btnActualizar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnActualizar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnActualizar.setBackground(tema.getAzul());
			}
		});

		btnActualizar.setBackground(tema.getAzul());
		btnActualizar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					listaclientes.setClearGrid();
					listaclientes.RefrescarTablaBotones(Ocliente.ListarClientes());
					Ocliente.desconectar();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnActualizar.setIcon(new ImageIcon(ruta+"/images/refresh.png"));
		btnActualizar.setBounds(245, 421, 164, 44);
		frmAdministrarClientes.getContentPane().add(btnActualizar);
		
		JButton button_1 = new JButton("Volver");
		button_1.setForeground(Color.WHITE);
		button_1.setBorderPainted(false);
		button_1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				button_1.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				button_1.setBackground(tema.getAzul());
			}
		});
		button_1.setBackground(tema.getAzul());
		button_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Principal ventana;
				try {
					ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarClientes.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		button_1.setIcon(new ImageIcon(ruta+"/images/back.png"));
		button_1.setBounds(544, 421, 164, 44);
		frmAdministrarClientes.getContentPane().add(button_1);
		
		JLabel lblBuscar = new JLabel("BUSCAR:");
		lblBuscar.setFont(new Font("Arial", Font.BOLD, 11));
		lblBuscar.setBounds(37, 41, 56, 14);
		frmAdministrarClientes.getContentPane().add(lblBuscar);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
				if(((caracter < 'a') || (caracter > 'z')) && ((caracter < 'A') || (caracter > 'Z')) && ((caracter < '0') || (caracter > '9')) &&(caracter != evt.VK_BACK_SPACE) && caracter != evt.VK_SPACE ) {
			         evt.consume();
			    }
			}
			@Override
			public void keyReleased(KeyEvent e) {
				listaclientes.setClearGrid();
				try {
					listaclientes.RefrescarTablaBotones(Ocliente.FiltraClientes(textField.getText()));
					Ocliente.desconectar();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		textField.setToolTipText("Busqueda por Raz\u00F3n Social");
		textField.setFont(new Font("Arial", Font.PLAIN, 11));
		textField.setColumns(10);
		textField.setBounds(92, 37, 346, 20);
		frmAdministrarClientes.getContentPane().add(textField);
		
		JLabel borde = new JLabel("");
		borde.setBounds(10, 11, 934, 471);
		borde.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ADMINISTRAR CLIENTES", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAdministrarClientes.getContentPane().add(borde);
	}
}
