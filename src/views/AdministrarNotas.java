package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;

import models.Usuario;
import utility.Conexion;
import utility.EstiloHorus;
import utility.Tabla;

import java.awt.Font;
import java.awt.Toolkit;
import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.io.File;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdministrarNotas {

	public JFrame frmAdministrarNotas;
	private JTextField textField;
	private Tabla listanotas;
	public String ruta = new File ("").getAbsolutePath ();
	private Usuario user;
	
	public AdministrarNotas(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}
	
	public int RetornaIdFactura(String factura) throws SQLException{
		int id = 0;
		Conexion conn = new Conexion();
		
		ResultSet busca;
		String sql = "SELECT  pk_id FROM ads_facturacion WHERE numero_factura = '"+factura+"'";
		busca = conn.Consulta(sql);
		if(busca.next()){
			id = Integer.parseInt(busca.getString("pk_id").toString());
			busca.close();
		}
		
		return id;
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmAdministrarNotas = new JFrame();
		frmAdministrarNotas.setResizable(false);
		frmAdministrarNotas.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAdministrarNotas.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarNotas.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		frmAdministrarNotas.setTitle("Administrar Notas de Cr\u00E9dito");
		frmAdministrarNotas.setBounds(100, 100, 754, 473);
		frmAdministrarNotas.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmAdministrarNotas.getContentPane().setLayout(null);
		Conexion conn = new Conexion();
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAdministrarNotas.getSize();  
        frmAdministrarNotas.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
        EstiloHorus tema = new EstiloHorus();
        
		JLabel lblBuscar = new JLabel("BUSCAR:");
		lblBuscar.setFont(new Font("Arial", Font.BOLD, 11));
		lblBuscar.setBounds(42, 41, 56, 14);
		frmAdministrarNotas.getContentPane().add(lblBuscar);
		
		textField = new JTextField();
		textField.addKeyListener(new KeyAdapter() {
			@Override
			public void keyReleased(KeyEvent e) {
				String n_fact = "";
				for(int i = 0; i< 7-textField.getText().length(); i++){
					n_fact = n_fact + "0";
				}
				n_fact = n_fact + textField.getText();
				if(n_fact.equals("0000000")){
					n_fact = "";
				}
				String sentencia = "SELECT ads_notascredito.pk_id, numero_nota, numero_factura, motivo_anulacion,ads_notascredito.fecha_documento, ads_contrataciones.total_pago, nombre FROM tb_administrador, ads_notascredito, ads_facturacion, ads_contrataciones WHERE tb_administrador.id_admin = ads_notascredito.id_usuario AND ads_notascredito.numero_documento = ads_facturacion.pk_id AND ads_facturacion.numero_documento = ads_contrataciones.pk_id AND ads_notascredito.numero_nota ILIKE '%"+n_fact+"%' ORDER BY ads_notascredito.fecha_documento";
				listanotas.setClearGrid();
				try {
					listanotas.Refrescar(conn.Consulta(sentencia));
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if((((caracter < '0') || (caracter > '9')) && (caracter != evt.VK_BACK_SPACE)) || textField.getText().length() == 7 ){
		            evt.consume();
		        }
			}
		});
		textField.setToolTipText("Busqueda por N\u00B0 de Nota de Cr\u00E9dito");
		textField.setFont(new Font("Arial", Font.PLAIN, 11));
		textField.setColumns(10);
		textField.setBounds(98, 37, 346, 20);
		frmAdministrarNotas.getContentPane().add(textField);
		
		String campos[] = {"ID","N� NOTA","N� FACTURA","MOTIVO ANULACI�N","FECHA","TOTAL","PROCESADO POR:"};
		int ancho[] = {1,50,50,150,50,50,100};
		int editable[] = null;
		String sentencia = "SELECT ads_notascredito.pk_id, numero_nota, numero_factura, motivo_anulacion,ads_notascredito.fecha_documento, ads_contrataciones.total_pago, nombre FROM tb_administrador, ads_notascredito, ads_facturacion, ads_contrataciones WHERE tb_administrador.id_admin = ads_notascredito.id_usuario AND ads_notascredito.numero_documento = ads_facturacion.pk_id AND ads_facturacion.numero_documento = ads_contrataciones.pk_id ORDER BY ads_notascredito.pk_id";
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(37, 68, 680, 292);
		listanotas = new Tabla(campos, editable, ancho, null);
		listanotas.Listar(conn.Consulta(sentencia));
		
		frmAdministrarNotas.getContentPane().add(scrollPane);
		scrollPane.setViewportView(listanotas);
		
		JButton btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.setForeground(Color.WHITE);
		btnVolver.setBorderPainted(false);
		btnVolver.setBackground(tema.getAzul());
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Principal ventana;
				try {
					ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarNotas.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}	
			}
		});
		btnVolver.setBounds(431, 376, 164, 44);
		frmAdministrarNotas.getContentPane().add(btnVolver);
		
		JButton btnReimprimir = new JButton("Reimprimir");
		btnReimprimir.setBackground(tema.getAzul());
		btnReimprimir.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnReimprimir.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnReimprimir.setBackground(tema.getAzul());
			}
		});
		btnReimprimir.setForeground(Color.WHITE);
		btnReimprimir.setBorderPainted(false);
		btnReimprimir.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if (listanotas.getSelectedRow() != -1){
					int id;
					try {
						id = RetornaIdFactura(listanotas.getValueAt(listanotas.getSelectedRow(), 2).toString());
						ReimprimirNota a = new ReimprimirNota(id);
						a.frmReimprimirNota.setVisible(true);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un item para Reimprimir","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnReimprimir.setIcon(new ImageIcon(ruta+"/images/printer.png"));
		btnReimprimir.setBounds(154, 376, 164, 44);
		frmAdministrarNotas.getContentPane().add(btnReimprimir);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 730, 428);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ADMINISTRAR NOTAS DE CR�DITO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAdministrarNotas.getContentPane().add(lblNewLabel);
	}
}
