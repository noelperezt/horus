package views;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.HeadlessException;
import java.awt.Toolkit;
import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JRadioButton;

import net.sf.jasperreports.engine.JRException;
import utility.AbstractJasperReports;
import utility.Conexion;
import utility.EstiloHorus;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class ReimprimirNota {

	public JFrame frmReimprimirNota;
	private int idfactura;
	public String ruta = new File ("").getAbsolutePath ();
	
	public ReimprimirNota(int id_factura) {
		setIdFactura(id_factura);
		initialize();
	}
	
	
	public void setIdFactura(int id_factura){
		this.idfactura = id_factura;
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmReimprimirNota = new JFrame();
		frmReimprimirNota.setResizable(false);
		frmReimprimirNota.setTitle("Reimprimir Nota de Cdto");
		frmReimprimirNota.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmReimprimirNota.setBounds(100, 100, 259, 243);
		frmReimprimirNota.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmReimprimirNota.getSize();  
        frmReimprimirNota.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		frmReimprimirNota.getContentPane().setLayout(null);
		
		EstiloHorus tema = new EstiloHorus();
		
		JLabel lblNewLabel_1 = new JLabel("");
		lblNewLabel_1.setIcon(new ImageIcon(ruta+"/images/hoja_nuevo.png"));
		lblNewLabel_1.setBounds(40, 36, 46, 48);
		frmReimprimirNota.getContentPane().add(lblNewLabel_1);
		
		JLabel label = new JLabel("");
		label.setIcon(new ImageIcon(ruta+"/images/hoja_blanco.png"));
		label.setBounds(40, 90, 46, 48);
		frmReimprimirNota.getContentPane().add(label);
		
		JRadioButton blanco = new JRadioButton("Nuevo Formato");
		blanco.setBounds(92, 50, 119, 23);
		blanco.setSelected(true);
		frmReimprimirNota.getContentPane().add(blanco);
		
		JRadioButton nuevo = new JRadioButton("Hoja en Blanco");
		nuevo.setBounds(92, 101, 119, 23);
		frmReimprimirNota.getContentPane().add(nuevo);
		
		ButtonGroup grupo1 = new ButtonGroup();
		grupo1.add(blanco);
		grupo1.add(nuevo);
		
		JButton btnNewButton = new JButton("Imprimir");
		btnNewButton.setBackground(tema.getAzul());
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.setBorderPainted(false);
		btnNewButton.addActionListener(new ActionListener() {
			@SuppressWarnings({ "resource" })
			public void actionPerformed(ActionEvent e) {
				if(blanco.isSelected()){
					VistaImpresion carga = new VistaImpresion();
					carga.setLocationRelativeTo(frmReimprimirNota);
					carga.setVisible(true);
					Conexion a = new Conexion();
					String funcion = "SELECT numero_nota as controles FROM ads_notascredito WHERE numero_documento = '"+idfactura+"'";
					String nro_nota = "";
					try {
						ResultSet nro = a.Consulta(funcion);
						if(nro.next()) {
							String controles = nro.getString("controles");
							String e1 = controles.replace("(", "");
							String b = e1.replace(")", "");
							String nota[] = b.split(",");
							nro_nota = nota[0];
							nro.close();
						}} catch (Exception exc) {
							throw new RuntimeException(exc);
					}
					String query = "SELECT ads_clientes.fk_id_usuario FROM ads_clientes, ads_facturacion WHERE ads_facturacion.fk_id_cliente = ads_clientes.pk_id AND ads_facturacion.pk_id = '"+idfactura+"'";
					ResultSet nro = null;
					String valor = null;
					try {
						nro = a.Consulta(query);
						if(nro.next()) {
							valor = nro.getString("fk_id_usuario");
							nro.close();
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					try {
					String rif_cliente = "";
					String nombre_cliente = "";
					String direccion_cliente = "";
					String telefono_cliente = "";
					int id = Integer.parseInt(valor);
					if (id != 0){
						query = "SELECT tb_usuario.nombre, tb_usuario.rif_cedula, tb_usuario.direccion_envio, tb_usuario.nro_telefono FROM ads_clientes, tb_usuario, ads_facturacion WHERE ads_clientes.fk_id_usuario = tb_usuario.id_usuario AND ads_clientes.pk_id = ads_facturacion.fk_id_cliente AND ads_facturacion.pk_id = '"+idfactura+"';";
						nro = a.Consulta(query);
						if(nro.next()) {
							rif_cliente = nro.getString("rif_cedula");
							nombre_cliente = nro.getString("nombre");
							direccion_cliente = nro.getString("direccion_envio");
							telefono_cliente = nro.getString("nro_telefono");
							nro.close();
						}
						AbstractJasperReports.createNotaCredito(a.getConnection(), ruta+"/reports/nota credito/", idfactura, nro_nota, rif_cliente, nombre_cliente, direccion_cliente, telefono_cliente );
					}else{
						query = "SELECT ads_clientes.rif, ads_clientes.razon_social, ads_clientes.telefono_cliente, ads_clientes.direccion_cliente FROM ads_clientes, ads_facturacion WHERE ads_clientes.pk_id = ads_facturacion.fk_id_cliente AND ads_facturacion.pk_id = '"+idfactura+"';";
						nro = a.Consulta(query);
						if(nro.next()) {
							rif_cliente = nro.getString("rif");
							nombre_cliente = nro.getString("razon_social");
							direccion_cliente = nro.getString("direccion_cliente");
							telefono_cliente = nro.getString("telefono_cliente");
							nro.close();
						}
						AbstractJasperReports.createNotaCredito(a.getConnection(), ruta+"/reports/nota credito/", idfactura, nro_nota, rif_cliente, nombre_cliente, direccion_cliente, telefono_cliente );
					}
						if(AbstractJasperReports.Print()){
							query = "SELECT numero_control()";
							a.Consulta(query);
							frmReimprimirNota.dispose();
							a.desConectar();
						}else{
							Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
							JOptionPane.showMessageDialog(null, "Error de Impresión","Error" , JOptionPane.ERROR_MESSAGE, icono); 
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					} catch (HeadlessException e1) {
						e1.printStackTrace();
					} catch (JRException e1) {
						e1.printStackTrace();
						frmReimprimirNota.dispose();
						Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
						JOptionPane.showMessageDialog(null, "Error de Impresión","Error" , JOptionPane.ERROR_MESSAGE, icono); 	
					}
				}else{
					Conexion a = new Conexion();
					String funcion = "SELECT numero_nota as controles FROM ads_notascredito WHERE numero_documento = '"+idfactura+"'";
					String nro_nota = "";
					try {
						ResultSet nro = a.Consulta(funcion);
						if(nro.next()) {
							String controles = nro.getString("controles");
							String e1 = controles.replace("(", "");
							String b = e1.replace(")", "");
							String nota[] = b.split(",");
							nro_nota = nota[0];
							nro.close();
						}} catch (Exception exc) {
							throw new RuntimeException(exc);
					}
					String query = "SELECT ads_clientes.fk_id_usuario FROM ads_clientes, ads_facturacion WHERE ads_facturacion.fk_id_cliente = ads_clientes.pk_id AND ads_facturacion.pk_id = '"+idfactura+"'";
					ResultSet nro = null;
					String valor = null;
					try {
						nro = a.Consulta(query);
						if(nro.next()) {
							valor = nro.getString("fk_id_usuario");
							nro.close();
						}
					} catch (SQLException e1) {
						e1.printStackTrace();
					}
					try {
					String rif_cliente = "";
					String nombre_cliente = "";
					String direccion_cliente = "";
					String telefono_cliente = "";
					int id = Integer.parseInt(valor);
					if (id != 0){
						query = "SELECT tb_usuario.nombre, tb_usuario.rif_cedula, tb_usuario.direccion_envio, tb_usuario.nro_telefono FROM ads_clientes, tb_usuario, ads_facturacion WHERE ads_clientes.fk_id_usuario = tb_usuario.id_usuario AND ads_clientes.pk_id = ads_facturacion.fk_id_cliente AND ads_facturacion.pk_id = '"+idfactura+"';";
						nro = a.Consulta(query);
						if(nro.next()) {
							rif_cliente = nro.getString("rif_cedula");
							nombre_cliente = nro.getString("nombre");
							direccion_cliente = nro.getString("direccion_envio");
							telefono_cliente = nro.getString("nro_telefono");
							nro.close();
						}
						AbstractJasperReports.createNotaInterna(a.getConnection(), ruta+"/reports/nota credito copia/", idfactura, nro_nota, rif_cliente, nombre_cliente, direccion_cliente, telefono_cliente, ruta );
					}else{
						query = "SELECT ads_clientes.rif, ads_clientes.razon_social, ads_clientes.telefono_cliente, ads_clientes.direccion_cliente FROM ads_clientes, ads_facturacion WHERE ads_clientes.pk_id = ads_facturacion.fk_id_cliente AND ads_facturacion.pk_id = '"+idfactura+"';";
						nro = a.Consulta(query);
						if(nro.next()) {
							rif_cliente = nro.getString("rif");
							nombre_cliente = nro.getString("razon_social");
							direccion_cliente = nro.getString("direccion_cliente");
							telefono_cliente = nro.getString("telefono_cliente");
							nro.close();
						}
						AbstractJasperReports.createNotaInterna(a.getConnection(), ruta+"/reports/nota credito copia/", idfactura, nro_nota, rif_cliente, nombre_cliente, direccion_cliente, telefono_cliente, ruta );
					}
						frmReimprimirNota.dispose();
						AbstractJasperReports.showViewer();
					} catch (SQLException e1) {
						e1.printStackTrace();
					} catch (HeadlessException e1) {
						e1.printStackTrace();
					}
				}
			}
		});
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/printer.png"));
		btnNewButton.setBounds(40, 148, 171, 40);
		frmReimprimirNota.getContentPane().add(btnNewButton);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 232, 193);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "SELECCIONE TIPO DE REIMPRESIÓN", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Arial", 1, 12), new java.awt.Color(13, 86, 180)));
		frmReimprimirNota.getContentPane().add(lblNewLabel);
	}
}
