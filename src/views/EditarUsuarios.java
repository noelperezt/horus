package views;

import java.awt.Dimension;
import javax.swing.JFrame;
import javax.swing.JTextField;

import models.Usuario;
import utility.EstiloHorus;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import java.awt.Font;
import java.awt.Toolkit;
import java.io.File;
import java.sql.SQLException;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.awt.event.ActionEvent;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;


public class EditarUsuarios {

	public JFrame frmRegistroDeUsuarios;
	private JTextField usuario;
	private JTextField nombre;
	private Usuario user;
	private int id_usuario;
	String ruta = new File ("").getAbsolutePath ();
	
	public EditarUsuarios(Usuario user, int id) throws SQLException {
		this.user = user;
		this.id_usuario = id;
		initialize();
	}
	

	public void Ejecutar(){
		if(nombre.getText().equals("") || usuario.getText().equals("") ){
			Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/error.png"));
			JOptionPane.showMessageDialog(null, "Hay campos vacios","Error" , JOptionPane.ERROR_MESSAGE,icono);
		}else{
			Usuario Ouser = new Usuario(nombre.getText(),usuario.getText(),"");
			try {
				if(Ouser.Editar(id_usuario)){
					user.setNombre(Ouser.getNombre());
					user.setUsario(Ouser.getUsuario()); 
					nombre.setText("");
					usuario.setText("");
					AdministrarUsuarios ventana = new AdministrarUsuarios(user);
					ventana.frmAdministrarUsuarios.setVisible(true);
					frmRegistroDeUsuarios.dispose();	
				}
			} catch (SQLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
		}	
	}
	
	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmRegistroDeUsuarios = new JFrame();
		frmRegistroDeUsuarios.setResizable(false);
		frmRegistroDeUsuarios.setTitle("Registro de Usuarios");
		frmRegistroDeUsuarios.setBounds(100, 100, 411, 177);
		frmRegistroDeUsuarios.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		frmRegistroDeUsuarios.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmRegistroDeUsuarios.getContentPane().setLayout(null);
		frmRegistroDeUsuarios.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					AdministrarUsuarios ventana = new AdministrarUsuarios(user);
					ventana.frmAdministrarUsuarios.setVisible(true);
					frmRegistroDeUsuarios.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		Usuario Ouser = new Usuario();
		Ouser.TraerRegistro(id_usuario);
		
		
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmRegistroDeUsuarios.getSize();  
        frmRegistroDeUsuarios.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		
		EstiloHorus tema = new EstiloHorus();
        
		nombre = new JTextField();
		nombre.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				nombre.nextFocus();
			}
		});
		nombre.setBounds(106, 32, 268, 20);
		nombre.setText(Ouser.getNombre());
		frmRegistroDeUsuarios.getContentPane().add(nombre);
		nombre.setColumns(10);
		
		usuario = new JTextField();
		usuario.addActionListener(new ActionListener() {
			@SuppressWarnings("deprecation")
			public void actionPerformed(ActionEvent e) {
				usuario.nextFocus();
			}
		});
		usuario.setBounds(106, 63, 268, 20);
		usuario.setText(Ouser.getUsuario());
		frmRegistroDeUsuarios.getContentPane().add(usuario);
		usuario.setColumns(10);
		
		JLabel usuario_usuario = new JLabel("USUARIO:");
		usuario_usuario.setFont(new Font("Arial", Font.BOLD, 11));
		usuario_usuario.setBounds(45, 65, 55, 17);
		frmRegistroDeUsuarios.getContentPane().add(usuario_usuario);
		
		JLabel usurio_nombre = new JLabel("NOMBRE:");
		usurio_nombre.setFont(new Font("Arial", Font.BOLD, 11));
		usurio_nombre.setBounds(45, 35, 55, 14);
		frmRegistroDeUsuarios.getContentPane().add(usurio_nombre);
		
		JButton cancelar = new JButton("Volver");
		cancelar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				cancelar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				cancelar.setBackground(tema.getAzul());
			}
		});
		cancelar.setBackground(tema.getAzul());
		cancelar.setForeground(Color.WHITE);
		cancelar.setBorderPainted(false);
		cancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					AdministrarUsuarios ventana = new AdministrarUsuarios(user);
					ventana.frmAdministrarUsuarios.setVisible(true);
					frmRegistroDeUsuarios.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
			}
		});
		cancelar.setBounds(225, 94, 120, 36);
		cancelar.setIcon(new ImageIcon(ruta+"/images/back.png"));
		frmRegistroDeUsuarios.getContentPane().add(cancelar);
		
		JButton registrar = new JButton("Guardar");
		registrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				registrar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				registrar.setBackground(tema.getAzul());
			}
		});
		registrar.setForeground(Color.WHITE);
		registrar.setBackground(tema.getAzul());
		registrar.setBorderPainted(false);
		registrar.addKeyListener(new KeyAdapter() {
			@SuppressWarnings("static-access")
			@Override
			public void keyTyped(KeyEvent evt) {
				char caracter = evt.getKeyChar();
		        if(caracter == evt.VK_ENTER){
		        	Ejecutar();
		        }
			}
		});
		registrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				Ejecutar();	
			}
		});
		registrar.setIcon(new ImageIcon(ruta+"/images/save.png"));
		registrar.setBounds(67, 94, 120, 36);
		frmRegistroDeUsuarios.getContentPane().add(registrar);
		
		JLabel borde = new JLabel("");
		borde.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "MODIFICAR USUARIOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		borde.setBounds(10, 4, 387, 138);
		frmRegistroDeUsuarios.getContentPane().add(borde);
	}
}
