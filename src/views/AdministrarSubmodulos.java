package views;

import java.awt.Dimension;
import java.awt.Toolkit;
import java.sql.SQLException;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.ScrollPaneConstants;

import models.Submodulo;
import models.Usuario;
import utility.EstiloHorus;
import utility.Tabla;

import javax.swing.JButton;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.awt.event.ActionEvent;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import java.awt.Color;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

public class AdministrarSubmodulos {

	public JFrame frmAdministrarSubmdulos;
	public Tabla listasubmodulos;
	private Usuario user;
	private JButton btnNewButton;
	private JButton btnEditar;
	private JButton btnBorrar;
	private JButton btnVolver;
	public String ruta = new File ("").getAbsolutePath ();
	
	public AdministrarSubmodulos(Usuario user) throws SQLException {
		this.user = user;
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 * @throws SQLException 
	 */
	private void initialize() throws SQLException {
		frmAdministrarSubmdulos = new JFrame();
		frmAdministrarSubmdulos.setTitle("Administrar Subm\u00F3dulos");
		frmAdministrarSubmdulos.setIconImage(Toolkit.getDefaultToolkit().getImage(ruta+"/images/pinttoadmin.png"));
		frmAdministrarSubmdulos.setBounds(100, 100, 553, 287);
		frmAdministrarSubmdulos.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
		frmAdministrarSubmdulos.addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosing(WindowEvent arg0) {
				try {
					Principal ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarSubmdulos.dispose();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		});
		Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();  
        Dimension ventana = frmAdministrarSubmdulos.getSize();  
        frmAdministrarSubmdulos.setLocation((pantalla.width - ventana.width) / 2, (pantalla.height - ventana.height) / 2); 
		frmAdministrarSubmdulos.getContentPane().setLayout(null);
		
		EstiloHorus tema = new EstiloHorus();
		
		String campos[] = {"ID","PADRE","NOMBRE","ESTADO"};
		int ancho[] = {10,100,120,50};
		int editable[] = null;
		Submodulo Osubmodulo = new Submodulo();
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setBounds(34, 45, 346, 180);
		listasubmodulos= new Tabla(campos, editable, ancho, null);
		listasubmodulos.Listar(Osubmodulo.ListarSubmodulos());
		Osubmodulo.desconectar();
		
		frmAdministrarSubmdulos.getContentPane().add(scrollPane);
		scrollPane.setViewportView(listasubmodulos);
		
		btnNewButton = new JButton("Registrar");
		btnNewButton.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnNewButton.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnNewButton.setBackground(tema.getAzul());
			}
		});
		btnNewButton.setBackground(tema.getAzul());
		btnNewButton.setForeground(Color.WHITE);
		btnNewButton.setBorderPainted(false);
		btnNewButton.setIcon(new ImageIcon(ruta+"/images/add.png"));
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				try {
					RegistrarSubmodulo ventana = new RegistrarSubmodulo(0,user);
					ventana.frmRegistroDeSubmdulos.setVisible(true);
					frmAdministrarSubmdulos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnNewButton.setBounds(390, 46, 120, 35);
		frmAdministrarSubmdulos.getContentPane().add(btnNewButton);
		
		btnEditar = new JButton("Editar");
		btnEditar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnEditar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnEditar.setBackground(tema.getAzul());
			}
		});
		btnEditar.setBackground(tema.getAzul());
		btnEditar.setForeground(Color.WHITE);
		btnEditar.setBorderPainted(false);
		btnEditar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(listasubmodulos.getSelectedRow() != -1){
					int id = Integer.parseInt(listasubmodulos.getValueAt(listasubmodulos.getSelectedRow(), 0).toString());
					try {
						RegistrarSubmodulo ventana = new RegistrarSubmodulo(id,user);
						ventana.frmRegistroDeSubmdulos.setVisible(true);
						frmAdministrarSubmdulos.dispose();
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe seleccionar un sub-m�dulo para editar","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnEditar.setIcon(new ImageIcon(ruta+"/images/edit.png"));
		btnEditar.setBounds(390, 92, 120, 35);
		frmAdministrarSubmdulos.getContentPane().add(btnEditar);
		
		btnBorrar = new JButton("Estado");
		btnBorrar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				if(listasubmodulos.getSelectedRow() != -1){
					String id = listasubmodulos.getValueAt(listasubmodulos.getSelectedRow(), 0).toString();
					try {
						listasubmodulos.CambioEstadoSubmodulo(id, listasubmodulos.getSelectedRow(), 3);
					} catch (SQLException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}	
				}else{
					Icon icono = new ImageIcon(Toolkit.getDefaultToolkit().getImage(ruta+"/images/precaucion.png"));
					JOptionPane.showMessageDialog(null, "Debe Seleccionar un Concepto para cambiar su Estado","Advertencia" , JOptionPane.WARNING_MESSAGE, icono); 
				}
			}
		});
		btnBorrar.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnBorrar.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnBorrar.setBackground(tema.getAzul());
			}
		});
		btnBorrar.setForeground(Color.WHITE);
		btnBorrar.setBackground(tema.getAzul());
		btnBorrar.setBorderPainted(false);
		btnBorrar.setIcon(new ImageIcon(ruta+"/images/del.png"));
		btnBorrar.setBounds(390, 136, 120, 35);
		frmAdministrarSubmdulos.getContentPane().add(btnBorrar);
		
		btnVolver = new JButton("Volver");
		btnVolver.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseEntered(MouseEvent e) {
				btnVolver.setBackground(tema.getGris());
			}
			@Override
			public void mouseExited(MouseEvent e) {
				btnVolver.setBackground(tema.getAzul());
			}
		});
		btnVolver.setForeground(Color.WHITE);
		btnVolver.setBackground(tema.getAzul());
		btnVolver.setBorderPainted(false);
		btnVolver.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				Principal ventana;
				try {
					ventana = new Principal(user);
					ventana.frmSistemaAdministrativoPinttosoft.setVisible(true);
					frmAdministrarSubmdulos.dispose();
				} catch (SQLException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
			}
		});
		btnVolver.setIcon(new ImageIcon(ruta+"/images/back.png"));
		btnVolver.setBounds(390, 182, 120, 35);
		frmAdministrarSubmdulos.getContentPane().add(btnVolver);
		
		JLabel lblNewLabel = new JLabel("");
		lblNewLabel.setBounds(10, 11, 519, 233);
		lblNewLabel.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "ADMINISTRAR SUBM�DULOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14), new java.awt.Color(13, 86, 180)));
		frmAdministrarSubmdulos.getContentPane().add(lblNewLabel);
	}

}
