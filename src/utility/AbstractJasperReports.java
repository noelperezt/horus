package utility;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.view.JasperViewer;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JRException;


public abstract class AbstractJasperReports
{
	private static JasperReport	report;
	private static JasperPrint	reportFilled;
	private static JasperViewer	viewer;
	
	@SuppressWarnings("unchecked")
	public static void createPresupuesto(String path, int id_presupuesto, String nro, String rif, String nombre, String direccion, String telefono, int validez, String ruta_imagen ) throws SQLException
	{
		try {
			Conexion conn = new Conexion();
			@SuppressWarnings("rawtypes")
			Map param = new HashMap();  
            param.put("id", id_presupuesto);
            param.put("ruta", path);
            param.put("nro", nro);
            param.put("rif", rif); 
            param.put("nombre", nombre);
            param.put("direccion", direccion);
            param.put("telefono", telefono);
            param.put("validez", validez);
            param.put("ruta_imagen", ruta_imagen);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"presupuesto.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn.getConnection());
			conn.desConectar();
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void createFactura(String path, Integer id_factura, String nro, String rif, String nombre, String direccion, String telefono  ) throws SQLException
	{
		try {
			Conexion conn = new Conexion();
			@SuppressWarnings("rawtypes")
			Map param = new HashMap();  
            param.put("factura", id_factura);
            param.put("ruta", path);
            param.put("nro", nro);
            param.put("rif", rif); 
            param.put("nombre", nombre);
            param.put("direccion", direccion);
            param.put("telefono", telefono);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"factura.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn.getConnection());
			conn.desConectar();
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void createNotaCredito( Connection conn, String path, Integer factura, String nro, String rif, String nombre, String direccion, String telefono )
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("factura", factura);
            param.put("ruta", path);
            param.put("nro", nro);
            param.put("rif", rif);
            param.put("nombre", nombre);
            param.put("direccion", direccion);
            param.put("telefono", telefono);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"notacredito.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn);
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void RegistradosDumga( Connection conn, String path,  String ruta )
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"listaClientesDumga.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn);
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void TodasFacturas( Connection conn, String path,  String ruta, double total_anulado, double total_no_anulado, int cantidad, Date date, Date date2 )
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("anulado", total_anulado);
            param.put("total", total_no_anulado);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"todasFacturas.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn);
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void TodasNotas( Connection conn, String path,  String ruta, double total_no_anulado, int cantidad, Date date, Date date2 )
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("total", total_no_anulado);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"notascredito.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn);
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void TodasContrataciones( Connection conn, String path,  String ruta, double total_pendiente, double total_facturado, int cantidad, Date date, Date date2 )
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("pendiente", total_pendiente);
            param.put("facturada", total_facturado);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"todasContrataciones.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn);
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void FacturasAnuladas( Connection conn, String path,  String ruta, double total_anulado,  int cantidad, Date date, Date date2 )
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("anulado", total_anulado);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"anuladas.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn);
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void FacturasActivas( Connection conn, String path,  String ruta, double total, double subtotal, double iva,  int cantidad, Date date, Date date2 )
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
            param.put("subtotal", subtotal);
            param.put("iva", iva);
            param.put("total", total);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"activas.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn);
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void Facturadas( Connection conn, String path,  String ruta, double total,  int cantidad, Date date, Date date2 )
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
            param.put("facturada", total);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"facturadas.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn);
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void Pendientes( Connection conn, String path,  String ruta, double total,  int cantidad, Date date, Date date2 )
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
            param.put("pendiente", total);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"pendientes.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn);
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void RegistradosClientes(Connection conn, String path,  String ruta)
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"listaClientes.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn);
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void createFacturaInterna( Connection conn, String path, Integer factura, String nro, String rif, String nombre, String direccion, String telefono, String ruta_imagen  )
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();
            param.put("factura", factura);
            param.put("ruta", path);
            param.put("nro", nro);
            param.put("rif", rif);
            param.put("nombre", nombre);
            param.put("direccion", direccion);
            param.put("telefono", telefono);
            param.put("ruta_imagen", ruta_imagen);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"factura.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn);
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void createNotaInterna( Connection conn, String path, Integer factura, String nro, String rif, String nombre, String direccion, String telefono, String ruta_imagen  )
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();
            param.put("factura", factura);
            param.put("ruta", path);
            param.put("nro", nro);
            param.put("rif", rif);
            param.put("nombre", nombre);
            param.put("direccion", direccion);
            param.put("telefono", telefono);
            param.put("ruta_imagen", ruta_imagen);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"notacredito.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void TodosPresupuestos(Connection conn, String path,  String ruta, int cantidad, Date date, Date date2)
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"TTodos.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}

	@SuppressWarnings("unchecked")
	public static void TodosFacturados(Connection conn, String path, String ruta, int cantidad, Date date, Date date2)
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"TFacturado.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void TodosNFacturados(Connection conn, String path, String ruta, int cantidad, Date date, Date date2)
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"TNFacturado.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void AnuladosTodos(Connection conn, String path, String ruta, int cantidad, Date date, Date date2)
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"ATodos.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void AnuladosFacturados(Connection conn, String path, String ruta, int cantidad, Date date, Date date2)
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"AFacturados.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void AnuladosNFacturados(Connection conn, String path, String ruta, int cantidad, Date date, Date date2)
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"ANFacturados.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void NAnuladosTodos(Connection conn, String path, String ruta, int cantidad, Date date, Date date2)
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"NTodos.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void NAnuladosFacturados(Connection conn, String path, String ruta, int cantidad, Date date, Date date2)
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"NFacturados.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	@SuppressWarnings("unchecked")
	public static void NAnuladosNFacturados(Connection conn, String path, String ruta, int cantidad, Date date, Date date2)
	{
		try {
			@SuppressWarnings("rawtypes")
			Map param=new HashMap();  
            param.put("ruta", ruta);
            param.put("cantidad", cantidad);
            param.put("inicio", date);
            param.put("fin", date2);
			report = (JasperReport) JRLoader.loadObjectFromFile( path+"NNFacturados.jasper" );
			reportFilled = JasperFillManager.fillReport(report, param, conn );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}
	
	public static void showViewer()
	{
		viewer = new JasperViewer(reportFilled, false);
		if(reportFilled.getPages().size() != 0){
			viewer.setVisible( true );
			viewer.setExtendedState(JasperViewer.MAXIMIZED_BOTH);
		}	
	}

	public static boolean Print() throws JRException
	{
		JasperPrint jPrint = reportFilled;
		if(JasperPrintManager.printReport(jPrint, false)){
			return true;
		}else{
			return false;
		}		
	}
	
	public static void exportToPDF( String destination )
	{
		try { 
			JasperExportManager.exportReportToPdfFile( reportFilled, destination );
		}
		catch( JRException ex ) {
			ex.printStackTrace();
		}
	}

	
	
}
