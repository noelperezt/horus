package utility;

public class Npermiso {
	private int id;
    private String estado;
    private String descripcion;
    private Npermiso sig;
    
    public Npermiso (int id, String estado, String descripcion,  Npermiso n)
    {
        this.id = id;
        this.estado = estado;
        this.descripcion = descripcion;
        this.sig = n;
    }
    public int GetId(){
        return this.id;
    }
    public String GetEstado(){
        return this.estado;
    }
    public String GetDescripcion(){
        return this.descripcion;
    }
    public Npermiso GetSig(){
        return sig;
    }
    public void SetId(int id){
        this.id = id;
    }
    public void SetEstado(String estado){
        this.estado = estado;
    }
    public void SetDescripcion(String descripcion){
        this.descripcion = descripcion;
    }
    public void SetSig(Npermiso N){
        this.sig = N;
    }
}
