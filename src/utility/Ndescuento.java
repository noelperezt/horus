package utility;

public class Ndescuento {
	private int id;
    private int id_concepto;
    private int descuento;
    private Ndescuento sig;
    
    public Ndescuento(int id, int id_concepto, int descuento,  Ndescuento n){
        this.id = id;
        this.id_concepto = id_concepto;
        this.descuento = descuento;
        this.sig = n;
    }
    public int GetId(){
        return this.id;
    }
    public int GetIdConcepto(){
        return this.id_concepto;
    }
    public int GetDescuento(){
        return this.descuento;
    }
    public Ndescuento GetSig(){
        return sig;
    }
    public void SetId(int id){
        this.id = id;
    }
    public void SetIdConcepto(int id_concepto){
        this.id_concepto = id_concepto;
    }
    public void SetDescuento(int descuento){
        this.descuento = descuento;
    }
    public void SetSig(Ndescuento N){
        this.sig = N;
    }
}
