package utility;

import java.sql.ResultSet;
import java.sql.SQLException;
import javax.swing.JOptionPane;

public class Preferencias {
	private double iva;
	private String control_prefijo;
	private int digitos_factura;
	private int digitos_control;
	private int nro_control;
	private int nro_control_nota;
	private int digitos_nota;
	private int digitos_nro_control_nota;
	
	public Preferencias(){} 
	
	public Preferencias(double iva, String control_prefijo, int digitos_factura, int digitos_control, int nro_control, int nro_control_nota, int digitos_nota, int digitos_nro_control_nota){ //Constructor principal de la Clase
		this.iva = iva;
		this.control_prefijo = control_prefijo;
		this.digitos_factura = digitos_factura;
		this.digitos_control = digitos_control;
		this.nro_control = nro_control;
		this.nro_control_nota = nro_control_nota;
		this.digitos_nota = digitos_nota;
		this.digitos_nro_control_nota = digitos_nro_control_nota;
	}
	
	public double getIva(){
		return iva;
	}
	
	public void setIva(double iva){
		this.iva = iva;
	}
	
	public String getCtrlPrefijo(){
		return control_prefijo;
	}
	
	public void setCtrlPrefijo(String control_prefijo){
		this.control_prefijo = control_prefijo;
	}
	
	public int getDigitosFactura(){
		return digitos_factura;
	}
	
	public void getDigitosFactura(int digitos_factura){
		this.digitos_factura = digitos_factura;
	}
	
	public int getDigitosCtrl(){
		return digitos_control;
	}
	
	public void setDigitosCtrl(int digitos_control){
		this.digitos_control = digitos_control;
	}
	
	public int getNroControl(){
		return nro_control;
	}
	
	public void setNroControl(int nro_control){
		this.nro_control = nro_control;
	}
	
	public int getNroControlNota(){
		return nro_control_nota; 
	}
	
	public void setNroControlNota(int nro_control_nota){
		this.nro_control_nota = nro_control_nota;
	}
	
	public int getDigitosNota(){
		return digitos_nota;
	}
	
	public void setDigitosNota(int digitos_nota){
		this.digitos_nota = digitos_nota;
	}
	
	public int getDigitosNotaControl(){
		return digitos_nro_control_nota;		
	}
	
	public void setDigitosNotaControl(int digitos_nro_control_nota){
		this.digitos_nro_control_nota = digitos_nro_control_nota;
	}
	
	public void TraerRegistro() throws SQLException{ 
		Conexion conn = new Conexion();
		String query = "SELECT * FROM ads_administracion WHERE pk_id = 1 ";
		ResultSet rs = conn.Consulta(query);
		if(rs.next()){
			this.iva = rs.getDouble("porcentaje_iva");
			this.control_prefijo = rs.getString("prefijo_control");
			this.digitos_factura = rs.getInt("digitos_factura");
			this.digitos_control = rs.getInt("digitos_control");
			this.nro_control = rs.getInt("numero_control");
			this.nro_control_nota = rs.getInt("numero_control_nota");
			this.digitos_nota = rs.getInt("digitos_nota");
			this.digitos_nro_control_nota = rs.getInt("digitos_numero_control_nota");
			rs.close();
		}
		conn.desConectar();
	}
	
	public boolean Editar() throws SQLException{
		Conexion conn = new Conexion();
		String query = "UPDATE ads_administracion SET porcentaje_iva = "+iva+", prefijo_control = '"+control_prefijo+"', digitos_factura = "+digitos_factura+", digitos_control = "+digitos_control+", numero_control = "+nro_control+", numero_control_nota = "+nro_control_nota+", digitos_nota = "+digitos_nota+", digitos_numero_control_nota = "+digitos_nro_control_nota+" WHERE pk_id = 1";
		boolean status = conn.ejecutar(query);
		if(status == true){
			JOptionPane.showMessageDialog(null, "Modificaci�n realizada con �xito", "�xito", JOptionPane.INFORMATION_MESSAGE);
		}
		conn.desConectar();
		return status;
	}
}
