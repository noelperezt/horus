package utility;

import java.awt.Color;
import java.awt.Component;
import java.awt.Font;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import models.Banco;
import models.Cliente;
import models.Concepto;
import models.Modulo;
import models.Permiso;
import models.Submodulo;
import models.TiposPago;
import models.Usuario;
import views.RegistroClientes;

@SuppressWarnings("serial")
public class Tabla extends JTable {

	public DefaultTableModel modelo;
	public int fila;
	public int columna;
	public String ruta = new File ("").getAbsolutePath ();
	private String[] nombre_columnas;
	private int[] columnas_editables;
	private int[] ancho_columnas;
	EstiloHorus tema = new EstiloHorus();
	
	public Tabla(){
	}

	public Tabla(String[] nombre_columnas, int[] editable, int[] anchos , @SuppressWarnings("rawtypes") Class[] datos){
		
		setNombre_columnas(nombre_columnas);
		setColumnas_editables(editable);
		setAnchos_Columnas(anchos);
		
		this.setModel(new DefaultTableModel(null,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = datos;

			@SuppressWarnings({ "rawtypes", "unchecked" })
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});
		backgroundHeader();
		setResizeColumn();
	}
	

	private void backgroundHeader(){
		getTableHeader().setForeground(tema.getAzul());
		getTableHeader().setFont(new Font("Arial", Font.BOLD, 11)); 
	}
	
	public void setAnchos_Columnas(int[] anchos){
		ancho_columnas = anchos;
	}
	
	public void setResizeColumn(){
		for(int i = 0; i < ancho_columnas.length; i++) {
			getColumnModel().getColumn(i).setPreferredWidth(ancho_columnas[i]);
		}	
	}
	
	public String[] getNombre_columnas(){
		return nombre_columnas;
	}
	
	public void setNombre_columnas(String[] columnas){
		nombre_columnas = columnas;
	}
	
	public int[] getColumnas_editables(){
		return columnas_editables;
	}
	
	public void setColumnas_editables(int[] editables){
		columnas_editables = editables;
	}
	
	public void addRow(){
		Object[] col={};
		modelo = (DefaultTableModel) getModel();
		modelo.addRow(col);
	}
	
	public void addRow(Object[] fila){
		modelo = (DefaultTableModel) getModel();
		modelo.addRow(fila);
	}
	
	public void deleteRow(int RowSelected){
		modelo = (DefaultTableModel) getModel();
		modelo.removeRow(RowSelected);
	}

	public void setClearGrid(){
		int x;
		DefaultTableModel newModelTable = new DefaultTableModel();
		newModelTable = (DefaultTableModel) getModel();
		for (x =  newModelTable.getRowCount()-1; x>=0;x--){
			newModelTable.removeRow(x);
		}
	} 

	public Object getValueAt(int Row, int Col) {
		modelo = (DefaultTableModel) getModel();
		return  modelo.getValueAt(Row, Col);
	} 

	public void setValueAt(Object var, int Row, int Col){
		modelo = (DefaultTableModel) getModel();
		modelo.setValueAt(var, Row, Col);
		modelo.fireTableDataChanged();
	
	}
	
	@SuppressWarnings({ "unchecked" })
	public void PrimeraColumnaBoolean(ResultSet rs, boolean estado){
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		@SuppressWarnings("rawtypes")
		ArrayList datosTemp = new ArrayList<>();

		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;
		int adicional = 1;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[adicional+numeroColumnas];
			tiposColumnas = new Class[adicional+numeroColumnas];
			
			for (int i = 0; i < adicional; i++){
				columnas[i] = "";
				tiposColumnas[i] = java.lang.Boolean.class;
			}
			for (int i = 0; i < numeroColumnas; i++){
				columnas[adicional+i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[adicional+i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for(int i = 0; i < adicional; i++){
					fila[i] = estado;
				}
				for (int i = 0; i < numeroColumnas; i++) 
					fila[adicional+i] = rsUsuarios.getObject(i + 1);
				
				datosTemp.add(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
		@SuppressWarnings("rawtypes")
		final Class[] tiposColumnasFinal = tiposColumnas;
		Object[][] datos = new Object[datosTemp.size()][columnas.length];
		datosTemp.toArray(datos);

		this.setModel(new DefaultTableModel(datos,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = tiposColumnasFinal;

			@SuppressWarnings("rawtypes")
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});
		setResizeColumn();
	}
	
	@SuppressWarnings({ "unchecked" })
	public void UltimaColumnaBoolean(ResultSet rs, boolean estado){
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		@SuppressWarnings("rawtypes")
		ArrayList datosTemp = new ArrayList<>();

		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;
		int adicional = 1;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas+adicional];
			tiposColumnas = new Class[numeroColumnas+adicional];
			
			for (int i = 0; i < numeroColumnas; i++){
				columnas[i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}
			for (int i = 0; i < adicional; i++){
				columnas[numeroColumnas+i] = "";
				tiposColumnas[numeroColumnas+i] = java.lang.Boolean.class;
			}
			

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				
				for(int i = 0; i < adicional; i++){
					fila[numeroColumnas+i] = estado;
				}
				datosTemp.add(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
		@SuppressWarnings("rawtypes")
		final Class[] tiposColumnasFinal = tiposColumnas;
		Object[][] datos = new Object[datosTemp.size()][columnas.length];
		datosTemp.toArray(datos);

		this.setModel(new DefaultTableModel(datos,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = tiposColumnasFinal;

			@SuppressWarnings("rawtypes")
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});
		setResizeColumn();
	}
	
	@SuppressWarnings("unchecked")
	public void Listar(ResultSet rs){
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		@SuppressWarnings("rawtypes")
		ArrayList datosTemp = new ArrayList<>();

		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas];
			tiposColumnas = new Class[numeroColumnas];
			
			for (int i = 0; i < numeroColumnas; i++){
				columnas[i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				datosTemp.add(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
		@SuppressWarnings("rawtypes")
		final Class[] tiposColumnasFinal = tiposColumnas;
		Object[][] datos = new Object[datosTemp.size()][columnas.length];
		datosTemp.toArray(datos);

		this.setModel(new DefaultTableModel(datos,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = tiposColumnasFinal;

			@SuppressWarnings("rawtypes")
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});
		setResizeColumn();
	}
	
	public void CambioEstadoSubmodulo(String cod,int fila, int columna) throws SQLException{
		Submodulo Osubmodulo = new Submodulo();
		setValueAt(Osubmodulo.CambiarEstado(cod), fila, columna);
	}
	
	public void CambioEstadoCliente(int cliente,int fila, int columna) throws SQLException{
		Cliente Ocliente = new Cliente();
		setValueAt(Ocliente.CambiarEstado(cliente), fila, columna);
	}
	
	public void CambioEstadoConcepto(String cod,int fila, int columna) throws SQLException{
		Concepto Oconcepto = new Concepto();
		setValueAt(Oconcepto.CambiarEstado(cod), fila, columna);
	}
	
	public void CambioEstadoTipoPago(String cod,int fila, int columna) throws SQLException{
		TiposPago Otipospago = new TiposPago();
		setValueAt(Otipospago.CambiarEstado(cod), fila, columna);
	}
	
	public void CambioVisivilidadModulo(int modulo,int fila, int columna) throws SQLException{
		Modulo Omodulo = new Modulo();
		setValueAt(Omodulo.CambiarVisibilidad(modulo), fila, columna);
	}
	
	public void CambioEstadoUsuario(int usuario,int fila, int columna) throws SQLException{
		Usuario Ouser = new Usuario();
		setValueAt(Ouser.CambiarEstado(usuario), fila, columna);
	}
	
	public void CambioEstadoBanco(int id,int fila, int columna) throws SQLException{
		Banco Obanco = new Banco();
		setValueAt(Obanco.CambiarEstado(id), fila, columna);
	}
	
	public void CambioEstadoPermiso(int permiso,int fila, int columna) throws SQLException{
		Permiso Opermiso = new Permiso();
		setValueAt(Opermiso.CambiarEstado(permiso), fila, columna);
	}
	
	public void RefrescarTablaBotones(ResultSet rs){
		String[] botones = new String[] {
				"Editar",
				"Inhabilitar",
		};
			
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;
		int numeroBotones = botones.length;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas + numeroBotones];
			tiposColumnas = new Class[numeroColumnas + numeroBotones];
			
			for (int i = 0; i < numeroColumnas; i++){
				columnas[i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

			for (int i = 0; i < botones.length; i++){
				columnas[numeroColumnas + i] = botones[i];
				tiposColumnas[numeroColumnas + i] = JButton.class;
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				for (int j = 0; j < botones.length; j++){
					JButton boton = new JButton("");
					if (botones[j].equals("Editar")){
						boton.setIcon(new ImageIcon(ruta+"/images/ico_editar.png"));
						boton.setToolTipText("Editar");
					}else{
						boton.setIcon(new ImageIcon(ruta+"/images/ico_etiqueta_azul.png"));
						boton.setToolTipText("Cambiar Estado");
					}
					fila[numeroColumnas + j] = boton;	
				}
				modelo.addRow(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
	}
	
	public void Refrescar(ResultSet rs){		
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas ];
			tiposColumnas = new Class[numeroColumnas ];
			
			for (int i = 0; i < numeroColumnas; i++){
				columnas[i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				modelo.addRow(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
	}

	public void RefrescarPrimeraColumnaBoolean(ResultSet rs, boolean estado){
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;
		int adicional = 1;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[adicional+numeroColumnas];
			tiposColumnas = new Class[adicional+numeroColumnas];
			
			for (int i = 0; i < adicional; i++){
				columnas[i] = "";
				tiposColumnas[i] = java.lang.Boolean.class;
			}
			for (int i = 0; i < numeroColumnas; i++){
				columnas[adicional+i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[adicional+i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for(int i = 0; i < adicional; i++){
					fila[i] = estado;
				}
				for (int i = 0; i < numeroColumnas; i++) 
					fila[adicional+i] = rsUsuarios.getObject(i + 1);
				
				modelo.addRow(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
	}
	
	@SuppressWarnings({ "unchecked" })
	public void TableBotones(ResultSet rs, JFrame ventanas, Usuario user) throws SQLException{
		String[] botones = new String[] {
				"Editar",
				"Inhabilitar",
		};
			
		String[] columnas = nombre_columnas;
		@SuppressWarnings("rawtypes")
		Class[] tiposColumnas = null;
		@SuppressWarnings("rawtypes")
		ArrayList datosTemp = new ArrayList<>();

		ResultSet rsUsuarios = rs; 
		ResultSetMetaData metaDatos = null;
		int numeroColumnas = 0;
		int numeroBotones = botones.length;

		try{
			metaDatos = rsUsuarios.getMetaData();
			numeroColumnas = metaDatos.getColumnCount();
			columnas = new String[numeroColumnas + numeroBotones];
			tiposColumnas = new Class[numeroColumnas + numeroBotones];
			
			for (int i = 0; i < numeroColumnas; i++){
				columnas[i] = metaDatos.getColumnLabel(i + 1);
				tiposColumnas[i] = Class.forName(metaDatos.getColumnClassName(i + 1));
			}

			for (int i = 0; i < botones.length; i++){
				columnas[numeroColumnas + i] = botones[i];
				tiposColumnas[numeroColumnas + i] = JButton.class;
			}

		} catch (SQLException | ClassNotFoundException e)
		{}

		try{
			while (rsUsuarios.next()){
				Object[] fila = new Object[columnas.length];
				for (int i = 0; i < numeroColumnas; i++) 
					fila[i] = rsUsuarios.getObject(i + 1);
				for (int j = 0; j < botones.length; j++){
					JButton boton = new JButton("");
					boton.setBorderPainted(false);
					boton.setBackground(new Color(149, 149, 149));
					boton.addMouseListener(new MouseAdapter() {
						@Override
						public void mouseEntered(MouseEvent e) {
							boton.setBackground(new Color(149, 149, 149));
						}
						@Override
						public void mouseExited(MouseEvent e) {
							boton.setBackground(new Color(149, 149, 149));
						}
					});
					if (botones[j].equals("Editar")){
						boton.setIcon(new ImageIcon(ruta+"/images/ico_editar.png"));
						boton.setToolTipText("Editar");
					}else{
						boton.setIcon(new ImageIcon(ruta+"/images/ico_etiqueta_azul.png"));
						boton.setToolTipText("Cambiar Estado");
					}
					fila[numeroColumnas + j] = boton;	
				}
				datosTemp.add(fila);
			}
			rsUsuarios.close();
		} catch (Exception e)
		{}
		@SuppressWarnings("rawtypes")
		final Class[] tiposColumnasFinal = tiposColumnas;
		Object[][] datos = new Object[datosTemp.size()][columnas.length];
		datosTemp.toArray(datos);

		this.setModel(new DefaultTableModel(datos,nombre_columnas) {
			@SuppressWarnings("rawtypes")
			Class[] tipos = tiposColumnasFinal;

			@SuppressWarnings("rawtypes")
			@Override
			public Class getColumnClass(int columnIndex) {
				return tipos[columnIndex];
			}

			@Override
			public boolean isCellEditable (int fila, int iCols) {
				try{ 
					for(int z = 0; z < columnas_editables.length; z++){ 
						if(iCols==columnas_editables[z]){ 
							return true; 
						}	
					}	
				}catch(java.lang.NullPointerException e){ 
					return false; 
				} 
				return false;
			}
		});

		this.setDefaultRenderer(JButton.class, new TableCellRenderer() {
			@Override
			public Component getTableCellRendererComponent(JTable jtable, Object objeto, boolean estaSeleccionado, boolean tieneElFoco, int fila, int columna) {
				return (Component) objeto;
			}
		});
		
		this.addMouseListener(new MouseAdapter() {
			public void mouseClicked(MouseEvent e) {
				fila = rowAtPoint(e.getPoint()); 
				columna = columnAtPoint(e.getPoint());
					if (getModel().getColumnClass(columna).equals(JButton.class)) {
						int cliente = (int) getValueAt(fila, 0);
						if (columna == 7){
							try {
								RegistroClientes ventana = new RegistroClientes(cliente, user);
								ventana.RegistroClientes.setVisible(true);
								ventanas.dispose();
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}else if (columna == 8){
							try {
								CambioEstadoCliente(cliente, fila, 6);
							} catch (SQLException e1) {
								// TODO Auto-generated catch block
								e1.printStackTrace();
							}
						}
					}
				}
			});
		setResizeColumn();
	}

}
