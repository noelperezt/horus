package utility;

public class Ldescuento {
	private Ndescuento cabecera;
	 @SuppressWarnings("unused")
	private int tam;
	 
	 public Ldescuento(){
	     cabecera = null;
	     tam = 0;
	 }
	 public void InsPrimero(int id, int id_concepto, int descuento){
	     Ndescuento nuevo = new Ndescuento(id, id_concepto, descuento,null);
	     nuevo.SetSig(cabecera);
	     cabecera = nuevo;
	     tam++;
	 }
	 public void InsFinal(int id, int id_concepto, int descuento){
		 Ndescuento nuevo = new Ndescuento(id, id_concepto,descuento, null);
	     Ndescuento actual = cabecera;
	     @SuppressWarnings("unused")
		Ndescuento anterior = null;
	     
	     if (cabecera == null){
	         cabecera = nuevo;
	         return;
	     }
	     else{
	         while(actual.GetSig() != null){
	             anterior = actual;
	             actual = actual.GetSig();
	         }
	         actual.SetSig(nuevo);
	     }
	 }
	 public void Mostrar(){
	     Ndescuento aux = cabecera;
	     while(aux != null){
	         System.out.println(aux.GetId()+" "+aux.GetIdConcepto()+" "+aux.GetDescuento());
	         aux = aux.GetSig();
	     }
	 }
	 public boolean Vacia(){
	     if(cabecera == null){
	         return true;
	     }
	     else{
	         return false;
	     }
	 }
	 public void RemoverPrimero(){
	     @SuppressWarnings("unused")
		Ndescuento actual = cabecera;
	     cabecera = cabecera.GetSig();
	     actual = null;
	 }
	 
	 public void RemoverNodo(int id){
		 Ndescuento aux = cabecera;
		 Ndescuento anterior = null;
		 if(!Vacia()){
			 while( aux != null){
				 if (aux.GetIdConcepto() == id){
	                if(anterior == null){
	                   cabecera = cabecera.GetSig();
	                }else {
	                   anterior.SetSig(aux.GetSig());
	                }
	                aux=null;   
	              }else{
	                anterior=aux;
	                aux=aux.GetSig();
	             }
			 }
		 }
	 }
	 
	 public void RemoverDescuentoContratacion(int id){
		 Ndescuento aux = cabecera;
		 Ndescuento anterior = null;
		 if(!Vacia()){
			 while( aux != null){
				 if (aux.GetId() == id){
	                if(anterior == null){
	                   cabecera = cabecera.GetSig();
	                }else {
	                   anterior.SetSig(aux.GetSig());
	                }
	                aux=null;   
	              }else{
	                anterior=aux;
	                aux=aux.GetSig();
	             }
			 }
		 }
	 }
	 
	 public boolean cambios(int id, int descuento){
		boolean cambio = false;
		Ndescuento aux = cabecera;
		if(!Vacia()){
			while(aux != null){
				if(aux.GetIdConcepto() == id){
					if(aux.GetDescuento() != descuento){
						cambio = true;
						break;
					}
				}
				aux = aux.GetSig();
			}
		}
		return cambio; 
	 }
	 
	 public void EditarDescuento(int id, int descuento){
		 Ndescuento aux = cabecera;
		 if(!Vacia()){
			 while( aux != null){
				 if(aux.GetIdConcepto() == id){
					 aux.SetDescuento(descuento);
				 }
				 aux = aux.GetSig();
			 }
		 }
	 }
	 
	 public boolean NodoExistente(int id){
		 Ndescuento aux = cabecera;
		 boolean estado = false;
		 while(aux != null){
			 if(aux.GetIdConcepto() == id){
				 estado = true;
				 break;
			 }
			 aux = aux.GetSig();
		 }
		 return estado;
	 }
	 
	 public int Contar(){
	     Ndescuento aux = cabecera;
	     int cont = 0;
	     while(aux != null){
	         cont ++;
	         aux = aux.GetSig();
	     }
	     return cont;
	 }
	 
	 public void vaciar(){
		 cabecera = null;
	 }
	 
	 public Ndescuento Tope(){
	    return cabecera;
	 }
}
