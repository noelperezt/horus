package utility;




import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.swing.JOptionPane;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;


public class Conexion{
	
	private Connection cnn; 
	private Statement sentencia; 
	private String folderInstall = "";
	private ResultSet rs; 
	private String jSQL = "";
	private String ipServer = "dnaconections.ddns.net";
	//private String ip = "192.168.1.1:5432";
	private String nameDB = "admin_college";
	//private String user = "dna";
	private String userDB = "postgres";
	//private String pass = "postgres";
	private String passDB = "dnapzo15";
	
	
	private static String folderFile = "";
	
	public static String Sede = "";
	public String sede = "";
	public static float iva = (float) 0.12;
	public static int cuotas;
	public static String idUser = "";
	public static String nameUser = "";
	public static String shema = "public";
	public String getSede(){
		return Sede;
	}
	/******************** abrir xml para la configuracion de la conexion a la DB ******************************/
	public Conexion(){
		if(openFileConfigure()){
			configureConnectionVariables();
			setConexion();
		}
	}
	private boolean openFileConfigure(){
		getPathFolderInstallation();
		if(folderInstall.isEmpty()){
			return false;
		}
		return true;
	}
	private void getPathFolderInstallation(){
		File miDir = new File (".");
		try {
			folderInstall = String.valueOf(miDir.getCanonicalPath());
		}
	    catch(Exception e) { }
	}
	private void configureConnectionVariables(){
		File file = new File(folderInstall+"/server.xml");
		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		DocumentBuilder db;
		try {
			db = dbf.newDocumentBuilder();
			Document doc;
			doc = db.parse(file);
			
			doc.getDocumentElement().normalize();
			NodeList nodos = doc.getElementsByTagName("server");
			
			Node fstNode = nodos.item(0);
			Element Elmnt = (Element) fstNode;
			
			ipServer = Elmnt.getElementsByTagName("ipserver").item(0).getTextContent();
			Elmnt.getElementsByTagName("type_server").item(0).getTextContent();
			userDB = Elmnt.getElementsByTagName("user_db").item(0).getTextContent();
			passDB = Elmnt.getElementsByTagName("pass_user").item(0).getTextContent();
			nameDB = Elmnt.getElementsByTagName("name_db").item(0).getTextContent();
			
			
		} catch (Exception  e) {
			e.printStackTrace();
		}
		 
	}
	public static  String getFolderFile(){
		File miDir = new File (".");
		try {
			folderFile  =  String.valueOf(miDir.getCanonicalPath())+"/";
		} catch (IOException e) { e.printStackTrace(); }
		return folderFile;
	}
	/*********************************************************************************/
	public Connection getConnection(){
		return cnn;
	}
	private void setConexion(){ 

		String server="jdbc:postgresql://"+this.ipServer+"/"+this.nameDB; 
		
		try{ 
			Class.forName("org.postgresql.Driver"); 
			cnn = DriverManager.getConnection(server,this.userDB,this.passDB); 
			cnn.setSchema(Conexion.shema);
			
		} catch(Exception e){ 
			//JOptionPane.showMessageDialog(null,"Imposible realizar cnn con la BD"); 
			e.printStackTrace(); 
		}
	} 
	
	
	/** * Ejecuta SQL de INSERT, UPDATE, DELETE. * 
	@param autoComit: si el parametro autoComit es falso activas la transaccion. * 
	@param comit: si el parametro autoComit es falso y el parametro comit es true bajara a disco la sentencia * */ 
	public void setBeginTrans(boolean TrueFalse){ 
		try{ 
			if(TrueFalse){ 
				cnn.setAutoCommit(false); 
			}else 
				cnn.setAutoCommit(true); 
		}catch(Exception e){ 
			System.out.println(e.getMessage()); 
		} 
	} 
	public Object execQuery(String jSQL, String fieldPk) throws SQLException{
		sentencia = cnn.createStatement(); 
		sentencia.executeUpdate(jSQL ,Statement.RETURN_GENERATED_KEYS);
		ResultSet rs = sentencia.getGeneratedKeys();
		rs.next();
		return rs.getLong(fieldPk);
	}
	public ResultSet Consulta(String jSQL)throws SQLException{ 
		sentencia = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
		rs = sentencia.executeQuery(jSQL);
		return rs;
	} 
	
	public boolean ejecutar(String jSQL){
		boolean status = false;
		try {
			sentencia = cnn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
			sentencia.executeUpdate(jSQL);
			status = true;
		} catch (SQLException e) {
			status = false;
			e.printStackTrace();
		}  
		return status;
	}
	
	public Object execQuery(String jSQL)throws SQLException{ 
		/*sentencia = cnn.createStatement(); 
		sentencia.executeUpdate(jSQL); */
		@SuppressWarnings("unused")
		int key = 0;
		Object obj = null;
		
		sentencia = cnn.createStatement(); 
		key = sentencia.executeUpdate(jSQL,Statement.RETURN_GENERATED_KEYS); 
		ResultSet rs = sentencia.getGeneratedKeys();
		if (rs.next()){
		    obj = rs.getObject(1);
		    
		}
		return obj;
	} 
	public void setShema(String nameShema){
		try {
			cnn.setSchema(nameShema);

		} catch (SQLException e) {
			System.out.println(e.getMessage());
		}
	}
	public void setCommitTrans(){ 
		try{ 
			cnn.commit(); 
		}catch(Exception e){ 
			System.out.println(e.getMessage()); 
		} 
	} 
	public void setRollbackTrans(){ 
		try{ 
			cnn.rollback(); 
		}catch(Exception e){ 
			System.out.println(e.getMessage()); 
		} 
	}  /** * * @param SQL * Ejecuta un SQL y retorna true si la consulta retorna datos o false si no se hallaron datos */ 
	public boolean setBuscar_Datos(String SQL) throws SQLException{ 
		try{ 
			sentencia = cnn.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, 0); 
			
			rs = sentencia.executeQuery(SQL);
			rs.first();
			int cnt = rs.getRow();
			if (cnt > 0){
				return true; 
			} else 
				return false; 
		}catch(Exception e){ 
			JOptionPane.showMessageDialog(null, e.getMessage()); 
			return false; 
		} 
	} /** * * @param nameCol * retorna el valor del campo de una consulta previamente realizada */
	public ResultSet getResultSet(){ 
		return rs; 
	}
	public String getValueCol(String nameCol,ResultSet Rs) throws SQLException{ 
		String x = Rs.getString(nameCol); 
		if(x.length()>0) 
			return x; 
		else 
			return ""; 
	} 
	public boolean conextionActive() throws SQLException{
		if (!cnn.isClosed()) { 		
			return false;
		}else{
			return true;
		}
		
	}
	public void desConectar() throws SQLException{ 
		if (!cnn.isClosed()) { 		
			try{
				if(!sentencia.isClosed()){
					sentencia.close();
				}
				cnn.close();
			}catch(NullPointerException e){
				//cnn.close();
			}
			 
		}
	} 
	public int getRowsCount(ResultSet rsTmp) throws SQLException{ 
		int current = rsTmp.getRow(); 
		rsTmp.last(); 
		int count = rsTmp.getRow(); 
		rsTmp.first(); 
		rsTmp.relative(current); 
		return count; 
	} 
	public boolean primerRegistro(String tabla,String campoClave,String varCampoClave){ 
		jSQL = "SELECT * FROM " + tabla + " WHERE " + campoClave + "=(SELECT MIN(" + campoClave + ") FROM " + tabla + ")"; 
		try { 
			if(setBuscar_Datos(jSQL)) 
				return true; 
			else 
				return false; 
		} catch (SQLException e) {	
			System.out.println(e.getErrorCode() + e.getMessage()); 
			return false; 
		} 
	} 
	public boolean anteriorRegistro(String NombreTabla, String CampoClave, String ValorDelCampoClave){ 
		if (ValorDelCampoClave.length()>0){ 
			jSQL = "SELECT * FROM " + NombreTabla + " WHERE " + CampoClave + "=(SELECT MAX(" + CampoClave + ") FROM " + NombreTabla + " WHERE RTRIM(" + CampoClave + ")<'" + ValorDelCampoClave.trim() + "')";	
			try { 
				if(setBuscar_Datos(jSQL)){ 
					return true; }else{ 
						if(ultimoRegistro(NombreTabla, CampoClave, ValorDelCampoClave)) 
							return true;
						else 
							return false;	
						} 
			} catch (SQLException e) { 
				System.out.println(e.getErrorCode() + e.getMessage());
				return false; 
			} 
		}else{ 
			if(primerRegistro(NombreTabla, CampoClave, ValorDelCampoClave)) 
				return true; 
			else 
				return false;
		} 
	} 
	public boolean ultimoRegistro(String NombreTabla, String CampoClave, String ValorDelCampoClave){ 
		jSQL = "SELECT * FROM " + NombreTabla + " WHERE " + CampoClave + "=(SELECT MAX(" + CampoClave + ") FROM " + NombreTabla + ")"; 
		try { 
			if (setBuscar_Datos(jSQL)) 
				return true;
			else 
				return false; 
			} catch (SQLException e) { 
				System.out.println(e.getErrorCode() + e.getMessage()); 
				return false; 
			} 
	} 
	public boolean proximoRegistro(String NombreTabla, String CampoClave , String ValorDelCampoClave){ 
		if(ValorDelCampoClave.length()>0){ 
			jSQL = "SELECT * FROM " + NombreTabla + " WHERE " + CampoClave + "=(SELECT MIN(" + CampoClave + ") FROM " + NombreTabla + " WHERE RTRIM(" + CampoClave + ")>'" + ValorDelCampoClave.trim() + "')";	
			try { 
				if(setBuscar_Datos(jSQL)) return true; else{ if(primerRegistro(NombreTabla, CampoClave, ValorDelCampoClave)) 
					return true; 
				else 
					return false; 
				} 
			} catch (SQLException e) { 
				System.out.println(e.getErrorCode() + e.getMessage()); 
				return false; 
			} 
		}else{	
			if(primerRegistro(NombreTabla, CampoClave, ValorDelCampoClave)) 
				return true; 
			else 
				return false;	
		} 
	} 
   
	
}