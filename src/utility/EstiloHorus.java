package utility;
import java.awt.Color;

import javax.swing.plaf.*;
import javax.swing.plaf.metal.*;

public class EstiloHorus  extends DefaultMetalTheme {
   public String getName() { return "Horus"; }
   
   private final ColorUIResource primary1 = new ColorUIResource(76, 76, 76); //Color de Borde de linea de Menus y de la fuente de carga
   private final ColorUIResource primary2 = new ColorUIResource(201, 201, 201); //Color del fondo de los item del menu
   private final ColorUIResource primary3 = new ColorUIResource(133, 133, 133); //Color elemento seleccionado de la tabla

   private final ColorUIResource secondary1 = new ColorUIResource( 76,  76,  76); //Color de borde de elementos
   private final ColorUIResource secondary2 = new ColorUIResource(76, 76, 76); //Color de borde de las pantallas y boton presionado
   private final ColorUIResource secondary3 = new ColorUIResource(248, 248, 248); //Color de fondo del sistema

   private final ColorUIResource black = new ColorUIResource(0, 0, 0);
   private final ColorUIResource white = new ColorUIResource(255, 255, 255);
   
   private Color azul = new Color(13,86,180); //Color comun de los botones
   private Color gris = new Color(76,76,76); //Color del hover de los botones
   
   //No invertir los colores blanco y negro ya que se modificaran los colores de las cajas de texto
   
   protected ColorUIResource getPrimary1() { return primary1; }
   protected ColorUIResource getPrimary2() { return primary2; }
   protected ColorUIResource getPrimary3() { return primary3; }

   protected ColorUIResource getSecondary1() { return secondary1; }
   protected ColorUIResource getSecondary2() { return secondary2; }
   protected ColorUIResource getSecondary3() { return secondary3; }
   
   protected ColorUIResource getBlack() { return black; }
   protected ColorUIResource getWhite() { return white; }
   
   public Color getAzul(){ //Retorna el color para los botones
	   return azul;
   }
   
   public Color getGris(){//Retorna le color para el hover de los botones
	   return gris;
   }
}


	

