package utility;

public class Lpermiso {
	 private Npermiso cabecera;
	 @SuppressWarnings("unused")
	private int tam;
	 
	 public Lpermiso(){
	     cabecera = null;
	     tam = 0;
	 }
	 public void InsPrimero(int id, String estado, String descripcion){
	     Npermiso nuevo = new Npermiso(id, estado, descripcion,null);
	     nuevo.SetSig(cabecera);
	     cabecera = nuevo;
	     tam++;
	 }
	 public void InsFinal(int id, String estado, String descripcion){
		 Npermiso nuevo = new Npermiso(id, estado,descripcion, null);
	     Npermiso actual = cabecera;
	     @SuppressWarnings("unused")
		Npermiso anterior = null;
	     
	     if (cabecera == null){
	         cabecera = nuevo;
	         return;
	     }
	     else{
	         while(actual.GetSig() != null){
	             anterior = actual;
	             actual = actual.GetSig();
	         }
	         actual.SetSig(nuevo);
	     }
	 }
	 public void Mostrar(){
	     Npermiso aux = cabecera;
	     while(aux != null){
	         System.out.println(aux.GetId()+" "+aux.GetEstado());
	         aux = aux.GetSig();
	     }
	 }
	 public boolean Vacia(){
	     if(cabecera == null){
	         return true;
	     }
	     else{
	         return false;
	     }
	 }
	 public void RemoverPrimero(){
	     @SuppressWarnings("unused")
		Npermiso actual = cabecera;
	     cabecera = cabecera.GetSig();
	     actual = null;
	 }
	 
	 public int Contar(){
	     Npermiso aux = cabecera;
	     int cont = 0;
	     while(aux != null){
	         cont ++;
	         aux = aux.GetSig();
	     }
	     return cont;
	 }
	 
	 public Npermiso Tope(){
	        return cabecera;
	    }
	 
}
