 import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import javax.swing.UIManager; 
import javax.swing.plaf.metal.MetalLookAndFeel;
import javazoom.jl.decoder.JavaLayerException;
import javazoom.jl.player.Player;
import utility.EstiloHorus;
import views.Carga;

public class Main {

	public static void main(String[] args) throws InterruptedException {
		String ruta = new File ("").getAbsolutePath ();
		MetalLookAndFeel.setCurrentTheme(new EstiloHorus());
	      try{
	        UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
	      } catch (Exception ex) {
	         System.out.println("Fall� la carga del tema");
	         System.out.println(ex);
	      }
		Carga iniciar = new Carga();
		iniciar.frame.setVisible(true);
		try {
			FileInputStream fis;
	        Player player;
	        fis = new FileInputStream(ruta+"/sounds/ini.mp3");
	        BufferedInputStream bis = new BufferedInputStream(fis);
	        player = new Player(bis);
	        player.play();           
	    }
	    catch (JavaLayerException e){
	            e.printStackTrace();
	    }
	    catch (FileNotFoundException e){
	            e.printStackTrace();
	   }
	}
}
